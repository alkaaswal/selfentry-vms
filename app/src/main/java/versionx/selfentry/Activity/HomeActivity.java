package versionx.selfentry.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.PopupMenu;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.wrapp.floatlabelededittext.FloatLabeledEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import versionx.selfentry.CustomControls.AutoCompleteText;
import versionx.selfentry.CustomControls.AutoCompleteTextViewClickListener;
import versionx.selfentry.CustomControls.CheckBox;
import versionx.selfentry.DataBase.CustomFields.AsyncTaskListner;
import versionx.selfentry.DataBase.CustomFields.CustomFieldAsync;
import versionx.selfentry.DataBase.CustomFields.FieldMappingAsync;
import versionx.selfentry.DataBase.CustomFields.FieldOptionsAsync;
import versionx.selfentry.DataBase.ImagesDataSource;
import versionx.selfentry.MyApplication;
import versionx.selfentry.Networking.NetworkingApiConnect;
//import versionx.selfentry.Printing.Global;
import versionx.selfentry.Util.DeleteOldDataFromDbAsync;
import versionx.selfentry.Util.Global;
import versionx.selfentry.R;
import versionx.selfentry.Util.CaseInsensitiveMap;
import versionx.selfentry.Camera.CameraActivity;
import versionx.selfentry.CustomControls.TextViewOpenSans;
import versionx.selfentry.Util.FCMTokens;
import versionx.selfentry.Util.FirebaseDatabaseSettings;
import versionx.selfentry.Util.PersistentDatabase;
import versionx.selfentry.Printing.DrawerService;

import versionx.selfentry.Util.CommonMethods;

import versionx.selfentry.Util.TokenUpdate;
import versionx.selfentry.Util.TransactionData;
import versionx.selfentry.adapter.AutoCompeleteArrayAdapter;
import versionx.selfentry.adapter.FieldsAutoCompleteAdapter;
import versionx.selfentry.gettersetter.CustomFields;
import versionx.selfentry.gettersetter.FieldInfo;
import versionx.selfentry.gettersetter.FieldMapping;
import versionx.selfentry.gettersetter.FieldsOptions;
import versionx.selfentry.gettersetter.Images;
import versionx.selfentry.gettersetter.RestrictFields;
import versionx.selfentry.gettersetter.ToMeet;
import versionx.selfentry.gettersetter.Visitor;

import static java.security.AccessController.getContext;

public class HomeActivity extends AppCompatActivity
        implements PopupMenu.OnMenuItemClickListener, View.OnClickListener, AsyncTaskListner {


    Bitmap bitmap = null;
    LinearLayout VisitorLayoutPage;
    TextView btn_update;
    ImageView iv_barcode;
    Button btn_save, btn_nxt;
    // Bitmap vizImgBit, imgBit;
    LinearLayout ll_for_ver_exp;
    EditText ed_visitorName, ed_visitorMobile;
    //List<AutoCompleteTextView> allAutoCompleteText;
    AutoCompleteTextView ed_purpose, ed_toMeet;
    private Bitmap bit;
    private ImageView iv_visitorpic, iv_ed_toMeet_clear, iv_ed_mob_clear, iv_ed_nm_clear, iv_ed_purpose_clear;

    private TelephonyManager telManager;
    String selectedPurpose, visitorPhotoUrl, prevVisitorPhotoFilePath;
    Context context;
    TextView iv_clear_data, iv_clear_meet_data;
    Dialog dialog;
    Bitmap vizImgBit;

    //ImageLoader orgimageLoader;
    public static byte[] buf;
    LinearLayout llForCustomFields_ToMeet, llForCustomFields_Visitor, ll_for_whocame,
            ll_for_toMeet;
    ArrayList<EditText> allEds;
    ArrayList<AutoCompleteTextView> allAutoCompleteTextView;


    //  LinearLayout ll_for_subscription_expired;
    //  String visitorType;
    HashMap<String, String> purposeArray;
    ArrayList<CustomFields> customFieldsMeetList;
    ArrayList<ImageView> allImageview;
    boolean isMobileEntered = false, isAppointmnetChecked = false, isRadioAutofilled = false;
    int calling = 0;
    static boolean isOutsidefragment = false;
    String purpose_selected;
    //  SharedPreferences imgPathSharedPref;
    String hiskey;
    ValueEventListener purposeListner, fieldmeetListner, fieldWhoCameRefListner;
    ValueEventListener toMeetListner;
    DatabaseReference purposeRef, fieldmeetRef, toMeetRef, fieldWhoCameRef, deviceSettingsRef;
    AutoCompeleteArrayAdapter toMeetadapter;
    ArrayList<ToMeet> toMeetList, tempList;
    ToMeet selectedToMeet;
    ToMeet to_meet;
    Visitor visitor;
    ArrayList<TextView> imgText;
    int count = 0;
    int menuItem;
    String tempFile;
    String type = "visitor", accessKey = null, scannedType, vehicle_no;

    ArrayList<String> vehNoList;
    ChildEventListener toMeetchildEventListener;
    DatabaseReference visitorRef, vehRef;
    DatabaseReference toMeetaccessRef;
    DatabaseReference HisRef;
    long callTime = 0;
    DatabaseReference adminDb;
    ValueEventListener adminListener;
    SharedPreferences bluetoothSp;
    private String toMeetTyp = "access";
    private BroadcastReceiver broadcastReceiver = null;
    private IntentFilter intentFilter = null;
    private static Handler mHandler = null;

    SharedPreferences loginsp;


    DatabaseReference alarmRef = null, connectedRef = null;
    DatabaseReference waitRef = null, deviceDeregister = null, masterPrintRef = null;
    ValueEventListener deviceVerRefListner = null,
            deviceSettingsRefListner = null,
            deviceNameListner = null, bizImageListner = null;
    ValueEventListener deviceDeregisterListner, connectedRefListner, masterPrintRefListner;


    private int alarmCount = 0;
    File visitorImageFile;
    Bitmap imageBitmap;
    boolean isPicSelected = false;
    String visitorPhotoFilePath;

    String vizFieldKey, vizGroup, accessType, imgNm, vehNo;
    int PLAY_SERVICES_RESOLUTION_REQUEST = 111;
    ImagesDataSource imagesDataSource;
    ArrayList<String> imgPath;
    private boolean onlyImgRequired;
    // public static boolean homeActivityVisible = true;
    ImageView iv_call_logo;
    LinearLayout ll_call;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 0;
    private GestureDetector gestureDetector;
    private String lastPhoneNo, lastHistoryKey;
    int lastCalling;
    ArrayList<String> lastArrayList;
    DatabaseReference nouploadRef, deviceRef;
    Menu menu;
    private int prevCalling;
    String prevTomeet;
    private ValueEventListener userListner;
    private ValueEventListener visitorMobListner;
    DatabaseReference userMobileGroupRef;
    private ValueEventListener masterRefListner;
    private DatabaseReference masterRef;
    private ValueEventListener deviceNmListner;
    private DatabaseReference deviceNmRef;
    private FirebaseDatabaseSettings firebaseDatabaseSettings;
    private AlertDialog.Builder pinAlertDialog;
    private AlertDialog pinDialog;
    View tomeet_view;
    private DatabaseReference deviceDialRef;
    private Dialog startdialog;
    private TextView startTv;
    private LinearLayout ll_start;
    private Button btn_epass;
    private DatabaseReference modeRef;
    private ValueEventListener deviceDialRefListner;
    private ValueEventListener modeRefListner;
    private Handler waitHandler;
    private Runnable waitrunnable;
    private Dialog waitprogressdialog;
    private ValueEventListener waitRefListner;
    private TextView wait_tv;
    ArrayList<HashMap> toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap;
    private DatabaseReference tokenDbRef;
    private ValueEventListener tokenDbRefListner;
    private ArrayList<String> urlList;
    private String lastToken;
    private boolean isPass = false;
    HashMap<String, String> selectedOptions = new HashMap<>();
    List<RadioGroup> allRadioGroup;
    List<RadioButton> allradiobutton;
    ArrayList<TextViewOpenSans> radioButtonData;


    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        initGUI(); // initialize UI elements
        isPermissionGranted();


        setPrinterservice();
        removeAllTheListners();
        setListners();
        checkVersion(HomeActivity.this);// check app version
     /*   getPurpose(HomeActivity.this);
       getMobLength();

    //    getCustomFields(HomeActivity.this);
        getToMeetAccess(HomeActivity.this);*/

        getDbCustomFields(this);


        if (getIntent() != null && getIntent().hasExtra("phoneNo") && getIntent().getStringExtra("phoneNo") != null) {
            updateCallNumber(getIntent().getStringExtra("phoneNo"));
        }

        getDatafromFirebase(HomeActivity.this);

        if (loginsp.getBoolean(Global.TOKEN, true) && loginsp.getBoolean(Global.HARDWARE, false))
            deleteDbData();

    }

    void deleteDbData() {
        DeleteOldDataFromDbAsync deleteOldDataFromDbAsync = new DeleteOldDataFromDbAsync(loginsp, context);
        deleteOldDataFromDbAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && checkCallingOrSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.BLUETOOTH)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED
                    ) {

                return true;
            } else {


                ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE",
                        "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CALL_PHONE"
                        , "android.permission.CAMERA", "android.permission.BLUETOOTH",
                        "android.permission.BLUETOOTH_ADMIN", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"}, 1);
                return false;
            }
        } else {
           /* if (loginsp.getBoolean(Global.USB_PRINT, false))
                setUSBPrinter();
            if (loginsp.getBoolean(Global.IP_PRINT, false) && DrawerService.workThread != null)
                DrawerService.workThread.connectNet(loginsp.getString(Global.IP_PRINT_ADDRESS, ""), 9100);
*/

            return true;
        }

    }


    private void getMobLength() {

        DatabaseReference masteVizrRef = PersistentDatabase.getDatabase().
                getReference("masterData/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")).child("visitor")
                .child("mobLen");
        masteVizrRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(dataSnapshot.getValue(Integer.class));
                    ed_visitorMobile.setFilters(filterArray);
                    loginsp.edit().putInt(versionx.selfentry.Util.Global.MOB_LENGTH, dataSnapshot.getValue(Integer.class)).apply();
                } else {
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(10);
                    ed_visitorMobile.setFilters(filterArray);
                    loginsp.edit().putInt(versionx.selfentry.Util.Global.MOB_LENGTH, 10).apply();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void getDevicePhoneNumber() {
        DatabaseReference deviceImgRef = PersistentDatabase.getDatabase().getReference("device")
                .child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                .child(versionx.selfentry.Util.Global.APP_NAME).child(loginsp.getString(versionx.selfentry.Util.Global.UUID, "")).child("callImg");

        deviceImgRef.keepSynced(true);

        deviceImgRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.exists()) {
                    // tv_dial_no.setText(dataSnapshot.getValue().toString());


                    Glide.with(getApplicationContext()).load(dataSnapshot.getValue(String.class)).diskCacheStrategy(DiskCacheStrategy.RESULT).centerCrop().into(iv_call_logo);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void getDatafromFirebase(final Context context) {


        modeRef = PersistentDatabase.getDatabase().getReference("device").child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                .child(versionx.selfentry.Util.Global.APP_NAME).child(loginsp.getString(versionx.selfentry.Util.Global.UUID, "")).child("mode");
        modeRefListner = modeRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                {
                    if (dataSnapshot.exists()) {

                        loginsp.edit().putInt(versionx.selfentry.Util.Global.BTN_REQUIRE, dataSnapshot.getValue(Integer.class)).apply();

                        invalidateOptionsMenu();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        deviceDialRef = PersistentDatabase.getDatabase().getReference("device").child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                .child(versionx.selfentry.Util.Global.APP_NAME).child(loginsp.getString(versionx.selfentry.Util.Global.UUID, "")).child("dial");


        deviceDialRefListner = deviceDialRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    loginsp.edit().putBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, dataSnapshot.getValue(boolean.class)).apply();

                } else {
                    loginsp.edit().putBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, false).apply();
                }
                invalidateOptionsMenu();

                showDialingScreen(false);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        getDevicePhoneNumber();

        firebaseDatabaseSettings = new FirebaseDatabaseSettings(context, btn_epass);
        firebaseDatabaseSettings.getAllSettings();

        deviceNmRef = deviceRef.child("nm");

        deviceNmListner = deviceNmRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    loginsp.edit().putString(versionx.selfentry.Util.Global.DEVICE_NAME, dataSnapshot.getValue(String.class)).apply();
                    if (!loginsp.getString(versionx.selfentry.Util.Global.LOGGNED_IN_USER_NM, "").isEmpty())
                        getSupportActionBar().setTitle("Self Entry - " + loginsp.getString(versionx.selfentry.Util.Global.DEVICE_NAME, "")
                                + "(" + loginsp.getString(versionx.selfentry.Util.Global.LOGGNED_IN_USER_NM, "") + ")");
                    else
                        getSupportActionBar().setTitle("Self Entry - " + loginsp.getString(versionx.selfentry.Util.Global.DEVICE_NAME, ""));
                } else {
                    String dtTime = CommonMethods.getCurrentTime("hh:mm a");
                    deviceRef.child("nm").setValue(dtTime);
                    loginsp.edit().putString(versionx.selfentry.Util.Global.DEVICE_NAME, dtTime).apply();
                    getSupportActionBar().setTitle("Self Entry - " + loginsp.getString(versionx.selfentry.Util.Global.DEVICE_NAME, ""));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        //////

        deviceRef.child("code").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    loginsp.edit().putString(Global.DEVICE_CODE, dataSnapshot.getValue().toString()).apply();
                } else {
                    loginsp.edit().putString(Global.DEVICE_CODE, "").apply();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        //////////////////////////////

        deviceRef.child("tkn")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.exists() && dataSnapshot.hasChild(CommonMethods.getTodaysDate().getTimeInMillis() + "")) {

                            loginsp.edit().putInt(Global.TOKEN_NO, dataSnapshot.child(CommonMethods.getTodaysDate().getTimeInMillis() + "").child("no").getValue(Integer.class)).apply();


                        } else {

                            loginsp.edit().putInt(Global.TOKEN_NO, 0).apply();

                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


        //////////////////////////////////////////
        uploadAllImages(HomeActivity.this);


        /////////////////////////// upload all images ///////////////////////


        final DatabaseReference deviceImgRef = PersistentDatabase.getDatabase().
                getReference("device/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/"
                        + versionx.selfentry.Util.Global.APP_NAME
                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.UUID, "") + "/syncImg");

        deviceImgRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.exists() && dataSnapshot.getValue(Boolean.class)) {
                    if (loginsp.getBoolean(versionx.selfentry.Util.Global.IS_LOGGED_IN, false)) {
                        uploadAllImages(HomeActivity.this);
                    }
                    deviceImgRef.setValue(false);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        ////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////// keep visitor profile node synced ////////////////////////////////////

       /* DatabaseReference visitorProfileSynced = PersistentDatabase.getDatabase().
                getReference("visitor/" + loginSp.getString(Global.BIZ_ID , ""));

        visitorProfileSynced.keepSynced(true);*/


        /////////History Syncing

     /*   DatabaseReference visitorProfileSynced = PersistentDatabase.getDatabase().
                getReference(CommonMethods.getAccessHistoryNode(Global.VISITOR)
                        + "/" + loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, ""));

        visitorProfileSynced.keepSynced(true);*/


        DatabaseReference visitorProfileSynced = PersistentDatabase.getDatabase().
                getReference(CommonMethods.getAccessNode(versionx.selfentry.Util.Global.ALLOWED_VISITOR)
                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")).child(loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

        visitorProfileSynced.keepSynced(true);


        //////////////////// getting User login

        masterRef = PersistentDatabase.getDatabase().
                getReference("masterData/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

        masterRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    loginsp.edit().putBoolean(versionx.selfentry.Util.Global.DEV_USER_LOGIN,
                            dataSnapshot.child("visitor").child("uLog").getValue(Boolean.class)).apply();

                    // Log.e("u log", loginSp.getBoolean(Global.DEV_USER_LOGIN, false) + "");

                    if (loginsp.getBoolean(versionx.selfentry.Util.Global.DEV_USER_LOGIN, false) &&
                            !loginsp.getBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, false)
                            && !loginsp.getBoolean(versionx.selfentry.Util.Global.IS_DEV_USER_LOGGEDIN, false)
                            && ed_visitorMobile.getText().toString().trim().isEmpty()) {
                        Intent in = new Intent(getApplicationContext(), UserLogin.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        overridePendingTransition(R.anim.left_to_right,
                                R.anim.left_to_right);
                        startActivity(in);


                    }
                    if (dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).hasChild("startMsg")) {
                        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.START_SCREEN, true).apply();
                        loginsp.edit().putString(versionx.selfentry.Util.Global.START_SCREEN_MSG, dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).child("startMsg").getValue(String.class)).apply();
                        //      loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, false).apply();
                        //     clearData(false);
                    } else {
                        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.START_SCREEN, false).apply();
                        loginsp.edit().putString(versionx.selfentry.Util.Global.START_SCREEN_MSG, "").apply();
                        //       loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, true).apply();
                        //    clearData(false);

                    }

                    if (dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).hasChild("waitMsg")) {
                        loginsp.edit().putString(versionx.selfentry.Util.Global.WAIT_MSG, dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).child("waitMsg").getValue(String.class)).apply();
                    }
                    if (dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).hasChild("noRespMsg")) {
                        loginsp.edit().putString(versionx.selfentry.Util.Global.NO_RESPONSE_MSG, dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).child("noRespMsg").getValue(String.class)).apply();
                    }

                    if (dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).hasChild("rejectMsg")) {
                        loginsp.edit().putString(versionx.selfentry.Util.Global.REJECT_MSG, dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).child("rejectMsg").getValue(String.class)).apply();
                    }


                    if (dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).hasChild("waitTm")) {
                        loginsp.edit().putInt(versionx.selfentry.Util.Global.WAITING_TM, dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).child("waitTm").getValue(Integer.class)).apply();
                    }

                    if (dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).hasChild("blockMsg")) {
                        loginsp.edit().putString(versionx.selfentry.Util.Global.BLOCK_MSG, dataSnapshot.child(versionx.selfentry.Util.Global.APP_NAME).child("blockMsg").getValue(String.class)).apply();
                    }

                    if (dataSnapshot.child("visitor").hasChild("reMeet")) {
                        loginsp.edit().putBoolean(Global.RE_ENTER_TO_MEET, dataSnapshot.child("visitor").child("reMeet").getValue(Boolean.class)).apply();
                    }


                } catch (Exception e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        masterRef.keepSynced(true);
        masterRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("visitor").hasChild("tkn") &&
                        dataSnapshot.child("visitor").child("tkn").getValue(Boolean.class)) {
                    loginsp.edit().putBoolean(Global.TOKEN, true).apply();
                } else {
                    loginsp.edit().putBoolean(Global.TOKEN, false).apply();
                }
                if (dataSnapshot.child(Global.APP_NAME).hasChild("hw") &&
                        dataSnapshot.child(Global.APP_NAME).child("hw").child("st").getValue(Boolean.class)
                        && dataSnapshot.child(Global.APP_NAME).child("hw").hasChild("ip")) {
                    loginsp.edit().putBoolean(Global.HARDWARE, dataSnapshot.child(Global.APP_NAME).child("hw").child("st").getValue(Boolean.class)).apply();
                    loginsp.edit().putString(Global.HARDWARE_IP, dataSnapshot.child(Global.APP_NAME).child("hw").child("ip").getValue(String.class)).apply();
                    if (dataSnapshot.child(Global.APP_NAME).child("hw").hasChild("u"))
                        loginsp.edit().putString(Global.HARDWARE_DB_USER, dataSnapshot.child(Global.APP_NAME).child("hw").child("u").getValue(String.class)).apply();
                    if (dataSnapshot.child(Global.APP_NAME).child("hw").hasChild("p"))
                        loginsp.edit().putString(Global.HARDWARE_DB_PASSWORD, dataSnapshot.child(Global.APP_NAME).child("hw").child("p").getValue(String.class)).apply();
                    if (dataSnapshot.child(Global.APP_NAME).child("hw").hasChild("dbNm"))
                        loginsp.edit().putString(Global.HARDWARED_DB, dataSnapshot.child(Global.APP_NAME).child("hw").child("dbNm").getValue(String.class)).apply();


                } else {
                    loginsp.edit().putBoolean(Global.HARDWARE, false).apply();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // printer
     /*   masterPrintRef = PersistentDatabase.getDatabase().
                getReference("masterData/" + loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, ""))
                .child(Global.APP_NAME).child("printer");

        masterPrintRefListner = masterPrintRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    loginSp.edit().putBoolean(Global.AUTO_PRINT, dataSnapshot.child("autoPrint").getValue(Boolean.class)).apply();
                    loginSp.edit().putBoolean(Global.USB_PRINT, dataSnapshot.child("usb").getValue(Boolean.class)).apply();
                    loginSp.edit().putBoolean(Global.IP_PRINT, dataSnapshot.child("ip").getValue(Boolean.class)).apply();
                    if (DrawerService.workThread != null) {
                        DrawerService.workThread.disconnectNet();
                        DrawerService.workThread.disconnectUsb();
                        buf = null;
                    }

                    if (dataSnapshot.hasChild("addr"))
                        loginSp.edit().putString(Global.IP_PRINT_ADDRESS, dataSnapshot.child("addr").getValue(String.class)).apply();

                 *//*   if (loginsp.getBoolean(Global.USB_PRINT, false))
                        setUSBPrinter();*//*

                    if (loginsp.getBoolean(Global.IP_PRINT, false) && DrawerService.workThread != null)
                        DrawerService.workThread.connectNet(loginsp.getString(Global.IP_PRINT_ADDRESS, ""), 9100);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
*/

    }


    private void checkVersion(final Context context) {
        adminDb = PersistentDatabase.getDatabase().getReference("admin/app/" + versionx.selfentry.Util.Global.APP_NAME);
        adminDb.keepSynced(true);
        adminListener = adminDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot child) {
                try {
                    PackageInfo pInfo = null;
                    try {
                        pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        // e.printStackTrace();
                    }
                    String version = pInfo.versionName;

                    loginsp.edit().putString(versionx.selfentry.Util.Global.VERSION_NUMBER, version).apply();


                    /*if (!child.child("ver").getValue().toString().equalsIgnoreCase(version))

                    {
                        ll_for_ver_exp.setVisibility(View.VISIBLE);

                        if (System.currentTimeMillis() >= child.child("dt").getValue(Long.class)) {
                            btn_save.setVisibility(View.GONE);
                            loginsp.edit().putBoolean(Global.VERSION_EXPIRED, true).apply();

                        } else {

                            //     btn_save.setVisibility(View.VISIBLE);
                            loginsp.edit().putBoolean(Global.VERSION_EXPIRED, false).apply();
                        }
                    } else {
                        ll_for_ver_exp.setVisibility(View.GONE);
                        //  btn_save.setVisibility(View.VISIBLE);
                        loginsp.edit().putBoolean(Global.VERSION_EXPIRED, false).apply();
                    }*/
                } catch (Exception e) {
                    Crashlytics.logException(e);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void isConfirmationRequired(Context context) {
        deviceSettingsRef = PersistentDatabase.getDatabase().getReference("deviceSetting/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/confirm/state");


        deviceSettingsRefListner = deviceSettingsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {

                    if (dataSnapshot != null && dataSnapshot.getValue() != null && Boolean.parseBoolean(dataSnapshot.getValue().toString())) {

                        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.SETTINGS_CONFIRM_REQUIRED,
                                Boolean.parseBoolean(dataSnapshot.getValue().toString())).apply();
                    }


                } catch (Exception e) {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


  /*  // for ferching custom fields data
    public void getCustomFields(final Context context) {
        loginsp = context.getSharedPreferences(versionx.selfentry.Util.Global.LOGIN_SP, Context.MODE_PRIVATE);

        fieldmeetRef = PersistentDatabase.getDatabase()
                .getReference("field/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

        // Query query = fieldmeetRef.orderByPriority();


        fieldmeetListner = fieldmeetRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                imgText.clear();
                allImageview.clear();
                allEds.clear();
                allAutoCompleteTextView.clear();
                llForCustomFields_ToMeet.removeAllViews();
                llForCustomFields_Visitor.removeAllViews();
                customFieldsMeetList.clear();

                try {
                    ArrayList<FieldInfo> fieldInfoList = new ArrayList<FieldInfo>();
                    fieldInfoList.clear();

                    for (DataSnapshot groupSnapShot : dataSnapshot.getChildren()) {

                        for (DataSnapshot childs : groupSnapShot.getChildren()) {

                           *//* if (!groupSnapShot.getKey().equals("print"))

                            {*//*

                            if (groupSnapShot.getKey().equals("meet") || groupSnapShot.getKey().equals("whoCame")) {
                                FieldInfo fieldInfo = new FieldInfo();

                                if (groupSnapShot.getKey().equals("meet")) {
                                    fieldInfo.setFieldGroupNm("meet");
                                }
                                if (groupSnapShot.getKey().equals("whoCame")) {
                                    fieldInfo.setFieldGroupNm("whoCame");
                                }
                                fieldInfo.setFieldNm(childs.child("nm").getValue().toString());
                                fieldInfo.setFieldTyp(childs.child("typ").getValue().toString());
                                fieldInfo.setPrintable(childs.child("print").getValue(boolean.class));
                                // fieldInfo.setSize(childs.child("size").getValue().toString());


                                fieldInfo.setFieldMandatory(Boolean.parseBoolean(childs.child("req").getValue().toString()));
                                fieldInfo.setFieldKey(childs.getKey());
                                if (childs.hasChild("hide")) {
                                    fieldInfo.setFieldHide(Boolean.parseBoolean(childs.child("hide").getValue().toString()));
                                    if (childs.child("hide").getValue(Boolean.class)) {
                                        fieldInfo.setFieldMandatory(false);
                                    }
                                } else {
                                    fieldInfo.setFieldHide(false);
                                }

                                if (childs.hasChild("re")) {
                                    fieldInfo.setReEnter(childs.child("re").getValue(Boolean.class));
                                } else {
                                    fieldInfo.setReEnter(false);
                                }


                                if (childs.hasChild("val"))
                                    fieldInfo.setFieldValue(childs.child("val").getValue().toString());
                                else
                                    fieldInfo.setFieldValue("");


                                if ((childs.child("typ").getValue().toString().equalsIgnoreCase(versionx.selfentry.Util.Global.DROPDOWN)
                                        || childs.child("typ").getValue().toString().equalsIgnoreCase(versionx.selfentry.Util.Global.SEARCHABLE))
                                        && !childs.child("typ").getValue().toString().equalsIgnoreCase("num")) {
                                    CaseInsensitiveMap optionList = new CaseInsensitiveMap();
                                    for (DataSnapshot list : childs.child("opt").getChildren()) {


                                        optionList.put(list.getValue(String.class), list.getKey());

                                    }

                                    fieldInfo.setFieldOptions(optionList);
                                }

                                if (childs.hasChild("dopt")) {

                                    fieldInfo.setDepOptions((HashMap<String, Object>) childs.child("dopt").getValue());
                                }
                                if (childs.hasChild("dep")) {
                                    fieldInfo.setDepKey(childs.child("dep").getValue(String.class));
                                }


                                customFieldsMeetList.add(fieldInfo);
                                //}
                            }

                        }
                    }


                    setCustomFieldsList(customFieldsMeetList);

                } catch (Exception e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
// set the fetched customfields in UI

    private void setCustomFieldsList(final ArrayList<FieldInfo> fieldsMeetList) {
        try {
            if (llForCustomFields_ToMeet != null) {
                llForCustomFields_ToMeet.removeAllViews();
            }
            if (llForCustomFields_Visitor != null)
                llForCustomFields_Visitor.removeAllViews();

            for (int i = 0; i < fieldsMeetList.size(); i++) {

                final FieldInfo fieldInfo = fieldsMeetList.get(i);
                if (fieldInfo.getFieldTyp().equalsIgnoreCase(versionx.selfentry.Util.Global.TEXTBOX)
                        || fieldInfo.getFieldTyp().equalsIgnoreCase("num")
                        || fieldInfo.getFieldTyp().equals(versionx.selfentry.Util.Global.TEXTAREA)) {

                    View root = LayoutInflater.from(context).inflate(R.layout.float_label_edit_text, null, false);
                    TextInputLayout textInputLayout = (TextInputLayout) root.findViewById(R.id.float_label);
                    final ImageView imageView = (ImageView) root.findViewById(R.id.iv_clear_ed);
                    View view_float_label = (View) root.findViewById(R.id.float_ed_view);
                    *//*   textInputLayout.setLayoutParams(layoutParams);*//*
                    textInputLayout.setHint(fieldInfo.getFieldNm());
                    EditText editText = (EditText) root.findViewById(R.id.editText);
                    //  editText.setLayoutParams(layoutParams);
                    editText.setTag(fieldInfo);
                    editText.setText(fieldInfo.getFieldValue());

                    //Rajesh...if custom field key is vehicle...we're adding the captital letters input filter
                    if (fieldInfo.getFieldKey().equalsIgnoreCase(versionx.selfentry.Util.Global.VEHICLE)) {
                        editText.setFilters(new InputFilter[]{CommonMethods.getCapsTextNumFilter()});
                    }

                    if (fieldInfo.getFieldTyp().equalsIgnoreCase("num"))
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    if (fieldInfo.getFieldTyp().equalsIgnoreCase(versionx.selfentry.Util.Global.TEXTBOX)) {
                        editText.setSingleLine(true);
                        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    } else if (fieldInfo.getFieldTyp().equals(versionx.selfentry.Util.Global.TEXTAREA)) {
                        editText.setSingleLine(false);
                        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        editText.setGravity(Gravity.TOP | Gravity.LEFT);
                        editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                        editText.setLines(3);
                        editText.setMaxLines(5);
                    }

                    allEds.add(editText);
                    imageView.setTag(allEds.size() - 1);


                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            allEds.get((Integer) view.getTag()).setText("");
                            ed_toMeet.setText("");
                        }
                    });

                    if (!fieldInfo.getFieldHide()) {
                        if (fieldInfo.getFieldGroupNm().equals("meet"))
                            llForCustomFields_ToMeet.addView(root);
                        else
                            llForCustomFields_Visitor.addView(root);
                    }


                    if (fieldInfo.getFieldHide()) {
                        textInputLayout.setVisibility(View.GONE);
                        editText.setVisibility(View.GONE);
                        view_float_label.setVisibility(View.GONE);

                    }
                }


                if (fieldInfo.getFieldTyp().equalsIgnoreCase(versionx.selfentry.Util.Global.DROPDOWN)
                        || fieldInfo.getFieldTyp().equalsIgnoreCase(versionx.selfentry.Util.Global.SEARCHABLE)) {

                    final View root = LayoutInflater.from(context).inflate(R.layout.float_label_autocompletetext, null, false);
                    TextInputLayout textInputLayout = (TextInputLayout) root.findViewById(R.id.autocomplete_floatLable);
                    View view_float_label = (View) root.findViewById(R.id.view_float_label);
                    ImageView imageView = (ImageView) root.findViewById(R.id.iv_clear_ed);
                    //textInputLayout.setLayoutParams(layoutParams);
                    textInputLayout.setHint(fieldInfo.getFieldNm());
                    final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) root.findViewById(R.id.autocompletetxt);

                    ArrayList<String> optionListValue = new ArrayList<>();
                    ArrayList<String> optionListkey = new ArrayList<>();


                    Iterator myVeryOwnIterator = fieldInfo.getFieldOptions().keySet().iterator();
                    while (myVeryOwnIterator.hasNext()) {
                        String key = (String) myVeryOwnIterator.next();
                        optionListkey.add(key);
                        optionListValue.add(fieldInfo.getFieldOptions().get(key));
                    }


                    Collections.sort(optionListkey, new Comparator<String>() {
                        public int compare(String o1, String o2) {
                            return extractInt(o1) - extractInt(o2);
                        }

                        int extractInt(String s) {
                            String num = s.replaceAll("\\D", "");
                            // return 0 if no digits found
                            return num.isEmpty() ? 0 : Integer.parseInt(num);
                        }
                    });


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (context, R.layout.dropdown_text_view, optionListkey);

                    if (fieldInfo.getFieldTyp().equalsIgnoreCase(versionx.selfentry.Util.Global.SEARCHABLE)) {
                        autoCompleteTextView.setFocusable(true);
                        autoCompleteTextView.setCursorVisible(true);
                        autoCompleteTextView.setThreshold(0);

                    } else {
                        autoCompleteTextView.setFocusable(false);
                        autoCompleteTextView.setCursorVisible(false);
                        autoCompleteTextView.setThreshold(256);
                    }

                    autoCompleteTextView.setAdapter(adapter);

                    autoCompleteTextView.setText(fieldInfo.getFieldValue());
                    autoCompleteTextView.setTag(fieldInfo);


                    allAutoCompleteTextView.add(autoCompleteTextView);

                    imageView.setTag(allAutoCompleteTextView.size() - 1);

                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            allAutoCompleteTextView.get((Integer) view.getTag()).setText("");

                        }
                    });



                 *//*   final int finalI = i;

                    autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String fieldName = autoCompleteTextView.getAdapter().getItem(position).toString();
                            String fieldKey = fieldInfo.getFieldOptions().get(fieldName);


                            for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
                                FieldInfo fieldInfo = (FieldInfo) allAutoCompleteTextView.get(i).getTag();
                                if (fieldInfo.getDepOptions() != null && fieldInfo.getDepOptions().containsKey(fieldKey)) {
                                    allAutoCompleteTextView.get(i).setTag(R.integer.depKey, fieldKey);
                                }
                            }


                        }
                    });*//*

                    autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            autoCompleteTextView.showDropDown();
                            autoCompleteTextView.requestFocus();
                            return false;
                        }
                    });

                    autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                autoCompleteTextView.showDropDown();

                            }
                        }
                    });



                   *//* autoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {


                            if (keyCode == KeyEvent.KEYCODE_TAB) {
                                autoCompleteTextView.dismissDropDown();
                                autoCompleteTextView.requestFocus();
                            }
                            return false;
                        }

                    });*//*


                    if (fieldInfo.getFieldGroupNm().equals("meet"))
                        llForCustomFields_ToMeet.addView(root);
                    else
                        llForCustomFields_Visitor.addView(root);

                    if (fieldInfo.getFieldHide()) {
                        textInputLayout.setVisibility(View.GONE);
                        view_float_label.setVisibility(View.GONE);
                        autoCompleteTextView.setVisibility(View.GONE);
                        // llForCustomFields_ToMeet.setVisibility(View.GONE);
                    }


                }

                if (fieldInfo.getFieldTyp().equalsIgnoreCase(versionx.selfentry.Util.Global.IMG)) {

                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams
                            (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    LinearLayout ll = new LinearLayout(HomeActivity.this);
                    ll.setOrientation(LinearLayout.VERTICAL);
                    ll.setLayoutParams(viewparams);

                    LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    imageParams.setMargins(5, 45, 0, 0);
                    LinearLayout.LayoutParams imgtextParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);

                    imgtextParams.setMargins(10, 0, 0, 0);
                    ImageView imageView = new ImageView(HomeActivity.this);
                    imageView.setTag(fieldInfo);
                    imageView.setId(count);

                    count++;
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView.setLayoutParams(imageParams);
                    if (fieldInfo.getFieldKey().equals(versionx.selfentry.Util.Global.VISITOR_ID)) {
                        imageView.setImageResource(R.drawable.ic_id_card);

                    } else
                        imageView.setImageResource(R.drawable.ic_photo);
                    ll.addView(imageView);
                    if (fieldInfo.getFieldGroupNm().equals("meet"))
                        llForCustomFields_ToMeet.addView(ll);
                    else
                        llForCustomFields_Visitor.addView(ll);
                    TextViewOpenSans imgtext;
                    imgtext = new TextViewOpenSans(context);
                    imgtext.setTextColor(Color.parseColor("#a8a8a8"));
                    imgtext.setLayoutParams(imgtextParams);
                    imgtext.setGravity(Gravity.CENTER);
                    imgtext.setText(fieldInfo.getFieldNm());
                    imgText.add(imgtext);
                    allImageview.add(imageView);
                    imgtext.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                    ll.addView(imgtext);
                    if (fieldInfo.getFieldHide()) {
                        imgtext.setVisibility(View.GONE);
                        //view.setVisibility(View.GONE);
                        imageView.setVisibility(View.GONE);
                        //   llForCustomFields_ToMeet.setVisibility(View.GONE);
                    }
                }
            }
            setCameraListners();
        } catch (Exception e) {

        }

    }
*/

    // get all the purpose from firebase
    public void getPurpose(final Context context, String lbl) {

        loginsp = getSharedPreferences(versionx.selfentry.Util.Global.LOGIN_SP, Context.MODE_PRIVATE);
        purposeRef = PersistentDatabase.getDatabase().getReference("purpose/" +
                loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/"
                + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

        purposeArray = new HashMap<>();
        Query query = null;
        if (lbl.equals("all"))
            query = purposeRef;
        else
            query = purposeRef.orderByChild("lbl" + "/" + lbl).equalTo(true);

        purposeListner = query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                purposeArray.clear();

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    //purposeArray.add(child.getKey());


                    purposeArray.put(child.getKey(), child.child("nm").getValue().toString());


                }
                setPurposeAdapter(context);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private ArrayList<String> getpurposeArray() {
        ArrayList<String> purposeList = new ArrayList<>();
        Iterator myVeryOwnIterator = purposeArray.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            purposeList.add(purposeArray.get(key).trim());
        }


        return purposeList;
    }


    // get to meet dropdown data
    private void getToMeetAccess(final Context context, String lbl) {


        if (toMeetchildEventListener != null) {
            toMeetaccessRef.removeEventListener(toMeetchildEventListener);
        }
        toMeetList.clear();
        tempList.clear();
        toMeetList.clear();
        setToMeet(context);

        final DatabaseReference toMeetRef = PersistentDatabase.getDatabase()
                .getReference(toMeetTyp).child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                .child(loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

        //listening to change only if datetimestamp of the reference key is updated


        Query query = toMeetRef.orderByChild("lbl" + "/" + lbl).equalTo(true);


        toMeetchildEventListener = query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                ToMeet toMeet;
                if (dataSnapshot.exists()) {
                    toMeet = dataSnapshot.getValue(ToMeet.class);
                    toMeet.setFmlyMember(false);
                    toMeet.setId(dataSnapshot.getKey());

                    if (dataSnapshot.hasChild("dnd")) {
                        toMeet.setDndMode(true);
                        toMeet.setvTd(dataSnapshot.child("dnd").child("vDt").getValue(Long.class));
                        toMeet.setDndRsn(dataSnapshot.child("dnd").child("rsn").getValue(String.class));
                    }

                    if (dataSnapshot.child("lbl").hasChild("q")) {

                        toMeet.setQ(dataSnapshot.child("lbl").child("q").getValue(Boolean.class));
                    } else {
                        toMeet.setQ(false);
                    }

                    toMeetList.add(0, toMeet);
                    tempList.add(0, toMeet);

                    // if it is resident and has family node , it will show family members also in to meet drop down

                    for (DataSnapshot familySnap : dataSnapshot.child("fmly").getChildren()) {
                        toMeet = dataSnapshot.getValue(ToMeet.class);
                        toMeet.setNm(familySnap.child("nm").getValue(String.class));
                        toMeet.setFmlyMember(true);
                        toMeet.setFmlyMob(familySnap.getKey());
                        toMeet.setAptNo(toMeet.getAptNo());
                        toMeet.setId(dataSnapshot.getKey());
                        toMeet.setMob(dataSnapshot.child("mob").getValue(String.class));
                        toMeetList.add(0, toMeet);
                        tempList.add(0, toMeet);

                    }

                    setToMeet(context);

                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {


                if (dataSnapshot.exists()) {

                    ToMeet toMeet = dataSnapshot.getValue(ToMeet.class);
                    toMeet.setFmlyMember(false);
                    toMeet.setId(dataSnapshot.getKey());

                    if (dataSnapshot.hasChild("dnd")) {
                        toMeet.setDndMode(true);
                        toMeet.setvTd(dataSnapshot.child("dnd").child("vDt").getValue(Long.class));
                        toMeet.setDndRsn(dataSnapshot.child("dnd").child("rsn").getValue(String.class));
                    }
                    if (dataSnapshot.child("lbl").hasChild("q")) {

                        toMeet.setQ(dataSnapshot.child("lbl").child("q").getValue(Boolean.class));
                    } else {
                        toMeet.setQ(false);
                    }


                    Iterator<ToMeet> it = tempList.iterator();
                    int count = 0;
                    while (it.hasNext()) {
                        ToMeet meet = it.next();
                        if (!meet.isFmlyMember() && (meet.getId().equals(dataSnapshot.getKey())
                                || (meet.getMob() != null && meet.getMob().equals(toMeet.getMob())))) {
//                                              toMeetList.set(count, toMeet);
                            tempList.set(count, toMeet);
                            //setToMeet(context);
                            break;
                        }
                        count++;
                    }

                    for (DataSnapshot familySnap : dataSnapshot.child("fmly").getChildren()) {
                        boolean oldChild = false;
                        toMeet = new ToMeet();
                        toMeet.setAptNo(dataSnapshot.child("aptNo").getValue(String.class));
                        toMeet.setNm(familySnap.child("nm").getValue(String.class));
                        toMeet.setFmlyMob(familySnap.getKey());
                        toMeet.setMob(dataSnapshot.child("mob").getValue(String.class));
                        toMeet.setFmlyMember(true);
                        toMeet.setId(dataSnapshot.getKey());

                        Iterator<ToMeet> it1 = tempList.iterator();
                        int count1 = 0;
                        while (it1.hasNext()) {
                            ToMeet meet = it1.next();
                            if (toMeet.getFmlyMob() != null && meet.getId().equals(dataSnapshot.getKey())
                                    && meet.isFmlyMember() && toMeet.getFmlyMob().equals(meet.getFmlyMob())) {
                                //                                toMeetList.set(count1, toMeet);
                                tempList.set(count1, toMeet);
                                oldChild = true;
                                break;
                            }

                            count1++;
                        }

                        if (!oldChild) {
                            tempList.add(0, toMeet);
                        }


                    }

                    setToMeet(context);
                }


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {


                try {
                    if (dataSnapshot.exists()) {
                        ToMeet toMeet = dataSnapshot.getValue(ToMeet.class);
                        toMeet.setId(dataSnapshot.getKey());

                        for (DataSnapshot familySnap : dataSnapshot.child("fmly").getChildren()) {
                            toMeet = new ToMeet();
                            toMeet.setNm(familySnap.child("nm").getValue(String.class));
                            toMeet.setId(dataSnapshot.getKey());
                        }


                        int count = 0;
                        Iterator<ToMeet> it = tempList.iterator();
                        while (it.hasNext()) {
                            ToMeet meet = it.next();
                            if (meet.getId().equals(dataSnapshot.getKey()) || (meet.getMob() != null && meet.getMob().equals(toMeet.getMob()))) {
                                //   toMeetList.set(count, toMeet);
                                it.remove();
                                setToMeet(context);
                                break;
                            }
                            count++;
                        }
                    }
                } catch (Exception e) {

                }


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    // setting adapter for toMeet
    public void setToMeet(Context context) {
        toMeetadapter.setTempItems(tempList);
    }


    public void setListners() {
        btn_update.setOnClickListener(this);
        iv_visitorpic.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        btn_epass.setOnClickListener(this);
     /*   iv_ed_toMeet_clear.setOnClickListener(this);
        iv_ed_mob_clear.setOnClickListener(this);
        iv_ed_nm_clear.setOnClickListener(this);
        iv_clear_meet_data.setOnClickListener(this);*/
        iv_clear_data.setOnClickListener(this);
        btn_nxt.setOnClickListener(this);


    }

    private void setConfirmBtn() {
        btn_save.setText("Confirm"); //if has token
        btn_save.setBackgroundResource(R.drawable.round_circle_orange);

    }

   /* private void setSaveBtn() {
        btn_save.setText("DONE");  //if no token
        btn_save.setBackgroundResource(R.drawable.round_circle_green);
    }*/


    // autofill data if present


    public void setDataFromMobileNumber(final Context context) {
        userMobileGroupRef = PersistentDatabase.getDatabase().
                getReference("visitor/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") +
                        "/" + ed_visitorMobile.getText().toString().trim().substring(0, 4) + "/"
                        + ed_visitorMobile.getText().toString().trim());

        // userMobileGroupRef.keepSynced(true);


        visitorMobListner = userMobileGroupRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot childData) {

                try {
                    if (childData.exists() && ed_visitorMobile.getText().toString().trim().length() == loginsp.getInt(versionx.selfentry.Util.Global.MOB_LENGTH, 10)
                            && (childData.getKey().equals(ed_visitorMobile.getText().toString().trim()))) {


                        if (!childData.hasChild("block")) {
                            if (childData.hasChild("ref") &&
                                    !CommonMethods.isTimeExceeded(System.currentTimeMillis(),
                                            childData.child("ref").child("dt").getValue(Long.class), 120000)
                                    && childData.child("ref").child("a").getValue(String.class).equals("in")) {
                                hiskey = childData.child("ref").child("id").getValue(String.class);
                                visitor.setPrevCalling(childData.child("ref").child("c").getValue(Integer.class));

                                if (childData.child("ref").hasChild("tknNo")) {
                                    visitor.setTknNo(childData.child("ref").child("tknNo").getValue(String.class));

                                }

                                if (childData.child("meet").hasChild("id"))
                                    visitor.setPrevTomeet(childData.child("meet").child("id").getValue(String.class));
                                if (childData.child("meet").hasChild("pId")) {
                                    visitor.getPrevPurpose().setpId(childData.child("meet").child("pId").getValue(String.class));
                                    visitor.getPrevPurpose().setP(childData.child("meet").child("p").getValue(String.class));
                                }


                                if (childData.child("ref").hasChild("tDt")) {
                                    visitor.setPrevtDt(childData.child("ref").child("tDt").getValue(Long.class));
                                }

                                visitor.setRepeated(true);  // if it is a repeated user


                            } else {
                                visitor.setRepeated(false);
                            }

                            if (childData.hasChild("ref")) {
                                visitor.setFirstTime(false);
                                if (childData.child("ref").hasChild("act") && childData.child("ref").child("act").getValue(String.class).equals("a")) {
                                    visitor.setAllowedBefore(true);
                                } else {
                                    visitor.setAllowedBefore(false);
                                }
                            } else {
                                visitor.setFirstTime(true);
                                visitor.setAllowedBefore(false);
                            }

                            isAppointmnetChecked = false;
                            if (childData.hasChild("typ"))

                                accessType = childData.child("typ").getValue().toString();


                            //written by Rajesh.....
                            DataSnapshot allowMeetSnap = childData.child("meet");


                            if (childData.hasChild("allow")) {

                                if (childData.child("allow").hasChildren()) {
                                    allowMeetSnap = childData.child("allow").getChildren().iterator().next();
                                    accessType = versionx.selfentry.Util.Global.ALLOWED_VISITOR;
                                    type = "allow";

                                   /* showAlert(getActivity(), "Allow", "", "OK", "", "Allow", false, "", userMobileGroupRef,
                                            Global.ALLOWED_VISITOR, null, visitor);*/
                                }
                                // break allowBreak;

                            }

                            visitor.setType(accessType);

                            if (accessType.equals(versionx.selfentry.Util.Global.VISITOR)) {


                                isMobileEntered = false;


                                getCustomFieldData(childData, context, false); // set the customfields from visitor profile

                                if (!loginsp.getBoolean(Global.IMG_REQUIRED, false)) {
                                    if (childData.hasChild("img") && !childData.child("img").getValue().toString().trim().equals("")) {


                                        Glide.with(HomeActivity.this).load(childData.child("img").getValue().toString())
                                                .override(200, 200)
                                                .fitCenter()
                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .placeholder(R.drawable.default_user_picture).into(iv_visitorpic);

                                        visitorPhotoUrl = childData.child("img").getValue().toString();

                                        visitorPhotoFilePath = null;
                                        isPicSelected = true;
                                    } else {


                                        imagesDataSource.open();
                                        final String imgPath = imagesDataSource.getcustomImg(null, null, ed_visitorMobile.getText().toString().trim());
                                        imagesDataSource.close();

                                        if (imgPath != null) {
                                            visitorPhotoFilePath = imgPath;
                                            isPicSelected = true;
                                            visitorPhotoUrl = null;
                                        }


                                        Glide.with(HomeActivity.this).load(imgPath).asBitmap().override(300, 400).placeholder(R.drawable.default_user_picture)
                                                .into(new SimpleTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                                        visitorPhotoFilePath = imgPath;
                                                        iv_visitorpic.setImageBitmap(resource);

                                                        isPicSelected = true;
                                                        visitorPhotoUrl = null;
                                                    }
                                                });
                                    }
                                }

                                if (!isPicSelected) {
                                    visitorPhotoUrl = null;
                                    visitorPhotoFilePath = null;
                                    isPicSelected = false;

                                }
                                if (childData.child("nm").exists())
                                    ed_visitorName.setText(childData.child("nm").getValue().toString());


                                if (childData.hasChild("ntfy"))
                                    visitor.setNotify(true);
                                else
                                    visitor.setNotify(false);


                                if (childData.hasChild("appt") && childData.child("appt")
                                        .hasChild("" + CommonMethods.getTodaysDate().
                                                getTimeInMillis())) {

                                    visitor.setAppointment(true);

                                    getCustomFieldData(childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis()), context, true);

                                    if (childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis()).child("meet").hasChild("id")) {
                                        to_meet.setId(childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis()).child("meet").child("id").getValue().toString());
                                        to_meet.setMob(childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis()).child("meet").child("mob").getValue().toString());
                                    }
                                    to_meet.setNm(childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis()).child("meet").child("nm").getValue().toString());

                                    ed_toMeet.setText(childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis()).child("meet").child("nm").getValue().toString());


                                    setPurposeObject(childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis())
                                                    .child("meet").child("p").getValue().toString(),
                                            childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis())
                                                    .child("meet").hasChild("pId") ?
                                                    childData.child("appt").child("" + CommonMethods.getTodaysDate().getTimeInMillis())
                                                            .child("meet").child("pId").getValue().toString() : null);

                                    onlyImageRequired(context, btn_save.getText().toString(), "appointment",
                                            accessType, visitorPhotoUrl, visitorPhotoFilePath, visitor, calling, 3, versionx.selfentry.Util.Global.SCREEN_TYPE_WHOCAME);


                          /*  showAlert(HomeActivity.this, "Appointment Valid", "Let " + ed_visitorName.getText().toString()
                                            + " come in and meet " + childData.child("appt").
                                            child("" + CommonMethods.getTodaysDate().getTimeInMillis()).child("meet").child("nm").getValue().toString(),
                                    "OK", "", "appointment", false, "", userMobileGroupRef, childData.child("typ").getValue(String.class), null);*/


                                    //    setSaveBtn();
                                    return;
                                }


                                if (allowMeetSnap.exists()) {

                                    //if (!loginsp.getBoolean(Global.RE_ENTER_TO_MEET, false)) {
                                    if (allowMeetSnap.hasChild("id")) {
                                        to_meet.setId(allowMeetSnap.child("id").getValue().toString());
                                        to_meet.setMob(allowMeetSnap.child("mob").getValue().toString());
                                    }
                                    to_meet.setNm(allowMeetSnap.child("nm").getValue().toString());


                                    ed_toMeet.setText(allowMeetSnap.child("nm").getValue().toString());
                                    //  }

                                    updatePurpose(allowMeetSnap.child("p").getValue().toString());


                                    onlyImageRequired(context, btn_save.getText().toString(),
                                            type, accessType, visitorPhotoUrl, visitorPhotoFilePath, visitor, calling, 3, versionx.selfentry.Util.Global.SCREEN_TYPE_WHOCAME);


                                    return;

                          /*  showAlert(HomeActivity.this, "Allow", "", "OK", "", "Allow", false, "", userMobileGroupRef,
                                    childData.child("typ").getValue(String.class), null);*/
                                }


                            }

                            if (accessType.equals(versionx.selfentry.Util.Global.STAFF)
                                    || accessType.equals(versionx.selfentry.Util.Global.RESIDENT)
                                    || accessType.equals(versionx.selfentry.Util.Global.PARENT) ||
                                    accessType.equals(versionx.selfentry.Util.Global.STUDENT) ||
                                    accessType.equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR)
                                    || accessType.equals(versionx.selfentry.Util.Global.HELPER)) {


                                DatabaseReference dbRef = null;
                                Query query = null;

                                dbRef = PersistentDatabase.getDatabase().getReference(CommonMethods.getAccessNode(accessType) +
                                        "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") +
                                        "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));


                                query = dbRef.orderByChild("mob").equalTo(ed_visitorMobile.getText().toString());


                                // dbRef.keepSynced(true);
                                query.keepSynced(true);
                                try {
                                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            vehNoList.clear();
                                            if (dataSnapshot.exists()) {


                                                for (DataSnapshot staff : dataSnapshot.getChildren()) {

                                                    if (!staff.hasChild("act") ||
                                                            (staff.hasChild("act") && staff.child("act").getValue(Boolean.class).equals(true))) {

                                                        if (staff.hasChild("ref") &&
                                                                !CommonMethods.isTimeExceeded(System.currentTimeMillis(),
                                                                        staff.child("ref").child("dt").getValue(Long.class), 120000)
                                                                && staff.child("ref").child("a").getValue(String.class).equals("in")
                                                                && !accessType.equals(versionx.selfentry.Util.Global.STAFF) &&
                                                                !accessType.equals(versionx.selfentry.Util.Global.HELPER)) {
                                                            hiskey = staff.child("ref").child("id").getValue(String.class);
                                                            visitor.setPrevCalling(staff.child("ref").child("c").getValue(Integer.class));

                                                            if (staff.child("ref").hasChild("tknNo"))
                                                                visitor.setTknNo(staff.child("ref").child("tknNo").getValue(String.class));

                                                            if (staff.child("meet").hasChild("id"))
                                                                visitor.setPrevTomeet(staff.child("meet").child("id").getValue(String.class));
                                                            if (staff.child("meet").hasChild("pId")) {
                                                                visitor.getPrevPurpose().setpId(staff.child("meet").child("pId").getValue(String.class));
                                                                visitor.getPrevPurpose().setP(staff.child("ref").child("p").getValue(String.class));
                                                            }

                                                            if (childData.child("ref").hasChild("tDt")) {
                                                                visitor.setPrevtDt(staff.child("ref").child("tDt").getValue(Long.class));
                                                            }
                                                            visitor.setRepeated(true);

                                                   /* prevTomeet = staff.child("ref").child("toMeet").getValue(String.class);
                                                    prevCalling = staff.child("ref").child("c").getValue(Integer.class);*/
                                                        } else {
                                                            visitor.setRepeated(false);
                                                   /* prevCalling = 0;
                                                    prevTomeet = null;*/
                                                        }
                                                        if (staff.hasChild("ref")) {
                                                            visitor.setFirstTime(false);
                                                        } else {
                                                            visitor.setFirstTime(true);
                                                        }

                                                        if (staff.hasChild("sNo")) {
                                                            visitor.setsNo(staff.child("sNo").getValue().toString());
                                                        }


                                                        getCustomFieldData(staff, context, false); // set the customfields from visitor profile
                                                        accessKey = staff.getKey();
                                                        ed_visitorName.setText(staff.child("nm").getValue(String.class));

                                                        if (!loginsp.getBoolean(Global.IMG_REQUIRED, false)) {
                                                            if (staff.hasChild("img")) {
                                                                // orgimageLoader.DisplayImage(staff.child("img").getValue(String.class), iv_visitorpic);

                                                                Glide.with(HomeActivity.this).load(staff.child("img").getValue(String.class))
                                                                        .override(200, 200)
                                                                        .fitCenter()
                                                                        .diskCacheStrategy(DiskCacheStrategy.SOURCE).
                                                                        placeholder(R.drawable.default_user_picture).into(iv_visitorpic);


                                                                visitorPhotoUrl = staff.child("img").getValue().toString();
                                                                visitorPhotoFilePath = null;
                                                                isPicSelected = true;
                                                            } else {
                                                                imagesDataSource.open();

                                                                final String imgPath = imagesDataSource.getcustomImg(null, null, ed_visitorMobile.getText().toString().trim());
                                                                imagesDataSource.close();


                                                                if (imgPath != null) {
                                                                    visitorPhotoFilePath = imgPath;
                                                                    isPicSelected = true;
                                                                    visitorPhotoUrl = null;
                                                                }


                                                                Glide.with(HomeActivity.this).load(imgPath).asBitmap().placeholder(R.drawable.default_user_picture)
                                                                        .override(300, 400).into(new SimpleTarget<Bitmap>() {
                                                                    @Override
                                                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                                                        visitorPhotoFilePath = imgPath;
                                                                        iv_visitorpic.setImageBitmap(resource);

                                                                        isPicSelected = true;
                                                                        visitorPhotoUrl = null;


                                                                    }
                                                                });


                                                            }
                                                        }
                                                        if (staff.hasChild("ntfy"))
                                                            visitor.setNotify(true);
                                                        else
                                                            visitor.setNotify(false);

                                                        if (staff.hasChild("meet")) {
                                                            if (staff.child("meet").hasChild("id")) {
                                                                to_meet.setId(staff.child("meet").child("id").getValue().toString());

                                                                to_meet.setMob(staff.child("mob").getValue().toString());

                                                            }
                                                            to_meet.setNm(staff.child("meet").child("nm").getValue().toString());

                                                            ed_toMeet.setText(staff.child("meet").child("nm").getValue().toString());

                                                            ed_purpose.setText(staff.child("meet").child("p").getValue().toString());
                                                            setPurposeObject(staff.child("meet").child("p").getValue().toString(),
                                                                    staff.child("meet").hasChild("pId") ? staff.child("meet").child("pId").getValue().toString()
                                                                            : null);
                                                        }

                                               /* showAlert(HomeActivity.this, "Allow", "", "OK", "", "Allow", false, "", userMobileRef,
                                                        childData.child("typ").getValue(String.class), null);
*/
                                           /* } else {
                                                CommonMethods.showExpiredDialog(HomeActivity.this);
                                                clearData();
                                            }*/
                                                    }

                                                }


                                                //  setSaveBtn();
                                                if (accessType.equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR)) {
                                                    onlyImageRequired(context, btn_save.getText().toString(), type, accessType,
                                                            visitorPhotoUrl, visitorPhotoFilePath, visitor, calling, 3, versionx.selfentry.Util.Global.SCREEN_TYPE_WHOCAME);

                                                }


                                            } else {
                                                CommonMethods.showToast(HomeActivity.this, "Record not Found!", Toast.LENGTH_SHORT);
                                            }


                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });


                                } catch (Exception e) {
                                    Crashlytics.logException(e);
                                }

                            }


                        } else {

                            BlockVisitor(context);

                        }
                    } else {

                        visitor.setTknNo(null);
                        hiskey = null;
                        userMobileGroupRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot child) {

                                if (child.exists()) {
                                    isMobileEntered = false;

                                    accessType = versionx.selfentry.Util.Global.VISITOR;


                                    if (child.hasChild("img") && !child.child("img").getValue().toString().trim().equals("")) {


                                        Glide.with(HomeActivity.this).load(child.child("img").getValue().toString())
                                                .override(200, 200)
                                                .fitCenter()
                                                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                                .placeholder(R.drawable.default_user_picture).into(iv_visitorpic);

                                        visitorPhotoUrl = child.child("img").getValue().toString();

                                        visitorPhotoFilePath = null;
                                        isPicSelected = true;
                                    } else {


                                        imagesDataSource.open();

                                        final String imgPath = imagesDataSource.getcustomImg(null, null, ed_visitorMobile.getText().toString().trim());

                                        imagesDataSource.close();


                                        Glide.with(HomeActivity.this).load(imgPath).asBitmap().override(300, 400)
                                                .placeholder(R.drawable.default_user_picture).into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                                visitorPhotoFilePath = imgPath;
                                                iv_visitorpic.setImageBitmap(resource);
                                                isPicSelected = true;
                                                visitorPhotoUrl = null;
                                            }
                                        });

                                    }

                                    if (!isPicSelected) {
                                        visitorPhotoUrl = null;
                                        visitorPhotoFilePath = null;
                                        isPicSelected = false;

                                    }
                                    if (child.child("nm").exists())
                                        ed_visitorName.setText(child.child("nm").getValue().toString());

                                }


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }


                } catch (Exception e) {

                } finally {
                    if (visitorMobListner != null && userMobileGroupRef != null) {
                        userMobileGroupRef.removeEventListener(this);

                    }
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void clearData(boolean reprint) {
        isRadioAutofilled = false;
        lastToken = null;
        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, false).apply();
        visitor.clear();
        showWhocame();
        hiskey = null;
        removeRedFields();
        //visitorType=null;
        ed_visitorMobile.setText("");
        ed_purpose.setText("");
        ed_toMeet.setText("");
        ed_visitorMobile.setText("");
        ed_visitorName.setText("");
        ed_purpose.setText("");
        iv_visitorpic.setImageResource(0);
        iv_visitorpic.setImageDrawable(null);
        iv_visitorpic.setImageResource(R.drawable.default_user_picture);
        bit = null;
        selectedPurpose = null;
        isPicSelected = false;
        type = "visitor";
        /*  photo=null;*/
        visitorPhotoUrl = null;
        visitorPhotoFilePath = null;
        vizImgBit = null;
        //    ed_visitorMobile.setTag(null);
        accessType = versionx.selfentry.Util.Global.VISITOR;
        accessKey = null;
        callTime = 0;
        prevVisitorPhotoFilePath = null;
        vehNoList.clear();
        ed_visitorMobile.setEnabled(true);

        // custom fields clearing
        // custom fields clearing
        if (allEds != null) {
            for (int i = 0; i < allEds.size(); i++) {
                CustomFields customFields = (CustomFields) allEds.get(i).getTag();
                if (!customFields.getId().equals("p") && !customFields.getId().equals("host")) {
                    if (customFields.getVal() != null && !customFields.getVal().isEmpty()) {
                        allEds.get(i).setText(customFields.getVal());
                    } else {
                        allEds.get(i).setText("");
                    }

                    if (customFields.isDep()) {
                        allEds.get(i).setEnabled(false);
                    }

                    if (!customFields.isDsbl())
                        customFields.getView().setVisibility(View.GONE);

                    allEds.get(i).setError(null);
                    customFields.setDepReq(false);
                    allEds.get(i).setTag(customFields);

                }
            }
        }

        if (allAutoCompleteTextView != null) {
            for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
                CustomFields customFields = (CustomFields) allAutoCompleteTextView.get(i).getTag();
                if (!customFields.getId().equals("p") && !customFields.getId().equals("host")) {
                    if (customFields.getVal() != null && !customFields.getVal().isEmpty()) {
                        allAutoCompleteTextView.get(i).setText(customFields.getVal());
                    } else {
                        allAutoCompleteTextView.get(i).setText("");
                    }
                    allAutoCompleteTextView.get(i).setError(null);
                    if (customFields.isDep()) {
                        allAutoCompleteTextView.get(i).setEnabled(false);

                    }
                    if (!customFields.isDsbl())
                        customFields.getView().setVisibility(View.GONE);

                    FieldsAutoCompleteAdapter adapter1 = new FieldsAutoCompleteAdapter(context, android.R.layout.simple_dropdown_item_1line, customFields.getOptions());
                    allAutoCompleteTextView.get(i).setAdapter(adapter1);
                }
                if (customFields.getId().equals("host")) {
                    setToMeet(HomeActivity.this);
                }

                if (customFields.isDep()) {
                    allAutoCompleteTextView.get(i).setEnabled(false);
                    if (customFields.getId().equals("p")) {
                        ed_purpose.setEnabled(false);
                    }

                    if (customFields.getId().equals("host")) {
                        ed_toMeet.setEnabled(false);
                    }
                    customFields.setDepReq(false);
                    allAutoCompleteTextView.get(i).setTag(customFields);
                }


            }
        }


        if (allRadioGroup != null) {
            for (int i = 0; i < allRadioGroup.size(); i++) {
                CustomFields customFields = (CustomFields) allRadioGroup.get(i).getTag();
                allRadioGroup.get(i).removeAllViews();
                 /*   if (customFields.getVal() != null && !customFields.getVal().isEmpty()) {
                        allRadioGroup.get(i).setText(customFields.getVal());
                    } else {*/
                allRadioGroup.get(i).clearCheck();
                //  }

                if (customFields.isDep()) {
                    allRadioGroup.get(i).setEnabled(false);

                }
                if (!customFields.isDsbl())
                    customFields.getView().setVisibility(View.GONE);

                allRadioGroup.get(i).removeAllViews();

                if (customFields.isDep()) {
                    allRadioGroup.get(i).setEnabled(false);

                    customFields.setDepReq(false);
                    allRadioGroup.get(i).setTag(customFields);
                }


            }
        }


        for (int i = 0; i < allImageview.size(); i++) {
            // imgPath.clear();
            CustomFields fields = (CustomFields) allImageview.get(i).getTag();
            if (!fields.getId().equals("img")) {
                imgPath.set(i, null);
                imgText.get(i).setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.app_text_color));
                allImageview.get(i).setImageResource(0);
                CustomFields fieldInfo = (CustomFields) allImageview.get(i).getTag();
                if (fieldInfo.getId().equals(Global.VISITOR_ID))
                    allImageview.get(i).setImageResource(R.drawable.ic_id_card);
                else
                    allImageview.get(i).setImageResource(R.drawable.ic_photo);

                if (fields.isDep()) {
                    allImageview.get(i).setEnabled(false);
                }
                if (!fields.isDsbl())
                    fields.getView().setVisibility(View.GONE);


                Glide.clear(allImageview.get(i));

                fields.setDepReq(false);
                allImageview.get(i).setTag(fields);
            }
        }

        FieldOptionsAsync fieldOptionsAsync = new FieldOptionsAsync(context, "GETALL", null, null);
        fieldOptionsAsync.setListner(this);
        fieldOptionsAsync.execute();


        selectedToMeet = null;
        to_meet.reset();
        ed_visitorMobile.setError(null);
        ed_visitorName.setError(null);
        ed_purpose.setError(null);
        ed_toMeet.setError(null);


        tempFile = null;
        scannedType = null;

        Glide.clear(iv_visitorpic);

        showDialingScreen(reprint);


        ed_visitorMobile.requestFocus();

        if (loginsp.getBoolean(versionx.selfentry.Util.Global.DEV_USER_LOGIN, false) &&
                !loginsp.getBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, false)
                && !loginsp.getBoolean(versionx.selfentry.Util.Global.IS_DEV_USER_LOGGEDIN, false)) {
            Intent in = new Intent(getApplicationContext(), UserLogin.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();

            overridePendingTransition(R.anim.left_to_right,
                    R.anim.left_to_right);
            startActivity(in);
        }

        if (visitorMobListner != null && userMobileGroupRef != null) {
            userMobileGroupRef.removeEventListener(visitorMobListner);
        }


    }


    // based on accessType , get the correct reference to set the data in firebase
    public void setData(final String btnType, final String accessType, final String visitorPhotoUrl,
                        final String visitorPhotoFilePath, final String hiskey, final Visitor visitor, final int calling, final String prevTomeet, final int prevCalling) {


        ////////////////////////////////////////////////////////STAFF ENTRY//////////////////////////////////////////////////////

        if (selectedToMeet.isDndMode() && CommonMethods.getTodaysDate().getTimeInMillis() <= selectedToMeet.getvTd()) {
            DndDialog();
            clearData(false);
            return;
        }
        if (accessType.equalsIgnoreCase(versionx.selfentry.Util.Global.STAFF) ||
                accessType.equalsIgnoreCase(versionx.selfentry.Util.Global.VENDOR)
                || accessType.equals(versionx.selfentry.Util.Global.PARENT)
                || accessType.equals(versionx.selfentry.Util.Global.STUDENT)
                || accessType.equals(versionx.selfentry.Util.Global.RESIDENT)
                || accessType.equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR)
                || accessType.equalsIgnoreCase(versionx.selfentry.Util.Global.HELPER)) {
            final DatabaseReference accessRef;
            Query query;

            accessRef = PersistentDatabase.getDatabase()
                    .getReference(CommonMethods.getAccessNode(accessType) + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                            + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));
            query = accessRef.orderByChild("mob").equalTo(ed_visitorMobile.getText().toString());

            query.keepSynced(true);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot staff : dataSnapshot.getChildren()) {
                        String hKey = hiskey;

                        DatabaseReference staffRef = PersistentDatabase.getDatabase()
                                .getReference(
                                        CommonMethods.getAccessNode(accessType) + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                                                + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/"
                                                + staff.getKey());

                        HisRef = PersistentDatabase.getDatabase().
                                getReference(CommonMethods.getAccessHistoryNode(accessType) + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

                        DatabaseReference accessUpdateRef = null;


                        HisRef = PersistentDatabase.getDatabase().
                                getReference(CommonMethods.getAccessHistoryNode(accessType) + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/" + CommonMethods.getFirstDayOfMonth().getTimeInMillis());


                        accessUpdateRef = PersistentDatabase.getDatabase().
                                getReference(CommonMethods.getAccessHistoryNode(accessType) + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                                        + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/" + CommonMethods.getFirstDayOfMonth().getTimeInMillis() + "/" + hKey + "/whoCame");


                        submitData(btnType, HisRef, accessUpdateRef, staffRef, hiskey, accessType, visitorPhotoFilePath, visitorPhotoUrl, visitor, calling, prevTomeet, prevCalling
                                , toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap);
                        CommonMethods.updateAccessTime(accessRef, staff.getKey(), "in", hiskey, calling);


                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        ////////////////////////////////////////////////////////VISITOR ENTRY//////////////////////////////////////////////////////


        else {


            final DatabaseReference visitorRef = PersistentDatabase.getDatabase().getReference(versionx.selfentry.Util.Global.VISITOR_HISTORY_NODE + "/"
                    + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/" +
                    loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/" + CommonMethods.getFirstDayOfMonth().getTimeInMillis());


            final DatabaseReference visitorUpdateRef = PersistentDatabase.getDatabase().
                    getReference(versionx.selfentry.Util.Global.VISITOR_HISTORY_NODE + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")
                            + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/" + CommonMethods.getFirstDayOfMonth().getTimeInMillis() + "/" + hiskey + "/whoCame");
            DatabaseReference visitorGroupRef = PersistentDatabase.getDatabase()
                    .getReference("visitor/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") +
                            "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/" + ed_visitorMobile.getText().toString().trim().substring(0, 4) + "/" +
                            ed_visitorMobile.getText().toString().trim());

            submitData(btnType, visitorRef, visitorUpdateRef, visitorGroupRef,
                    hiskey, accessType, visitorPhotoFilePath, visitorPhotoUrl,
                    visitor, calling, prevTomeet, prevCalling, toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap);


        }


    }


    private void submitData(final String btnType, DatabaseReference visitorRef,
                            final DatabaseReference visitorUpdateRef,
                            final DatabaseReference visitorGroupRef, String historyKey, String accessType, String visitorPhotoFilePath
            , String visitorPhotoUrl, Visitor visitor, int calling, String prevTomeet, int prevCalling,
                            ArrayList<HashMap> toMeetCustomFieldsHashmAp, ArrayList<HashMap> whoCameCustomFiledsHashmap) {


        urlList = CommonMethods.getHistoryNode(accessType, CommonMethods.getTodaysDate().getTimeInMillis(), loginsp);


        if (!ed_visitorName.getText().toString().trim().isEmpty()) {
            try {

                updateNotifyHis(visitor, accessType, accessKey); // update notify data
            } catch (Exception e) {

            } finally {

                visitor.setName(ed_visitorName.getText().toString().trim());
                visitor.setMobile(ed_visitorMobile.getText().toString().trim());
                visitor.setPurposeNm(ed_purpose.getText().toString().trim());
                long currentDtTm = System.currentTimeMillis();

                String vizMob = ed_visitorMobile.getText().toString().trim();


                loginsp.edit().putString(versionx.selfentry.Util.Global.LAST_HIS_KEY, historyKey).apply();
                loginsp.edit().putString(versionx.selfentry.Util.Global.LAST_PHONE_NO, ed_visitorMobile.getText().toString().trim()).apply();
                loginsp.edit().putInt(versionx.selfentry.Util.Global.LAST_CALLING, calling);
                if (loginsp.getBoolean(versionx.selfentry.Util.Global.EPASS, false)) {
                    calling = versionx.selfentry.Util.Global.CALL_EPASS;
                }

                //  updateLastToMeet();

                final HashMap<String, Object> visitorDataMap = new HashMap<>();
                HashMap<String, Object> meetDataMap = new HashMap<>();
                final HashMap<String, Object> whocameDatamap = new HashMap<>();
                HashMap<String, DatabaseReference> reservedFieldRef = new HashMap<>();
                meetDataMap.put("pId", visitor.getPurpose().getpId());
                meetDataMap.put("p", visitor.getPurpose().getP());

                String edName, edValue;

                for (int i = 0; i < allEds.size(); i++) {

                    CustomFields fieldInfo = (CustomFields) allEds.get(i).getTag();
                    if (!fieldInfo.getId().equals("mob") && !fieldInfo.getId().equals("nm")) {
                        edName = fieldInfo.getId();
                        edValue = allEds.get(i).getText().toString().trim();
                        HashMap<String, Object> customFieldMap = new HashMap<>();
                        customFieldMap.put("nm", fieldInfo.getName());
                        if (!fieldInfo.getType().equals(Global.NUMBER))
                            customFieldMap.put("val", edValue);
                        else if (!edValue.isEmpty() && fieldInfo.getType().equals(Global.NUMBER))
                            customFieldMap.put("val", Integer.parseInt(edValue));

                        if (TextUtils.isEmpty(allEds.get(i).getText().toString()) || fieldInfo.isHide()) {
                            customFieldMap = null;
                        }
                        if (!edValue.equals("") && fieldInfo.getFieldGrp().equals("meet") && !fieldInfo.isHide()) {
                            meetDataMap.put(edName, customFieldMap);
                            visitorGroupRef.child("meet").child(edName).setValue(customFieldMap);

                            //  customMap.put(edName, customFieldMap);
                            //  toMeetCustomFieldsHashmAp.add(customMap);

                        } else if (!edValue.equals("") && !fieldInfo.isHide()) {
                            whocameDatamap.put(edName, customFieldMap);
                            visitorGroupRef.child("whoCame").child(edName).setValue(customFieldMap);
                            //  customMap.put(edName, customFieldMap);
                            // whoCameCustomFiledsHashmap.add(customMap);

                        }

                        if (fieldInfo.getId().equalsIgnoreCase(Global.COUNT_FIELD_ID) && !fieldInfo.isHide()
                                && !edValue.isEmpty()) {
                            visitor.setCount(Integer.parseInt(edValue));
                        }
                    }
                }


                for (int i = 0; i < allAutoCompleteTextView.size(); i++) {

                    CustomFields fieldInfo = (CustomFields) allAutoCompleteTextView.get(i).getTag();

                    if (!fieldInfo.getId().equals("p") && !fieldInfo.getId().equals("host")) {
                        edName = fieldInfo.getId();
                        edValue = null;
                        ArrayList<FieldsOptions> options = fieldInfo.getOptions();
                        for (FieldsOptions opt : options) {
                            if (opt.getName().equalsIgnoreCase(allAutoCompleteTextView.get(i).getText().toString().trim())) {
                                edValue = opt.getOptionId();
                                break;
                            }

                        }

                        //written by Rajesh...
                        HashMap<String, Object> customFieldMap = new HashMap<>();
                        //  HashMap<String, Object> customMap = new HashMap<>();
                        customFieldMap.put("nm", fieldInfo.getName());

                        HashMap<String, Object> ddMap = new HashMap<>();
                        ddMap.put(edValue, allAutoCompleteTextView.get(i).getText().toString());
                        customFieldMap.put("val", ddMap);
                        if (TextUtils.isEmpty(allAutoCompleteTextView.get(i).getText().toString())
                                || fieldInfo.isHide()) {
                            customFieldMap = null;
                        }
                        if (fieldInfo.getFieldGrp().equals("meet") && !edName.equals("") && !fieldInfo.isHide()) {
                            meetDataMap.put(edName, customFieldMap);

                            visitorGroupRef.child("meet").child(edName).setValue(customFieldMap);

                            // customMap.put(edName, customFieldMap);

                            //  toMeetCustomFieldsHashmAp.add(customMap);
                        } else if (!edName.equals("") && !fieldInfo.isHide()) {
                            whocameDatamap.put(edName, customFieldMap);

                            visitorGroupRef.child("whoCame").child(edName).setValue(customFieldMap);

                            //  customMap.put(edName, customFieldMap);
                            //  whoCameCustomFiledsHashmap.add(customMap);
                        }

                    }
                }


                for (int i = 0; i < allradiobutton.size(); i++) {

                    CustomFields fieldInfo = (CustomFields) allradiobutton.get(i).getTag();

                    edName = fieldInfo.getId();
                    edValue = null;
                    ArrayList<FieldsOptions> options = fieldInfo.getOptions();
                    for (FieldsOptions opt : options) {
                        if (opt.getName().equalsIgnoreCase(allradiobutton.get(i).getText().toString().trim())) {
                            edValue = opt.getOptionId();
                            break;
                        }

                    }

                    //written by Rajesh...
                    HashMap<String, Object> customFieldMap = new HashMap<>();
                    //  HashMap<String, Object> customMap = new HashMap<>();
                    customFieldMap.put("nm", fieldInfo.getName());

                    HashMap<String, Object> ddMap = new HashMap<>();
                    ddMap.put(edValue, allradiobutton.get(i).getText().toString().trim());
                    customFieldMap.put("val", ddMap);
                    if (TextUtils.isEmpty(allradiobutton.get(i).getText().toString().trim())
                            || fieldInfo.isHide()) {
                        customFieldMap = null;
                    }
                    if (fieldInfo.getFieldGrp().equals("meet") && !edName.equals("") && !fieldInfo.isHide()
                            && allradiobutton.get(i).isChecked()) {
                        meetDataMap.put(edName, customFieldMap);

                        visitorGroupRef.child("meet").child(edName).setValue(customFieldMap);

                    } else if (!edName.equals("") && !fieldInfo.isHide()
                            && allradiobutton.get(i).isChecked()) {
                        whocameDatamap.put(edName, customFieldMap);

                        visitorGroupRef.child("whoCame").child(edName).setValue(customFieldMap);
                    }


                }


                for (int i = 0; i < allImageview.size(); i++) {
                    CustomFields fieldInfo = (CustomFields) allImageview.get(i).getTag();
                    edName = fieldInfo.getId();
                    edValue = imgPath.get(i);

                    //written by Rajesh...
                    HashMap<String, Object> customFieldMap = new HashMap<>();
                    customFieldMap.put("nm", fieldInfo.getName());


                    if (imgPath.get(i) != null && !imgPath.get(i).contains("https://")) {

                        if (!edValue.equals("") && fieldInfo.getFieldGrp().equals("meet") && !fieldInfo.isHide()) {

                            meetDataMap.put(edName, customFieldMap);
                            visitorGroupRef.child("meet").child(edName).updateChildren(customFieldMap);


                            ArrayList<String> imgUrls = new ArrayList<>();
                            imgUrls.add("" + visitorGroupRef.child("meet").child(edName).child("val"));
                            imgUrls.add(PersistentDatabase.getDatabase().getReference(urlList.get(0))
                                    .child(historyKey).child("meet").child(edName).child("val") + "");
                            imgUrls.add(PersistentDatabase.getDatabase().getReference(urlList.get(1))
                                    .child(historyKey).child("meet").child(edName).child("val") + "");


                            saveImg(historyKey, visitor.getMobile(), edName, imgPath.get(i),
                                    "" + FirebaseStorage.getInstance().getReference(loginsp.getString(Global.BIZ_ID, "")
                                            + "/" + loginsp.getString(Global.GROUP_ID, "")
                                            + "/meet/" + historyKey + "_" + edName + ".jpg"), imgUrls);


                        } else if (!edValue.equals("") && !fieldInfo.isHide()) {

                            whocameDatamap.put(edName, customFieldMap);
                            visitorGroupRef.child("whoCame").child(edName).updateChildren(customFieldMap);
                            ArrayList<String> imgUrls = new ArrayList<>();
                            imgUrls.add("" + visitorGroupRef.child("whoCame").child(edName).child("val"));
                            imgUrls.add(PersistentDatabase.getDatabase().getReference(urlList.get(0)).child(historyKey)
                                    .child("whoCame").child(edName).child("val") + "");
                            imgUrls.add(PersistentDatabase.getDatabase().getReference(urlList.get(1)).child(historyKey)
                                    .child("whoCame").child(edName).child("val") + "");

                            saveImg(historyKey, visitor.getMobile(), edName, imgPath.get(i), "" + FirebaseStorage.getInstance().getReference(loginsp.getString(Global.BIZ_ID, "")
                                    + "/" + loginsp.getString(Global.GROUP_ID, "")
                                    + "/whoCame/" + historyKey + "_" + edName + ".jpg"), imgUrls);


                        }


                    } else if (imgPath.get(i) != null && imgPath.get(i).contains("https://") && !fieldInfo.isHide()) {

                        customFieldMap.put("val", imgPath.get(i));

                        if (fieldInfo.getFieldGrp().equals("meet")) {
                            meetDataMap.put(edName, customFieldMap);

                        } else {
                            whocameDatamap.put(edName, customFieldMap);
                        }
                    } else if (imgPath.get(i) == null) {
                        if (fieldInfo.getFieldGrp().equals("meet")) {
                            meetDataMap.put(edName, null);
                            visitorGroupRef.child("meet").child(edName).setValue(null);
                        } else {
                            whocameDatamap.put(edName, null);
                            visitorGroupRef.child("whoCame").child(edName).setValue(null);
                        }

                    }

                }

                if (!loginsp.getBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, false))
                    visitorDataMap.put("uId", loginsp.getString(versionx.selfentry.Util.Global.LOGGNED_IN_USER_ID, ""));


                whocameDatamap.put("id", accessKey);
                whocameDatamap.put("mob", visitor.getMobile());
                whocameDatamap.put("nm", visitor.getName());
                meetDataMap.put("nm", ed_toMeet.getText().toString().trim());

                if (selectedToMeet != null && selectedToMeet.getId() != null) {
                    meetDataMap.put("id", selectedToMeet.getId());
                    meetDataMap.put("mob", selectedToMeet.getMob());
                    meetDataMap.put("q", (selectedToMeet.isQ()) ? true : null);

                } else {
                    meetDataMap.put("id", null);
                }

                visitorDataMap.put("meet", meetDataMap);
                visitorDataMap.put("whoCame", whocameDatamap);
                if (accessType.equals(versionx.selfentry.Util.Global.STAFF))
                    visitorDataMap.put("ntfy", visitor.isNotify());

                if (visitor.isFirstTime() && !accessType.equals(versionx.selfentry.Util.Global.STAFF)
                        && accessType.equals(versionx.selfentry.Util.Global.HELPER))
                    visitorDataMap.put("fst", true);

                visitorDataMap.put("c", calling);
                // visitorDataMap.put("agg", false);
                visitorDataMap.put("mApp", selectedToMeet.isManualAllowed());
                visitorDataMap.put("in", currentDtTm);
                visitorDataMap.put("appt", visitor.isAppointment() ? true : null);
                visitorDataMap.put("o", true);

                HashMap<String, Object> hashMapDev = new HashMap<>();
                hashMapDev.put("id", loginsp.getString(versionx.selfentry.Util.Global.UUID, ""));
                hashMapDev.put("nm", loginsp.getString(versionx.selfentry.Util.Global.DEVICE_NAME, ""));
                hashMapDev.put("app", Global.APP_NAME);
                visitorDataMap.put("dev", hashMapDev);

                visitorDataMap.put("e", loginsp.getBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, false) ? versionx.selfentry.Util.Global.KIOSK_SELF : versionx.selfentry.Util.Global.KIOSK_DESKTOP);


                if (accessType.equals(versionx.selfentry.Util.Global.VISITOR) || accessType.equals(versionx.selfentry.Util.Global.PARENT))
                    visitorDataMap.put(accessType, currentDtTm);
                whocameDatamap.put("img", visitorPhotoUrl);

                if (isPicSelected) {
                    visitorDataMap.put("img", true);
                }


                visitorDataMap.put("by", "g");

                final HashMap<String, Object> hashMap = new HashMap<>();

                String tknNo = null;
                /////// update token in local database

                if (loginsp.getBoolean(Global.TOKEN, false) && selectedToMeet != null && selectedToMeet.isQ() && accessType.equals(Global.VISITOR)) {
                    if (visitor.getTknNo() == null) {
                        tknNo = loginsp.getString(Global.DEVICE_CODE, "") + loginsp.getInt(Global.TOKEN_NO, 0);
                    } else
                        tknNo = visitor.getTknNo();
                    if (loginsp.getBoolean(Global.HARDWARE, false)) {

                        if ((!visitor.isAppointment() && !visitor.getType().equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR)) &&
                                ((loginsp.getBoolean(versionx.selfentry.Util.Global.CONFIRM, false)
                                        && (visitor.isFirstTime() || !visitor.isAllowedBefore()))
                                        || loginsp.getBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false))) {

                        } else {

                            TokenUpdate g = new TokenUpdate(loginsp, selectedToMeet.getNm(), selectedToMeet.getId(), System.currentTimeMillis(),
                                    tknNo, visitor.getName(), historyKey, context);
                            // g.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            g.execute();
                        }
                    }


                    visitorDataMap.put("tknNo", tknNo);
                    loginsp.edit().putString(versionx.selfentry.Util.Global.LAST_TOKEN_NO, tknNo).apply();
                } else {
                    loginsp.edit().putString(versionx.selfentry.Util.Global.LAST_TOKEN_NO, null).apply();
                }

                hashMap.put(historyKey, visitorDataMap);

                CommonMethods.updateData(context, historyKey, urlList, visitorDataMap);  //// updating data in sync and visitor node


                //checking for reserved field vehicle.....
                if (whocameDatamap.containsKey(versionx.selfentry.Util.Global.VEHICLE)) {
                    final HashMap<String, Object> vehObj = (HashMap<String, Object>) whocameDatamap.get(versionx.selfentry.Util.Global.VEHICLE);
                    final HashMap<String, Object> vehObjMap = new HashMap<>();
                    vehObjMap.put("allow", true);
                    vehObjMap.put("mob", visitor.getMobile());
                    vehObjMap.put("pNm", visitor.getName());
                    vehObjMap.put("pImg", visitorPhotoUrl);
                    vehObjMap.put("typ/" + accessType, true);
                    reservedFieldRef.putAll(CommonMethods.saveReservedFieldData(versionx.selfentry.Util.Global.VEHICLE, vehObj.get("val").toString(),
                            vehObjMap, HomeActivity.this));
                    vehicle_no = vehObj.get("val").toString();
                }

                StorageReference riversRef = FirebaseStorage.getInstance().getReference().child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/"
                        + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")
                        + "/whoCame/" + historyKey + ".jpg");
                String vehicleno = vehicle_no;

                ArrayList<String> imgUrls = new ArrayList<>();
                imgUrls.add(PersistentDatabase.getDatabase().getReference(urlList.get(1)).child(historyKey).child("whoCame").child("img") + "");
                imgUrls.add(PersistentDatabase.getDatabase().getReference(urlList.get(0)).child(historyKey).child("whoCame").child("img") + "");
                imgUrls.add("" + visitorGroupRef.child("img"));
                imgUrls.add((!whocameDatamap.containsKey(versionx.selfentry.Util.Global.VEHICLE)) ? null : "" + vehRef.child(vehicleno).child("pImg"));


                saveImg(historyKey, visitor.getMobile(), null, visitorPhotoFilePath,
                        "" + riversRef, imgUrls);    // save img in database
                if (accessType.equals(versionx.selfentry.Util.Global.VISITOR) || accessType.equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR) || accessType.equals(versionx.selfentry.Util.Global.PARENT)) {
                    new TransactionData(context, loginsp, calling == 2 ? false : true, historyKey,
                            CommonMethods.getFirstDayOfMonth().getTimeInMillis(), visitorPhotoUrl,
                            currentDtTm, visitor, selectedToMeet).visitorUpdate();

                    if (selectedToMeet != null && selectedToMeet.getId() != null && selectedToMeet.isQ())
                        updateQNode(historyKey, selectedToMeet);
                }

                ///////visitorGRp entry////////////

                HashMap<String, Object> visitordata = new HashMap<>();
                visitordata.put("img", whocameDatamap.get("img"));
                visitordata.put("nm", visitor.getName());


                HashMap<String, Object> refhashMap = new HashMap<>();
                refhashMap.put("dt", currentDtTm);
                refhashMap.put("id", historyKey);
                refhashMap.put("a", "in");
                refhashMap.put("c", calling);
                if (selectedToMeet != null && selectedToMeet.getId() != null)
                    refhashMap.put("toMeet", selectedToMeet.getId());
                if (visitor.getPurpose().getpId() != null)
                    refhashMap.put("pId", visitor.getPurpose().getpId());
                refhashMap.put("tknNo", tknNo);
                refhashMap.put("p", visitor.getPurpose().getP());
                visitordata.put("ref", refhashMap);
                if (accessType.equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR))
                    visitorDataMap.put("allow", true);


                visitorGroupRef.updateChildren(visitordata);


                HashMap<String, Object> userMobileMap = new HashMap<>();
                userMobileMap.put("meet", meetDataMap);

                if (accessType == versionx.selfentry.Util.Global.VISITOR || accessType == versionx.selfentry.Util.Global.ALLOWED_VISITOR)
                    userMobileMap.put("typ", versionx.selfentry.Util.Global.VISITOR);
                visitorGroupRef.updateChildren(userMobileMap);


                visitorDataMap.put("img", false);


// sending notifation to one cast


                if (selectedToMeet != null && !selectedToMeet.equals(null) && selectedToMeet.getToken() != null &&
                        (accessType.equals(versionx.selfentry.Util.Global.VISITOR) || accessType.equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR)
                                || accessType.equals(versionx.selfentry.Util.Global.PARENT))) {


                    if ((!visitor.isAppointment() && !visitor.getType().equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR)) &&
                            ((loginsp.getBoolean(versionx.selfentry.Util.Global.CONFIRM, false)
                                    && (visitor.isFirstTime() || !visitor.isAllowedBefore()))
                                    || loginsp.getBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false))) {

                        //   visitorRef.child(historyKey).child("a").setValue("wait");

                        CommonMethods.setData(historyKey, urlList, "a", "wait");

                        visitorGroupRef.child("ref").child("act").setValue("wait");
                        DatabaseReference allowRef = PersistentDatabase.getDatabase().
                                getReference(versionx.selfentry.Util.Global.CAST_NODE + "/" +
                                        selectedToMeet.getMob().substring(0, 4) + "/" +
                                        selectedToMeet.getMob() + "/"
                                        + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") +
                                        "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/wait/who/");
                        HashMap<String, Object> allowMap = new HashMap<String, Object>();
                        allowMap.put(historyKey, CommonMethods.getFirstDayOfMonth().getTimeInMillis());
                        allowRef.updateChildren(allowMap);

                        DatabaseReference waitRef = PersistentDatabase.getDatabase().getReference("wait/" +
                                "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") +
                                "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

                        waitRef.child(historyKey).child("a").setValue("wait");
                        waitRef.child(historyKey).child("dt").setValue(CommonMethods.getFirstDayOfMonth().getTimeInMillis());
                        waitRef.child(historyKey).child("tDt").setValue(CommonMethods.getTodaysDate().getTimeInMillis());
                        waitRef.child(historyKey).child("devId").setValue(loginsp.getString(versionx.selfentry.Util.Global.UUID, ""));

                    } else {

                        //   visitorRef.child(historyKey).child("a").setValue("a");
                        CommonMethods.setData(historyKey, urlList, "a", "a");


                        if (loginsp.getBoolean(versionx.selfentry.Util.Global.CONFIRM, false) || loginsp.getBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false))
                            visitorGroupRef.child("ref").child("act").setValue("a");
                        if (selectedToMeet.getId() != null) {
                            DatabaseReference allowRef = PersistentDatabase.getDatabase().
                                    getReference(versionx.selfentry.Util.Global.CAST_NODE + "/" +
                                            selectedToMeet.getMob().substring(0, 4) + "/" +
                                            selectedToMeet.getMob()
                                            + "/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") +
                                            "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/a");
                            HashMap<String, Object> allowMap = new HashMap<String, Object>();

                            allowMap.put(historyKey, CommonMethods.getFirstDayOfMonth().getTimeInMillis());

                            allowRef.updateChildren(allowMap);
                        }
                    }

                    // if confimation required

                    sendNotification(historyKey, selectedToMeet, visitor, accessType, toMeetCustomFieldsHashmAp,
                            whoCameCustomFiledsHashmap, visitor.isFirstTime(), visitor.isAllowedBefore(),
                            visitorGroupRef, visitorRef.child(historyKey), visitor.isAppointment(), visitor.getType());

                } else {
                    CommonMethods.setData(historyKey, urlList, "a", "a");

                    clearData(false);
                }

                uploadImages(context); //upload images to server through local database
            }
        }

    }


    public long getCurrentTime() {
        Calendar c = Calendar.getInstance();
        long date = c.getTimeInMillis();
        return date;
    }

    @Override
    public void onStart() {
        super.onStart();


    }


    public void initGUI() {

        urlList = new ArrayList<String>();
        btn_epass = (Button) findViewById(R.id.btn_epass);
        startdialog = new Dialog(HomeActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        startdialog.setContentView(R.layout.start_layout);
        startdialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        startdialog.getWindow().setDimAmount(0.9f);
        startdialog.setCanceledOnTouchOutside(false);
        startdialog.setCancelable(false);
        startTv = (TextView) startdialog.findViewById(R.id.start_tv);
        ll_start = (LinearLayout) startdialog.findViewById(R.id.ll_start);
        //  homeActivityVisible = true;

        imagesDataSource = new ImagesDataSource(HomeActivity.this);

        imagesDataSource.open();
        bluetoothSp = getSharedPreferences(versionx.selfentry.Util.Global.BLUETOOTH_SP, MODE_PRIVATE);

        context = HomeActivity.this;
        visitor = new Visitor();

        vehNoList = new ArrayList<>();
        // cnt_btn=(Button)v.findViewById(R.id.cnt);
        VisitorLayoutPage = (LinearLayout) findViewById(R.id.VisitorLayoutPage);
        iv_call_logo = (ImageView) findViewById(R.id.iv_call_logo);
        ll_call = (LinearLayout) findViewById(R.id.ll_call);
        ll_for_ver_exp = (LinearLayout) findViewById(R.id.ll_version_expired);
        btn_update = (TextView) findViewById(R.id.btn_ver_update);
        btn_nxt = (Button) findViewById(R.id.btn_nxt);
        //    iv_clear_meet_data = (TextView) findViewById(R.id.iv_clear_meet_data);
        iv_clear_data = (TextView) findViewById(R.id.iv_clear_data);
        iv_ed_toMeet_clear = (ImageView) findViewById(R.id.iv_clear_toMeet);
        //    iv_ed_mob_clear = (ImageView) findViewById(R.id.iv_clear_mob);
        //    iv_ed_nm_clear = (ImageView) findViewById(R.id.iv_clear_nm);
        //   iv_ed_purpose_clear = (ImageView) findViewById(R.id.iv_clear_purpose);
        // iv_round = (ImageView) v.findViewById(in.mondays.whocame.R.id.iv_round);
        ed_purpose = (AutoCompleteTextView) findViewById(R.id.autocomplete_purpose);
        ed_toMeet = (AutoCompleteTextView) findViewById(R.id.ed_tomeet);
        ed_visitorMobile = (AutoCompleteTextView) findViewById(R.id.ed_visitorMobile);
        ed_visitorName = (EditText) findViewById(R.id.ed_visitorName);
        /* iv_call = (ImageView) v.findViewById(R.id.iv_visitorCall);*/
        iv_visitorpic = (ImageView) findViewById(R.id.iv_visitorpic);
        btn_save = (Button) findViewById(R.id.btn_save);
        //  tomeet_view = (View) findViewById(R.id.tomeet_view);
        llForCustomFields_ToMeet = (LinearLayout) findViewById(R.id.ll_for_custom_fields_toMeet);
        llForCustomFields_Visitor = (LinearLayout) findViewById(R.id.ll_for_custom_fields_Visitor);

        ll_for_whocame = (LinearLayout) findViewById(R.id.ll_for_whocame);
        ll_for_toMeet = (LinearLayout) findViewById(R.id.ll_for_toMeet);
        ll_for_toMeet.setVisibility(View.VISIBLE);
        //  ll_for_contact_person = (LinearLayout) findViewById(R.id.ll_for_contact_person);
        telManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        loginsp = getSharedPreferences(versionx.selfentry.Util.Global.LOGIN_SP, Context.MODE_PRIVATE);


        waitprogressdialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        waitprogressdialog.setContentView(R.layout.wait_dialog);
        waitprogressdialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        waitprogressdialog.getWindow().setDimAmount(0.8f);
        waitprogressdialog.setCanceledOnTouchOutside(false);
        waitprogressdialog.setCancelable(false);
        wait_tv = (TextView) waitprogressdialog.findViewById(R.id.wait_tv);


        //  hideKB(ed_visitorMobile);
        to_meet = new ToMeet();
        toMeetList = new ArrayList<>();
        tempList = new ArrayList<>();
        customFieldsMeetList = new ArrayList<>();
        allEds = new ArrayList<EditText>();
        allRadioGroup = new ArrayList<>();
        radioButtonData = new ArrayList<>();
        allAutoCompleteTextView = new ArrayList<>();
        imgText = new ArrayList<>();
        allImageview = new ArrayList<>();
        imgPath = new ArrayList<>();
        allradiobutton = new ArrayList<>();
        if (!loginsp.getString(versionx.selfentry.Util.Global.LOGGNED_IN_USER_NM, "").isEmpty())
            getSupportActionBar().setTitle("Self Entry - " + loginsp.getString(versionx.selfentry.Util.Global.DEVICE_NAME, "")
                    + "(" + loginsp.getString(versionx.selfentry.Util.Global.LOGGNED_IN_USER_NM, "") + ")");

        else

            getSupportActionBar().setTitle("Self Entry - " + loginsp.getString(versionx.selfentry.Util.Global.DEVICE_NAME, ""));

        if (loginsp.getString(versionx.selfentry.Util.Global.BIZ_TYPE, "").equalsIgnoreCase("school")) {
            toMeetTyp = "access";
        } else if (loginsp.getString(versionx.selfentry.Util.Global.BIZ_TYPE, "").equalsIgnoreCase("resident")) {
            toMeetTyp = "resident";
        }


        visitorRef = PersistentDatabase.getDatabase().getReference(versionx.selfentry.Util.Global.VISITOR_HISTORY_NODE + "/"
                + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/" +
                loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")).child("" + CommonMethods.getFirstDayOfMonth().getTimeInMillis());


//to meet adapter

      /*  toMeetadapter = new AutoCompeleteArrayAdapter(context, R.layout.dropdown_text_view, toMeetList);
        ed_toMeet.setAdapter(toMeetadapter);
        ed_toMeet.setThreshold(3);*/

        toMeetaccessRef = PersistentDatabase.getDatabase().getReference("label")
                .child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")).child(loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""))
                .child(toMeetTyp).child("toMeet").child("ref");
                /*"label/" + loginsp.getString(Global.BIZ_ID + "_0", "") +
                        "/" + loginsp.getString(Global.GROUP_ID, "")
                        + "/access/toMeet/ref/");*/


        toMeetaccessRef.keepSynced(true);


        vehRef = PersistentDatabase.getDatabase().getReference("vehicleProfile/" +
                loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));
        //  getPhoneNo();


        registerReceiver();


        gestureDetector = new GestureDetector(this, new MyGestureDetector());
        ll_call.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View view, final MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });


        //  ed_visitorMobile.requestFocus();


        nouploadRef = PersistentDatabase.getDatabase().getReference("temp").child("noupload").child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                .child(loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")).child(loginsp.getString(versionx.selfentry.Util.Global.UUID, ""));
        deviceRef = PersistentDatabase.getDatabase().getReference("device").child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "")).child(versionx.selfentry.Util.Global.APP_NAME)
                .child(loginsp.getString(versionx.selfentry.Util.Global.UUID, ""));

        showWhocame();

    }


    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("versionx.call");
        registerReceiver(receiver, filter);
    }


    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("versionx.call")) {

                //  if (MyApplication.isActivityVisible()) {


                if (loginsp.getBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, false)) {


                    VisitorLayoutPage.setVisibility(View.VISIBLE);
                    ll_call.setVisibility(View.GONE);
                    updateCallNumber(intent.getStringExtra("phoneNo"));
                }

                // }
            }
        }
    };


   /* public void setPurposeAdapter(Context context) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.dropdown_text_view, getpurposeArray());
        ed_purpose.setAdapter(adapter);
        ed_purpose.setThreshold(256);
        ed_purpose.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                hideKB(ed_purpose);
                ed_purpose.showDropDown();
                ed_purpose.requestFocus();

                return false;
            }
        });

       *//* ed_purpose.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKB(ed_purpose);

                    ed_purpose.showDropDown();

                }
            }
        });*//*


        ed_purpose.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setPurpose();


            }
        });


    }*/


    public void setPurposeAdapter(final Context context) {

        ArrayList<String> purposeList = getpurposeArray();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, purposeList);
        ed_purpose.setAdapter(adapter);
        ed_purpose.setThreshold(0);


        ed_purpose.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    /*  hideKB(ed_purpose);*/

                    ed_purpose.showDropDown();

                }
            }
        });


        ed_purpose.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                setPurpose();

                CustomFields customFields = (CustomFields) ed_purpose.getTag();

                FieldsOptions fieldsOptions = new FieldsOptions();
                fieldsOptions.setfId("p");
                fieldsOptions.setOptionId(visitor.getPurpose().getpId());
                fieldsOptions.setName(visitor.getPurpose().getP());

                FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context, "GET",
                        fieldsOptions, selectedOptions, "onselect", loginsp, customFields);
                fieldMappingAsync.setListner(HomeActivity.this);
                fieldMappingAsync.execute();


            }
        });

        if (getpurposeArray().size() == 1) {
            ed_purpose.setText(getpurposeArray().get(0));
            setPurpose();
            CustomFields customFields = (CustomFields) ed_purpose.getTag();


            FieldsOptions fieldsOptions = new FieldsOptions();
            fieldsOptions.setfId("p");
            fieldsOptions.setOptionId(visitor.getPurpose().getpId());

            fieldsOptions.setTyp(Global.DROPDOWN);
            fieldsOptions.setName(visitor.getPurpose().getP());

            FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context, "GET",
                    fieldsOptions, selectedOptions, "onselect", loginsp, customFields);
            fieldMappingAsync.setListner(HomeActivity.this);
            fieldMappingAsync.execute();
        }


    }


    private void setPurpose() {
        visitor.getPurpose().setP(ed_purpose.getText().toString().trim());
        visitor.getPurpose().setpId(null);
        for (Map.Entry<String, String> mapEntry : purposeArray.entrySet()) {
            if (mapEntry.getValue().trim().equals(ed_purpose.getText().toString().trim())) {
                visitor.getPurpose().copy(ed_purpose.getText().toString().trim(), mapEntry.getKey());

                return;
            }

        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_ver_update:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + versionx.selfentry.Util.Global.PACKAGE_NAME));
                startActivity(intent);

                break;

            case R.id.btn_save:

                loginsp.edit().putBoolean(versionx.selfentry.Util.Global.EPASS, false).apply();
                saveData(btn_save.getText().toString(), type, accessType, visitorPhotoUrl, visitorPhotoFilePath, HomeActivity.this, visitor, calling, prevTomeet, prevCalling);
                hideKB(ed_visitorName);

                break;

            case R.id.btn_epass:
                loginsp.edit().putBoolean(versionx.selfentry.Util.Global.EPASS, true).apply();
                saveData(btn_save.getText().toString(), type, accessType, visitorPhotoUrl, visitorPhotoFilePath, HomeActivity.this, visitor, calling, prevTomeet, prevCalling);
                hideKB(ed_visitorName);

                break;

            case R.id.iv_visitorpic:

                dispatchTakePictureIntent(100, calling, type); // taking picture from camera

                break;

            case R.id.iv_clear_toMeet:

                ed_toMeet.setText("");

                break;

          /*  case R.id.iv_clear_meet_data:
                try {

                    hideKB(iv_clear_meet_data);
                } finally {
                    showDialog();
                }

                break;

            case R.id.iv_clear_mob:
                ed_visitorMobile.setText("");
                break;


            case R.id.iv_clear_nm:
                ed_visitorName.setText("");
                break;

            case R.id.iv_clear_purpose:
                ed_purpose.setText("");

                break;

*/
            case R.id.iv_clear_data:
                try {

                    hideKB(iv_clear_data);
                } finally {
                    showDialog();
                }


                break;
            case R.id.btn_nxt:

                if (loginsp.getInt(versionx.selfentry.Util.Global.BTN_REQUIRE, 0) == versionx.selfentry.Util.Global.BOTH_EPASS_PRINT) {
                    btn_save.setVisibility(View.VISIBLE);
                    btn_epass.setVisibility(View.VISIBLE);
                } else if (loginsp.getInt(versionx.selfentry.Util.Global.BTN_REQUIRE, 0) == versionx.selfentry.Util.Global.ONLY_EPASS) {
                    btn_save.setVisibility(View.GONE);
                    btn_epass.setVisibility(View.VISIBLE);
                } else {
                    btn_save.setVisibility(View.VISIBLE);
                    btn_epass.setVisibility(View.GONE);
                }


                boolean view_print = validateWithoutPicFields(versionx.selfentry.Util.Global.SCREEN_TYPE_WHOCAME);
                if (view_print && validateMobile()) {
                    showToMeet();
                } else if (!validateMobile()) {
                    Toast toast = Toast.makeText(HomeActivity.this, "Please enter a valid mobile number!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (onlyImageRequired(context, "save", type, accessType, visitorPhotoUrl, visitorPhotoFilePath, visitor, calling, 3, versionx.selfentry.Util.Global.SCREEN_TYPE_WHOCAME)) {

                }

                try {
                    hideKB(btn_nxt);
                } catch (Exception e) {

                }

                break;


            default:
                break;
        }
    }

    private void showToMeet() {
        ll_for_toMeet.setVisibility(View.VISIBLE);
        ll_for_whocame.setVisibility(View.GONE);
        btn_nxt.setVisibility(View.GONE);


        // btn_save.requestFocus();

    }

    private void showWhocame() {
        ll_for_toMeet.setVisibility(View.GONE);
        ll_for_whocame.setVisibility(View.VISIBLE);
        btn_save.setVisibility(View.GONE);
        btn_nxt.setVisibility(View.VISIBLE);

    }

    private void dispatchTakePictureIntent(int i, int calling, String a) {

        Intent intent = new Intent(HomeActivity.this, CameraActivity.class);
        if (i == 100) {
            visitorPhotoFilePath = createFileFolder(new Date().getTime() + ".jpg");
            // visitorPhotoUrl = null;

            visitorImageFile = new File(visitorPhotoFilePath);
            intent.putExtra("a", a);
            intent.putExtra("calling", calling);

            intent.putExtra("path", visitorPhotoFilePath);
        } else {
            tempFile = createFileFolder(new Date().getTime() + ".jpg");
            intent.putExtra("path", tempFile);

        }

        startActivityForResult(intent, i);


    }


    // create folder
    public String createFileFolder(String filename) {
        File WhoCameDirectory = new File(Environment.getExternalStorageDirectory() +
                File.separator + "SelfEntry" + File.separator + "Images");

        if (!WhoCameDirectory.exists())
            WhoCameDirectory.mkdirs();

        File outputFile = new File(WhoCameDirectory, filename);


        return outputFile.getAbsolutePath();
    }

    public boolean onlyImageRequired(Context context, String btnType, String type, String accessType,
                                     String visitorPhotoUrl, String visitorPhotoFilePath, Visitor visitor, int calling,
                                     int count, String screen) {
        boolean imgRequired;
        this.type = type;
        if (!isPicSelected) {

            if (loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_PHOTO_REQUIRED, false)) {
                imgRequired = true;
                iv_visitorpic.setImageResource(0);
                iv_visitorpic.setImageResource(R.drawable.default_user_red);
            } else {
                imgRequired = false;

                iv_visitorpic.setImageResource(0);
                iv_visitorpic.setImageResource(R.drawable.default_user_picture);
            }

        } else {
            imgRequired = false;


        }

        if (screen.equals(versionx.selfentry.Util.Global.SCREEN_TYPE_MEET) && validateWithoutPicFields(versionx.selfentry.Util.Global.SCREEN_TYPE_MEET) && imgRequired) {

            onlyImgRequired = true;
            dispatchTakePictureIntent(100, calling, type);
            return false;
        } else if ((((type.equals("appointment") || type.equals("allow")) && validateWithoutPicFields(screen))) && !imgRequired) {

            if (calling == versionx.selfentry.Util.Global.CALL_DIALED && loginsp.getBoolean(versionx.selfentry.Util.Global.AUTO_PRINT, true)) {

                saveData(btn_save.getText().toString(), type, accessType, visitorPhotoUrl, visitorPhotoFilePath, context, visitor, calling, prevTomeet, prevCalling);

            }


            onlyImgRequired = false;
            return true;
        } else {
            onlyImgRequired = false;
            return false;
        }


    }


    /*public boolean validateWithoutPicFields(int count, String screen) {
        int size = count + allEds.size() + allAutoCompleteTextView.size() + allImageview.size();
        boolean[] view = new boolean[size];


        if (ed_visitorMobile.getText().toString().trim().equals("") || ed_visitorMobile.getText().toString().length() < loginsp.getInt(versionx.selfentry.Util.Global.MOB_LENGTH, 10)) {
            view[0] = false;
            ed_visitorMobile.setHintTextColor(Color.RED);
            ed_visitorMobile.setError("Can't be blank");


        } else {
            view[0] = true;
            ed_visitorMobile.setHintTextColor(getResources().getColor(R.color.app_text_color));
            ed_visitorMobile.setError(null);
        }


        if (ed_visitorName.getText().toString().trim().equals("")) {
            view[1] = false;
            ed_visitorName.setHintTextColor(Color.RED);
            ed_visitorName.setError("Can't be blank");

        } else {
            view[1] = true;
            ed_visitorName.setHintTextColor(getResources().getColor(R.color.app_text_color));
            ed_visitorName.setError(null);
        }
        if (ed_purpose.getText().toString().trim().equals("")) {
            view[2] = false;
            ed_purpose.setHintTextColor(Color.RED);
            ed_purpose.setError("Can't be blank");

        } else {
            view[2] = true;
            ed_purpose.setHintTextColor(getResources().getColor(R.color.app_text_color));
            ed_purpose.setError(null);
        }

        if (screen.equals(versionx.selfentry.Util.Global.SCREEN_TYPE_MEET)) {
            if (ed_toMeet.getText().toString().trim().equals("")) {
                view[3] = false;
                ed_toMeet.setHintTextColor(Color.RED);
                ed_toMeet.setError("Can't be blank");
            } else {
                view[3] = true;
                ed_toMeet.setHintTextColor(getResources().getColor(R.color.app_text_color));
                ed_toMeet.setError(null);
            }


            count = 4;

        }

        if (allAutoCompleteTextView != null && allAutoCompleteTextView.size() > 0) {
            for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allAutoCompleteTextView.get(i).getTag();

                if (allAutoCompleteTextView.get(i).getText().toString().trim().equals("") && fieldInfo.isRequired()) {
                    allAutoCompleteTextView.get(i).setError("Can't be blank");
                    view[count] = false;
                } else if (!fieldInfo.isEdt() && !allAutoCompleteTextView.get(i).getText().toString().isEmpty()
                        && !isOptionsExists(fieldInfo.getOptions(), allAutoCompleteTextView.get(i).getText().toString())) { // check this

                    allAutoCompleteTextView.get(i).setError("Select from list");
                    allAutoCompleteTextView.get(i).setText("");
                    view[count] = false;


                } else {
                    allAutoCompleteTextView.get(i).setError(null);
                    view[count] = true;
                }

                count++;
            }
        }


        if (allEds != null && allEds.size() > 0) {
            for (int i = 0; i < allEds.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allEds.get(i).getTag();

                if (allEds.get(i).getText().toString().trim().equals("") && fieldInfo.isRequired()) {
                    allEds.get(i).setError("Can't be blank");
                    view[count] = false;

                } else {
                    allEds.get(i).setError(null);
                    view[count] = true;
                }

                count++;
            }
        }


        if (allImageview != null && allImageview.size() > 0) {
            for (int i = 0; i < allImageview.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allImageview.get(i).getTag();

                if (imgPath.get(i) == null && fieldInfo.isRequired()) {
                    imgText.get(i).setTextColor(Color.RED);
                    view[count] = false;
                } else {
                    imgText.get(i).setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.app_text_color));
                    view[count] = true;
                }


                count++;

            }
        }

        return validate(view);
    }
*/


    public boolean validateWithoutPicFields(String screen) {
        int wSize = 0;
        int mSize = 0;
        int size = 0;

        for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allAutoCompleteTextView.get(i).getTag();
            if (fieldInfo.getFieldGrp().equals("whoCame") && screen.equals(Global.SCREEN_TYPE_WHOCAME)) {
                wSize++;
            } else if (fieldInfo.getFieldGrp().equals("meet") && screen.equals(Global.SCREEN_TYPE_MEET)) {
                mSize++;
            }

        }

        for (int i = 0; i < allRadioGroup.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allRadioGroup.get(i).getTag();
            if (fieldInfo.getFieldGrp().equals("whoCame") && screen.equals(Global.SCREEN_TYPE_WHOCAME)) {
                wSize++;
            } else if (fieldInfo.getFieldGrp().equals("meet") && screen.equals(Global.SCREEN_TYPE_MEET)) {
                mSize++;
            }

        }


        for (int i = 0; i < allEds.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allEds.get(i).getTag();
            if (
                    fieldInfo.getFieldGrp().equals("whoCame") && screen.equals(Global.SCREEN_TYPE_WHOCAME)) {
                wSize++;
            } else if (fieldInfo.getFieldGrp().equals("meet") && screen.equals(Global.SCREEN_TYPE_MEET)) {
                mSize++;
            }

        }


        for (int i = 0; i < allImageview.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allImageview.get(i).getTag();
            if (!fieldInfo.getId().equals("img")) {

                if (fieldInfo.getFieldGrp().equals("whoCame") && screen.equals(Global.SCREEN_TYPE_WHOCAME)) {
                    wSize++;
                } else if (fieldInfo.getFieldGrp().equals("meet") && screen.equals(Global.SCREEN_TYPE_MEET)) {
                    mSize++;
                }
            }


        }
        size = wSize + mSize;
        //  int size = allEds.size() + allAutoCompleteTextView.size() + allImageview.size();
        boolean[] view = new boolean[size];
        int count = 0;
        if (allAutoCompleteTextView != null && allAutoCompleteTextView.size() > 0) {
            for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allAutoCompleteTextView.get(i).getTag();
                if (fieldInfo.getFieldGrp().equals(screen)) {
                    if (allAutoCompleteTextView.get(i).getText().toString().trim().equals("") && (fieldInfo.isRequired() || fieldInfo.isDepReq())
                            && allAutoCompleteTextView.get(i).isEnabled() && allAutoCompleteTextView.get(i).isShown()) {
                        allAutoCompleteTextView.get(i).setError("Can't be blank");
                        view[count] = false;

                    } else if (!fieldInfo.isEdt() && !allAutoCompleteTextView.get(i).getText().toString().isEmpty()
                            && !isOptionsExists(fieldInfo.getOptions(), allAutoCompleteTextView.get(i).getText().toString())
                            && allAutoCompleteTextView.get(i).isEnabled() &&
                            allAutoCompleteTextView.get(i).getVisibility() == View.VISIBLE) { // check this

                        allAutoCompleteTextView.get(i).setError("Select from list");
                        allAutoCompleteTextView.get(i).setText("");
                        view[count] = false;

                    } else {
                        allAutoCompleteTextView.get(i).setHintTextColor(getResources().getColor(R.color.app_text_color));
                        allAutoCompleteTextView.get(i).setError(null);
                        view[count] = true;
                    }
                    count++;
                }
            }
        }


        if (allEds != null && allEds.size() > 0)

        {
            for (int i = 0; i < allEds.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allEds.get(i).getTag();
                if (fieldInfo.getFieldGrp().equals(screen)) {
                    if (allEds.get(i).getText().toString().trim().equals("") && (fieldInfo.isRequired() || fieldInfo.isDepReq())
                            && allEds.get(i).isEnabled() && allEds.get(i).isShown()) {
                        allEds.get(i).setError("Can't be blank");
                        view[count] = false;

                    } else {
                        allEds.get(i).setHintTextColor(getResources().getColor(R.color.app_text_color));
                        allEds.get(i).setError(null);
                        view[count] = true;
                    }
                    count++;
                }
            }
        }

        if (allRadioGroup != null && allRadioGroup.size() > 0) {
            for (int i = 0; i < allRadioGroup.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allRadioGroup.get(i).getTag();
                if (fieldInfo.getFieldGrp().equals(screen)) {
                    if (allRadioGroup.get(i).getCheckedRadioButtonId() == -1 && (fieldInfo.isRequired() || fieldInfo.isDepReq())
                            && allRadioGroup.get(i).isEnabled() && allRadioGroup.get(i).isShown()) {
                        radioButtonData.get(i).setTextColor(Color.RED);
                        view[count] = false;


                    } else {
                        radioButtonData.get(i).setTextColor(Color.parseColor("#606060"));
                        view[count] = true;
                    }

                    count++;
                }
            }
        }


        if (allImageview != null && allImageview.size() > 0) {
            for (int i = 0; i < allImageview.size(); i++) {
                CustomFields customFields = (CustomFields) allImageview.get(i).getTag();
                if (!customFields.getId().equals("img") && customFields.getFieldGrp().equals(screen)) {


                    if (imgPath.get(i) == null && (customFields.isRequired() || customFields.isDepReq())
                            && allImageview.get(i).isEnabled() && allImageview.get(i).isShown()) {
                        imgText.get(i).setTextColor(Color.RED);
                        view[count] = false;
                    } else {
                        imgText.get(i).setTextColor(Color.parseColor("#606060"));
                        view[count] = true;
                    }


                    count++;
                }
            }

        }

        return

                validate(view);

    }


    public boolean validateDataFields() {
        int size = allEds.size() + allAutoCompleteTextView.size() + allImageview.size() + allRadioGroup.size() + 1;
        boolean[] view = new boolean[size];
        if (!isPicSelected) {
            CustomFields fieldInfo = (CustomFields) iv_visitorpic.getTag();
            if (!fieldInfo.isHide() && fieldInfo.isRequired()) {
                view[0] = false;
                iv_visitorpic.setImageResource(0);
                iv_visitorpic.setImageResource(R.drawable.default_user_red);
            } else {
                view[0] = true;

                iv_visitorpic.setImageResource(0);
                iv_visitorpic.setImageResource(R.drawable.default_user_picture);


            }

        } else {
            view[0] = true;


        }


        int count = 1;


        if (allAutoCompleteTextView != null && allAutoCompleteTextView.size() > 0) {
            for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allAutoCompleteTextView.get(i).getTag();

                if (allAutoCompleteTextView.get(i).getText().toString().trim().equals("") && (fieldInfo.isRequired() || fieldInfo.isDepReq())
                        && allAutoCompleteTextView.get(i).isEnabled() && allAutoCompleteTextView.get(i).isShown()) {
                    allAutoCompleteTextView.get(i).setError("Can't be blank");
                    view[count] = false;
                } else if (!fieldInfo.isEdt() && !allAutoCompleteTextView.get(i).getText().toString().isEmpty()
                        && !isOptionsExists(fieldInfo.getOptions(), allAutoCompleteTextView.get(i).getText().toString())
                        && allAutoCompleteTextView.get(i).isEnabled() && allAutoCompleteTextView.get(i).isShown()) { // check this

                    allAutoCompleteTextView.get(i).setError("Select from list");
                    allAutoCompleteTextView.get(i).setText("");
                    view[count] = false;


                } else {
                    allAutoCompleteTextView.get(i).setError(null);
                    view[count] = true;
                }

                count++;
            }
        }


        if (allRadioGroup != null && allRadioGroup.size() > 0) {
            for (int i = 0; i < allRadioGroup.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allRadioGroup.get(i).getTag();

                if (allRadioGroup.get(i).getCheckedRadioButtonId() == -1 && (fieldInfo.isRequired() || fieldInfo.isDepReq())
                        && allRadioGroup.get(i).isEnabled() && allRadioGroup.get(i).isShown()) {
                    radioButtonData.get(i).setTextColor(Color.RED);
                    view[count] = false;


                } else {
                    radioButtonData.get(i).setTextColor(Color.parseColor("#606060"));
                    view[count] = true;
                }

                count++;
            }
        }


        if (allEds != null && allEds.size() > 0) {
            for (int i = 0; i < allEds.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allEds.get(i).getTag();

                if (allEds.get(i).getText().toString().trim().equals("") && (fieldInfo.isRequired() || fieldInfo.isDepReq())
                        && allEds.get(i).isEnabled() && allEds.get(i).isShown()) {
                    allEds.get(i).setError("Can't be blank");
                    view[count] = false;

                } else {
                    allEds.get(i).setError(null);
                    view[count] = true;
                }

                count++;
            }
        }


        if (allImageview != null && allImageview.size() > 0) {
            for (int i = 0; i < allImageview.size(); i++) {
                CustomFields fieldInfo = (CustomFields) allImageview.get(i).getTag();

                if (imgPath.get(i) == null && (fieldInfo.isRequired() || fieldInfo.isDepReq())
                        && allImageview.get(i).isShown()) {
                    imgText.get(i).setTextColor(Color.RED);
                    view[count] = false;
                } else {
                    imgText.get(i).setTextColor(Color.parseColor("#606060"));
                    view[count] = true;
                }


                count++;

            }
        }


        return validate(view);
    }

    public boolean validateMobile() {
        if (ed_visitorMobile.getText().toString().trim().isEmpty() ||
                (ed_visitorMobile.getText().toString().trim().length() > 0 &&

                        ed_visitorMobile.getText().toString().trim().length() < 10)) {

            ed_visitorMobile.setHintTextColor(Color.RED);

            ed_visitorMobile.setError("Invalid Number");
            return false;
        } else {
            ed_visitorMobile.setError(null);
            ed_visitorMobile.setHintTextColor(getResources().getColor(R.color.app_text_color));
            return true;
        }
    }

    public boolean validate(boolean[] view) {
        for (int i = 0; i < view.length; i++) {
            if (!view[i]) {
                return false;
            }
        }

        return true;

    }


    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Are you sure you want to clear the form?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                dialogInterface.dismiss();
                clearData(false);
            }
        });

        builder.create().show();
    }


    @Override
    public void onActivityResult(final int requestCode, int resultCode, final Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                if (data.getStringExtra("path") != null) {
                    visitorPhotoFilePath = data.getStringExtra("path");
                    prevVisitorPhotoFilePath = visitorPhotoFilePath;
                    final File file = new File(data.getStringExtra("path"));
                    if (file.exists()) {
                        Glide.with(context.getApplicationContext()).load(file).asBitmap().
                                override(200, 200).fitCenter().diskCacheStrategy(DiskCacheStrategy.RESULT)
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {



                      /*  if (onlyImgRequired) {
                            saveData(btn_save.getText().toString().trim(), type, accessType);
                        }*/

                                        if (loginsp.getBoolean(versionx.selfentry.Util.Global.FACE_DETECTION, false) && !detectFace(resource)) {
                                            if (file.exists()) {
                                                file.delete();
                                                isPicSelected = false;
                                                iv_visitorpic.setImageResource(R.drawable.default_user_picture);

                                            }

                                            Toast.makeText(HomeActivity.this, "Face not detected!", Toast.LENGTH_SHORT).show();
                                            dispatchTakePictureIntent(100, calling, type);
                                        } else {
                                            visitorPhotoUrl = null;
                                            isPicSelected = true;
                                            iv_visitorpic.setImageBitmap(resource);

                                            if (validateDataFields()
                                                    && loginsp.getBoolean(versionx.selfentry.Util.Global.AUTO_PRINT, true)) {

                                                // showToMeet();
                                                saveData(btn_save.getText().toString(), type, accessType,
                                                        visitorPhotoUrl, visitorPhotoFilePath, HomeActivity.this, visitor, calling, prevTomeet, prevCalling);

                                            }

                                        }

                                    }
                                });


                    } else {
                        Toast.makeText(HomeActivity.this, "Something went wrong! try again", Toast.LENGTH_SHORT).show();
                        iv_visitorpic.setImageResource(R.drawable.default_user_picture);
                    }
                }
            } else {
                // final Bitmap vizImgBit;
                try {
                    File visitorImageFilefinal = new File(new File(data.getStringExtra("path")).getAbsolutePath());


                    imgPath.set(requestCode, tempFile);
                    Glide.with(context.getApplicationContext()).load(visitorImageFilefinal).asBitmap().fitCenter().override(200, 200).fitCenter().
                            diskCacheStrategy(DiskCacheStrategy.RESULT).
                            into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {


                                    allImageview.get(requestCode).setImageBitmap(resource);
                                    // deleteLastImgCaptured(getActivity());
                                }
                            });


                } catch (Exception e) {

                    Toast.makeText(HomeActivity.this, "Something went wrong! try again", Toast.LENGTH_SHORT).show();

                }
            }


        } else if (data == null && resultCode != RESULT_CANCELED) {
            Toast.makeText(HomeActivity.this, "Something went wrong! try again", Toast.LENGTH_SHORT).show();
        } else if (resultCode == RESULT_CANCELED && requestCode == 100) {

            if (prevVisitorPhotoFilePath != null) {


                try {
                    if (visitorPhotoFilePath != null) {
                        File file = new File(visitorPhotoFilePath).getAbsoluteFile();
                        if (file.exists())
                            file.delete();
                    }
                } finally {
                    visitorImageFile = new File(prevVisitorPhotoFilePath);
                    visitorPhotoFilePath = prevVisitorPhotoFilePath;
                    visitorPhotoUrl = null;
                    isPicSelected = true;
                }

            } /*else {
                visitorImageFile = null;
                visitorPhotoFilePath = null;
                isPicSelected = false;
            }*/
        } else if (resultCode == RESULT_CANCELED) {
            // imgPath.remove(requestCode);
            imgPath.set(requestCode, null);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.activityResumed();
        // setUSBPrinter();

    }


    public void removeRedFields() {
        iv_visitorpic.setImageResource(0);
        iv_visitorpic.setImageResource(R.drawable.default_user_picture);
        ed_visitorMobile.setHintTextColor(getResources().getColor(R.color.app_text_color));
        ed_toMeet.setHintTextColor(getResources().getColor(R.color.app_text_color));
        ed_purpose.setHintTextColor(getResources().getColor(R.color.app_text_color));
        ed_visitorName.setHintTextColor(getResources().getColor(R.color.app_text_color));

    }


    public void hideKB(View v) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            // homeActivityVisible = false;

            unregisterReceiver(receiver);
            if (bitmap != null && !bitmap.isRecycled())
                bitmap.recycle();
            if (vizImgBit != null && !vizImgBit.isRecycled())
                vizImgBit.recycle();


            if (bit != null && !bit.isRecycled())
                bit.recycle();

            DrawerService.delHandler(mHandler);
            mHandler = null;
            uninitBroadcast();
/*
        alarmRef.removeEventListener(alarmListner);
        waitRef.removeEventListener(childWaitRed);*/
            if (deviceDeregister != null && deviceDeregisterListner != null)
                deviceDeregister.removeEventListener(deviceDeregisterListner);
            if (masterPrintRef != null && masterPrintRefListner != null)
                masterPrintRef.removeEventListener(masterPrintRefListner);

            if (connectedRef != null && connectedRefListner != null)
                connectedRef.removeEventListener(connectedRefListner);
            if (adminDb != null && adminListener != null)
                adminDb.removeEventListener(adminListener);
            if (deviceNmRef != null && deviceNmListner != null) {
                deviceNmRef.removeEventListener(deviceNmListner);
            }


            if (imageBitmap != null && imageBitmap.isRecycled()) {
                imageBitmap.recycle();
                imageBitmap = null;
            }

          /*  if (masterRef != null && masterRefListner != null) {
                masterRef.removeEventListener(masterRefListner);
            }*/
            if (deviceDialRef != null && deviceDialRefListner != null) {
                deviceDialRef.removeEventListener(deviceDialRefListner);
            }

            if (modeRef != null && modeRefListner != null) {
                modeRef.removeEventListener(modeRefListner);
            }


            firebaseDatabaseSettings.removeListners();
            if (tokenDbRef != null && tokenDbRefListner != null) {
                tokenDbRef.removeEventListener(tokenDbRefListner);
            }
        } catch (Exception e) {

        }


    }

    private void uninitBroadcast() {
        if (broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);


       /* if(statusBroadCastReceiver!=null)
            unregisterReceiver(statusBroadCastReceiver);*/
    }


    @Override
    public void onPause() {
        super.onPause();

        MyApplication.activityPaused();

    }


    public void updateCallNumber(String num) {

        try {
            //loginsp.edit().putBoolean(Global.CALL_BLOCK, false).apply();

            //  if (callTime == 0) {
            // clearData(false);

            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, false).apply();
            calling = Global.CALL_DIALED;
            updateText(num);
            //   }


        } catch (Exception e) {


        } finally {
            if (pinAlertDialog != null && pinDialog != null && pinDialog.isShowing()) {
                pinDialog.cancel();
                pinDialog.dismiss();

            }
        }

    }

    public void updateText(String str) {


        String number = str;
        try {

            System.out.println("test num " + number);
            number = str.substring(str.length() - 10, str.length());
        } catch (Exception e) {
            Crashlytics.logException(e);

        } finally {

            //  ed_visitorMobile.setTag("calling");
            ed_visitorMobile.setText(number);
        }


    }

    public void updateVendorScan(String str, String scannedType) {
        this.scannedType = scannedType;
        String number = str;
        try {
            number = str.substring(str.length() - 10, str.length());
        } catch (Exception e) {
            Crashlytics.logException(e);
        } finally {

            ed_visitorMobile.setText(number);
        }


    }


    public static String getDate(long milliSeconds, String dateFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private void printingData(String btnType, final String accessType, String visitorPhotoUrl, String visitorPhotoFilePath,
                              final Context context, final Visitor visitor, String type, String prevTomeet, int prevCalling) {

        if (toMeetCustomFieldsHashmAp != null)
            toMeetCustomFieldsHashmAp.clear();
        if (whoCameCustomFiledsHashmap != null)
            whoCameCustomFiledsHashmap.clear();

        if (hiskey == null)
            hiskey = visitorRef.push().getKey(); //visitor key


        setPurpose();


        visitor.setVisitorData(ed_visitorName.getText().toString().trim(), ed_visitorMobile.getText().toString().trim(), visitor.getPurpose(), visitor.getPrevPurpose());

        String oldToken = null;
        boolean isQ = false;

        if (selectedToMeet != null && selectedToMeet.isQ()) {

            isQ = true;

            if (loginsp.getBoolean(Global.TOKEN, false)) {
                oldToken = visitor.getTknNo();

                if (oldToken == null) {
                    loginsp.edit().putInt(Global.TOKEN_NO, loginsp.getInt(Global.TOKEN_NO, 0) + 1).apply();
                }
            }

        }

        final boolean isQReq = isQ;

        final String oldTkn = oldToken;


        if (loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_PRINT_REQUIRED, false)) {


            String edName, edValue;


            toMeetCustomFieldsHashmAp = new ArrayList<>();

            whoCameCustomFiledsHashmap = new ArrayList<>();


            for (int i = 0; i < allEds.size(); i++) {

                CustomFields fieldInfo = (CustomFields) allEds.get(i).getTag();

                if (!fieldInfo.getId().equals("nm") && !fieldInfo.getId().equals("mob")) {

                    edName = fieldInfo.getId();
                    edValue = allEds.get(i).getText().toString().trim();

                    HashMap<String, Object> customFieldMap = new HashMap<>();
                    customFieldMap.put("nm", fieldInfo.getName());
                    customFieldMap.put("val", edValue);
                    HashMap<String, Object> customMap = new HashMap<>();


                    if (!edValue.equals("") && fieldInfo.getFieldGrp().equals("meet")) {
                        customMap.put(edName, customFieldMap);
                        toMeetCustomFieldsHashmAp.add(customMap);

                    } else if (!edValue.equals("")) {
                        customMap.put(edName, customFieldMap);
                        whoCameCustomFiledsHashmap.add(customMap);
                    }
                }
            }


            for (int i = 0; i < allAutoCompleteTextView.size(); i++) {

                CustomFields fieldInfo = (CustomFields) allAutoCompleteTextView.get(i).getTag();
                if (!fieldInfo.getId().equals("p") && !fieldInfo.getId().equals("host")) {

                    edName = fieldInfo.getId();
                    // edValue = fieldInfo.getFieldOptions().get(allAutoCompleteTextView.get(i).getText().toString());
                    edValue = null;
                    ArrayList<FieldsOptions> options = fieldInfo.getOptions();
                    for (FieldsOptions opt : options) {
                        if (opt.getName().equalsIgnoreCase(allAutoCompleteTextView.get(i).getText().toString().trim())) {
                            edValue = opt.getOptionId();
                            break;
                        }

                    }
                    HashMap<String, Object> customFieldMap = new HashMap<>();

                    customFieldMap.put("nm", fieldInfo.getName());
                    if (!TextUtils.isEmpty(allAutoCompleteTextView.get(i).getText().toString())) {
                        HashMap<String, Object> ddMap = new HashMap<>();
                        ddMap.put(edValue, allAutoCompleteTextView.get(i).getText().toString());
                        HashMap<String, Object> customMap = new HashMap<>();
                        customFieldMap.put("val", ddMap);

                        if (fieldInfo.getFieldGrp().equals("meet") && !edName.equals("")) {

                            customMap.put(edName, customFieldMap);
                            toMeetCustomFieldsHashmAp.add(customMap);

                        } else if (!edName.equals("")) {

                            customMap.put(edName, customFieldMap);
                            whoCameCustomFiledsHashmap.add(customMap);

                        }

                    }
                }

            }


            if (selectedToMeet != null && selectedToMeet.getToken() != null &&
                    ((loginsp.getBoolean(versionx.selfentry.Util.Global.CONFIRM, false)
                            && (visitor.isFirstTime() || !visitor.isAllowedBefore()))
                            || loginsp.getBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false))) {

                setData(btnType, accessType, visitorPhotoUrl, visitorPhotoFilePath, hiskey, visitor, calling, prevTomeet, prevCalling);
                // clearData();

            } else {

                final String nm = ed_visitorName.getText().toString().trim();
                final String mob = ed_visitorMobile.getText().toString().trim();

                final String toMeet = ed_toMeet.getText().toString().trim();
                final long inMili = getCurrentTime();

                final String inTime = getDate(inMili, "d MMM h:mm a");

                final String purpose = visitor.getPurpose().getP();
                final String vizKey = hiskey;
                //   imgBit = vizImgBit;


                try {

                    // this takes the customfields to be printed , we are puting it here because it is asynchronous call inside setData

                    if (loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_PRINT_PHOTO, false)) {


                        //  vizImgBit = orgimageLoader.getBitmap(visitorPhotoUrl);
                        if (visitorPhotoUrl != null) {
                            Glide.with(context.getApplicationContext()).load(visitorPhotoUrl)
                                    .asBitmap().override(200, 200).diskCacheStrategy(DiskCacheStrategy.RESULT).centerCrop()
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                            vizImgBit = resource;

                                            new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                    toMeetCustomFieldsHashmAp,
                                                    whoCameCustomFiledsHashmap, accessType, vizKey, vizImgBit, visitor.getsNo(), inMili, isQReq, oldTkn); // connect to the printer and print slip
                                        }

                                        @Override
                                        public void onLoadFailed(Exception e, Drawable errorDrawable) {

                                            new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                    toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap,
                                                    accessType, vizKey, vizImgBit, visitor.getsNo(), inMili, isQReq, oldTkn); //
                                        }
                                    });
                        } else if (visitorPhotoFilePath != null) {
                            Glide.with(context.getApplicationContext()).load(visitorPhotoFilePath)
                                    .asBitmap().override(200, 200).diskCacheStrategy(DiskCacheStrategy.RESULT).centerCrop()
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                            vizImgBit = resource;

                                            new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                    toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap,
                                                    accessType, vizKey, vizImgBit, visitor.getsNo(), inMili, isQReq, oldTkn); // connect to the printer and print slip
                                        }

                                        @Override
                                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                            new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                    toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap,
                                                    accessType, vizKey, vizImgBit, visitor.getsNo(), inMili, isQReq, oldTkn); //
                                        }
                                    });
                        } else {
                            new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                    toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap,
                                    accessType, vizKey, vizImgBit, visitor.getsNo(), inMili, isQReq, oldTkn); //
                        }
                        // imgBit = vizImgBit;
                    } else {
                        new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                toMeetCustomFieldsHashmAp, whoCameCustomFiledsHashmap,
                                accessType, vizKey, vizImgBit, visitor.getsNo(), inMili, isQReq, oldTkn); // connect to the printer and print slip
                    }


                } finally {
                    setData(btnType, accessType, visitorPhotoUrl, visitorPhotoFilePath, hiskey, visitor, calling, prevTomeet, prevCalling); // firebase call to save data


                }
            }

        } else

        {
            setData(btnType, accessType, visitorPhotoUrl, visitorPhotoFilePath, hiskey, visitor, calling, prevTomeet, prevCalling);
            // clearData();
        }

    }


    public boolean isPurposeOther(String purpose) {

        Iterator myVeryOwnIterator = purposeArray.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            if (purposeArray.get(key).equalsIgnoreCase(purpose))
                return false;
        }
        return true;
    }


    private void setCameraListners() {
        try {

            for (int iv = 0; iv < allImageview.size(); iv++) {
                final int i = iv;
                imgPath.add(iv, null);

                allImageview.get(iv).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        menuItem = i;
                        showPopup(v, i);

                    }
                });


            }
        } catch (Exception e) {
            Crashlytics.logException(e);

        }

    }


    public void showPopup(View menuItemView, int i) {
        try {
            PopupMenu popup = new PopupMenu(HomeActivity.this, menuItemView);
            MenuInflater inflate = popup.getMenuInflater();
            inflate.inflate(R.menu.photo_menu, popup.getMenu());
            Menu menu = popup.getMenu();

            if (imgPath.get(i) == null) {
                menu.findItem(R.id.action_take_photo).setTitle("Take Photo");
                menu.findItem(R.id.action_zoom_photo).setVisible(false);
                menu.findItem(R.id.action_remove).setVisible(false);
            } else {
                menu.findItem(R.id.action_take_photo).setTitle("Retake Photo");
                menu.findItem(R.id.action_zoom_photo).setVisible(true);
                menu.findItem(R.id.action_remove).setVisible(true);
            }
            popup.setOnMenuItemClickListener(this);

            popup.show();

        } catch (Exception e) {
            Crashlytics.logException(e);
        }


    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_take_photo:


                dispatchTakePictureIntent(menuItem, calling, "");

                return true;

            case R.id.action_remove:


                imgPath.set(menuItem, null);

                CustomFields fieldInfo = (CustomFields) allImageview.get(menuItem).getTag();

                allImageview.get(menuItem).setImageResource(fieldInfo.getId().equals(versionx.selfentry.Util.Global.VISITOR_ID) ? R.drawable.ic_id_card : R.drawable.ic_photo);
                try {
                    if (new File(imgPath.get(menuItem)).exists()) {
                        new File(imgPath.get(menuItem)).delete();
                    }
                } catch (Exception e) {

                }
                return true;

            case R.id.action_zoom_photo:

                if (imgPath != null) {
                    final Dialog imgDialog = new Dialog(HomeActivity.this, android.R.style.Theme_NoTitleBar_Fullscreen);


                    imgDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    imgDialog.setContentView(R.layout.image_dialog);

                    try {
                        final ImageView iv = (ImageView) imgDialog.findViewById(R.id.zoomed_img);


                        Glide.with(getApplicationContext()).load(imgPath.get(menuItem)).asBitmap().fitCenter().override(300, 300)
                                .diskCacheStrategy(DiskCacheStrategy.RESULT)

                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                        iv.setImageBitmap(resource);
                                    }
                                });

                        iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imgDialog.dismiss();

                            }

                        });
                        imgDialog.show();

                    } catch (Exception e) {
                        Crashlytics.logException(e);

                    }
                }


                return true;

            default:
                return false;

        }


    }


    private void getCustomFieldData(DataSnapshot child, Context context, boolean isAppointment) {


        Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.default_user_picture);
        CustomFields customFields = (CustomFields) iv_visitorpic.getTag();
        if (!customFields.isHide() && !customFields.isReenter()) {
            if (child.hasChild("img") && !child.child("img").getValue().toString().trim().equals("")) {


                Glide.with(context.getApplicationContext()).load(child.child("img").getValue().toString())
                        .override(200, 200)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
                        .placeholder(drawable).into(iv_visitorpic);

                visitorPhotoUrl = child.child("img").getValue().toString();

                //  visitorPhotoFilePath = null;
                isPicSelected = true;
            } else {


                imagesDataSource.open();
                final String imgPath = imagesDataSource.getcustomImg(null, null, ed_visitorMobile.getText().toString().trim());
                imagesDataSource.close();
                if (imgPath != null) {
                    visitorPhotoFilePath = imgPath;
                    isPicSelected = true;
                    visitorPhotoUrl = null;
                }

                Glide.with(context.getApplicationContext()).load(imgPath).asBitmap().override(200, 200).centerCrop()
                        .placeholder(drawable)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                // visitorPhotoFilePath = imgPath;
                                iv_visitorpic.setImageBitmap(resource);

                                // isPicSelected = true;
                                //  visitorPhotoUrl = null;

                            }
                        });

            }


        }


        for (int i = 0; i < allEds.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allEds.get(i).getTag();
            if (!fieldInfo.getId().equals("mob") && !fieldInfo.getId().equals("nm")) {
                if (child.hasChild("meet")
                        && child.child("meet")
                        .hasChild(fieldInfo.getId().trim()) &&
                        (!fieldInfo.isReenter() || isAppointment || !accessType.equals(Global.VISITOR))) {


                    if (fieldInfo.getType().equals(Global.NUMBER)) {
                        String num = "";

                        if (!num.matches("[0-9]+")) {
                            allEds.get(i).setText("");
                        } else {
                            allEds.get(i).setText(child.child("meet")
                                    .child(fieldInfo.getId()).child("val").getValue().toString());
                        }


                    } else {
                        allEds.get(i).setText(child.child("meet")
                                .child(fieldInfo.getId()).child("val").getValue().toString());
                    }
                }


                if (child.hasChild("whoCame")
                        && child.child("whoCame")
                        .hasChild(fieldInfo.getId()) && (!fieldInfo.isReenter() || isAppointment || !accessType.equals(Global.VISITOR))) {


                    if (fieldInfo.getType().equals(Global.NUMBER)) {

                        String num = "";
                        if (!num.matches("[0-9]+")) {
                            allEds.get(i).setText("");
                        } else {

                            allEds.get(i).setText(child.child("whoCame")
                                    .child(fieldInfo.getId()).child("val").getValue().toString());
                        }
                    } else {

                        allEds.get(i).setText(child.child("whoCame")
                                .child(fieldInfo.getId()).child("val").getValue().toString());
                    }


                }
            } else {
                if (fieldInfo.getId().equals("nm")) {
                    ed_visitorName.setText(child.child("nm").getValue(String.class));
                }

            }


        }


        for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allAutoCompleteTextView.get(i).getTag();
            String id = "";
            if (!fieldInfo.getId().equals("p") && !fieldInfo.getId().equals("host")) {
                if (child.hasChild("meet")
                        && child.child("meet")
                        .hasChild(fieldInfo.getId().trim()) && (!fieldInfo.isReenter() || isAppointment || !accessType.equals(Global.VISITOR))) {

                    //written by Rajesh...
                    try {


                        HashMap<String, Object> hashMap = (HashMap<String, Object>)
                                child.child("meet")
                                        .child(fieldInfo.getId()).child("val").getValue();


                        for (FieldsOptions fieldsOptions : fieldInfo.getOptions()) {

                            if (hashMap.containsKey(fieldsOptions.getOptionId())) {

                                allAutoCompleteTextView.get(i).setText(fieldsOptions.getName().trim());
                                FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context, "GET",
                                        fieldsOptions, selectedOptions, "autofill", loginsp, fieldInfo);
                                fieldMappingAsync.setListner(HomeActivity.this);
                                fieldMappingAsync.execute();

                            }
                        }
                    } catch (ClassCastException e) {
                        allAutoCompleteTextView.get(i).setText(child.child("meet")
                                .child(fieldInfo.getId()).child("val").getValue().toString());
                    }
                }

                if (child.hasChild("whoCame")
                        && child.child("whoCame")
                        .hasChild(fieldInfo.getId().trim()) && (!fieldInfo.isReenter() || isAppointment
                        || !accessType.equals(Global.VISITOR)) && !fieldInfo.isHide()) {

                    try {

                        HashMap<String, Object> hashMap = (HashMap<String, Object>)
                                child.child("whoCame")
                                        .child(fieldInfo.getId()).child("val").getValue();

                        for (FieldsOptions fieldsOptions : fieldInfo.getOptions()) {

                            if (hashMap.containsKey(fieldsOptions.getOptionId())) {
                                allAutoCompleteTextView.get(i).setText(fieldsOptions.getName().trim());
                                selectedOptions.put(fieldsOptions.getfId(), fieldsOptions.getOptionId());

                                FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context,
                                        "GET", fieldsOptions, selectedOptions, "autofill", loginsp, fieldInfo);
                                fieldMappingAsync.setListner(HomeActivity.this);
                                fieldMappingAsync.execute();

                            }


                        }
                    } catch (ClassCastException e) {
                        allAutoCompleteTextView.get(i).setText(child.child("whoCame")
                                .child(fieldInfo.getId()).child("val").getValue().toString());
                    }

                }
            } else {

                if (child.hasChild("meet")) {
                    if (fieldInfo.getId().equals("host")) {
                        if (child.child("meet").hasChild("id")) {

                            FieldsOptions fieldsOptions = new FieldsOptions();
                            fieldsOptions.setfId("host");
                            fieldsOptions.setOptionId(child.child("meet").child("id").getValue().toString());
                            selectedOptions.put(fieldsOptions.getfId(), fieldsOptions.getOptionId());
                            FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context,
                                    "GET", fieldsOptions, selectedOptions, "autofill", loginsp, fieldInfo);
                            fieldMappingAsync.setListner(HomeActivity.this);
                            fieldMappingAsync.execute();

                            to_meet.setId(child.child("meet").child("id").getValue().toString());
                            to_meet.setMob(child.child("meet").child("mob").getValue().toString());
                        }
                        to_meet.setNm(child.child("meet").child("nm").getValue().toString());
                        ed_toMeet.setText(child.child("meet").child("nm").getValue().toString());
                    }
                    if (fieldInfo.getId().equals("p")) {

                        ed_purpose.setText(child.child("meet").child("p").getValue().toString());
                        visitor.getPurpose().setP(child.child("meet").child("p").getValue().toString());
                        if (child.child("meet").hasChild("pId")) {
                            visitor.getPurpose().setpId(child.child("meet").child("pId").getValue().toString());

                            FieldsOptions fieldsOptions = new FieldsOptions();
                            fieldsOptions.setfId("p");
                            fieldsOptions.setOptionId(child.child("meet").child("pId").getValue().toString());

                            selectedOptions.put(fieldsOptions.getfId(), fieldsOptions.getOptionId());

                            FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context,
                                    "GET", fieldsOptions, selectedOptions, "autofill", loginsp, fieldInfo);
                            fieldMappingAsync.setListner(HomeActivity.this);
                            fieldMappingAsync.execute();
                        }
                    }

                }
            }

        }


        for (int i = 0; i < allRadioGroup.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allRadioGroup.get(i).getTag();

            if (child.hasChild("meet")
                    && child.child("meet")
                    .hasChild(fieldInfo.getId().trim()) && (!fieldInfo.isReenter() || isAppointment || !accessType.equals(Global.VISITOR))) {


                HashMap<String, Object> hashMap = (HashMap<String, Object>)
                        child.child("meet")
                                .child(fieldInfo.getId()).child("val").getValue();


                for (FieldsOptions fieldsOptions : fieldInfo.getOptions()) {

                    if (hashMap.containsKey(fieldsOptions.getOptionId())) {
                        isRadioAutofilled = true;
                        allRadioGroup.get(i).check(allRadioGroup.get(i).getChildAt(getpositionOfRb(fieldsOptions.getName(), allRadioGroup.get(i))).getId());
                        selectedOptions.put(fieldsOptions.getfId(), fieldsOptions.getOptionId());

                    }

                }

            }

            if (child.hasChild("whoCame")
                    && child.child("whoCame")
                    .hasChild(fieldInfo.getId().trim()) && (!fieldInfo.isReenter() || isAppointment
                    || !accessType.equals(Global.VISITOR)) && !fieldInfo.isHide()) {


                HashMap<String, Object> hashMap = (HashMap<String, Object>)
                        child.child("whoCame")
                                .child(fieldInfo.getId()).child("val").getValue();

                for (FieldsOptions fieldsOptions : fieldInfo.getOptions()) {

                    if (hashMap.containsKey(fieldsOptions.getOptionId())) {
                        isRadioAutofilled = true;
                        allRadioGroup.get(i).check(allRadioGroup.get(i).getChildAt(getpositionOfRb(fieldsOptions.getName(), allRadioGroup.get(i))).getId());

                        // findViewById(j).setSelected(true);

                        selectedOptions.put(fieldsOptions.getfId(), fieldsOptions.getOptionId());

                    }


                }


            }


        }


        for (int i = 0; i < allImageview.size(); i++) {
            CustomFields fieldInfo = (CustomFields) allImageview.get(i).getTag();

            //written by Rajesh...
            if (!getImgNotSynced(fieldInfo, i, context))

            {
                if (child.hasChild("meet")
                        && child.child("meet")
                        .hasChild(fieldInfo.getId()) && (!fieldInfo.isReenter() || isAppointment || !accessType.equals(Global.VISITOR))) {

                    Glide.with(context.getApplicationContext()).load(child.child("meet")
                            .child(fieldInfo.getId()).child("val").getValue(String.class))
                            .override(200, 200)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.RESULT)
                            .placeholder((fieldInfo.getId().equals(Global.VISITOR_ID)) ? R.drawable.ic_id_card : R.drawable.ic_photo).
                            into(allImageview.get(i));

                    imgPath.set(i, child.child("meet")
                            .child(fieldInfo.getId()).child("val").getValue(String.class));

                }

                if (child.hasChild("whoCame")
                        && child.child("whoCame")
                        .hasChild(fieldInfo.getId()) && (!fieldInfo.isReenter() || isAppointment || !accessType.equals(Global.VISITOR))) {


                    String url = child.child("whoCame")
                            .child(fieldInfo.getId()).child("val").getValue(String.class);

                    Glide.with(context.getApplicationContext()).load(url)
                            .override(200, 200)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.RESULT)
                            .placeholder((fieldInfo.getId().equals(Global.VISITOR_ID)) ? R.drawable.ic_id_card : R.drawable.ic_photo).
                            into(allImageview.get(i));


                    imgPath.set(i, child.child("whoCame")
                            .child(fieldInfo.getId()).child("val").getValue(String.class));

                }

            }
        }


    }


    public void removeAllTheListners() {

        try {


            if (purposeListner != null) {
                purposeRef.removeEventListener(purposeListner);
            }

            if (fieldmeetListner != null) {
                fieldmeetRef.removeEventListener(fieldmeetListner);
            }

            if (fieldWhoCameRefListner != null) {
                fieldWhoCameRef.removeEventListener(fieldWhoCameRefListner);
            }

            if (toMeetListner != null) {
                toMeetRef.removeEventListener(toMeetListner);
            }

            if (deviceSettingsRefListner != null) {
                deviceSettingsRef.removeEventListener(deviceSettingsRefListner);
            }

            if (tokenDbRef != null && tokenDbRefListner != null) {
                tokenDbRef.removeEventListener(tokenDbRefListner);
            }
        } catch (Exception e) {

        }


    }


    private boolean getImgNotSynced(final CustomFields fieldInfo, final int i, Context context) {


        imagesDataSource.open();
        final String imglocalPath = imagesDataSource.getcustomImg(null, fieldInfo.getId(), ed_visitorMobile.getText().toString().trim());
        imagesDataSource.close();
        Glide.with(context.getApplicationContext()).load(imglocalPath).asBitmap().override(200, 200).fitCenter().placeholder(R.drawable.default_user_picture).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {


                allImageview.get(i).setImageBitmap(resource);


            }
        });
        if (imglocalPath != null) {
            imgPath.add(i, imglocalPath);
            return true;
        } else
            return false;

    }


    private void updatePurpose(String purposeSelected) {
        if (isPurposeOther(purposeSelected)) {


            selectedPurpose = purposeSelected;
            ed_purpose.setText(purposeSelected);
        } else {
            selectedPurpose = purposeSelected;
            ed_purpose.setText(purposeSelected);
        }
    }


    private void saveImg(String hiskey, String mob, String customKey, String photoPath,
                         String storageRef, ArrayList<String> urlList) {
        imagesDataSource.open();
        Images images = new Images();
        images.setRef(hiskey);
        images.setMob(mob);
        images.setCustomKey(customKey);
        images.setStoragePath(storageRef);
        images.setImgPath(photoPath);
        images.setPathRef(urlList);
        imagesDataSource.addImage(images);
        imagesDataSource.close();
    }


    public static void uploadImages(final Context context) {


        final ImagesDataSource imagesDataSource = new ImagesDataSource(context);
        final SharedPreferences loginsp = context.getSharedPreferences(versionx.selfentry.Util.Global.LOGIN_SP, MODE_PRIVATE);

        if (CommonMethods.isInternetWorking(context)) {
            try {
                imagesDataSource.close();
                imagesDataSource.open();
                imagesDataSource.getallData();
                final List<Images> imagesList = imagesDataSource.getAllImages();


                if ((imagesList.size() > 0 && imagesList.get(0).getImgPath() == null) ||
                        (imagesList.size() > 0 && (!new File(imagesList.get(0).getImgPath()).exists() ||
                                !new File(imagesList.get(0).getImgPath()).getAbsoluteFile().exists()))) {


                    imagesDataSource.deleteImages(imagesList.get(0).getId());
                    return;


                }
                imagesDataSource.close();

                if (imagesList.size() > 0 && imagesList.get(0).getImgPath() != null) {
                    imagesDataSource.open();

                    final ArrayList<String> refUrls = imagesDataSource.getAllRefUrls(imagesList.get(0).getId());
                    imagesDataSource.close();
                    final String s = imagesList.get(0).getStoragePath();

                    int j = s.indexOf('/', 1 + s.indexOf('/', 1 + s.indexOf('/')));
                    final String secondPart = s.substring(j + 1);


                    final String fileStr = imagesList.get(0).getImgPath();


                    if (new File(fileStr).exists() || new File(fileStr).getAbsoluteFile().exists()) {

                        Glide.with(context.getApplicationContext()).load(imagesList.get(0).getImgPath()).asBitmap().override(400, 500).fitCenter().
                                into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                        final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(secondPart);


                                        Uri file = Uri.fromFile(new File(fileStr));
                                        UploadTask task1;


                                        task1 = storageReference.putFile(file);


                                        task1.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                                                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                    @Override
                                                    public void onSuccess(Uri uri) {


                                                        final Map<String, Object> userUpdates = new HashMap<String, Object>();

                                                        for (int i = 0; i < refUrls.size(); i++) {


                                                            if (refUrls.get(i) != null) {

                                                                int k = refUrls.get(i).indexOf('/', 1 + refUrls.get(i).indexOf('/', 1 + refUrls.get(i).indexOf('/')));
                                                                final String ref2 = refUrls.get(i).substring(k + 1);

                                                                userUpdates.put("" + ref2, "" + uri);

                                                            }
                                                        }


                                                        {


                                                            PersistentDatabase.getDatabase().getReference().updateChildren(userUpdates).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception e) {

                                                                    Crashlytics.logException(new Exception("image url not updated " + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "_"
                                                                            + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "___" + e.toString() + "----------------" + " url is " + storageReference.getDownloadUrl()));


                                                                }
                                                            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void aVoid) {

                                                                    imagesDataSource.open();
                                                                    imagesDataSource.deleteImages(imagesList.get(0).getId());


                                                                    if (!imagesDataSource.isMoreRef(imagesList.get(0).getImgPath())) {
                                                                        File WhoCameDirectory = new File(imagesList.get(0).getImgPath());
                                                                        if (WhoCameDirectory.getAbsoluteFile().exists()) {
                                                                            WhoCameDirectory.getAbsoluteFile().delete();
                                                                        }
                                                                    }


                                                                }
                                                            });// multipath


                                                        }

                                                    }
                                                });


                                            }
                                        });
                                    }

                                    @Override
                                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                        super.onLoadFailed(e, errorDrawable);

                                        Crashlytics.logException(new Exception("Glide upload failed " + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + " , " + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "" + e.toString()));
                                    }


                                });
                        //   }

                    } else {


                        imagesDataSource.open();
                        imagesDataSource.deleteImages(imagesList.get(0).getId());
                        imagesDataSource.close();


                    }
                }
            } catch (Exception e) {

                Crashlytics.logException(new Exception(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + " " + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")
                        + e.toString()));
            }
        }
    }


    public static void uploadAllImages(final Context context) {


        final ImagesDataSource imagesDataSource = new ImagesDataSource(context);
        final SharedPreferences loginsp = context.getSharedPreferences(versionx.selfentry.Util.Global.LOGIN_SP, MODE_PRIVATE);
        final DatabaseReference nouploadRef = PersistentDatabase.getDatabase().getReference("temp").child("noupload").child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                .child(loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")).child(loginsp.getString(versionx.selfentry.Util.Global.UUID, ""));


        if (CommonMethods.isInternetWorking(context)) {
            try {
                imagesDataSource.close();
                imagesDataSource.open();
                imagesDataSource.getallData();
                final List<Images> imagesList = imagesDataSource.getAllImages();


                if ((imagesList.size() > 0 && imagesList.get(0).getImgPath() == null) ||
                        (imagesList.size() > 0 && (!new File(imagesList.get(0).getImgPath()).exists() ||
                                !new File(imagesList.get(0).getImgPath()).getAbsoluteFile().exists()))) {


                    imagesDataSource.deleteImages(imagesList.get(0).getId());
                    uploadImages(context);
                    return;


                }
                imagesDataSource.close();

                if (imagesList.size() > 0 && imagesList.get(0).getImgPath() != null) {
                    imagesDataSource.open();

                    final ArrayList<String> refUrls = imagesDataSource.getAllRefUrls(imagesList.get(0).getId());
                    imagesDataSource.close();
                    final String s = imagesList.get(0).getStoragePath();

                    int j = s.indexOf('/', 1 + s.indexOf('/', 1 + s.indexOf('/')));
                    final String secondPart = s.substring(j + 1);


                    final String fileStr = imagesList.get(0).getImgPath();


                    if (new File(fileStr).exists() || new File(fileStr).getAbsoluteFile().exists()) {

                        Glide.with(context.getApplicationContext()).load(imagesList.get(0).getImgPath()).asBitmap().override(400, 500).fitCenter().
                                into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                        final StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(secondPart);


                                        Uri file = Uri.fromFile(new File(fileStr));
                                        UploadTask task1;


                                        task1 = storageReference.putFile(file);


                                        task1.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                                                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                    @Override
                                                    public void onSuccess(Uri uri) {


                                                        final Map<String, Object> userUpdates = new HashMap<String, Object>();

                                                        for (int i = 0; i < refUrls.size(); i++) {


                                                            if (refUrls.get(i) != null) {

                                                                int k = refUrls.get(i).indexOf('/', 1 + refUrls.get(i).indexOf('/', 1 + refUrls.get(i).indexOf('/')));
                                                                final String ref2 = refUrls.get(i).substring(k + 1);

                                                                userUpdates.put("" + ref2, "" + uri);

                                                            }
                                                        }


                                                        PersistentDatabase.getDatabase().getReference().updateChildren(userUpdates).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception e) {

                                                                imagesDataSource.open();
                                                                imagesDataSource.deleteImages(imagesList.get(0).getId());
                                                                uploadAllImages(context);

                                                                Crashlytics.logException(new Exception("image url not updated " + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "_"
                                                                        + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "___" + e.toString() + "----------------" + " url is " + storageReference.getDownloadUrl()));


                                                            }
                                                        }).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {

                                                                imagesDataSource.open();
                                                                imagesDataSource.deleteImages(imagesList.get(0).getId());


                                                                if (!imagesDataSource.isMoreRef(imagesList.get(0).getImgPath())) {
                                                                    File WhoCameDirectory = new File(imagesList.get(0).getImgPath());
                                                                    if (WhoCameDirectory.getAbsoluteFile().exists()) {
                                                                        WhoCameDirectory.getAbsoluteFile().delete();
                                                                    }
                                                                }


                                                                uploadAllImages(context);

                                                            }
                                                        });// multipath


                                                    }
                                                });


                                            }
                                        });
                                    }

                                    @Override
                                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                        super.onLoadFailed(e, errorDrawable);

                                        Crashlytics.logException(new Exception("Glide upload failed " + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + " , " + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "" + e.toString()));
                                    }


                                });
                        //   }

                    } else {


                        imagesDataSource.open();
                        imagesDataSource.deleteImages(imagesList.get(0).getId());
                        imagesDataSource.close();
                        uploadImages(context);


                    }
                }
            } catch (Exception e) {

                Crashlytics.logException(new Exception(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + " " + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")
                        + e.toString()));
            }
        }
    }


    private boolean detectFace(Bitmap image) {

        FaceDetector faceDetector = new FaceDetector.Builder(HomeActivity.this)
                .setTrackingEnabled(false).build();

        if (!faceDetector.isOperational()) {
            new AlertDialog.Builder(HomeActivity.this).setMessage("Could not set up the face detector!").show();
            return false;
        }

        Frame frame = new Frame.Builder().setBitmap(image).build();
        SparseArray<Face> faces = faceDetector.detect(frame);
        faceDetector.release();

        return faces.size() > 0;
    }


    public void setPrinterservice() {
        try {
            mHandler = new MHandler(this);
            DrawerService.addHandler(mHandler);
            Intent intent = new Intent(this, DrawerService.class);
            startService(intent);


            //initBroadcast();
        } catch (Exception e) {

            Crashlytics.logException(e);

            //  CommonMethods.writeToLogFile("threading " + e.toString());
        }
    }

    @Override
    public void OnFieldsLoaded(List<CustomFields> customFields) {

        setDBCustomFieldsList(customFields);

        FieldOptionsAsync fieldOptionsAsync = new FieldOptionsAsync(context, "GETALL", null, null);
        fieldOptionsAsync.setListner(this);
        fieldOptionsAsync.execute();


        toMeetadapter = new AutoCompeleteArrayAdapter(context, R.layout.dropdown_text_view, toMeetList);
        ed_toMeet.setAdapter(toMeetadapter);
        ed_toMeet.setThreshold(3);


        getPurpose(HomeActivity.this, loginsp.getString(Global.PURPOSE_LBL, "all"));
        getMobLength();

        //    getCustomFields(HomeActivity.this);
        getToMeetAccess(HomeActivity.this, loginsp.getString(Global.TO_MEET_LBL, "toMeet"));


        ed_toMeet.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

                    if (keyCode == KeyEvent.KEYCODE_TAB) {
                        ed_toMeet.dismissDropDown();
                        ed_toMeet.requestFocus();

                    }

                }
                return false;
            }


        });

        ed_purpose.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

                    if (keyCode == KeyEvent.KEYCODE_TAB) {
                        ed_purpose.dismissDropDown();
                        ed_purpose.requestFocus();
                    }

                }
                return false;
            }
        });

        ed_visitorMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (ed_visitorMobile.getText().toString().trim().length() == 10) {

                    if (calling == versionx.selfentry.Util.Global.CALL_DIALED)
                        ed_visitorMobile.setEnabled(false);
                    if (s.toString().trim().length() == 10) {
                        isMobileEntered = true;
                        isAppointmnetChecked = true;
                        setDataFromMobileNumber(HomeActivity.this);
                        callTime = System.currentTimeMillis();
                        hideKB(ed_visitorMobile);
                    } else {
                        callTime = 0;
                        accessType = versionx.selfentry.Util.Global.VISITOR;
                        type = "visitor";
                    }
                }
            }
        });


        ed_toMeet.setOnItemClickListener(new AdapterView.OnItemClickListener()

        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //   selectedToMeet=null;


                if (toMeetList.get(position).getAptNo() != null) {
                    ed_toMeet.setText(toMeetList.get(position).getAptNo() + " - " + toMeetList.get(position).getNm());
                } else {
                    ed_toMeet.setText(toMeetList.get(position).getNm());
                }

                selectedToMeet = toMeetList.get(position);


                if (selectedToMeet.isDndMode() && CommonMethods.getTodaysDate().getTimeInMillis() <= selectedToMeet.getvTd()
                        && (accessType.equalsIgnoreCase(versionx.selfentry.Util.Global.VISITOR))) {

                    DndDialog();

                } else {


                    final DatabaseReference dbRef = PersistentDatabase.getDatabase().getReference(versionx.selfentry.Util.Global.CAST_NODE + "/" +
                            toMeetList.get(position).getMob().substring(0, 4) + "/" +
                            toMeetList.get(position).getMob() +
                            "/ntfy");
                    dbRef.keepSynced(true);
                    dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if (dataSnapshot.exists() && selectedToMeet != null && selectedToMeet.getId() != null
                                    && !ed_toMeet.getText().toString().trim().isEmpty()) {

                                // selectedToMeet.setToken(dataSnapshot.getValue(String.class));

                                FCMTokens FCMTokens = new FCMTokens(dataSnapshot, context);
                                selectedToMeet.setToken(FCMTokens.getTokenId());
                                selectedToMeet.setAllTokens(FCMTokens.getAllTokens());


                                if (dataSnapshot.hasChild("mApp") && loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_CONFIRM_REQUIRED, false)) {
                                    selectedToMeet.setManualAllowed(dataSnapshot.child("mApp").getValue(Boolean.class));
                                }
                                if (dataSnapshot.hasChild("vi")) {
                                    selectedToMeet.setNotiReq(dataSnapshot.child("vi").getValue(Boolean.class));
                                }
                                if (dataSnapshot.hasChild("viC") && loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_CONFIRM_REQUIRED, false)) {
                                    selectedToMeet.setConfirmReq(dataSnapshot.child("viC").getValue(Boolean.class));
                                }

                            }


                            to_meet.reset();
                            // dbRef.removeEventListener(valueEventListener);


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                    //   setSaveBtn();


                }
            }
        });


        ed_toMeet.addTextChangedListener(new

                                                 TextWatcher() {
                                                     @Override
                                                     public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                     }

                                                     @Override
                                                     public void onTextChanged(CharSequence s, int start, int before, int count) {

                                                     }

                                                     @Override
                                                     public void afterTextChanged(Editable s) {


                                                         if (ed_toMeet.isPerformingCompletion()) {
                                                             // An item has been selected from the list. Ignore.
                                                             return;
                                                         }

                                                         selectedToMeet = new ToMeet();
                                                         if (to_meet.getId() != null && ed_toMeet.getText().length() > 0) {
                                                             selectedToMeet.setMob(to_meet.getMob());
                                                             selectedToMeet.setId(to_meet.getId());
                                                             selectedToMeet.setNm(to_meet.getNm());
                                                         }


                                                         //  setSaveBtn();
                                                         if (tempList.indexOf(to_meet) != -1) {

                                                             selectedToMeet = tempList.get(tempList.indexOf(to_meet));
                                                             selectedToMeet.setQ(tempList.get(tempList.indexOf(to_meet)).isQ());
                                                             to_meet.reset();

                                                         }

                                                         if (ed_toMeet.getText().length() > 0 && selectedToMeet != null
                                                                 && selectedToMeet.getId() != null) {
                                                             tokenDbRef = PersistentDatabase.getDatabase().getReference(versionx.selfentry.Util.Global.CAST_NODE + "/" +
                                                                     selectedToMeet.getMob().substring(0, 4) + "/" +
                                                                     selectedToMeet.getMob()
                                                                     + "/ntfy");
                                                             tokenDbRef.keepSynced(true);
                                                             tokenDbRefListner = tokenDbRef.addValueEventListener(new ValueEventListener() {
                                                                 @Override
                                                                 public void onDataChange(DataSnapshot dataSnapshot) {

                                                                     tokenDbRef.removeEventListener(this);
                                                                     if (dataSnapshot.exists() && selectedToMeet != null
                                                                             && selectedToMeet.getId() != null && !ed_toMeet.getText().toString().trim().isEmpty()) {
                                                                         //    selectedToMeet.setToken(dataSnapshot.getValue(String.class));

                                                                         FCMTokens FCMTokens = new FCMTokens(dataSnapshot, context);
                                                                         selectedToMeet.setToken(FCMTokens.getTokenId());
                                                                         selectedToMeet.setAllTokens(FCMTokens.getAllTokens());

                                                                         if (dataSnapshot.hasChild("mApp") && loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_CONFIRM_REQUIRED, false)) {
                                                                             selectedToMeet.setManualAllowed(dataSnapshot.child("mApp").getValue(Boolean.class));
                                                                         }
                                                                         if (dataSnapshot.hasChild("vi")) {
                                                                             selectedToMeet.setNotiReq(dataSnapshot.child("vi").getValue(Boolean.class));
                                                                         }
                                                                         if (dataSnapshot.hasChild("viC") && loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_CONFIRM_REQUIRED, false)) {
                                                                             selectedToMeet.setConfirmReq(dataSnapshot.child("viC").getValue(Boolean.class));
                                                                         }

                                                                     }
                                                                     to_meet.reset();
                                                                 }

                                                                 @Override
                                                                 public void onCancelled(DatabaseError databaseError) {

                                                                 }
                                                             });

                                                         }
                                                     }
                                                 });


    }

    @Override
    public void OnOptionsLoaded(List<FieldsOptions> options) {

        setAutocompleteOptions(options);

    }


    ///// on dropdown item selected
    @Override
    public void OnMappingLoaded(List<FieldMapping> mappedOptions, String fId, String flag) {


        for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
            CustomFields fields = (CustomFields) allAutoCompleteTextView.get(i).getTag();


            if (mappedOptions.size() > 0) {

                for (FieldMapping fieldMapping : mappedOptions) {

                    selectedOptions.put(fieldMapping.getfId(), fieldMapping.getOptionId());
                    if (fieldMapping.getDepFid().equals(fields.getId())) {
                        if (!fieldMapping.getDepOptions().equals("hide")) {
                            if (!fields.isDsbl()) {
                                fields.getView().setVisibility(View.VISIBLE);
                            }
                            allAutoCompleteTextView.get(i).setEnabled(true);
                            fields.setDepReq(fieldMapping.isReq());
                            allAutoCompleteTextView.get(i).setTag(fields);

                            if (fieldMapping.getDepOptions().equals("all") || fieldMapping.getDepOptions().equals("show")) {


                                if (flag.equals("onselect"))
                                    allAutoCompleteTextView.get(i).setText("");
                                if (fieldMapping.getMod() != null) {
                                    if (fieldMapping.getMod().equals("purpose")) {
                                        getPurpose(context, fieldMapping.getLbl() == null ? "all" : fieldMapping.getLbl());
                                        setPurposeAdapter(context);
                                        if (flag.equals("onselect"))
                                            ed_purpose.setText("");
                                    } else if (fieldMapping.getMod().equals("access")) {
                                        getToMeetAccess(context, fieldMapping.getLbl() == null ? "toMeet" : fieldMapping.getLbl());
                                        if (flag.equals("onselect"))
                                            ed_toMeet.setText("");
                                    } else {
                                        setModuleAdapter(context, fieldMapping, allAutoCompleteTextView.get(i));
                                    }


                                } else {
                                    if (fields.getOptions() != null && fields.getOptions().size() == 1) {
                                        allAutoCompleteTextView.get(i).setText(fields.getOptions().get(0).getName());
                                    }
                                    FieldsAutoCompleteAdapter adapter1 = new FieldsAutoCompleteAdapter(context, android.R.layout.simple_dropdown_item_1line, fields.getOptions());
                                    allAutoCompleteTextView.get(i).setAdapter(adapter1);
                                }


                            } else {
                                if (fieldMapping.getMod() != null) {
                                    if (fieldMapping.getMod().equals("purpose")) {
                                        getPurpose(context, fieldMapping.getLbl());
                                        setPurposeAdapter(context);
                                        if (flag.equals("onselect"))
                                            ed_purpose.setText("");
                                    } else if (fieldMapping.getMod().equals("access")) {
                                        getToMeetAccess(context, fieldMapping.getLbl());
                                        if (flag.equals("onselect"))
                                            ed_toMeet.setText("");
                                    } else {
                                        setModuleAdapter(context, fieldMapping, allAutoCompleteTextView.get(i));
                                    }

                                }


                                FieldOptionsAsync fieldOptionsAsync = new FieldOptionsAsync(context, "GETDEP", fieldMapping.getDepOptions(), null);
                                fieldOptionsAsync.setListner(this);
                                fieldOptionsAsync.execute();
                                //break;

                            }
                        } else {
                            if (flag.equals("onselect")) {
                                allAutoCompleteTextView.get(i).setText("");
                            }
                            if (fields.isDsbl()) {
                                allAutoCompleteTextView.get(i).setEnabled(false);
                            } else {
                                fields.getView().setVisibility(View.GONE);
                            }

                        }


                    } else if (fieldMapping.getAllDepIds() != null && fieldMapping.getAllDepIds().contains(fields.getId())) {
                        if (flag.equals("onselect")) {
                            allAutoCompleteTextView.get(i).setText("");
                        }
                        if (fields.isDsbl()) {
                            allAutoCompleteTextView.get(i).setEnabled(false);
                        } else {
                            fields.getView().setVisibility(View.GONE);
                        }
                    }

                }
            }

        }


        for (int i = 0; i < allRadioGroup.size(); i++) {
            CustomFields fields = (CustomFields) allRadioGroup.get(i).getTag();


            if (mappedOptions.size() > 0) {

                for (FieldMapping fieldMapping : mappedOptions) {

                    selectedOptions.put(fieldMapping.getfId(), fieldMapping.getOptionId());
                    if (fieldMapping.getDepFid().equals(fields.getId())) {
                        if (!fieldMapping.getDepOptions().equals("hide")) {
                            if (!fields.isDsbl()) {
                                fields.getView().setVisibility(View.VISIBLE);
                            }
                            allRadioGroup.get(i).setEnabled(true);
                            fields.setDepReq(fieldMapping.isReq());
                            allRadioGroup.get(i).setTag(fields);

                            if (fieldMapping.getDepOptions().equals("all") ||
                                    fieldMapping.getDepOptions().equals("show")) {


                                ArrayList<FieldsOptions> options = fields.getOptions();


                                for (int j = 0; j < options.size(); j++) {

                                    if (getpositionOfRb(options.get(j).getName(), allRadioGroup.get(i)) == -1) {
                                        RadioButton radioButton = new RadioButton(context);
                                        radioButton.setTag(fields);
                                        //  radioButton.setId(j);
                                        radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_view_hint_size));
                                        radioButton.setText(options.get(j).getName());
                                        allRadioGroup.get(i).addView(radioButton);
                                        allradiobutton.add(radioButton);

                                    }
                                }


                            } else {


                                FieldOptionsAsync fieldOptionsAsync = new FieldOptionsAsync(context, "GETDEP", fieldMapping.getDepOptions(), null);
                                fieldOptionsAsync.setListner(this);
                                fieldOptionsAsync.execute();
                                //break;

                            }
                        } else {
                            if (flag.equals("onselect")) {
                                allRadioGroup.get(i).clearCheck();
                            }
                            if (fields.isDsbl()) {
                                allRadioGroup.get(i).setEnabled(false);
                            } else {
                                fields.getView().setVisibility(View.GONE);
                            }

                        }


                    } else if (fieldMapping.getAllDepIds() != null && fieldMapping.getAllDepIds().contains(fields.getId())) {
                        if (flag.equals("onselect")) {
                            allRadioGroup.get(i).clearCheck();
                        }
                        if (fields.isDsbl()) {
                            allRadioGroup.get(i).setEnabled(false);
                        } else {
                            fields.getView().setVisibility(View.GONE);
                        }
                    }

                }
            }

        }


        for (int i = 0; i < allEds.size(); i++)

        {
            CustomFields fields = (CustomFields) allEds.get(i).getTag();
            if (mappedOptions.size() > 0) {
                for (FieldMapping fieldMapping : mappedOptions) {
                    if (fieldMapping.getDepFid().equals(fields.getId())) {

                        fields.setDepReq(fieldMapping.isReq());
                        allEds.get(i).setTag(fields);


                        if (!fieldMapping.getDepOptions().equals("hide")) {
                            if (fieldMapping.getDepOptions().equals("show")) {
                                if (!fields.isDsbl()) {
                                    fields.getView().setVisibility(View.VISIBLE);
                                }
                                allEds.get(i).setEnabled(true);

                                if (flag.equals("onselect"))
                                    allEds.get(i).setText("");

                            }
                        } else {
                            if (fields.isDsbl()) {
                                allEds.get(i).setEnabled(false);
                            } else {
                                fields.getView().setVisibility(View.GONE);
                            }
                            allEds.get(i).setText("");
                        }
                    } else if (fieldMapping.getAllDepIds() != null && fieldMapping.getAllDepIds().contains(fields.getId())) {
                        if (fields.isDsbl()) {
                            allEds.get(i).setEnabled(false);
                        } else {
                            fields.getView().setVisibility(View.GONE);

                        }
                        allEds.get(i).setText("");
                    }
                }
            }

        }


        for (int i = 0; i < allImageview.size(); i++)

        {
            CustomFields fields = (CustomFields) allImageview.get(i).getTag();


            if (mappedOptions.size() > 0) {
                for (FieldMapping fieldMapping : mappedOptions) {
                    if (fieldMapping.getDepFid().equals(fields.getId())) {

                        fields.setDepReq(fieldMapping.isReq());
                        allImageview.get(i).setTag(fields);

                        if (!fieldMapping.getDepOptions().equals("hide")) {
                            if (fieldMapping.getDepOptions().equals("show")) {
                                if (!fields.isDsbl()) {
                                    allImageview.get(i).setVisibility(View.VISIBLE);
                                    imgText.get(i).setVisibility(View.VISIBLE);
                                    fields.getView().setVisibility(View.VISIBLE);
                                }
                                allImageview.get(i).setEnabled(true);


                            }
                        } else {

                            if (fields.getId().equals(Global.VISITOR_ID))
                                allImageview.get(i).setImageResource(R.drawable.ic_id_card);
                            else
                                allImageview.get(i).setImageResource(R.drawable.ic_photo);
                            if (fields.isDsbl()) {
                                allImageview.get(i).setEnabled(false);
                            } else {
                                allImageview.get(i).setVisibility(View.GONE);
                                imgText.get(i).setVisibility(View.GONE);

                                fields.getView().setVisibility(View.GONE);

                            }
                            imgPath.set(i, null);


                        }


                    } else if (fieldMapping.getAllDepIds() != null && fieldMapping.getAllDepIds().contains(fields.getId())) {
                        if (fields.getId().equals(Global.VISITOR_ID))
                            allImageview.get(i).setImageResource(R.drawable.ic_id_card);
                        else
                            allImageview.get(i).setImageResource(R.drawable.ic_photo);
                        if (fields.isDsbl()) {
                            allImageview.get(i).setEnabled(false);
                        } else {
                            allImageview.get(i).setVisibility(View.GONE);
                            imgText.get(i).setVisibility(View.GONE);
                            fields.getView().setVisibility(View.GONE);
                        }

                        imgPath.set(i, null);
                        fields.getView().setVisibility(View.GONE);

                    }
                }
            }
        }


    }


    ////  items binded to dependent field
    @Override
    public void OnDepOptionLoaded(List<FieldsOptions> depOptions) {


        for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
            CustomFields fields = (CustomFields) allAutoCompleteTextView.get(i).getTag();
            for (FieldsOptions options : depOptions) {
                if (fields.getId().equals(options.getfId())) {
                    // allAutoCompleteTextView.get(i).setText("");
                    FieldsAutoCompleteAdapter adapter1 = new FieldsAutoCompleteAdapter(context, android.R.layout.simple_dropdown_item_1line, new ArrayList<FieldsOptions>(depOptions));
                    allAutoCompleteTextView.get(i).setAdapter(adapter1);

                }
            }
        }

    }


    private void setAutocompleteOptions(final List<FieldsOptions> options) {


        for (int i = 0; i < allAutoCompleteTextView.size(); i++) {
            final ArrayList<FieldsOptions> opt = new ArrayList<>();
            ArrayList<String> opts = new ArrayList<>();
            final CustomFields fields = (CustomFields) allAutoCompleteTextView.get(i).getTag();
            if (!fields.getId().equals("p") && !fields.getId().equals("host")) {
                for (int j = 0; j < options.size(); j++) {
                    if (fields.getId().equals(options.get(j).getfId())) {
                        opts.add(options.get(j).getName());
                        FieldsOptions fieldsOptions = new FieldsOptions();
                        fieldsOptions.setfId(options.get(j).getfId());
                        fieldsOptions.setOptionId(options.get(j).getOptionId());
                        fieldsOptions.setName(options.get(j).getName());
                        fieldsOptions.setTyp(options.get(j).getTyp());
                        opt.add(fieldsOptions);
                    }
                }
                fields.setOptions(opt);
                allAutoCompleteTextView.get(i).setTag(fields);

                FieldsAutoCompleteAdapter adapter = new FieldsAutoCompleteAdapter(context, android.R.layout.simple_dropdown_item_1line, opt);
                allAutoCompleteTextView.get(i).setAdapter(adapter);
                allAutoCompleteTextView.get(i).setOnItemClickListener(new AutoCompleteTextViewClickListener(allAutoCompleteTextView.get(i), new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        CustomFields customFields = (CustomFields) view.getTag();

                        FieldsOptions fieldsOptions = (FieldsOptions) parent.getItemAtPosition(position);
                        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) view;
                        autoCompleteTextView.setText(fieldsOptions.getName());

                        FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context, "GET",
                                fieldsOptions, selectedOptions, "onselect", loginsp, customFields);
                        fieldMappingAsync.setListner(HomeActivity.this);
                        fieldMappingAsync.execute();

                    }


                }));


            }


        }


        for (int i = 0; i < allRadioGroup.size(); i++) {

            final ArrayList<FieldsOptions> opt = new ArrayList<>();
            ArrayList<String> opts = new ArrayList<>();
            final CustomFields fields = (CustomFields) allRadioGroup.get(i).getTag();

            for (int j = 0; j < options.size(); j++) {
                if (fields.getId().equals(options.get(j).getfId())) {
                    opts.add(options.get(j).getName());
                    FieldsOptions fieldsOptions = new FieldsOptions();
                    fieldsOptions.setfId(options.get(j).getfId());
                    fieldsOptions.setOptionId(options.get(j).getOptionId());
                    fieldsOptions.setName(options.get(j).getName());
                    opt.add(fieldsOptions);

                    RadioButton radioButton = new RadioButton(context);

                    radioButton.setTag(fields);
                    radioButton.setTextColor(Color.parseColor("#606060"));
                    radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_view_hint_size));
                    radioButton.setText(options.get(j).getName());


                    allRadioGroup.get(i).addView(radioButton);
                    allradiobutton.add(radioButton);

                }

            }
            fields.setOptions(opt);

            allRadioGroup.get(i).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {


                    RadioButton radioButton = (RadioButton) findViewById(checkedId);
                    if (radioButton != null && radioButton.isChecked()) {
                        CustomFields customFields = (CustomFields) group.getTag();



                            //radioButton.setChecked(true);
                            FieldsOptions fieldsOptions = null;
                            for (FieldsOptions opt : customFields.getOptions()) {
                                if (opt.getName().equals(radioButton.getText().toString())) {
                                    fieldsOptions = opt;
                                    FieldMappingAsync fieldMappingAsync = new FieldMappingAsync(context, "GET",
                                            fieldsOptions, selectedOptions, "onselect", loginsp, customFields);
                                    fieldMappingAsync.setListner(HomeActivity.this);
                                    fieldMappingAsync.execute();
                                    break;
                                }
                            }




                    }
                }
            });


        }


    }


    private void setDBCustomFieldsList(List<CustomFields> fields) {
        //   try {
        if (llForCustomFields_Visitor != null)
            llForCustomFields_Visitor.removeAllViews();

        if (llForCustomFields_ToMeet != null)
            llForCustomFields_ToMeet.removeAllViews();


        for (int i = 0; i < fields.size(); i++) {
            if (!fields.get(i).getId().equals("sep")) {
                final CustomFields fieldInfo = fields.get(i);
                if (fieldInfo.getType().equalsIgnoreCase(Global.TEXTBOX)
                        || fieldInfo.getType().equalsIgnoreCase(Global.NUMBER)
                        || fieldInfo.getType().equals(Global.TEXTAREA)) {

                    View root = LayoutInflater.from(context).inflate(R.layout.float_label_edit_text, null, false);
                    TextInputLayout textInputLayout = (TextInputLayout) root.findViewById(R.id.float_label);
                    ImageView clrImg = (ImageView) root.findViewById(R.id.iv_clear_ed);
                    fieldInfo.setClrImg(clrImg);
                    fieldInfo.setView(root);
                    textInputLayout.setHint(fieldInfo.getName());
                    final EditText editText = (EditText) root.findViewById(R.id.editText);
                    editText.setTag(fieldInfo);
                    editText.setText(fieldInfo.getVal());

                    //Rajesh...if custom field key is vehicle...we're adding the captital letters input filter


                    if (fieldInfo.getId().equalsIgnoreCase(Global.VEHICLE)) {
                        editText.setFilters(new InputFilter[]{CommonMethods.getCapsTextNumFilter()});
                    }

                    if (fieldInfo.getType().equalsIgnoreCase(Global.NUMBER)) {
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        if (fieldInfo.getName().equalsIgnoreCase(Global.COUNT_FIELD_ID)) {
                            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});

                        } else {
                            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        }
                    }
                    if (fieldInfo.getType().equalsIgnoreCase(Global.TEXTBOX)) {
                        editText.setSingleLine(true);
                        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    } else if (fieldInfo.getType().equals(Global.TEXTAREA)) {
                        editText.setSingleLine(false);
                        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        editText.setGravity(Gravity.TOP | Gravity.LEFT);
                        editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                        editText.setLines(3);
                        editText.setMaxLines(5);
                    }
                    if (fieldInfo.getId().equals("mob")) {
                        ed_visitorMobile = editText;
                    }

                    if (fieldInfo.getId().equals("nm")) {
                        ed_visitorName = editText;
                    }

                    allEds.add(editText);

                    if (fieldInfo.getFieldGrp().equals(Global.SCREEN_TYPE_WHOCAME))
                        llForCustomFields_Visitor.addView(root);
                    else
                        llForCustomFields_ToMeet.addView(root);

                    if (fieldInfo.isHide() || !fieldInfo.isDsbl()) {
                        root.setVisibility(View.GONE);
                       /* textInputLayout.setVisibility(View.GONE);
                        editText.setVisibility(View.GONE);
                        view_float_label.setVisibility(View.GONE);*/
                    }
                    if (fieldInfo.isDep()) {
                        editText.setEnabled(false);
                    }

                    clrImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (editText.isEnabled())
                                editText.setText("");
                        }
                    });
                }


                if (fieldInfo.getType().equalsIgnoreCase(Global.DROPDOWN)) {

                    View root = LayoutInflater.from(context).inflate(R.layout.float_label_autocompletetext, null, false);
                    TextInputLayout textInputLayout = (TextInputLayout) root.findViewById(R.id.autocomplete_floatLable);
                    ImageView clrImg = (ImageView) root.findViewById(R.id.iv_clear_ed);
                    fieldInfo.setClrImg(clrImg);
                    fieldInfo.setView(root);
                    textInputLayout.setHint(fieldInfo.getName());
                    final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) root.findViewById(R.id.autocompletetxt);


                    if (fieldInfo.isSrch()) {
                        autoCompleteTextView.setFocusable(true);
                        autoCompleteTextView.setCursorVisible(true);
                        autoCompleteTextView.setThreshold(0);

                    } else {
                        autoCompleteTextView.setFocusable(false);
                        autoCompleteTextView.setCursorVisible(false);
                        autoCompleteTextView.setThreshold(256);

                    }


                    autoCompleteTextView.setText(fieldInfo.getVal());
                    autoCompleteTextView.setTag(fieldInfo);
                    allAutoCompleteTextView.add(autoCompleteTextView);


                    clrImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (autoCompleteTextView.isEnabled())
                                autoCompleteTextView.setText("");
                        }
                    });


                    autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (autoCompleteTextView.isEnabled()) {
                                autoCompleteTextView.showDropDown();
                                autoCompleteTextView.requestFocus();
                            }
                            return false;

                        }
                    });

                    autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus && autoCompleteTextView.isEnabled()) {
                                autoCompleteTextView.showDropDown();

                            }
                        }
                    });


                    if (fieldInfo.getFieldGrp().equals(Global.SCREEN_TYPE_WHOCAME))
                        llForCustomFields_Visitor.addView(root);
                    else
                        llForCustomFields_ToMeet.addView(root);
                    if (fieldInfo.isHide() || !fieldInfo.isDsbl()) {

                        root.setVisibility(View.GONE);

                    }

                    if (fieldInfo.isDep()) {
                        autoCompleteTextView.setEnabled(false);
                    }

                    if (fieldInfo.getId().equals("host")) {
                        ed_toMeet = autoCompleteTextView;
                    }

                    if (fieldInfo.getId().equals("p")) {
                        ed_purpose = autoCompleteTextView;
                    }


                }


                if (fieldInfo.getType().equalsIgnoreCase(Global.RADIOBUTTON)) {
                    View root = LayoutInflater.from(context).inflate(R.layout.ll_for_radio_group, null, false);
                    RadioGroup radioGroup = root.findViewById(R.id.radioGrp);
                    TextViewOpenSans checkText = root.findViewById(R.id.radio_txt);
                    fieldInfo.setView(root);
                    checkText.setText(fieldInfo.getName());
                    radioButtonData.add(checkText);

                    radioGroup.setTag(fieldInfo);
                    allRadioGroup.add(radioGroup);


                    if (fieldInfo.getFieldGrp().equals(Global.SCREEN_TYPE_WHOCAME))
                        llForCustomFields_Visitor.addView(root);
                    else
                        llForCustomFields_ToMeet.addView(root);

                    if (fieldInfo.isHide() || !fieldInfo.isDsbl()) {

                        root.setVisibility(View.GONE);

                    }

                    if (fieldInfo.isDep()) {
                        radioGroup.setEnabled(false);
                    }


                }


                if (fieldInfo.getType().equalsIgnoreCase(Global.SEPERATION)) {

                    View root = LayoutInflater.from(context).inflate(R.layout.separation_layout, null, false);
                    TextViewOpenSans imgtext = (TextViewOpenSans) root.findViewById(R.id.tv_sep);
                    fieldInfo.setView(root);

                    imgtext.setText(fieldInfo.getName());


                    if (fieldInfo.getFieldGrp().equals(Global.SCREEN_TYPE_WHOCAME))
                        llForCustomFields_Visitor.addView(root);
                    else
                        llForCustomFields_ToMeet.addView(root);

                    if (fieldInfo.isHide() || !fieldInfo.isDsbl()) {
                        imgtext.setVisibility(View.GONE);

                    }
                }


                if (fieldInfo.getType().equalsIgnoreCase(Global.IMG)) {

                    if (!fieldInfo.getId().equals("img")) {

                        View root = LayoutInflater.from(context).inflate(R.layout.customimg_layout, null, false);
                        TextViewOpenSans imgtext = (TextViewOpenSans) root.findViewById(R.id.tv_imgtxt);
                        ImageView imageView = (ImageView) root.findViewById(R.id.iv_img);
                        fieldInfo.setView(root);

                        imageView.setTag(fieldInfo);
                        imageView.setId(count);
                        count++;
                        if (fieldInfo.getId().equals(Global.VISITOR_ID)) {
                            imageView.setImageResource(R.drawable.ic_id_card);

                        } else
                            imageView.setImageResource(R.drawable.ic_photo);


                        if (fieldInfo.getFieldGrp().equals(Global.SCREEN_TYPE_WHOCAME))
                            llForCustomFields_Visitor.addView(root);
                        else
                            llForCustomFields_ToMeet.addView(root);


                        imgtext.setText(fieldInfo.getName());
                        imgText.add(imgtext);
                        allImageview.add(imageView);


                        if (fieldInfo.isHide() || (fieldInfo.isDep() && !fieldInfo.isDsbl())) {
                            imgtext.setVisibility(View.GONE);
                            //view.setVisibility(View.GONE);
                            imageView.setVisibility(View.GONE);
                            //   llForCustomFields_ToMeet.setVisibility(View.GONE);
                        }
                    } else {

                        iv_visitorpic.setTag(fieldInfo);
                        if (fieldInfo.isHide()) {
                            loginsp.edit().putBoolean(Global.SETTINGS_PHOTO_REQUIRED, false).apply();
                            iv_visitorpic.setVisibility(View.GONE);
                        } else {
                            if (fieldInfo.isRequired())
                                loginsp.edit().putBoolean(Global.SETTINGS_PHOTO_REQUIRED, true).apply();
                            else
                                loginsp.edit().putBoolean(Global.SETTINGS_PHOTO_REQUIRED, false).apply();
                            iv_visitorpic.setVisibility(View.VISIBLE);
                        }
                    }


                }
                setCameraListners();
            }
        }

       /* } catch (Exception e)

        {

        }*/

    }

    private boolean isOptionsExists(ArrayList<FieldsOptions> fieldsOptions, String val) {
        if (fieldsOptions == null) {
            return true;
        }
        for (FieldsOptions options : fieldsOptions) {
            if (options.getName().trim().equalsIgnoreCase(val.trim())) {
                return true;
            }
        }
        return false;
    }


    private void setModuleAdapter(Context context,
                                  final FieldMapping fieldMapping, AutoCompleteTextView autoCompleteTextView) {
        final ArrayList<FieldsOptions> optList = new ArrayList<>();
        Query modRef = PersistentDatabase.getDatabase().getReference(fieldMapping.getMod()).child(loginsp.getString(Global.BIZ_ID, ""))
                .child(loginsp.getString(Global.GROUP_ID, "")).orderByChild(fieldMapping.getLbl());
        modRef.keepSynced(true);

        final FieldsAutoCompleteAdapter adapter1 = new FieldsAutoCompleteAdapter(context, android.R.layout.simple_dropdown_item_1line, optList);
        autoCompleteTextView.setAdapter(adapter1);

        modRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    FieldsOptions fieldsOptions = new FieldsOptions();
                    fieldsOptions.setfId(fieldMapping.getDepFid());
                    fieldsOptions.setName(child.child("nm").getValue(String.class));
                    fieldsOptions.setTyp(Global.DROPDOWN);
                    fieldsOptions.setOptionId(child.getKey());
                    optList.add(fieldsOptions);
                }

                adapter1.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    static class MHandler extends Handler {

        WeakReference<Context> mActivity;

        MHandler(Context activity) {
            mActivity = new WeakReference<Context>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            Context theActivity = mActivity.get();


            switch (msg.what) {
                case versionx.selfentry.Printing.Global.MSG_WORKTHREAD_SEND_CONNECTUSBRESULT:
                case versionx.selfentry.Printing.Global.MSG_WORKTHREAD_SEND_CONNECTNETRESULT:
                case versionx.selfentry.Printing.Global.MSG_WORKTHREAD_SEND_CONNECTBTRESULT:
                    int result = msg.arg1;

                    if (result == 1) {
                        byte[] buf = HomeActivity.buf;

                        if (DrawerService.workThread != null &&
                                DrawerService.workThread.isConnected() && buf != null) {
                            Bundle data = new Bundle();
                            data.putByteArray(versionx.selfentry.Printing.Global.BYTESPARA1, buf);
                            data.putInt(versionx.selfentry.Printing.Global.INTPARA1, 0);
                            data.putInt(versionx.selfentry.Printing.Global.INTPARA2, buf.length);
                            data.putInt(Global.COMPLETED, 0);
                            DrawerService.workThread.handleCmd(versionx.selfentry.Printing.Global.CMD_POS_WRITE, data);
                            if (msg.what != versionx.selfentry.Printing.Global.MSG_WORKTHREAD_SEND_CONNECTNETRESULT)
                                CommonMethods.showToast(theActivity, "connected ", Toast.LENGTH_SHORT);

                            //  if (msg.what == team.why.entry.Printing.Global.MSG_WORKTHREAD_SEND_CONNECTNETRESULT) {
                            Bundle bundle = new Bundle();
                            String txt = " ";

                            bundle.putByteArray(Global.BYTESPARA1, txt.getBytes());
                            bundle.putInt(Global.INTPARA1, 0);
                            bundle.putInt(Global.INTPARA2, txt.getBytes().length);
                            bundle.putInt(Global.COMPLETED, 1);

                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, bundle);
                            // }
                        }
                    } else {
                        DrawerService.workThread.disconnectNet();
                    }

                    HomeActivity.buf = null;
                    return;

                case versionx.selfentry.Printing.Global.CMD_POS_WRITERESULT:


                    if (msg.arg2 == 1) {

                        DrawerService.workThread.disconnectNet();
                    }


                    return;


            }
        }
    }


    private void setUSBPrinter(Context context, UsbManager mUsbManager) {


        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        UsbDevice device = null;
        ArrayList<UsbDevice> usbDeviceArrayList = new ArrayList<>();
        while (deviceIterator.hasNext()) {
            device = deviceIterator.next();
            usbDeviceArrayList.add(device);


        }
        openDialogForGroupSelection(context, usbDeviceArrayList, mUsbManager);


    }


    public void openDialogForGroupSelection(Context context,
                                            final ArrayList<UsbDevice> usbDevices, final UsbManager mUsbManager) {

        try {
            android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(context);


            b.setTitle("Select USB Printer");
            final Context context1 = context;
            final String[] groupListName = new String[usbDevices.size()];
            for (int i = 0; i < usbDevices.size(); i++)
                if (Build.VERSION.SDK_INT >= 21)
                    groupListName[i] = usbDevices.get(i).getManufacturerName() + "-" + usbDevices.get(i).getProductName();
                else
                    groupListName[i] = usbDevices.get(i).getProductId() + "";


            b.setSingleChoiceItems(groupListName, 0, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        PendingIntent mPermissionIntent = PendingIntent
                                .getBroadcast(
                                        context1,
                                        0,
                                        new Intent(
                                                versionx.selfentry.Util.Global.ACTION_USB_PERMISSION),
                                        0);
                        if (usbDevices.get(i) != null) {
                            if (!mUsbManager.hasPermission(usbDevices.get(i))) {
                                mUsbManager.requestPermission(usbDevices.get(i),
                                        mPermissionIntent);

                            } else {
                                // DrawerService.workThread.connectUsb(mUsbManager, device);
                            }
                        }


                    } catch (Exception e) {
                        Crashlytics.logException(e);
                    } finally {
                        dialogInterface.dismiss();
                    }


                }
            });
            android.app.AlertDialog dialog = b.create();
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.show();
        } catch (Exception e) {

        }
    }


    private void updateNotifyHis(Visitor visitor, String accessType, String accessKey) {

        if ((loginsp.getBoolean(versionx.selfentry.Util.Global.ST_IN_NOTIFY, false) && visitor.isNotify() && accessType.equals(versionx.selfentry.Util.Global.STAFF))
                || loginsp.getBoolean(versionx.selfentry.Util.Global.VIZ_IN_NOTIFY, false) && (!accessType.equals(versionx.selfentry.Util.Global.STAFF)
                && !accessType.equals(versionx.selfentry.Util.Global.HELPER)) ||
                accessType.equals(versionx.selfentry.Util.Global.HELPER)) {

            DatabaseReference notifyRef = PersistentDatabase.getDatabase().getReference("notifyHis")
                    .child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                    .child(loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));
            HashMap<String, Object> notifyMap = new HashMap<>();
            notifyMap.put("dt", System.currentTimeMillis());
            notifyMap.put("mob", ed_visitorMobile.getText().toString());
            notifyMap.put("nm", ed_visitorName.getText().toString());
            notifyMap.put("toMeet", ed_toMeet.getText().toString());
            notifyMap.put("typ", visitor.getType());
            notifyMap.put("in", true);
            if (accessType.equalsIgnoreCase(versionx.selfentry.Util.Global.HELPER)) {
                notifyMap.put("id", accessKey);
            }
            notifyRef.push().updateChildren(notifyMap);
        }


    }

    private void saveData(String btnType, String type, String accessType, String
            visitorPhotoUrl, String visitorPhotoFilePath,
                          Context context, Visitor visitor, int calling, String prevTomeet, int prevCalling) {


        boolean view_print = validateDataFields();
        if (view_print) {
            // showToMeet();
            if (accessType.equals(versionx.selfentry.Util.Global.VISITOR)) {
                visitor.setParent(false);
                visitor.setVisitor(true);
            } else if (accessType.equals(versionx.selfentry.Util.Global.PARENT)) {
                visitor.setVisitor(false);
                visitor.setParent(true);
            }
            if (type.equalsIgnoreCase("appointment")) {
                visitor.setAppointment(true);
            }


            if (type != null && type.equalsIgnoreCase("appointment")) {
                DatabaseReference appointmentRef = PersistentDatabase.getDatabase()
                        .getReference(
                                "visitor/" + loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/" +
                                        loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/" + ed_visitorMobile.getText().toString().trim().substring(0, 4) + "/" +
                                        ed_visitorMobile.getText().toString().trim());

                DatabaseReference apptref = PersistentDatabase.getDatabase()
                        .getReference("appt/" +
                                loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/"
                                + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));

                apptref.child(ed_visitorMobile.getText().toString().trim()
                        + "_" + CommonMethods.getTodaysDate().getTimeInMillis())
                        .removeValue();

                appointmentRef.child("appt").child("" + CommonMethods.getTodaysDate()
                        .getTimeInMillis()).removeValue();

            }

            printingData(btnType, accessType, visitorPhotoUrl, visitorPhotoFilePath, context, visitor, type, prevTomeet, prevCalling);

        } else if (onlyImageRequired(context, "save", type, accessType, visitorPhotoUrl, visitorPhotoFilePath, visitor, calling, 6, versionx.selfentry.Util.Global.SCREEN_TYPE_MEET)) {

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:

                if (menu != null && loginsp.getInt(versionx.selfentry.Util.Global.LOCK_PIN, -1) != -1) {
                    showPinDialog(true);
                } else {
                    showPopupMenu();
                }

                break;


            case R.id.lock:
                clearData(false);
                //showDialingScreen();
                break;

          /*  case R.id.select_form:
                PersistentDatabase.getDatabase().getReference("masterData").child(loginsp.getString(Global.BIZ_ID, ""))
                        .child(loginsp.getString(Global.GROUP_ID, "")).child("restrict").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        openFormSelection(dataSnapshot, context);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                break;*/


            case R.id.rePrint:

                clearData(true);

                if (loginsp.getString(versionx.selfentry.Util.Global.LAST_PHONE_NO, null) != null && loginsp.getString(versionx.selfentry.Util.Global.LAST_HIS_KEY, null) != null) {

                    ed_visitorMobile.setText(loginsp.getString(versionx.selfentry.Util.Global.LAST_PHONE_NO, null));
                    hiskey = loginsp.getString(versionx.selfentry.Util.Global.LAST_HIS_KEY, null);
                    visitor.setTknNo(loginsp.getString(versionx.selfentry.Util.Global.LAST_TOKEN_NO, null));
                    return true;

                }

                break;


        }
        return super.onOptionsItemSelected(item);

    }

    public void openFormSelection(final DataSnapshot dataSnapshot, final Context context) {
        android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(this);
        b.setTitle("Select Form");
        b.setCancelable(false);
        final String[] groupListName = new String[Integer.parseInt(dataSnapshot.getChildrenCount() + "") + 1];
        final ArrayList<RestrictFields> restrictFieldsList = new ArrayList<>();
        int i = 0;
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            groupListName[i] = dataSnapshot1.child("nm").getValue(String.class);
            RestrictFields restrictFields = new RestrictFields();
            restrictFields.setRestrictId(dataSnapshot1.getKey());
            restrictFields.setRestrictNm(dataSnapshot1.child("nm").getValue(String.class));
            restrictFieldsList.add(restrictFields);
            i++;
        }
        groupListName[i] = "DEFAULT";
        RestrictFields restrictFields = new RestrictFields();
        restrictFields.setRestrictId("0");
        restrictFields.setRestrictNm("DEFAULT");
        restrictFieldsList.add(restrictFields);

        b.setSingleChoiceItems(groupListName, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                loginsp.edit().putString(Global.FORM_RESTRICT_ID, restrictFieldsList.get(i).getRestrictId()).apply();
                loginsp.edit().putString(Global.FORM_RESTRICT_NM, restrictFieldsList.get(i).getRestrictNm()).apply();
                dialogInterface.dismiss();

                getDbCustomFields(context);

            }
        });

        Dialog dialog = b.create();
        //   dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {


            if (loginsp.getInt(versionx.selfentry.Util.Global.LOCK_PIN, -1) == -1) {
                try {
                    if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                        return false;
                    // right to left swipe
                    if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {


                        VisitorLayoutPage.setVisibility(View.VISIBLE);
                        ll_call.setVisibility(View.GONE);

                  /*  overridePendingTransition(R.anim.left_to_right,
                            R.anim.left_to_right);*/
                    } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                        //  Toast.makeText(CallOnActivity.this, "Right Swipe", Toast.LENGTH_SHORT).show();
                        VisitorLayoutPage.setVisibility(View.VISIBLE);
                        ll_call.setVisibility(View.GONE);


                    }
                } catch (Exception e) {
                    // nothing
                }
            } else {
                showPinDialog(false);

            }


            return false;
        }


        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

    }

    private void showPinDialog(final boolean isMenu) {

        pinAlertDialog = new AlertDialog.Builder(HomeActivity.this);
        pinAlertDialog.setTitle("Enter Pin");

        LinearLayout container = new LinearLayout(HomeActivity.this);
        container.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(20, 0, 20, 0);
        final EditText input = new EditText(HomeActivity.this);
        input.setLayoutParams(lp);
        input.setGravity(android.view.Gravity.TOP | android.view.Gravity.LEFT);
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        input.setLines(1);
        input.setMaxLines(1);
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(4);
        input.setFilters(filterArray);
        container.addView(input, lp);
        pinAlertDialog.setView(container);


        pinAlertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {


                              /*  if (!isMenu) {
                                    VisitorLayoutPage.setVisibility(View.VISIBLE);
                                    ll_call.setVisibility(View.GONE);
                                }
                            */
                            int pin = Integer.parseInt(input.getText().toString().trim());
                            if (loginsp.getInt(versionx.selfentry.Util.Global.LOCK_PIN, -1) == pin) {


                                if (isMenu) {
                                    isPass = true;
                                    // setSetting();
                                    showPopupMenu();
                                } else {
                                    VisitorLayoutPage.setVisibility(View.VISIBLE);
                                    ll_call.setVisibility(View.GONE);
                                }

                            } else {

                                if (isMenu) {
                                    isPass = false;
                                    // invalidateOptionsMenu();

                                }

                                CommonMethods.showToast(HomeActivity.this, "Wrong Pin!", Toast.LENGTH_LONG);


                            }


                            try {
                                hideKB(input);
                            } catch (Exception e) {

                            }
                        } catch (Exception e) {
                            System.out.println("Pin" + e.getMessage());
                            if (isMenu) {
                                isPass = false;

                                // invalidateOptionsMenu();
                            }


                            CommonMethods.showToast(HomeActivity.this, "Wrong Pin!", Toast.LENGTH_LONG);

                        }

                    }
                });

        pinDialog = pinAlertDialog.create();
        pinDialog.setCanceledOnTouchOutside(false);
        pinDialog.show();
    }

    private void showPopupMenu() {

        final View menuItemView = findViewById(R.id.rePrint);
        final PopupMenu popupMenu = new PopupMenu(this, menuItemView);
        popupMenu.inflate(R.menu.submenu);

        Menu menu = popupMenu.getMenu();

        if (loginsp.getBoolean(versionx.selfentry.Util.Global.SELF_MODE, false)) {
            MenuItem logoutitem = menu.findItem(R.id.logout);
            logoutitem.setVisible(false);
            MenuItem kioskitem = menu.findItem(R.id.kiosk);
            kioskitem.setChecked(true);
            MenuItem deskitem = menu.findItem(R.id.desktop);
            deskitem.setChecked(false);
        } else {


            MenuItem kioskitem = menu.findItem(R.id.kiosk);
            kioskitem.setChecked(false);
            MenuItem deskitem = menu.findItem(R.id.desktop);
            deskitem.setChecked(true);

        }

        if (loginsp.getBoolean(versionx.selfentry.Util.Global.DEV_USER_LOGIN, false)) {
            MenuItem logoutitem = menu.findItem(R.id.logout);
            logoutitem.setVisible(true);
        } else {
            MenuItem logoutitem = menu.findItem(R.id.logout);
            logoutitem.setVisible(false);
        }

        if (loginsp.getBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, true)) {
            MenuItem dialItem = menu.findItem(R.id.dailScreen);
            dialItem.setChecked(true);
        } else {
            MenuItem dialItem = menu.findItem(R.id.dailScreen);
            dialItem.setChecked(false);
        }

        if (loginsp.getInt(versionx.selfentry.Util.Global.BTN_REQUIRE, 0) == versionx.selfentry.Util.Global.ONLY_EPASS) {
            MenuItem printItem = menu.findItem(R.id.print);
            printItem.setChecked(false);
            MenuItem epassItem = menu.findItem(R.id.epass);
            epassItem.setChecked(true);
            MenuItem printEpassitem = menu.findItem(R.id.print_epass);
            printEpassitem.setChecked(false);
        }

        if (loginsp.getInt(versionx.selfentry.Util.Global.BTN_REQUIRE, 0) == versionx.selfentry.Util.Global.ONLY_PRINT) {
            MenuItem printItem = menu.findItem(R.id.print);
            printItem.setChecked(true);
            MenuItem epassItem = menu.findItem(R.id.epass);
            epassItem.setChecked(false);
            MenuItem printEpassitem = menu.findItem(R.id.print_epass);
            printEpassitem.setChecked(false);

        }

        if (loginsp.getInt(versionx.selfentry.Util.Global.BTN_REQUIRE, 0) == versionx.selfentry.Util.Global.BOTH_EPASS_PRINT) {
            MenuItem printItem = menu.findItem(R.id.print);
            printItem.setChecked(false);
            MenuItem epassItem = menu.findItem(R.id.epass);
            epassItem.setChecked(false);
            MenuItem printEpassitem = menu.findItem(R.id.print_epass);
            printEpassitem.setChecked(true);
        }

        if (loginsp.getInt(versionx.selfentry.Util.Global.PRINTING, 0) == versionx.selfentry.Util.Global.NETWORK_PRINT) {
            MenuItem networkItem = menu.findItem(R.id.network);
            networkItem.setChecked(true);
            MenuItem usbItem = menu.findItem(R.id.usb);
            usbItem.setChecked(false);
        }

        if (loginsp.getInt(versionx.selfentry.Util.Global.PRINTING, 0) == versionx.selfentry.Util.Global.USB_PRINT) {
            MenuItem networkItem = menu.findItem(R.id.network);
            networkItem.setChecked(false);
            MenuItem usbItem = menu.findItem(R.id.usb);
            usbItem.setChecked(true);
        }

        if (loginsp.getBoolean(versionx.selfentry.Util.Global.CONFIRM, false)) {
            MenuItem notifyItem = menu.findItem(R.id.notify);
            notifyItem.setChecked(true);
            MenuItem alwaysNotifyItem = menu.findItem(R.id.always_notify);
            alwaysNotifyItem.setChecked(false);

        } else {
            MenuItem notifyItem = menu.findItem(R.id.notify);
            notifyItem.setChecked(false);
            MenuItem alwaysNotifyItem = menu.findItem(R.id.always_notify);
            alwaysNotifyItem.setChecked(true);
        }

        if (loginsp.getBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false)) {

            MenuItem alwaysNotifyItem = menu.findItem(R.id.always_notify);
            alwaysNotifyItem.setChecked(true);

        } else {
            MenuItem alwaysNotifyItem = menu.findItem(R.id.always_notify);
            alwaysNotifyItem.setChecked(false);
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.desktop:
                        desktopMode(popupMenu.getMenu());
                        showDialingScreen(false);
                        //dismissDialogScreen(true);
                        clearData(false);
                        break;

                    case R.id.kiosk:
                        showDialingScreen(false);
                        selfMode(popupMenu.getMenu());
                        clearData(false);
                        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.IS_DEV_USER_LOGGEDIN, false).apply();
                        loginsp.edit().putString(versionx.selfentry.Util.Global.LOGGNED_IN_USER_NM, "").apply();
                        /* getSupportActionBar().setTitle("Self Entry" + "-" + loginsp.getString(versionx.selfentry.Util.Global.DEVICE_NAME, ""));
                         */
                        break;

                    case R.id.logout:

                        Intent in = new Intent(getApplicationContext(), UserLogin.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.IS_DEV_USER_LOGGEDIN, false).apply();
                        loginsp.edit().putString(versionx.selfentry.Util.Global.LOGGNED_IN_USER_NM, "").apply();
                        deviceRef.child("u").removeValue();
                        finish();
                        overridePendingTransition(R.anim.left_to_right, R.anim.left_to_right);
                        startActivity(in);

                        break;
                    case R.id.epass:
                        btn_epass.setVisibility(View.VISIBLE);
                        btn_save.setVisibility(View.GONE);
                        deviceRef.child("mode").setValue(versionx.selfentry.Util.Global.ONLY_EPASS);
                        loginsp.edit().putInt(versionx.selfentry.Util.Global.BTN_REQUIRE, versionx.selfentry.Util.Global.ONLY_EPASS).apply();
                        invalidateOptionsMenu();
                        break;

                    case R.id.print:
                        btn_epass.setVisibility(View.GONE);
                        btn_save.setVisibility(View.VISIBLE);
                        deviceRef.child("mode").setValue(versionx.selfentry.Util.Global.ONLY_PRINT);
                        // clearData(false);

                        loginsp.edit().putInt(versionx.selfentry.Util.Global.BTN_REQUIRE, versionx.selfentry.Util.Global.ONLY_PRINT).apply();
                        invalidateOptionsMenu();
                        break;

                    case R.id.print_epass:
                        btn_save.setVisibility(View.VISIBLE);
                        btn_epass.setVisibility(View.VISIBLE);
                        deviceRef.child("mode").setValue(versionx.selfentry.Util.Global.BOTH_EPASS_PRINT);
                        loginsp.edit().putInt(versionx.selfentry.Util.Global.BTN_REQUIRE, versionx.selfentry.Util.Global.BOTH_EPASS_PRINT).apply();
                        invalidateOptionsMenu();
                        break;

                    case R.id.dailScreen:

                        if (!item.isChecked()) {
                            item.setChecked(true);
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, true).apply();
                            deviceRef.child("dial").setValue(true);

                        } else {
                            item.setChecked(false);
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, false).apply();
                            deviceRef.child("dial").setValue(false);
                        }

                        showDialingScreen(false);

                        invalidateOptionsMenu();

                        break;

                    case R.id.network:
                        loginsp.edit().putInt(versionx.selfentry.Util.Global.PRINTING, versionx.selfentry.Util.Global.NETWORK_PRINT).apply();
                        deviceRef.child("print").setValue(versionx.selfentry.Util.Global.NETWORK_PRINT);
                        invalidateOptionsMenu();
                        break;

                    case R.id.usb:
                        loginsp.edit().putInt(versionx.selfentry.Util.Global.PRINTING, versionx.selfentry.Util.Global.USB_PRINT).apply();
                        deviceRef.child("print").setValue(versionx.selfentry.Util.Global.USB_PRINT);
                        invalidateOptionsMenu();
                        break;

                    case R.id.notify:
                        if (!item.isChecked()) {
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CONFIRM, true).apply();
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false).apply();
                            deviceRef.child("cnfm").setValue(true);
                            deviceRef.child("acnfm").setValue(false);
                        } else {
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CONFIRM, false).apply();
                            deviceRef.child("cnfm").setValue(false);
                        }
                        invalidateOptionsMenu();
                        break;
                    case R.id.always_notify:

                        if (!item.isChecked()) {
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, true).apply();
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CONFIRM, false).apply();
                            deviceRef.child("acnfm").setValue(true);
                            deviceRef.child("cnfm").setValue(false);
                        } else {
                            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false).apply();
                            deviceRef.child("acnfm").setValue(false);
                        }
                        invalidateOptionsMenu();

                        break;
                }
                return false;
            }
        });

        popupMenu.show();
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_P:
                if (event.isCtrlPressed()) {
                    saveData(btn_save.getText().toString(), type, accessType, visitorPhotoUrl, visitorPhotoFilePath, context, visitor, calling, prevTomeet, prevCalling);
                    return true;
                } else {
                    return super.onKeyUp(keyCode, event);
                }


            case KeyEvent.KEYCODE_ESCAPE:
                if (event.isCtrlPressed() && loginsp.getBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, true)) {

                    showDialingScreen(false);


                 /*   ll_call.setVisibility(View.GONE);
                    VisitorLayoutPage.setVisibility(View.VISIBLE);*/

                    return true;
                } else {
                    return super.onKeyUp(keyCode, event);
                }


            case KeyEvent.KEYCODE_Q:
                if (event.isCtrlPressed()) {
                    clearData(false);
                    return true;
                } else {
                    return super.onKeyUp(keyCode, event);
                }


            case KeyEvent.KEYCODE_R:
                if (event.isCtrlPressed()) {
                    if (loginsp.getString(versionx.selfentry.Util.Global.LAST_PHONE_NO, null) != null && loginsp.getString(versionx.selfentry.Util.Global.LAST_HIS_KEY, null) != null) {

                        ed_visitorMobile.setText(loginsp.getString(versionx.selfentry.Util.Global.LAST_PHONE_NO, null));
                        hiskey = loginsp.getString(versionx.selfentry.Util.Global.LAST_HIS_KEY, null);


                        return true;

                    }
                } else {
                    return super.onKeyUp(keyCode, event);
                }


            default:
                return super.onKeyUp(keyCode, event);

        }


    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Are you sure you want to Exit?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                dialogInterface.dismiss();
                finish();
            }
        });

        builder.create().show();
    }

    @Override
    public void onBackPressed() {

        showExitDialog();


    }


    private void showDialingScreen(boolean reprint) {


        /*   if (loginsp.getBoolean(Global.DIAL_SCREEN, true) && loginsp.getBoolean(Global.SELF_MODE, true)) {*/

        if (loginsp.getBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, true) && !reprint) {
            if (!loginsp.getString(versionx.selfentry.Util.Global.START_SCREEN_MSG, "").isEmpty())
                loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, false).apply();
            else
                loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, true).apply();

            VisitorLayoutPage.setVisibility(View.GONE);
            ll_call.setVisibility(View.VISIBLE);
            //  loginsp.edit().putBoolean(Global.IS_DEV_USER_LOGGEDIN, false).apply();
            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.DIAL_SCREEN, true).apply();


            if (loginsp.getBoolean(versionx.selfentry.Util.Global.START_SCREEN, true) && !startdialog.isShowing()) {

                startTv.setText(loginsp.getString(versionx.selfentry.Util.Global.START_SCREEN_MSG, "CLICK HERE TO START"));

                ll_start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startdialog.cancel();
                        startdialog.dismiss();
                        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, true).apply();
                    }
                });
                startdialog.show();

            }

        } else {
            //  loginsp.edit().putBoolean(Global.DIAL_SCREEN, false).apply();
            if (startdialog.isShowing())
                startdialog.dismiss();

            loginsp.edit().putBoolean(versionx.selfentry.Util.Global.CALL_BLOCK, true).apply();
            VisitorLayoutPage.setVisibility(View.VISIBLE);
            ll_call.setVisibility(View.GONE);


        }
    }

    private void selfMode(Menu menu) {
        loginsp.edit().putInt(versionx.selfentry.Util.Global.CAM, 0).apply();
        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.SELF_MODE, true).apply();
        if (menu != null) {
            MenuItem logoutitem = menu.findItem(R.id.logout);
            logoutitem.setVisible(false);
            MenuItem kioskitem = menu.findItem(R.id.kiosk);
            kioskitem.setVisible(false);
            MenuItem deskitem = menu.findItem(R.id.desktop);
            deskitem.setVisible(true);
            supportInvalidateOptionsMenu();
        }

    }

   /*  private void dismissDialogScreen() {

        loginsp.edit().

    putBoolean(Global.DIAL_SCREEN, false).

    apply();
        loginsp.edit().

    putInt(Global.CAM, 1).

    apply();
        VisitorLayoutPage.setVisibility(View.VISIBLE);
        ll_call.setVisibility(View.GONE);


}*/


    private void desktopMode(Menu menu) {
        loginsp.edit().putInt(versionx.selfentry.Util.Global.CAM, 1).apply();
        loginsp.edit().putBoolean(versionx.selfentry.Util.Global.SELF_MODE, false).apply();
        loginsp.edit().putBoolean(Global.DIAL_SCREEN, false).apply();
        if (menu != null) {
            if (loginsp.getBoolean(versionx.selfentry.Util.Global.DEV_USER_LOGIN, false)) {
                MenuItem logoutitem = menu.findItem(R.id.logout);
                logoutitem.setVisible(true);
            } else {
                MenuItem logoutitem = menu.findItem(R.id.logout);
                logoutitem.setVisible(false);
            }
            MenuItem kioskitem = menu.findItem(R.id.kiosk);
            kioskitem.setVisible(true);
            MenuItem deskitem = menu.findItem(R.id.desktop);
            deskitem.setVisible(false);
            supportInvalidateOptionsMenu();
        }
    }


    public void setPurposeObject(String p, String pId) {

        visitor.getPurpose().setpId(pId);
        visitor.getPurpose().setP(p.trim());

        ed_purpose.setText(p.trim());

    }


    private void sendNotification(final String hiskey, final ToMeet selectedToMeet,
                                  final Visitor visitor, String accessType,
                                  ArrayList<HashMap> toMeetCustomFieldsHashmap, ArrayList<HashMap> whoCameCustomFieldsHashmap,
                                  boolean isFirstTime, boolean isAllowedBefore, DatabaseReference visitorGroupRef,
                                  DatabaseReference visitorHisRef, boolean isAppointment, String type) {


        JSONObject jsonObject = null;
        JSONObject jsonObjectTitle = new JSONObject();
        try {
            jsonObjectTitle.put("title", "Visitor Confirmation");
            jsonObjectTitle.put("text", visitor.getName() + " is here to meet you");
            jsonObjectTitle.put("click_action", versionx.selfentry.Util.Global.CAST_HOME_ACTIVITY);
            jsonObjectTitle.put("sound", "default");
            jsonObjectTitle.put("icon", "notification_icon");
            jsonObjectTitle.put("color", "#1C1C1C");
            JSONObject jsNm = new JSONObject();
            jsNm.put("BizId", loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""));
            jsNm.put("VisId", hiskey);
            jsNm.put("mob", visitor.getMobile());
            jsNm.put("nm", visitor.getName());
            jsNm.put("p", visitor.getPurposeNm());
            jsNm.put("toMeet", selectedToMeet.getNm());
            jsNm.put("meetId", selectedToMeet.getId());
            jsNm.put("toMeetMob", selectedToMeet.getMob());

            jsNm.put("notifyId", System.currentTimeMillis() + "");
            jsNm.put("vDt", CommonMethods.getFirstDayOfMonth().getTimeInMillis());
            jsNm.put("tDt", CommonMethods.getTodaysDate().getTimeInMillis());
            if (accessType.equals(versionx.selfentry.Util.Global.HELPER))
                jsNm.put("typ", "info");
            else
                jsNm.put("typ", "Visitor");
            jsNm.put("SenderToken", FirebaseInstanceId.getInstance().getToken());
            jsNm.put("GrpId", loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, ""));
            jsonObject = new JSONObject();
            jsonObject.put("data", jsNm);
            jsonObject.put("registration_ids", new JSONArray(selectedToMeet.getAllTokens()));

            // jsonObject.put("to", selectedToMeet.getToken());
            jsonObject.put("priority", "high");


            final NetworkingApiConnect networkingApiConnect = new NetworkingApiConnect(HomeActivity.this, "https://fcm.googleapis.com/fcm/send", "post", jsonObject.toString(), "", "Notification", 15000);
            networkingApiConnect.execute();

            jsNm.put("notifyId", System.currentTimeMillis() + "");
            jsonObject.put("data", jsNm);
            jsonObjectTitle.put("title", "Visitor Confirmation");
            jsonObjectTitle.put("text", visitor.getName() + " is here to meet you");
            jsonObjectTitle.put("click_action", versionx.selfentry.Util.Global.CAST_HOME_ACTIVITY);
            jsonObjectTitle.put("sound", "default");
            jsonObjectTitle.put("icon", "notification_icon");
            jsonObjectTitle.put("color", "#1C1C1C");
            jsonObject.put("notification", jsonObjectTitle);
            final NetworkingApiConnect networkingApiConnectwithNotifi = new NetworkingApiConnect(HomeActivity.this,
                    "https://fcm.googleapis.com/fcm/send", "post", jsonObject.toString(),
                    "", "Notification", 15000);
            networkingApiConnectwithNotifi.execute();

            if ((!isAppointment && !type.equals(versionx.selfentry.Util.Global.ALLOWED_VISITOR)) && ((loginsp.getBoolean(versionx.selfentry.Util.Global.CONFIRM, false)
                    && (isFirstTime || !isAllowedBefore))
                    || loginsp.getBoolean(versionx.selfentry.Util.Global.ALWAYS_CONFIRM, false))) {

                waitRef = PersistentDatabase.getDatabase().getReference("wait/" +
                        loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") + "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")).child(hiskey);
                wait_tv.setText(loginsp.getString(versionx.selfentry.Util.Global.WAIT_MSG, "Wait for Confirmation..."));
                waitprogressdialog.show();
                waitHandler = new Handler();
                waitrunnable = new Runnable() {
                    @Override
                    public void run() {
                        // startdialog.dismiss();
                        waitRef.removeValue();
                        waitprogressdialog.dismiss();
                        if (waitRefListner != null && waitRef != null) {
                            waitRef.removeEventListener(waitRefListner);
                        }
                        showExpiredDialog(context, loginsp, "wait");
                        if (networkingApiConnect != null) {
                            networkingApiConnect.cancel(true);
                        }
                        if (networkingApiConnectwithNotifi != null) {
                            networkingApiConnectwithNotifi.cancel(true);
                        }

                        // clearData(false);

                    }
                };
                waitHandler.postDelayed(waitrunnable, loginsp.getInt(versionx.selfentry.Util.Global.WAITING_TM, 10) * 1000);

                visitorAllowedOrReject(context, hiskey, accessType, visitor.getsNo(), toMeetCustomFieldsHashmap,
                        whoCameCustomFieldsHashmap, visitorGroupRef, visitorHisRef);
            } else {
                clearData(false);
            }


        } catch (Exception e) {

            clearData(false);
            // FirebaseCrash.report(e);

        }


    }


    public void visitorAllowedOrReject(final Context context, String hiskey,
                                       final String accessType, final String sNo,
                                       final ArrayList<HashMap> toMeetCustomFieldsHashmap,
                                       final ArrayList<HashMap> whoCameCustomFieldsHashmap,
                                       final DatabaseReference visitorGroupRef, final DatabaseReference visitorHisRef) {


        waitRef.keepSynced(true);
        waitRefListner = waitRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {


                    final DatabaseReference vizRef = PersistentDatabase.getDatabase()
                            .getReference(CommonMethods.getSyncHistoryNode(accessType) + "/" +
                                    loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, "") +
                                    "/" + loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "") + "/" + dataSnapshot.child("tDt").getValue(Long.class)
                                    + "/" + dataSnapshot.getKey());


                    if (dataSnapshot.exists() && dataSnapshot.hasChild("a") && dataSnapshot.child("a").getValue().equals("a")
                            && (dataSnapshot.hasChild("devId") &&
                            dataSnapshot.child("devId").getValue().toString()
                                    .equals(loginsp.getString(versionx.selfentry.Util.Global.UUID, "")))) {

                        vizRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(final DataSnapshot visitordataSnapshot) {

                                {

                                    //  startdialog.dismiss();
                                    waitprogressdialog.dismiss();
                                    waitHandler.removeCallbacks(waitrunnable);
                                    waitRef.removeEventListener(this);
                                    waitRef.removeValue();
                                    visitorHisRef.child("a").setValue("a");
                                    visitorGroupRef.child("ref").child("act").setValue("a");
                                    // print required
                                    if (loginsp.getBoolean(versionx.selfentry.Util.Global.SETTINGS_PRINT_REQUIRED, false)) {
                                        final String nm = " " + visitordataSnapshot.child("whoCame").child("nm").getValue().toString();

                                        final String mob = visitordataSnapshot.child("whoCame").child("mob").getValue().toString();

                                        final String toMeet = visitordataSnapshot.
                                                child("meet").child("nm").getValue().toString();

                                        final String inTime = getDate(Long.parseLong(visitordataSnapshot.child("in").getValue().toString())
                                                , "dd MMM hh:mm a");

                                        final String purpose = visitordataSnapshot.
                                                child("meet").child("p").getValue().toString();
                                        String oldToken = null;

                                        if (visitordataSnapshot.child("meet").hasChild("q")) {

                                            if (loginsp.getBoolean(Global.TOKEN, false)) {

                                                if (visitordataSnapshot.hasChild("tknNo"))
                                                    oldToken = visitordataSnapshot.child("tknNo").getValue(String.class);

                                                if (oldToken == null) {
                                                    oldToken = loginsp.getString(Global.DEVICE_CODE, "") + (loginsp.getInt(Global.TOKEN_NO, 0) + 1);
                                                    loginsp.edit().putInt(Global.TOKEN_NO, loginsp.getInt(Global.TOKEN_NO, 0) + 1).apply();

                                                }


                                                if (loginsp.getBoolean(Global.HARDWARE, false)) {

                                                    TokenUpdate g = new TokenUpdate(loginsp, toMeet, visitordataSnapshot.
                                                            child("meet").child("id").getValue().toString(), System.currentTimeMillis(),
                                                            oldToken, visitor.getName(), visitordataSnapshot.getKey(), context);
                                                    g.execute();
                                                }

                                            }

                                        }

                                        final String oldTkn = oldToken;


                                        if (visitordataSnapshot.child("whoCame").hasChild("img")) {


                                            Glide.with(context).load(visitordataSnapshot.child("whoCame")
                                                    .child("img").getValue().toString()).asBitmap().override(200, 200)
                                                    .into(new SimpleTarget<Bitmap>() {
                                                        @Override
                                                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {


                                                            new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                                    toMeetCustomFieldsHashmap,
                                                                    whoCameCustomFieldsHashmap, accessType, visitordataSnapshot.getKey(),
                                                                    resource, sNo, System.currentTimeMillis(), visitordataSnapshot.child("meet").hasChild("q")
                                                                    , oldTkn); // connect to the printer and print slip


                                                        }

                                                        @Override
                                                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                                            new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                                    toMeetCustomFieldsHashmap,
                                                                    whoCameCustomFieldsHashmap, accessType, visitordataSnapshot.child("meet").getKey(),
                                                                    null, sNo, System.currentTimeMillis(), visitordataSnapshot.hasChild("q"),
                                                                    oldTkn); // connect to the printer and print slip


                                                        }
                                                    });


                                        } else {

                                            ImagesDataSource imagesDataSource = new ImagesDataSource(context);
                                            imagesDataSource.open();

                                            String imgPath = imagesDataSource.getcustomImg(visitordataSnapshot.getKey()
                                                    , null, mob);
                                            imagesDataSource.close();


                                            if (imgPath == null) {

                                                new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                        toMeetCustomFieldsHashmap,
                                                        whoCameCustomFieldsHashmap, accessType, visitordataSnapshot.getKey(),
                                                        null, sNo, System.currentTimeMillis(), visitordataSnapshot.child("meet").hasChild("q")
                                                        , oldTkn); // connect to the printer and print slip


                                            } else {

                                                Glide.with(context).load(imgPath).asBitmap().override(200, 200)
                                                        .into(new SimpleTarget<Bitmap>() {
                                                            @Override
                                                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {


                                                                new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                                        toMeetCustomFieldsHashmap,
                                                                        whoCameCustomFieldsHashmap, accessType, visitordataSnapshot.getKey(),
                                                                        resource, sNo, System.currentTimeMillis(), visitordataSnapshot.child("meet").hasChild("q"),
                                                                        oldTkn); // connect to the printer and print slip


                                                            }

                                                            @Override
                                                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                                                new CommonMethods().printSlip(context, nm, mob, toMeet, inTime, purpose,
                                                                        toMeetCustomFieldsHashmap,
                                                                        whoCameCustomFieldsHashmap, accessType, visitordataSnapshot.getKey(),
                                                                        null, sNo, System.currentTimeMillis(), visitordataSnapshot.child("meet").hasChild("q"),
                                                                        oldTkn); // connect to the printer and print slip


                                                            }
                                                        });

                                            }
                                        }


                                    }


                                    waitRef.child(visitordataSnapshot.getKey()).removeValue();

                                    clearData(false);
                                }


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    } else if (dataSnapshot.child("a").getValue().equals("r")
                            && (dataSnapshot.hasChild("devId") &&
                            dataSnapshot.child("devId").getValue().toString()
                                    .equals(loginsp.getString(versionx.selfentry.Util.Global.UUID, "")))) {
                        waitprogressdialog.dismiss();
                        waitHandler.removeCallbacks(waitrunnable);
                        waitRef.removeEventListener(this);
                        waitRef.removeValue();
                        visitorHisRef.child("a").setValue("r");
                        visitorGroupRef.child("ref").child("act").setValue("r");
                        showExpiredDialog(context, loginsp, "r");


                    }
                } else {
                    waitprogressdialog.dismiss();
                    waitHandler.removeCallbacks(waitrunnable);
                    waitRef.removeEventListener(this);
                    clearData(false);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void DndDialog() {
        final Dialog dialog = new Dialog(context);

        //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 2.0f;

        dialog.setContentView(R.layout.dnddialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        TextView appointment_text = (TextView) dialog.findViewById(R.id.dnd_text);
        TextView dialog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        LinearLayout ll_dialog = (LinearLayout) dialog.findViewById(R.id.dialog_ll);

        ll_dialog.setBackgroundColor(Color.parseColor("#ffffff"));
        appointment_text.setTextColor(Color.parseColor("#000000"));
        appointment_text.setText(selectedToMeet.getDndRsn());
        appointment_text.setTextSize(40);
        dialog_title.setText("DND");

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();
                clearData(false);

            }
        };


        Handler handler = new Handler();

        handler.postDelayed(runnable, 8000);

    }


    private void BlockVisitor(Context context) {

        final Dialog dialog = new Dialog(context);

        //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 2.0f;

        dialog.setContentView(R.layout.allow_block_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        TextView appointment_text = (TextView) dialog.findViewById(R.id.appointment_text);
        LinearLayout ll_dialog = (LinearLayout) dialog.findViewById(R.id.dialog_ll);

        ll_dialog.setBackgroundColor(context.getResources().getColor(R.color.app_button_red));
        appointment_text.setTextColor(Color.parseColor("#ffffff"));
        appointment_text.setText(loginsp.getString(versionx.selfentry.Util.Global.BLOCK_MSG, "\n\nBLOCKED\n\n"));
        appointment_text.setTextSize(40);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();
                clearData(false);

            }
        };


        Handler handler = new Handler();

        handler.postDelayed(runnable, 5000);


    }


    private void showExpiredDialog(Context context, SharedPreferences loginSp, String type) {

        final Dialog dialog = new Dialog(context);

        //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 2.0f;

        dialog.setContentView(R.layout.allow_block_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        TextView appointment_text = (TextView) dialog.findViewById(R.id.appointment_text);
        LinearLayout ll_dialog = (LinearLayout) dialog.findViewById(R.id.dialog_ll);

        if (type.equals("r")) {
            ll_dialog.setBackgroundColor(context.getResources().getColor(R.color.app_button_red_pressed));
            appointment_text.setTextColor(Color.parseColor("#ffffff"));
            appointment_text.setText(loginSp.getString(versionx.selfentry.Util.Global.REJECT_MSG, "REJECTED!!\nContact Operator!"));
        } else {
            ll_dialog.setBackgroundColor(context.getResources().getColor(R.color.app_button_orange));
            appointment_text.setTextColor(Color.parseColor("#000000"));
            appointment_text.setText(loginSp.getString(versionx.selfentry.Util.Global.NO_RESPONSE_MSG, "Contact Operator!"));
        }


        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();
                clearData(false);

            }
        };


        Handler handler = new Handler();

        handler.postDelayed(runnable, 7000);

    }

    private void updateQNode(String hiskey, ToMeet selectedToMeet) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", selectedToMeet.getId());
        hashMap.put("nm", selectedToMeet.getNm());
        PersistentDatabase.getDatabase().getReference("q").child(loginsp.getString(versionx.selfentry.Util.Global.BIZ_ID, ""))
                .child(loginsp.getString(versionx.selfentry.Util.Global.GROUP_ID, "")).child(CommonMethods.getTodaysDate().getTimeInMillis() + "")
                .child("his").child(hiskey).updateChildren(hashMap);
    }


    private void getDbCustomFields(Context context) {
        CustomFieldAsync customFieldAsync = new CustomFieldAsync(context, "GET");
        customFieldAsync.setListner(this);
        customFieldAsync.execute();

    }

    private int getpositionOfRb(String txt, RadioGroup rg) {
        for (int i = 0; i < rg.getChildCount(); i++) {
            RadioButton btn = (RadioButton) rg.getChildAt(i);
            if (btn.getText().equals(txt)) {

                return i;
            }
        }
        return -1;
    }

}




