package versionx.selfentry.Activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import versionx.selfentry.CustomControls.TextViewOpenSans;
import versionx.selfentry.DataBase.CustomFields.CustomFieldsFromDb;
import versionx.selfentry.DataBase.CustomFields.RestrictFieldAsync;
import versionx.selfentry.DataBase.CustomFields.RestrictFieldDb;
import versionx.selfentry.R;
import versionx.selfentry.R;
import versionx.selfentry.Util.Global;
import versionx.selfentry.Util.PersistentDatabase;


public class SplashScreenActivity extends Activity {

    private ProgressBar progressBar;
    private SharedPreferences loginSp;
    int count = 0;
    ImageView iv_app_icon;
    TextViewOpenSans tv_app_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        initGUI();




        if (!loginSp.getBoolean(Global.IS_DATA_LOADED, false) && getIntent().getAction().equals("LOGIN")) {
            iv_app_icon.setVisibility(View.GONE);
            tv_app_name.setText("Loading Data...");
            loadData(getApplicationContext());


        }

        else {
            if (FirebaseAuth.getInstance() != null && FirebaseAuth.getInstance().getCurrentUser() != null && !loginSp.getString(Global.GROUP_NAME, "").isEmpty()) // this means user has already verified the mobile number
            {
                AddCustomFields(getApplicationContext());
            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    onSuccessLogin();
                }
            }, 5000);
        }
    }

    public void initGUI() {
        progressBar = (ProgressBar) findViewById(R.id.pb_splashScreen);
        loginSp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        iv_app_icon = (ImageView) findViewById(R.id.iv_app_icon);
        tv_app_name = (TextViewOpenSans) findViewById(R.id.tv_app_name);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#009A9A"), android.graphics.PorterDuff.Mode.MULTIPLY);


    }

    private void onSuccessLogin() {
        if (FirebaseAuth.getInstance() != null && FirebaseAuth.getInstance().getCurrentUser() != null && !loginSp.getString(Global.GROUP_NAME, "").isEmpty()) // this means user has already verified the mobile number
        {

            Intent in = new Intent(getApplicationContext(), HomeActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(in);


        } else {
            Intent in = new Intent(getApplicationContext(), VerificationActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(in);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void loadData(Context context) {
        AddCustomFields(getApplicationContext()); /// update customfields Database

        PersistentDatabase.getDatabase()
                .getReference("field/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "")).child("frm").child("0")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        System.out.println("alka here  " + dataSnapshot.getKey());
                        onSuccessLogin();


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });


    }

    public void AddCustomFields(final Context context) {

        DatabaseReference fieldmeetRef = PersistentDatabase.getDatabase()
                .getReference("field/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "")).child("frm").child("0");

        fieldmeetRef.keepSynced(true);
        new CustomFieldsFromDb(context).getCustomFields(fieldmeetRef);



        DatabaseReference depMapRef = PersistentDatabase.getDatabase()
                .getReference("field/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "")).child("depMap");

        depMapRef.keepSynced(true);
        new CustomFieldsFromDb(context).setMapping(depMapRef);

    //   new RestrictFieldDb(context);






    }

  /*  public void AddAccess(Context context) {

        new AccessDb(context).addAccess();

    }
*/

}