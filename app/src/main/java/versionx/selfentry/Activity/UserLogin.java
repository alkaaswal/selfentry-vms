package versionx.selfentry.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import versionx.selfentry.R;
import versionx.selfentry.Util.PersistentDatabase;
import versionx.selfentry.R;
import versionx.selfentry.Util.CommonMethods;
import versionx.selfentry.Util.Global;

public class UserLogin extends AppCompatActivity implements View.OnClickListener {


    EditText ed_pin;
    Button btn_submit;
    SharedPreferences spLogin;
    DatabaseReference deviceRef;
    private DatabaseReference masterRef;
    private ValueEventListener masterRefListner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        initGUI();
        getMobLength();

        setListner();


    }

    private void getMobLength() {

        DatabaseReference masteVizrRef = PersistentDatabase.getDatabase().
                getReference("masterData/" + spLogin.getString(Global.BIZ_ID, ""))
                .child("mobLen");
        masteVizrRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(dataSnapshot.getValue(Integer.class));
                    ed_pin.setFilters(filterArray);
                    spLogin.edit().putInt(Global.MOB_LENGTH, dataSnapshot.getValue(Integer.class)).apply();
                } else {
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(10);
                    ed_pin.setFilters(filterArray);
                    spLogin.edit().putInt(Global.MOB_LENGTH, 10).apply();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_login, menu);
       *//* MenuItem logoutitem = menu.findItem(R.id.logout);
        MenuItem deskitem = menu.findItem(R.id.desktop);
        MenuItem kioskitem = menu.findItem(R.id.kiosk);
        kioskitem.setVisible(true);
        logoutitem.setVisible(false);
        deskitem.setVisible(false);*//*


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.kiosk:


                spLogin.edit().putBoolean(Global.DIAL_SCREEN, true).apply();
                spLogin.edit().putInt(Global.CAM, 0).apply();
                Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                spLogin.edit().putBoolean(Global.IS_DEV_USER_LOGGEDIN, false).apply();

                finish();
                overridePendingTransition(R.anim.left_to_right,
                        R.anim.left_to_right);
                startActivity(in);
                break;


        }
        return super.

                onOptionsItemSelected(item);

    }*/


    private void initGUI() {
        spLogin = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);


        ed_pin = (EditText) findViewById(R.id.ed_login_pin);
        btn_submit = (Button) findViewById(R.id.btn_login);

        deviceRef = PersistentDatabase.getDatabase().getReference("device").child(spLogin.getString(Global.BIZ_ID, "")).child(Global.APP_NAME)
                .child(spLogin.getString(Global.UUID, ""));


        masterRef = PersistentDatabase.getDatabase().
                getReference("masterData/" + spLogin.getString(Global.BIZ_ID, "") + "/" + spLogin.getString(Global.GROUP_ID, ""));

        masterRefListner = masterRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {


                    if (dataSnapshot.hasChild("visitor")) {


                        spLogin.edit().putBoolean(Global.DEV_USER_LOGIN, dataSnapshot.child("visitor").child("uLog").getValue(Boolean.class)).apply();


                        if (!spLogin.getBoolean(Global.DEV_USER_LOGIN, false)) {

                            spLogin.edit().putBoolean(Global.DIAL_SCREEN, false).apply();

                            Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            finish();
                            overridePendingTransition(R.anim.left_to_right,
                                    R.anim.left_to_right);
                            startActivity(in);


                        }

                    }


                } catch (Exception e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void setListner() {
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_login:

                final ProgressDialog dialog = new ProgressDialog(UserLogin.this);
                dialog.setTitle("Verifying...");

                final Context context = UserLogin.this;

                if (ed_pin.getText().toString().trim().length() > 0) {
                    dialog.show();

                    PersistentDatabase.getDatabase().getReference("access").child(spLogin.getString(Global.BIZ_ID, "")).child(spLogin.getString(Global.GROUP_ID, ""))
                            .orderByChild("mob").equalTo(ed_pin.getText().toString().trim()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            dialog.dismiss();

                            if (dataSnapshot.exists()) {
                                for (DataSnapshot childsnapshot : dataSnapshot.getChildren()) {
                                    Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    spLogin.edit().putBoolean(Global.DIAL_SCREEN, false).apply();
                                    spLogin.edit().putBoolean(Global.IS_DEV_USER_LOGGEDIN, true).apply();
                                    spLogin.edit().putString(Global.LOGGNED_IN_USER_NM, childsnapshot.child("nm").getValue(String.class)).apply();
                                    spLogin.edit().putString(Global.LOGGNED_IN_USER_ID, childsnapshot.getKey()).apply();
                                    spLogin.edit().putString(Global.LOGGNED_IN_USER_MOB, childsnapshot.child("mob").getValue(String.class)).apply();
                                    HashMap<String, Object> hashMap = new HashMap<>();
                                    hashMap.put("dt", System.currentTimeMillis());
                                    hashMap.put("id", childsnapshot.getKey());
                                    hashMap.put("nm", childsnapshot.child("nm").getValue(String.class));
                                    hashMap.put("mob", childsnapshot.child("mob").getValue(String.class));

                                    deviceRef.child("u").updateChildren(hashMap);
                                    finish();
                                    overridePendingTransition(R.anim.left_to_right,
                                            R.anim.left_to_right);
                                    startActivity(in);
                                }
                            } else {
                                Toast.makeText(context, "User doesn't exits!", Toast.LENGTH_SHORT).show();
                                spLogin.edit().putBoolean(Global.IS_DEV_USER_LOGGEDIN, false).apply();
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {


                    CommonMethods.showToast(context, "Pin is mandatory!", Toast.LENGTH_SHORT);
                    ed_pin.setError("can't be empty");
                }

                break;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (masterRef != null && masterRefListner != null) {
            masterRef.removeEventListener(masterRefListner);
        }
    }
}
