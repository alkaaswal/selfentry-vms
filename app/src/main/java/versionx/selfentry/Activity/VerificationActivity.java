package versionx.selfentry.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import versionx.selfentry.R;
import versionx.selfentry.R;
import versionx.selfentry.Util.CommonMethods;
import versionx.selfentry.Util.Global;
import versionx.selfentry.Util.PersistentDatabase;
import versionx.selfentry.gettersetter.Groups;


public class VerificationActivity extends Activity implements OnClickListener {

    Button btn_verify;
    EditText mobile;
    SharedPreferences sp;
    private FirebaseAuth mAuth;
    DatabaseReference adminDb;
    ValueEventListener adminListener;
    AlertDialog.Builder alertdialog;
    AlertDialog alert;
    SharedPreferences loginSp;
    ProgressDialog dialog;
    int PLAY_SERVICES_RESOLUTION_REQUEST = 111;
    EditText ed_email, ed_pass;
    String domain = "@versionx.in";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        initGUI(); // initializing the UI elements
        setListners(); // set event listners on UI elements
        checkPlayServices();

    }

    public void initGUI() {
        btn_verify = (Button) findViewById(R.id.btn_verification_verify);
        ed_email = (EditText) findViewById(R.id.ed_verificationEmail);
        ed_pass = (EditText) findViewById(R.id.ed_verificationPass);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mAuth = FirebaseAuth.getInstance();
        loginSp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
    }

    public void setListners() {
        btn_verify.setOnClickListener(this);


    }


    private boolean checkPlayServices() {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                gApi.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this, "Google Play Service issue!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_verification_verify:
                if (ed_email.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "can't be blank!", Toast.LENGTH_SHORT).show();
                    ed_email.setHintTextColor(Color.RED);
                } else if (ed_pass.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "can't be blank!", Toast.LENGTH_SHORT).show();
                    ed_pass.setHintTextColor(Color.RED);
                    ed_email.setHintTextColor(getResources().getColor(R.color.app_text_color));
                } else {
                    ed_email.setHintTextColor(getResources().getColor(R.color.app_text_color));
                    ed_pass.setHintTextColor(getResources().getColor(R.color.app_text_color));
                    if (CommonMethods.isInternetWorking(VerificationActivity.this)) {
                        dialog = new ProgressDialog(VerificationActivity.this);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();

                        dialog.setMessage("Verifying...");
                        mAuth.signInWithEmailAndPassword(ed_email.getText().toString().trim() + domain, ed_pass.getText().toString().trim())
                                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        ;

                                        // If sign in fails, display a message to the user. If sign in succeeds
                                        // the auth state listener will be notified and logic to handle the
                                        // signed in user can be handled in the listener.
                                        if (!task.isSuccessful()) {
                                            //  System.out.println("not working ");

                                            Toast.makeText(VerificationActivity.this, "Invalid login!", Toast.LENGTH_SHORT).show();

                                            dialog.dismiss();
                                        } else {
                                            isUserRegistered(getApplicationContext());
                                        }

                                        // ...
                                    }
                                });


                    } else {
                        Toast.makeText(this, Global.NO_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                    }
                }


                break;


        }
    }


    public void isUserRegistered(final Context context) {
        final ArrayList<Groups> groupList = new ArrayList<>();


        sp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        int versionCode = 0;
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            sp.edit().putString(Global.VERSION_NUMBER, "" + versionCode).commit();


        } catch (PackageManager.NameNotFoundException e) {
            // e.printStackTrace();
            Crashlytics.logException(e);

        }

        final DatabaseReference bizRef = PersistentDatabase.getDatabase().getReference("biz/");

        bizRef.orderByChild("code").equalTo(ed_email.getText().toString().trim())

                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot mainSnapshot) {


                        if (mainSnapshot.exists()) {

                            for (DataSnapshot dataSnapshot : mainSnapshot.getChildren()) {
                                bizRef.child(dataSnapshot.getKey()).keepSynced(true);

                                if (dataSnapshot != null) {

                                    loginSp.edit().putString(Global.BIZ_ID, dataSnapshot.getKey()).apply();


                                    for (DataSnapshot child : dataSnapshot.child("grp").getChildren()) {
                                        Groups groups = new Groups();
                                        groups.setGroupId(child.getKey());
                                        groups.setGroupName(child.child("nm").getValue().toString());
                                        groupList.add(groups);
                                        dialog.dismiss();
                                    }
                                    if (groupList.size() > 1) {
                                        openDialogForGroupSelection(groupList, dataSnapshot, context);
                                    } else {


                                        SharedPreferences.Editor editor = loginSp.edit();
                                        editor.putString(Global.GROUP_ID, groupList.get(0).getGroupId().toString());
                                        editor.putString(Global.GROUP_NAME, groupList.get(0).getGroupName().toString());
                                        if (dataSnapshot.hasChild("expDt"))
                                            editor.putString(Global.BIZ_EXPIRY_DATE, dataSnapshot.child("expDt").getValue().toString());

                                        if (dataSnapshot.hasChild("img"))
                                            editor.putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString());

                                        editor.apply();


                                        String uuid =
                                                Settings.Secure.getString(getContentResolver(),
                                                        Settings.Secure.ANDROID_ID);

                                        loginSp.edit().putString(Global.UUID, uuid).apply();

                                        if (dataSnapshot.
                                                child("grp").child(groupList.get(0).getGroupId()).hasChild("typ")) {
                                            loginSp.edit().putString(Global.BIZ_TYPE, dataSnapshot.
                                                    child("grp").child(groupList.get(0).getGroupId()).child("typ").getValue(String.class)).apply();
                                        } else {
                                            loginSp.edit().putString(Global.BIZ_TYPE, dataSnapshot.child("typ").getValue(String.class)).apply();
                                        }


                                        updateDevice(context, uuid, groupList.get(0).getGroupId().toString());
                                        loadDataFromFirebase();

                                        dialog.dismiss();


                                    }


                                } else {
                                    dialog.dismiss();
                                    Toast toast = Toast.makeText(VerificationActivity.this, "Invalid code!", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            }

                        } else {
                            dialog.dismiss();
                            Toast toast = Toast.makeText(VerificationActivity.this, "Invalid code!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }


    @Override
    protected void onResume() {
        super.onResume();
        // checkVersion();
    }

    private void checkVersion() {
        adminDb = PersistentDatabase.getDatabase().getReference("admin/app/" + Global.APP_NAME+"/gen");
        adminListener = adminDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot child) {
                try {
                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        // e.printStackTrace();
                    }
                    String version = pInfo.versionName;
                    SharedPreferences loginSp = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
                    loginSp.edit().putString(Global.VERSION_NUMBER, version).apply();


                    if (!child.child("ver").getValue().toString().equalsIgnoreCase(version))
                        showAlert(child, version);
                    else {
                        if (alert != null && alert.isShowing()) {

                            alert.dismiss();
                            alert.cancel();
                        }

                    }
                } catch (Exception e) {
                    Crashlytics.logException(e);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void showAlert(DataSnapshot child, String version) {
        if (alert != null && alert.isShowing()) {

            alert.dismiss();
            alert.cancel();
        }

        alertdialog = new AlertDialog.Builder(this);
        alertdialog.setTitle("warning!");
        alertdialog.setMessage(child.child("msg").getValue().toString());
        alertdialog.setCancelable(false);
        alertdialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //    finish();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
                startActivity(intent);

            }
        });
        alert = alertdialog.create();


        alert.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //      adminDb.removeEventListener(adminListener);
    }

    public void openDialogForGroupSelection(final ArrayList<Groups> groupList, final DataSnapshot dataSnapshot, final Context context) {
        android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(this);
        b.setTitle("Select Group");
        final String[] groupListName = new String[groupList.size()];
        for (int i = 0; i < groupList.size(); i++)
            groupListName[i] = groupList.get(i).getGroupName();

        b.setSingleChoiceItems(groupListName, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                loginSp.edit().putString(Global.GROUP_ID, groupList.get(i).getGroupId().toString()).commit();
                loginSp.edit().putString(Global.BIZ_PERSON_NAME, dataSnapshot.child("person").child("nm").getValue().toString()).commit();

                SharedPreferences.Editor editor = loginSp.edit();

                editor.putString(Global.GROUP_NAME, groupListName[i]).apply();
                if (dataSnapshot.hasChild("expDt"))
                    editor.putString(Global.BIZ_EXPIRY_DATE, dataSnapshot.child("expDt").getValue().toString());
                if (dataSnapshot.hasChild("img"))
                    editor.putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString());
                editor.commit();

                String uuid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                loginSp.edit().putString(Global.UUID, uuid).commit();

                if (dataSnapshot.child("grp").child(groupList.get(i).getGroupId()).hasChild("typ")) {
                    loginSp.edit().putString(Global.BIZ_TYPE, dataSnapshot.
                            child("grp").child(groupList.get(i).getGroupId()).child("typ").getValue(String.class)).apply();
                } else {
                    loginSp.edit().putString(Global.BIZ_TYPE, dataSnapshot.child("typ").getValue(String.class)).apply();
                }


                updateDevice(context, uuid, groupList.get(i).getGroupId().toString());

                loadDataFromFirebase();
                dialogInterface.dismiss();


            }
        });
        b.show();
    }

    private void updateDevice(final Context context, String uuid, final String gId) {


        final DatabaseReference deviceRef = PersistentDatabase.getDatabase().
                getReference("device/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + Global.APP_NAME + "/" +
                        uuid);

        deviceRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("status", true);
                map.put("del", false);
                if (!dataSnapshot.hasChild("nm"))
                    map.put("nm", CommonMethods.getCurrentTime("hh:mm a"));
                map.put("token", FirebaseInstanceId.getInstance().getToken());
                map.put("ver", new CommonMethods().getVerisonNumber(context));
                map.put("dt", Calendar.getInstance().getTimeInMillis());
                map.put("force", true);
                map.put("app", Global.APP_NAME);
                map.put("lg", getAppVersionCode(context));
                deviceRef.updateChildren(map);
                deviceRef.child("gId").setValue(gId);
                Intent in = new Intent(getApplicationContext(), SplashScreenActivity.class);
                in.setAction("LOGIN");
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                loginSp.edit().putBoolean(Global.IS_LOGGED_IN, true).apply();
                finish();
                startActivity(in);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public int getAppVersionCode(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionCode;
    }


    private void loadDataFromFirebase() {
        DatabaseReference dbRef = PersistentDatabase.getDatabase().getReference("field/"
                + loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, ""));
        dbRef.keepSynced(true);

        PersistentDatabase.getDatabase().getReference("allow").child(loginSp.getString(Global.BIZ_ID, ""))
                .child(loginSp.getString(Global.GROUP_ID, "")).keepSynced(true);

        PersistentDatabase.getDatabase().getReference("helper").child(loginSp.getString(Global.BIZ_ID, ""))
                .child(loginSp.getString(Global.GROUP_ID, "")).keepSynced(true);

        PersistentDatabase.getDatabase()
                .getReference("parent/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "")).keepSynced(true);

        PersistentDatabase.getDatabase().getReference("access/" +
                loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, ""))
                .keepSynced(true);


        PersistentDatabase.getDatabase().getReference("resident/" +
                loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, ""))
                .keepSynced(true);

    }

}
