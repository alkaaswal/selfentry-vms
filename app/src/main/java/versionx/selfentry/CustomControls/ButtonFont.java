package versionx.selfentry.CustomControls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by developer on 12/8/16.
 */
public class ButtonFont extends Button {
    public ButtonFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonFont(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Roboto-Light.ttf");
        setTypeface(tf);
    }

}
