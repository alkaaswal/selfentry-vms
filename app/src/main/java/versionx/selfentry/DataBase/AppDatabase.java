package versionx.selfentry.DataBase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import versionx.selfentry.DataBase.CustomFields.FieldDao;
import versionx.selfentry.DataBase.CustomFields.FieldMappingDao;
import versionx.selfentry.DataBase.CustomFields.FieldOptionsDao;
import versionx.selfentry.DataBase.CustomFields.MultiDepMappingDao;
import versionx.selfentry.DataBase.CustomFields.RestrictFieldDao;
import versionx.selfentry.gettersetter.CustomFields;
import versionx.selfentry.gettersetter.FieldMapping;
import versionx.selfentry.gettersetter.FieldsOptions;
import versionx.selfentry.gettersetter.MultiDepMapping;
import versionx.selfentry.gettersetter.RestrictFields;


@Database(entities = {CustomFields.class, FieldsOptions.class, FieldMapping.class, MultiDepMapping.class, RestrictFields.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {


    public abstract FieldDao fieldDao();

    public abstract FieldMappingDao fieldMappingDao();

    public abstract FieldOptionsDao fieldOptionsDao();

    public abstract MultiDepMappingDao multiDepMappingDao();

   // public abstract AppointmentDao appointmentDao();

    public abstract RestrictFieldDao restrictFieldDao();



   // public abstract AccessDao accessDao();

}