package versionx.selfentry.DataBase.CustomFields;

import java.util.List;

import versionx.selfentry.gettersetter.CustomFields;
import versionx.selfentry.gettersetter.FieldMapping;
import versionx.selfentry.gettersetter.FieldsOptions;


public interface AsyncTaskListner {

    public void OnFieldsLoaded(List<CustomFields> customFields);

    public void OnOptionsLoaded(List<FieldsOptions> customFields);

    public void OnMappingLoaded(List<FieldMapping> mappedOptions, String fId, String flag);

    public void OnDepOptionLoaded(List<FieldsOptions> depOptions);

    //public void OnAppointMentSearchResult(List<Appointment> appointments, String searchTxt);
}
