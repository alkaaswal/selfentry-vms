package versionx.selfentry.DataBase.CustomFields;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import versionx.selfentry.Util.App;
import versionx.selfentry.Util.Global;
import versionx.selfentry.gettersetter.CustomFields;
import versionx.selfentry.gettersetter.FieldMapping;
import versionx.selfentry.gettersetter.FieldsOptions;
import versionx.selfentry.gettersetter.RestrictFields;


public class CustomFieldAsync extends AsyncTask<Void, Void, List<CustomFields>> {

    //Prevent leak
    private Context context;
    private CustomFields customFields;
    private FieldsOptions fieldsOptions;
    FieldMapping fieldMapping;
    String table;
    String action;
    AsyncTaskListner listner;
    SharedPreferences spLogin;


    public CustomFieldAsync(Context context, String action) {
        this.context = context;
        this.action = action;
        spLogin = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

    }

    public void setListner(AsyncTaskListner listner) {
        this.listner = listner;
    }


    @Override
    protected List<CustomFields> doInBackground(Void... params) {
          /*  AgentDao agentDao = MyApp.DatabaseSetup.getDatabase().agentDao();
            return agentDao.agentsCount(email, phone, license);*/

        switch (action) {
            case "GET": {


                List<CustomFields> customFieldsList = App.getDbInstance().fieldDao().getAll();
                List<CustomFields> fieldsList = new ArrayList<>();
                fieldsList.addAll(customFieldsList);

               /* if (spLogin.getString(Global.FORM_RESTRICT_ID, "0").equals("0")) {
                    *//*List<RestrictFields> restrictFieldsList = App.getDbInstance().restrictFieldDao().
                            getAll(spLogin.getString(Global.FORM_RESTRICT_ID, "rect1"));*//*
                    List<RestrictFields> restrictFieldsList = App.getDbInstance().restrictFieldDao().
                            getAll("rest1");

                    for (RestrictFields restrictFields : restrictFieldsList) {
                        for (CustomFields customFields : customFieldsList) {
                            if (restrictFields.getfId().equalsIgnoreCase(customFields.getId())) {

                                int j = fieldsList.indexOf(customFields);

                                if (restrictFields.isDel()) {
                                    fieldsList.remove(j);
                                } else {
                                        customFields.setHide(restrictFields.isHide());
                                    customFields.setRequired(restrictFields.isReq());
                                    fieldsList.set(j, customFields);
                                }
                            }
                        }
                    }
                    customFieldsList.clear();
                    customFieldsList.addAll(fieldsList);
                }*/
                return customFieldsList;
            }


        }


        return null;
    }

    @Override
    protected void onPostExecute(List result) {
        super.onPostExecute(result);

        if (result != null) {
            listner.OnFieldsLoaded(result);

        }
    }


}
