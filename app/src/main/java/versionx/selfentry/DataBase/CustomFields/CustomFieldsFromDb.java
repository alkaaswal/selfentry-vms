package versionx.selfentry.DataBase.CustomFields;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import versionx.selfentry.Util.App;
import versionx.selfentry.Util.Global;
import versionx.selfentry.gettersetter.CustomFields;
import versionx.selfentry.gettersetter.FieldInfo;
import versionx.selfentry.gettersetter.FieldMapping;
import versionx.selfentry.gettersetter.FieldsOptions;
import versionx.selfentry.gettersetter.MultiDepMapping;

import static versionx.selfentry.Util.Global.SCREEN_TYPE_MEET;
import static versionx.selfentry.Util.Global.SCREEN_TYPE_WHOCAME;


public class CustomFieldsFromDb {
    Context context;


    public CustomFieldsFromDb(Context context) {
        this.context = context;


    }


 /*   public void getCustomFields(DatabaseReference dbUrl) {


        dbUrl.orderByChild("o").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {


                for (DataSnapshot dataSnapshot : dataSnapshot1.getChildren()) {


                    CustomFields customFields = new CustomFields();
                    if (dataSnapshot.hasChild("_h"))
                        customFields.setFieldGrp(SCREEN_TYPE_MEET);
                    else
                        customFields.setFieldGrp(SCREEN_TYPE_WHOCAME);
                    customFields.setName(dataSnapshot.child("nm").getValue(String.class));
                    customFields.setId(dataSnapshot.getKey());
                    if (dataSnapshot.hasChild("hide"))
                        customFields.setHide(dataSnapshot.child("hide").getValue(Boolean.class));
                    else
                        customFields.setHide(false);

                    if (dataSnapshot.child("re").exists())
                        customFields.setReenter(dataSnapshot.child("re").getValue(Boolean.class));
                    else
                        customFields.setReenter(false);

                    customFields.setOrder(dataSnapshot.child("o").getValue(Integer.class));

                    customFields.setType(dataSnapshot.child("typ").getValue(String.class));
                    if (dataSnapshot.hasChild("val"))
                        customFields.setVal(dataSnapshot.child("val").getValue(String.class));
                    else
                        customFields.setVal("");
                    if (dataSnapshot.hasChild("dep"))
                        customFields.setDep(dataSnapshot.child("dep").getValue(Boolean.class));
                    else
                        customFields.setDep(false);
                    if (dataSnapshot.hasChild("_h"))
                        customFields.setFieldGrp(customFields.GRP_TOMEET);
                    else
                        customFields.setFieldGrp(customFields.GRP_WHOCAME);
                    if (dataSnapshot.hasChild("req"))
                        customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));

                    if (dataSnapshot.hasChild("srch"))
                        customFields.setSrch(dataSnapshot.child("srch").getValue(Boolean.class));
                    else
                        customFields.setSrch(false);
                    if (dataSnapshot.hasChild("edt"))
                        customFields.setEdt(dataSnapshot.child("edt").getValue(Boolean.class));
                    else
                        customFields.setEdt(false);

                    if (dataSnapshot.hasChild("lbl"))
                        customFields.setLbl(dataSnapshot.child("lbl").getValue(String.class));

                    if (dataSnapshot.hasChild("dsbl"))
                        customFields.setDsbl(dataSnapshot.child("dsbl").getValue(Boolean.class));
                    else
                        customFields.setDsbl(true);


                    if (dataSnapshot.hasChild("mod"))
                        customFields.setMod(dataSnapshot.child("mod").getValue(String.class));

                    if (dataSnapshot.child("typ").getValue(String.class).equals(Global.DROPDOWN)) {
                        for (DataSnapshot list : dataSnapshot.child("opt").getChildren()) {
                            FieldsOptions fieldsOptions = new FieldsOptions();
                            fieldsOptions.setfId(dataSnapshot.getKey());
                            fieldsOptions.setName(list.getValue().toString());
                            fieldsOptions.setOptionId(list.getKey());
                            new AgentAsyncTask(context, fieldsOptions, FieldsOptions.class.getName()).execute();

                        }
                    }

                    new AgentAsyncTask(context, customFields, CustomFields.class.getName(), "INSERT").execute();

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





    }
*/

    public void getCustomFields(DatabaseReference dbUrl) {


        dbUrl.orderByChild("o").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


                CustomFields customFields = new CustomFields();
                if (dataSnapshot.hasChild("_h"))
                    customFields.setFieldGrp(SCREEN_TYPE_MEET);
                else
                    customFields.setFieldGrp(SCREEN_TYPE_WHOCAME);
                customFields.setName(dataSnapshot.child("nm").getValue(String.class));
                customFields.setId(dataSnapshot.getKey());
                if (dataSnapshot.hasChild("hide"))
                    customFields.setHide(dataSnapshot.child("hide").getValue(Boolean.class));
                else
                    customFields.setHide(false);

                if (dataSnapshot.child("re").exists())
                    customFields.setReenter(dataSnapshot.child("re").getValue(Boolean.class));
                else
                    customFields.setReenter(false);

                if (dataSnapshot.child("req").exists())
                    customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));
                else
                    customFields.setRequired(false);

                customFields.setOrder(dataSnapshot.child("o").getValue(Integer.class));

                customFields.setType(dataSnapshot.child("typ").getValue(String.class));
                if (dataSnapshot.hasChild("val"))
                    customFields.setVal(dataSnapshot.child("val").getValue(String.class));
                else
                    customFields.setVal("");

                if (dataSnapshot.hasChild("_h"))
                    customFields.setFieldGrp(customFields.GRP_TOMEET);
                else
                    customFields.setFieldGrp(customFields.GRP_WHOCAME);
                if (dataSnapshot.hasChild("req"))
                    customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));

                if (dataSnapshot.hasChild("srch"))
                    customFields.setSrch(dataSnapshot.child("srch").getValue(Boolean.class));
                else
                    customFields.setSrch(false);
                if (dataSnapshot.hasChild("edt"))
                    customFields.setEdt(dataSnapshot.child("edt").getValue(Boolean.class));
                else
                    customFields.setEdt(false);

                if (dataSnapshot.hasChild("lbl"))
                    customFields.setLbl(dataSnapshot.child("lbl").getValue(String.class));

                if (dataSnapshot.hasChild("dsbl"))
                    customFields.setDsbl(dataSnapshot.child("dsbl").getValue(Boolean.class));
                else
                    customFields.setDsbl(true);
                if (dataSnapshot.hasChild("depRef")) {
                    String depIds = "";
                    for (DataSnapshot depSnap : dataSnapshot.child("depRef").getChildren()) {
                        if (depIds.isEmpty())
                            depIds = depSnap.getKey();
                        else
                            depIds = depIds + "," + depSnap.getKey();
                    }
                    customFields.setDepFids(depIds);
                }

                if (dataSnapshot.hasChild("dep")) {
                    customFields.setDep(dataSnapshot.child("dep").getValue(boolean.class));
                } else {
                    customFields.setDep(false);
                }


                if (dataSnapshot.hasChild("mod"))
                    customFields.setMod(dataSnapshot.child("mod").getValue(String.class));

                if (dataSnapshot.child("typ").getValue(String.class).equals(Global.DROPDOWN)
                        || dataSnapshot.child("typ").getValue(String.class).equals(Global.RADIOBUTTON)) {
                    for (DataSnapshot list : dataSnapshot.child("opt").getChildren()) {
                        FieldsOptions fieldsOptions = new FieldsOptions();
                        fieldsOptions.setfId(dataSnapshot.getKey());
                        fieldsOptions.setName(list.getValue().toString());
                        fieldsOptions.setOptionId(list.getKey());
                        fieldsOptions.setTyp(dataSnapshot.child("typ").getValue(String.class));
                        new AgentAsyncTask(context, fieldsOptions, FieldsOptions.class.getName(),"INSERT").execute();

                    }
                }

                new AgentAsyncTask(context, customFields, CustomFields.class.getName(), "INSERT").execute();


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


                CustomFields customFields = new CustomFields();
                if (dataSnapshot.hasChild("_h"))
                    customFields.setFieldGrp(SCREEN_TYPE_MEET);
                else
                    customFields.setFieldGrp(SCREEN_TYPE_WHOCAME);
                customFields.setName(dataSnapshot.child("nm").getValue(String.class));
                customFields.setId(dataSnapshot.getKey());
                if (dataSnapshot.hasChild("hide"))
                    customFields.setHide(dataSnapshot.child("hide").getValue(Boolean.class));
                else
                    customFields.setHide(false);

                if (dataSnapshot.child("re").exists())
                    customFields.setReenter(dataSnapshot.child("re").getValue(Boolean.class));
                else
                    customFields.setReenter(false);

                customFields.setOrder(dataSnapshot.child("o").getValue(Integer.class));
                if (dataSnapshot.child("req").exists())
                    customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));
                else
                    customFields.setRequired(false);

                customFields.setType(dataSnapshot.child("typ").getValue(String.class));
                if (dataSnapshot.hasChild("val"))
                    customFields.setVal(dataSnapshot.child("val").getValue(String.class));
                else
                    customFields.setVal("");

                if (dataSnapshot.hasChild("_h"))
                    customFields.setFieldGrp(customFields.GRP_TOMEET);
                else
                    customFields.setFieldGrp(customFields.GRP_WHOCAME);
                if (dataSnapshot.hasChild("req"))
                    customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));

                if (dataSnapshot.hasChild("srch"))
                    customFields.setSrch(dataSnapshot.child("srch").getValue(Boolean.class));
                else
                    customFields.setSrch(false);
                if (dataSnapshot.hasChild("edt"))
                    customFields.setEdt(dataSnapshot.child("edt").getValue(Boolean.class));
                else
                    customFields.setEdt(false);

                if (dataSnapshot.hasChild("lbl"))
                    customFields.setLbl(dataSnapshot.child("lbl").getValue(String.class));

                if (dataSnapshot.hasChild("dsbl"))
                    customFields.setDsbl(dataSnapshot.child("dsbl").getValue(Boolean.class));
                else
                    customFields.setDsbl(true);

                if (dataSnapshot.hasChild("depRef")) {
                    String depIds = "";
                    for (DataSnapshot depSnap : dataSnapshot.child("depRef").getChildren()) {
                        if (depIds.isEmpty())
                            depIds = depSnap.getKey();
                        else
                            depIds = depIds + "," + depSnap.getKey();
                    }
                    customFields.setDepFids(depIds);
                }

                if (dataSnapshot.hasChild("dep")) {
                    customFields.setDep(dataSnapshot.child("dep").getValue(boolean.class));
                } else {
                    customFields.setDep(false);
                }


                if (dataSnapshot.hasChild("mod"))
                    customFields.setMod(dataSnapshot.child("mod").getValue(String.class));

                if (dataSnapshot.child("typ").getValue(String.class).equals(Global.DROPDOWN)
                        || dataSnapshot.child("typ").getValue(String.class).equals(Global.RADIOBUTTON)) {
                    for (DataSnapshot list : dataSnapshot.child("opt").getChildren()) {
                        FieldsOptions fieldsOptions = new FieldsOptions();
                        fieldsOptions.setfId(dataSnapshot.getKey());
                        fieldsOptions.setName(list.getValue().toString());
                        fieldsOptions.setOptionId(list.getKey());
                        fieldsOptions.setTyp(dataSnapshot.child("typ").getValue(String.class));
                        new AgentAsyncTask(context, fieldsOptions, FieldsOptions.class.getName(),"UPDATE").execute();

                    }
                }


                new AgentAsyncTask(context, customFields, CustomFields.class.getName(), "UPDATE").execute();


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {


                CustomFields customFields = new CustomFields();
                if (dataSnapshot.hasChild("_h"))
                    customFields.setFieldGrp(SCREEN_TYPE_MEET);
                else
                    customFields.setFieldGrp(SCREEN_TYPE_WHOCAME);
                customFields.setName(dataSnapshot.child("nm").getValue(String.class));
                customFields.setId(dataSnapshot.getKey());
                if (dataSnapshot.hasChild("hide"))
                    customFields.setHide(dataSnapshot.child("hide").getValue(Boolean.class));
                else
                    customFields.setHide(false);

                if (dataSnapshot.child("re").exists())
                    customFields.setReenter(dataSnapshot.child("re").getValue(Boolean.class));
                else
                    customFields.setReenter(false);

                customFields.setOrder(dataSnapshot.child("o").getValue(Integer.class));
                if (dataSnapshot.child("req").exists())
                    customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));
                else
                    customFields.setRequired(false);

                customFields.setType(dataSnapshot.child("typ").getValue(String.class));
                if (dataSnapshot.hasChild("val"))
                    customFields.setVal(dataSnapshot.child("val").getValue(String.class));
                else
                    customFields.setVal("");

                if (dataSnapshot.hasChild("_h"))
                    customFields.setFieldGrp(customFields.GRP_TOMEET);
                else
                    customFields.setFieldGrp(customFields.GRP_WHOCAME);
                if (dataSnapshot.hasChild("req"))
                    customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));

                if (dataSnapshot.hasChild("srch"))
                    customFields.setSrch(dataSnapshot.child("srch").getValue(Boolean.class));
                else
                    customFields.setSrch(false);
                if (dataSnapshot.hasChild("edt"))
                    customFields.setEdt(dataSnapshot.child("edt").getValue(Boolean.class));
                else
                    customFields.setEdt(false);

                if (dataSnapshot.hasChild("lbl"))
                    customFields.setLbl(dataSnapshot.child("lbl").getValue(String.class));

                if (dataSnapshot.hasChild("dsbl"))
                    customFields.setDsbl(dataSnapshot.child("dsbl").getValue(Boolean.class));
                else
                    customFields.setDsbl(true);

                if (dataSnapshot.hasChild("depRef")) {
                    String depIds = "";
                    for (DataSnapshot depSnap : dataSnapshot.child("depRef").getChildren()) {
                        if (depIds.isEmpty())
                            depIds = depSnap.getKey();
                        else
                            depIds = depIds + "," + depSnap.getKey();
                    }
                    customFields.setDepFids(depIds);
                }

                if (dataSnapshot.hasChild("dep")) {
                    customFields.setDep(dataSnapshot.child("dep").getValue(boolean.class));
                } else {
                    customFields.setDep(false);
                }


                if (dataSnapshot.hasChild("mod"))
                    customFields.setMod(dataSnapshot.child("mod").getValue(String.class));

                if (dataSnapshot.child("typ").getValue(String.class).equals(Global.DROPDOWN)
                        || dataSnapshot.child("typ").getValue(String.class).equals(Global.RADIOBUTTON)) {
                    for (DataSnapshot list : dataSnapshot.child("opt").getChildren()) {
                        FieldsOptions fieldsOptions = new FieldsOptions();
                        fieldsOptions.setfId(dataSnapshot.getKey());
                        fieldsOptions.setName(list.getValue().toString());
                        fieldsOptions.setOptionId(list.getKey());
                        fieldsOptions.setTyp(dataSnapshot.child("typ").getValue(String.class));
                        new AgentAsyncTask(context, fieldsOptions, FieldsOptions.class.getName(),"DEL").execute();

                    }
                }

                new AgentAsyncTask(context, customFields, CustomFields.class.getName(), "DEL").execute();


            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void setMultiDep(DataSnapshot dataSnapshot, String table) {
        for (DataSnapshot child : dataSnapshot.getChildren()) {


            for (DataSnapshot childs : child.getChildren()) {


                for (DataSnapshot opt : childs.getChildren()) {
                    MultiDepMapping multiField = new MultiDepMapping();
                    multiField.setfId(child.getKey());
                    multiField.setRef(childs.getKey());
                    multiField.setOptNm(opt.getValue(String.class));
                    multiField.setOptId(opt.getKey());
                    new AgentAsyncTask(context, multiField, table).execute();

                }


            }


        }

    }


    public void setMapping(DatabaseReference databaseReference) {

       /* databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot child : dataSnapshot.getChildren()) {

                    if (child.hasChild("opt")) {
                        for (DataSnapshot optchild : child.child("opt").getChildren()) {

                            for (DataSnapshot depChild : optchild.getChildren()) {
                                FieldMapping fieldMapping = new FieldMapping();
                                fieldMapping.setfId(child.getKey());
                                fieldMapping.setOptionId(optchild.getKey());

                                fieldMapping.setDepFid(depChild.getKey());
                                String options = "";


                                if (depChild.hasChild("chk"))
                                    fieldMapping.setCheck(depChild.child("chk").getValue(Boolean.class));
                                else {

                                    if (depChild.hasChild("lbl"))
                                        fieldMapping.setLbl(depChild.child("lbl").getValue(String.class));

                                    if (depChild.hasChild("mod"))
                                        fieldMapping.setMod(depChild.child("mod").getValue(String.class));

                                    if (depChild.hasChild("show")) {
                                        fieldMapping.setHide(false);
                                        options = "show";
                                    }


                                    if (depChild.hasChild("hide")) {
                                        fieldMapping.setHide(true);
                                        options = "hide";

                                    }


                                    if (depChild.hasChild("req")) {
                                        fieldMapping.setReq(true);

                                    }


                                    if (depChild.hasChild("opt")) {
                                        for (DataSnapshot depopt : depChild.child("opt").getChildren()) {
                                            if (options.isEmpty())
                                                options = depopt.getKey();
                                            else
                                                options = depopt.getKey() + "," + options;
                                        }

                                    }


                                    if (depChild.hasChild("all")) {
                                        options = "all";
                                    }

                                    fieldMapping.setDepOptions(options);

                                }


                                new AgentAsyncTask(context, fieldMapping, FieldMapping.class.getName()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                            }


                        }
                    }
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot child, @Nullable String s) {


                for (DataSnapshot optchild : child.child("opt").getChildren()) {

                    for (DataSnapshot depChild : optchild.getChildren()) {
                        FieldMapping fieldMapping = new FieldMapping();
                        fieldMapping.setfId(child.getKey());
                        fieldMapping.setOptionId(optchild.getKey());

                        fieldMapping.setDepFid(depChild.getKey());
                        String options = "";


                        if (depChild.hasChild("chk"))
                            fieldMapping.setCheck(depChild.child("chk").getValue(Boolean.class));
                        else {

                            if (depChild.hasChild("lbl"))
                                fieldMapping.setLbl(depChild.child("lbl").getValue(String.class));

                            if (depChild.hasChild("mod"))
                                fieldMapping.setMod(depChild.child("mod").getValue(String.class));

                            if (depChild.hasChild("show")) {
                                fieldMapping.setHide(false);
                                options = "show";
                            }


                            if (depChild.hasChild("hide")) {
                                fieldMapping.setHide(true);
                                options = "hide";

                            }


                            if (depChild.hasChild("req")) {
                                fieldMapping.setReq(true);

                            }


                            if (depChild.hasChild("opt")) {
                                for (DataSnapshot depopt : depChild.child("opt").getChildren()) {
                                    if (options.isEmpty())
                                        options = depopt.getKey();
                                    else
                                        options = depopt.getKey() + "," + options;
                                }

                            }


                            if (depChild.hasChild("all")) {
                                options = "all";
                            }

                            fieldMapping.setDepOptions(options);

                        }


                        new AgentAsyncTask(context, fieldMapping, FieldMapping.class.getName(), "INSERT").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    }


                }


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot child, @Nullable String s) {

                for (DataSnapshot optchild : child.child("opt").getChildren()) {

                    for (DataSnapshot depChild : optchild.getChildren()) {
                        FieldMapping fieldMapping = new FieldMapping();
                        fieldMapping.setfId(child.getKey());
                        fieldMapping.setOptionId(optchild.getKey());

                        fieldMapping.setDepFid(depChild.getKey());
                        String options = "";


                        if (depChild.hasChild("chk"))
                            fieldMapping.setCheck(depChild.child("chk").getValue(Boolean.class));
                        else {

                            if (depChild.hasChild("lbl"))
                                fieldMapping.setLbl(depChild.child("lbl").getValue(String.class));

                            if (depChild.hasChild("mod"))
                                fieldMapping.setMod(depChild.child("mod").getValue(String.class));

                            if (depChild.hasChild("show")) {
                                fieldMapping.setHide(false);
                                options = "show";
                            }


                            if (depChild.hasChild("hide")) {
                                fieldMapping.setHide(true);
                                options = "hide";

                            }


                            if (depChild.hasChild("req")) {
                                fieldMapping.setReq(true);

                            }


                            if (depChild.hasChild("opt")) {
                                for (DataSnapshot depopt : depChild.child("opt").getChildren()) {
                                    if (options.isEmpty())
                                        options = depopt.getKey();
                                    else
                                        options = depopt.getKey() + "," + options;
                                }

                            }


                            if (depChild.hasChild("all")) {
                                options = "all";
                            }

                            fieldMapping.setDepOptions(options);

                        }


                        new AgentAsyncTask(context, fieldMapping, FieldMapping.class.getName(),"UPDATE").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    }


                }


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot child) {



                for (DataSnapshot optchild : child.child("opt").getChildren()) {

                    for (DataSnapshot depChild : optchild.getChildren()) {
                        FieldMapping fieldMapping = new FieldMapping();
                        fieldMapping.setfId(child.getKey());
                        fieldMapping.setOptionId(optchild.getKey());

                        fieldMapping.setDepFid(depChild.getKey());
                        String options = "";


                        if (depChild.hasChild("chk"))
                            fieldMapping.setCheck(depChild.child("chk").getValue(Boolean.class));
                        else {

                            if (depChild.hasChild("lbl"))
                                fieldMapping.setLbl(depChild.child("lbl").getValue(String.class));

                            if (depChild.hasChild("mod"))
                                fieldMapping.setMod(depChild.child("mod").getValue(String.class));

                            if (depChild.hasChild("show")) {
                                fieldMapping.setHide(false);
                                options = "show";
                            }


                            if (depChild.hasChild("hide")) {
                                fieldMapping.setHide(true);
                                options = "hide";

                            }


                            if (depChild.hasChild("req")) {
                                fieldMapping.setReq(true);

                            }


                            if (depChild.hasChild("opt")) {
                                for (DataSnapshot depopt : depChild.child("opt").getChildren()) {
                                    if (options.isEmpty())
                                        options = depopt.getKey();
                                    else
                                        options = depopt.getKey() + "," + options;
                                }

                            }


                            if (depChild.hasChild("all")) {
                                options = "all";
                            }

                            fieldMapping.setDepOptions(options);

                        }


                        new AgentAsyncTask(context, fieldMapping, FieldMapping.class.getName(),"DEL").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    }


                }


            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }


    private class AgentAsyncTask extends AsyncTask<Void, Void, Boolean> {

        //Prevent leak
        private Context context;
        private CustomFields customFields;
        private FieldsOptions fieldsOptions;
        FieldMapping fieldMapping;
        MultiDepMapping multiDepMapping;
        String table;
        String action;


        public AgentAsyncTask(Context context, CustomFields customFields, String table, String action) {
            this.context = context;
            this.customFields = customFields;
            this.table = table;
            this.action = action;
        }

        public AgentAsyncTask(Context context, FieldsOptions fieldsOptions, String table, String action) {
            this.context = context;
            this.fieldsOptions = fieldsOptions;
            this.table = table;
            this.action=action;

        }


        public AgentAsyncTask(Context context, FieldMapping fieldMapping, String table, String action) {
            this.context = context;
            this.fieldMapping = fieldMapping;
            this.table = table;
            this.action=action;

        }

        public AgentAsyncTask(Context context, MultiDepMapping multiDepMapping, String table) {
            this.context = context;
            this.multiDepMapping = multiDepMapping;
            this.table = table;
        }


        @Override
        protected Boolean doInBackground(Void... params) {

            if (table.equals(CustomFields.class.getName())) {
                if (action.equals("INSERT")) {

                    int i = App.getDbInstance().fieldDao().getFieldById(customFields.getId()).size();

                    if (i == 0)
                        App.getDbInstance().fieldDao().insertAll(customFields);
                    else
                        App.getDbInstance().fieldDao().update(customFields);
                } else if (action.equals("UPDATE")) {
                    App.getDbInstance().fieldDao().update(customFields);
                } else if (action.equals("DEL")) {
                    App.getDbInstance().fieldDao().delete(customFields.getId());
                }
            }

            if (table.equals(FieldsOptions.class.getName())) {

                if (action.equals("INSERT")) {

                    int i = App.getDbInstance().fieldOptionsDao().getItemId(fieldsOptions.getOptionId()).size();

                    if (i == 0)
                        App.getDbInstance().fieldOptionsDao().insertAll(fieldsOptions);
                    else
                        App.getDbInstance().fieldOptionsDao().update(fieldsOptions);
                }
                else if(action.equals("UPDATE"))
                {
                    App.getDbInstance().fieldOptionsDao().update(fieldsOptions);
                }
                else if(action.equals("DEL"))
                {
                    App.getDbInstance().fieldOptionsDao().delete(fieldsOptions.getOptionId());
                }
            }

            if (table.equals(FieldMapping.class.getName())) {


                int i = App.getDbInstance().fieldMappingDao().getMappingData(fieldMapping.getOptionId(), fieldMapping.getDepFid()).size();
                if (action.equals("INSERT")) {

                    if (i == 0)
                        App.getDbInstance().fieldMappingDao().insertAll(fieldMapping);
                    else
                        App.getDbInstance().fieldMappingDao().update(fieldMapping);
                }
                else if(action.equals("UPDATE"))
                {
                        App.getDbInstance().fieldMappingDao().update(fieldMapping);
                }
                else if(action.equals("DEL"))
                {
                    App.getDbInstance().fieldMappingDao().delete(fieldMapping.getOptionId(), fieldMapping.getDepFid());
                }
            }

            if (table.equals(MultiDepMapping.class.getName())) {
                int i = App.getDbInstance().multiDepMappingDao().getDependency(multiDepMapping.getRef(), multiDepMapping.getOptId(), multiDepMapping.getfId()).size();

                if (i == 0) {
                    App.getDbInstance().multiDepMappingDao().insertAll(multiDepMapping);
                } else
                    App.getDbInstance().multiDepMappingDao().update(multiDepMapping);
            }

            return true;
        }
    }


}


