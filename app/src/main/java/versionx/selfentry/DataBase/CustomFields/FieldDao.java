package versionx.selfentry.DataBase.CustomFields;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import versionx.selfentry.gettersetter.CustomFields;


@Dao
public interface FieldDao {


    @Query("SELECT * FROM CustomFields ORDER BY `order` ASC")
    List<CustomFields> getAll();

    @Query("SELECT * FROM CustomFields WHERE `id` = :fId")
    List<CustomFields> getFieldById(String fId);

    @Query("DELETE  FROM CustomFields WHERE `id` = :fId")
   void delete(String fId);


    @Insert
    void insertAll(CustomFields... fields);


    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(CustomFields... fields);



}
