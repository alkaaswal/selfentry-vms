package versionx.selfentry.DataBase.CustomFields;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import versionx.selfentry.Util.App;
import versionx.selfentry.Util.Global;
import versionx.selfentry.gettersetter.CustomFields;
import versionx.selfentry.gettersetter.FieldMapping;
import versionx.selfentry.gettersetter.FieldsOptions;
import versionx.selfentry.gettersetter.MultiDepMapping;
import versionx.selfentry.gettersetter.RestrictFields;


public class FieldMappingAsync extends AsyncTask<Void, Void, List<FieldMapping>> {

    //Prevent leak
    private Context context;

    String table;
    String action;
    FieldsOptions fieldsOptions;
    AsyncTaskListner listner;
    HashMap<String, String> selectedItem;
    SharedPreferences loginSp;
    String flag;
    String depFieldsIds;
    CustomFields customFields;


    public FieldMappingAsync(Context context, String action, FieldsOptions fieldsOptions,
                             HashMap<String, String> selectedItem, String flag, SharedPreferences loginSp, CustomFields customFields) {
        this.context = context;
        this.action = action;
        this.fieldsOptions = fieldsOptions;
        this.selectedItem = selectedItem;
        this.loginSp = loginSp;
        this.flag = flag;
        this.customFields = customFields;

    }


    @Override
    protected List<FieldMapping> doInBackground(Void... params) {
          /*  AgentDao agentDao = MyApp.DatabaseSetup.getDatabase().agentDao();
            return agentDao.agentsCount(email, phone, license);*/

        switch (action) {
            case "GET": {
                List<FieldMapping> options = App.getDbInstance().fieldMappingDao().getMapping(fieldsOptions.getOptionId());



                if(customFields.getDepFids()!=null) {

                    ArrayList<String> optList = new ArrayList<>();

                    for (FieldMapping opt : options) {
                        optList.add(opt.getDepFid());
                    }

                    // this mapping is for default options, So if mapping doesn't exits for any option it will take from fields table.

                    String[] depFids = customFields.getDepFids().split(",");

                    for (String fids : depFids) {

                        List<CustomFields> depFields = App.getDbInstance().fieldDao().getFieldById(fids);

                        FieldMapping fieldMapping = new FieldMapping();

                        fieldMapping.setDepFid(fids);
                        fieldMapping.setfId(fieldsOptions.getfId());
                        fieldMapping.setOptionId(fieldsOptions.getOptionId());
                        fieldMapping.setMod(depFields.get(0).getMod());
                        fieldMapping.setAllDepIds(depFields.get(0).getDepFids());
                        if (depFields.get(0).isDep())
                            fieldMapping.setDepOptions("hide");
                        else {
                            fieldMapping.setDepOptions("all");
                        }
                        fieldMapping.setReq(depFields.get(0).isRequired());
                        fieldMapping.setLbl(depFields.get(0).getLbl());
                        fieldMapping.setHide(depFields.get(0).isHide());
                        if (optList.size() == 0 || (optList.size() > 0 && !optList.contains(fids)))
                            options.add(fieldMapping);
                    }
                }





             /*    List<MultiDepMapping> options1;
                List<RestrictFields> restrictFieldsList = null;
                String filteredOpt = new String();

               if (options.size() > 0) {
                    for (FieldMapping mapping : options) {


                        if (loginSp.getString(Global.FORM_RESTRICT_ID, "0").equals("0")) {
                            restrictFieldsList = App.getDbInstance().restrictFieldDao().getAll(loginSp.getString(Global.FORM_RESTRICT_ID, "rest1"));

                            for (RestrictFields restrictFields : restrictFieldsList) {
                                if (restrictFields.getfId().equals(mapping.getDepFid())) {
                                    mapping.setDepOptions(restrictFields.getOpt());
                                    options.clear();
                                    options.add(mapping);

                                    break;
                                } else {
                                    if (mapping.isCheck()) {
                                        options1 = App.getDbInstance().multiDepMappingDao().getOptions(mapping.getfId(), selectedItem.values().toArray(new String[0]));

                                        for (MultiDepMapping multiDepMapping : options1) {
                                            if (mapping.getDepOptions().contains(multiDepMapping.getOptId())) {
                                                if (filteredOpt.isEmpty())
                                                    filteredOpt = multiDepMapping.getOptId();
                                                else
                                                    filteredOpt = filteredOpt + "," + multiDepMapping.getOptId();

                                            }
                                        }

                                        mapping.setDepOptions(filteredOpt);
                                        options.clear();
                                        options.add(mapping);


                                    }
                                }


                            }


                        } else {
                            if (mapping.isCheck()) {
                                options1 = App.getDbInstance().multiDepMappingDao().getOptions(mapping.getfId(), selectedItem.values().toArray(new String[0]));

                                for (MultiDepMapping multiDepMapping : options1) {
                                    if (mapping.getDepOptions().contains(multiDepMapping.getOptId())) {
                                        if (filteredOpt.isEmpty())
                                            filteredOpt = multiDepMapping.getOptId();
                                        else
                                            filteredOpt = filteredOpt + "," + multiDepMapping.getOptId();

                                    }
                                }

                                mapping.setDepOptions(filteredOpt);
                                options.clear();
                                options.add(mapping);


                            }

                        }


                    }
                }*/


                return options;
            }


        }


        return null;
    }

    @Override
    protected void onPostExecute(List result) {
        super.onPostExecute(result);

        if (result != null) {
            System.out.println("mapping db " + result);
            listner.OnMappingLoaded(result, fieldsOptions.getfId(), flag);

        }
    }


    public void setListner(AsyncTaskListner listner) {
        this.listner = listner;
    }
}
