package versionx.selfentry.DataBase.CustomFields;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import versionx.selfentry.gettersetter.FieldMapping;


@Dao
public interface FieldMappingDao {

    @Query("SELECT * from FieldMapping WHERE `optionId` =:optId AND `depFid` =:depFid")
    List<FieldMapping> getMappingData(String optId,String depFid);


    @Query("SELECT * from FieldMapping WHERE `optionId` =:optId")
    List<FieldMapping> getMapping(String optId);


    /*@Query("SELECT * FROM Fieldm INNER JOIN B on B.f = A.f")

    List<FieldMapping> getMultiDepOptions(String fId, String optId);*/

    @Insert
    void insertAll(FieldMapping... fields);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(FieldMapping... fields);


    @Query("DELETE  FROM FieldMapping WHERE `optionId` =:optId AND `depFid` =:depFid")
    void delete(String optId, String depFid);


}
