package versionx.selfentry.DataBase.CustomFields;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import versionx.selfentry.Util.App;
import versionx.selfentry.Util.Global;
import versionx.selfentry.gettersetter.FieldsOptions;
import versionx.selfentry.gettersetter.RestrictFields;


public class FieldOptionsAsync extends AsyncTask<Void, Void, List<FieldsOptions>> {

    //Prevent leak
    private Context context;

    String table;
    String action, fId;
    AsyncTaskListner listner;
    String[] depOptIds, restrictOpts;
    SharedPreferences loginSp;


    public FieldOptionsAsync(Context context, String action, String depOptIds, String fId) {
        this.context = context;
        this.action = action;
        if (depOptIds != null && !depOptIds.isEmpty())
            this.depOptIds = depOptIds.trim().split(",");
        loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        this.fId=fId;


    }




    public void setListner(AsyncTaskListner listner) {
        this.listner = listner;
    }


    @Override
    protected List<FieldsOptions> doInBackground(Void... params) {

        List<FieldsOptions> options = null;
        switch (action) {
            case "GETALL": {
                options = App.getDbInstance().fieldOptionsDao().getAll();
                ArrayList<FieldsOptions> fieldsOptions = new ArrayList<>();
                fieldsOptions.addAll(options);
              /*  if (loginSp.getString(Global.FORM_RESTRICT_ID, "0").equals("0")) {
                   // List<RestrictFields> restrictFieldsList = App.getDbInstance().restrictFieldDao().getAll(loginSp.getString(Global.FORM_RESTRICT_ID, "rect1"));

                    List<RestrictFields> restrictFieldsList = App.getDbInstance().restrictFieldDao().getAll("rest1");

                    for (RestrictFields restrictFields : restrictFieldsList) {

                        this.restrictOpts = restrictFields.getRestrictId().split(",");
                        for (String optId : restrictOpts) {
                            int i = 0;

                            for (FieldsOptions options1 : options) {
                                if (options1.getOptionId().equalsIgnoreCase(optId)) {


                                    fieldsOptions.get(i).setOptionId(optId);
                                    fieldsOptions.set(i, null);
                                    break;
                                }


                            }
                            i++;
                        }
                    }
                    options.clear();
                    options.addAll(fieldsOptions);
                }*/


                return options;
            }
            case "GETDEP": {
                if (depOptIds != null) {
                    options = App.getDbInstance().fieldOptionsDao().getDependencyOptions(depOptIds);
                }
                return options;
            }


        }


        return null;
    }

    @Override
    protected void onPostExecute(List result) {
        super.onPostExecute(result);

        if (result != null) {
            if (action.equals("GETALL")) {
                listner.OnOptionsLoaded(result);
            } else {
                listner.OnDepOptionLoaded(result);
            }

        }
    }


}
