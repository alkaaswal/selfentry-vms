package versionx.selfentry.DataBase.CustomFields;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import versionx.selfentry.gettersetter.FieldsOptions;


@Dao
public interface FieldOptionsDao {

    @Query("SELECT * FROM fieldsoptions WHERE optionId = :optId LIMIT 1")
    List<FieldsOptions> getItemId(String optId);
    @Insert
    void insertAll(FieldsOptions... fields);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(FieldsOptions... fields);


    @Query("SELECT * FROM FieldsOptions WHERE `fId`=:fId")
    List<FieldsOptions> getOptionsbyField(String fId);

    @Query("SELECT * FROM FieldsOptions WHERE `optionId` IN (:ids)")
    List<FieldsOptions> getDependencyOptions(String[] ids);



    @Query("SELECT * FROM FieldsOptions")
    List<FieldsOptions> getAll();


    @Query("DELETE  FROM FieldsOptions WHERE `optionId` =:optId")
    void delete(String optId);
}
