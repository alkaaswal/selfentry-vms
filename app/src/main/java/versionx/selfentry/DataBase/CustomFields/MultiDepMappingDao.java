package versionx.selfentry.DataBase.CustomFields;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import versionx.selfentry.gettersetter.MultiDepMapping;


@Dao
public interface MultiDepMappingDao {

    @Query("SELECT * from MultiDepMapping WHERE `ref` =:ref AND `optId`=:optId AND `fId`=:fId")
    List<MultiDepMapping> getDependency(String ref, String optId, String fId);


    @Query("SELECT * from MultiDepMapping WHERE `fId` =:fId AND `ref` IN (:optId)")
    List<MultiDepMapping> getOptions(String fId, String[] optId);


    @Insert
    void insertAll(MultiDepMapping... fields);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(MultiDepMapping... fields);
}
