package versionx.selfentry.DataBase.CustomFields;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import versionx.selfentry.Util.App;
import versionx.selfentry.gettersetter.RestrictFields;


public class RestrictFieldAsync extends AsyncTask<Void, Void, List<RestrictFields>> {

    RestrictFields restrictFields;
    Context context;
    String action;

    public RestrictFieldAsync(Context context, RestrictFields restrictFields, String action) {
        this.context = context;
        this.restrictFields = restrictFields;
        this.action = action;
    }

    @Override
    protected List<RestrictFields> doInBackground(Void... voids) {

        switch (action) {
            case "INSERT":
                if (App.getDbInstance().restrictFieldDao().getRestrictField(restrictFields.getfId()).size() == 0)
                    App.getDbInstance().restrictFieldDao().InsertAll(restrictFields);
                else
                    App.getDbInstance().restrictFieldDao().updateData(restrictFields);
                break;
        }

        return null;
    }
}
