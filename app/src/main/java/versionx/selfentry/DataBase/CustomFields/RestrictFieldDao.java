package versionx.selfentry.DataBase.CustomFields;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import versionx.selfentry.gettersetter.RestrictFields;


@Dao
public interface RestrictFieldDao {


    @Insert
    void InsertAll(RestrictFields... restrictFields);

    @Query("Select * FROM RestrictFields WHERE `fId`=:fId")
    List<RestrictFields> getRestrictField(String fId);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateData(RestrictFields... restrictFields);

    @Query("Select * FROM RestrictFields WHERE `restrictId`=:rectId")
    List<RestrictFields> getAll(String rectId);
}
