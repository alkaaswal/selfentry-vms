package versionx.selfentry.DataBase.CustomFields;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import versionx.selfentry.Util.Global;
import versionx.selfentry.Util.PersistentDatabase;
import versionx.selfentry.gettersetter.RestrictFields;


public class RestrictFieldDb {

    Context context;
    RestrictFields restrictFields;

    public RestrictFieldDb(final Context context) {
        SharedPreferences sp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        PersistentDatabase.getDatabase().getReference("field").child(sp.getString(Global.BIZ_ID, ""))
                .child(sp.getString(Global.GROUP_ID, "")).child("restrict").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot restrict : dataSnapshot.getChildren())
                for (DataSnapshot child : restrict.getChildren()) {
                    RestrictFields restrictFields = new RestrictFields();
                    restrictFields.setfId(child.getKey());
                    if (child.hasChild("del"))
                        restrictFields.setDel(child.child("del").getValue(Boolean.class));
                    else
                        restrictFields.setDel(false);

                    if(child.hasChild("hide"))
                        restrictFields.setHide(child.child("hide").getValue(Boolean.class));

                    if(child.hasChild("req"))
                        restrictFields.setReq(child.child("req").getValue(Boolean.class));


                    if(child.hasChild("opt"))
                    {
                        String opts = "";
                        for(DataSnapshot optChild : child.child("opt").getChildren())
                        {
                            if(opts.isEmpty())
                                opts = optChild.getKey();
                            else
                                opts = opts+","+ optChild.getKey();

                        }
                        restrictFields.setOpt(opts);
                    }

                    restrictFields.setRestrictId(restrict.getKey());

                    new RestrictFieldAsync(context, restrictFields,"INSERT").execute();


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
