package versionx.selfentry.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;


import java.util.ArrayList;

import versionx.selfentry.Util.PersistentDatabase;
import versionx.selfentry.Util.Global;
import versionx.selfentry.gettersetter.Images;

/**
 * Created by alkaaswal on 6/12/17.
 */

public class ImagesDataSource {
    // Database fields
    Context context;
    SharedPreferences loginsp;
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_IMAGE_PATH, MySQLiteHelper.COLUMN_STORAGE_PATH, MySQLiteHelper.COLUMN_HIS_REF};

    public ImagesDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
        loginsp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);


    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        // this.context = context;
        // dbHelper.onCreate(database);
    }

    public void close() {
        dbHelper.close();
    }


    public long addImage(Images images) {

        long insertId = 0;

        try {


            ContentValues values = new ContentValues();


            values.put(MySQLiteHelper.COLUMN_IMAGE_PATH, images.getImgPath());
            values.put(MySQLiteHelper.COLUMN_HIS_REF, images.getRef());
            values.put(MySQLiteHelper.COLUMN_MOB, images.getMob());
            values.put(MySQLiteHelper.COLUMN_CUST_KEY, images.getCustomKey());
            values.put(MySQLiteHelper.COLUMN_STORAGE_PATH, images.getStoragePath());
            values.put(MySQLiteHelper.COLUMN_SESSION_URI, images.getSessionUri());


            insertId = database.insert(MySQLiteHelper.TABLE_NAME, null,
                    values);
            addPath(insertId, images);
        } catch (Exception e) {


                          Crashlytics.logException(new Exception("database saving error " + loginsp.getString(Global.BIZ_ID , "") + "_"
                    + loginsp.getString(Global.GROUP_ID, "") + "___" + e.toString()));

            PersistentDatabase.getDatabase().getReference("temp").child("noupload").child(loginsp.getString(Global.BIZ_ID, ""))
                    .child(loginsp.getString(Global.GROUP_ID, "")).child(loginsp.getString(Global.UUID, ""))
                    .child("" + images.getRef()).setValue("" + images.getImgPath());




            return -1;
        }


        return insertId;
    }

    public void addPath(long columnID, Images images) {

        for (int i = 0; i < images.getPathRef().size(); i++) {
            ContentValues values = new ContentValues();
            values.put(MySQLiteHelper.COLUMN_ID, columnID);

            values.put(MySQLiteHelper.COLUMN_STORAGE_PATH, images.getPathRef().get(i));

            database.insert(MySQLiteHelper.TABLE_PATH_NAME, null,
                    values);
        }
    }


    public void addSessionURI(int columnId, String sessionUri) {


        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_SESSION_URI, sessionUri);


        database.update(MySQLiteHelper.TABLE_NAME, values, MySQLiteHelper.COLUMN_ID + "=" + columnId, null);


    }


    public Images getSessionURI(int columnId) {

        Images images = null;
        Cursor cursor = database.rawQuery("select " + MySQLiteHelper.COLUMN_SESSION_URI + " from " + MySQLiteHelper.TABLE_NAME + " Where "
                + MySQLiteHelper.COLUMN_ID + "=" + columnId, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            //   do {
            images = new Images();

            images.setSessionUri(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_SESSION_URI)));


            // } while (cursor.moveToNext());

        }

        return images;


    }


    public ArrayList<Images> getAllImages() {

        ArrayList<Images> imagesList = new ArrayList<Images>();
        // database = dbHelper.getReadableDatabase();
        //  database.beginTransaction();
        Cursor cursor = database.rawQuery("select * from " + MySQLiteHelper.TABLE_NAME, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            //   do {
            Images images = new Images();
            images.setId(cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID)));
            images.setCustomKey(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_CUST_KEY)));
            images.setImgPath(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_IMAGE_PATH)));
            images.setStoragePath(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_STORAGE_PATH)));
            images.setSessionUri(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_SESSION_URI)));
            imagesList.add(images);


            // } while (cursor.moveToNext());

        }

        // database.endTransaction();

        cursor.close();

        return imagesList;


    }

    public ArrayList<Images> getLastImg() {

        ArrayList<Images> imagesList = new ArrayList<Images>();
        Cursor cursor = database.rawQuery("select * from " + MySQLiteHelper.TABLE_NAME, null);
        if (cursor.getCount() > 0) {
            cursor.moveToLast();
            //   do {
            Images images = new Images();
            images.setId(cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID)));
            images.setCustomKey(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_CUST_KEY)));
            images.setImgPath(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_IMAGE_PATH)));
            images.setStoragePath(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_STORAGE_PATH)));
            images.setSessionUri(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_SESSION_URI)));
            imagesList.add(images);



        }

        cursor.close();

        return imagesList;


    }


    public boolean isMoreRef(String Path) {
        ArrayList<Images> imagesList = new ArrayList<Images>();
        // database = dbHelper.getReadableDatabase();
        //  database.beginTransaction();
        Cursor cursor = database.rawQuery("select * from " + MySQLiteHelper.TABLE_NAME + " where " + MySQLiteHelper.COLUMN_IMAGE_PATH + "='" + Path + "'", null);


        if (cursor.getCount() >= 1)
            return true;
        else
            return false;


    }


    public ArrayList<String> getAllRefUrls(int columnID) {

        ArrayList<String> imagesList = new ArrayList<String>();

        Cursor cursor = database.rawQuery("select " + MySQLiteHelper.COLUMN_STORAGE_PATH +
                " from " + MySQLiteHelper.TABLE_PATH_NAME + " where " + MySQLiteHelper.COLUMN_ID + "=" + columnID, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                imagesList.add(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_STORAGE_PATH)));


            } while (cursor.moveToNext());

        }


        // database.endTransaction();

        cursor.close();

        return imagesList;


    }


    public ArrayList<String> getAllRefUrls() {

        ArrayList<String> imagesList = new ArrayList<String>();

        Cursor cursor = database.rawQuery("select " + MySQLiteHelper.COLUMN_STORAGE_PATH +
                " from " + MySQLiteHelper.TABLE_PATH_NAME, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                imagesList.add(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_STORAGE_PATH)));

            } while (cursor.moveToNext());

        }


        // database.endTransaction();

        cursor.close();

        return imagesList;


    }


    public void getallData() {

        ArrayList<String> imagesList = new ArrayList<String>();

        Cursor cursor = database.rawQuery("select * from " + MySQLiteHelper.TABLE_PATH_NAME, null);


        // database.endTransaction();

        cursor.close();

        Cursor cursor1 = database.rawQuery("select * from " + MySQLiteHelper.TABLE_NAME, null);


        // database.endTransaction();

        cursor1.close();


    }


    public void deleteImages(int columnID) {


        database.delete(MySQLiteHelper.TABLE_NAME, MySQLiteHelper.COLUMN_ID + "=" + columnID, null);
        database.delete(MySQLiteHelper.TABLE_PATH_NAME, MySQLiteHelper.COLUMN_ID + "=" + columnID, null);
        // database.endTransaction();


    }


    public String getcustomImg(String hiskey, String customKey, String mob) {
        if (hiskey != null && customKey != null && mob != null) {
            Cursor cursor = database.rawQuery("select " + MySQLiteHelper.COLUMN_IMAGE_PATH +
                    " from " + MySQLiteHelper.TABLE_NAME + " where " + MySQLiteHelper.COLUMN_HIS_REF + "='" + hiskey + "' and "
                    + MySQLiteHelper.COLUMN_CUST_KEY + "='" + customKey + "'", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();


                return cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_IMAGE_PATH));


            } else
                return null;

        } else if (hiskey == null && customKey != null && mob != null) {

            Cursor cursor = database.rawQuery("select " + MySQLiteHelper.COLUMN_IMAGE_PATH +
                    " from " + MySQLiteHelper.TABLE_NAME + " where "
                    + MySQLiteHelper.COLUMN_CUST_KEY + "='" + customKey + "' and " + MySQLiteHelper.COLUMN_MOB + "='" + mob + "' ORDER BY " + MySQLiteHelper.COLUMN_ID + " DESC LIMIT 1", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();


                return cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_IMAGE_PATH));


            } else
                return null;

        } else if (hiskey != null && mob != null && customKey == null) {
            Cursor cursor = database.rawQuery("select " + MySQLiteHelper.COLUMN_IMAGE_PATH +
                    " from " + MySQLiteHelper.TABLE_NAME + " where "
                    + MySQLiteHelper.COLUMN_HIS_REF + "='" + hiskey + "' and "
                    + MySQLiteHelper.COLUMN_MOB + "='" + mob + "' and " + MySQLiteHelper.COLUMN_CUST_KEY + " is NULL" +
                    " ORDER BY " + MySQLiteHelper.COLUMN_ID + " DESC LIMIT 1", null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();


                return cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_IMAGE_PATH));


            } else
                return null;

        } else {
            {
                Cursor cursor = database.rawQuery("select " + MySQLiteHelper.COLUMN_IMAGE_PATH +
                        " from " + MySQLiteHelper.TABLE_NAME + " where "
                        + MySQLiteHelper.COLUMN_MOB + "='" + mob + "' and " + MySQLiteHelper.COLUMN_CUST_KEY + " is NULL" +
                        " ORDER BY " + MySQLiteHelper.COLUMN_ID + " DESC LIMIT 1", null);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();


                    return cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_IMAGE_PATH));


                } else
                    return null;

            }
        }
    }


   /* public byte[] getImage(String hisKey) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_NAME,
                allColumns, MySQLiteHelper.COLUMN_HIS_REF + "=?", new String[]{hisKey}, null, null, null);


        return cursor.getBlob(cursor.getColumnIndex(MySQLiteHelper.COL));

    }*/













     /*   public Comment createComment(String comment) {
            ContentValues values = new ContentValues();
            values.put(MySQLiteHelper.COLUMN_COMMENT, comment);
            long insertId = database.insert(MySQLiteHelper.TABLE_COMMENTS, null,
                    values);
            Cursor cursor = database.query(MySQLiteHelper.TABLE_COMMENTS,
                    allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                    null, null, null);
            cursor.moveToFirst();
            Comment newComment = cursorToComment(cursor);
            cursor.close();
            return newComment;
        }

        public void deleteComment(Comment comment) {
            long id = comment.getId();
            System.out.println("Comment deleted with id: " + id);
            database.delete(MySQLiteHelper.TABLE_COMMENTS, MySQLiteHelper.COLUMN_ID
                    + " = " + id, null);
        }

        public List<Comment> getAllComments() {
            List<Comment> comments = new ArrayList<Comment>();

            Cursor cursor = database.query(MySQLiteHelper.TABLE_COMMENTS,
                    allColumns, null, null, null, null, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Comment comment = cursorToComment(cursor);
                comments.add(comment);
                cursor.moveToNext();
            }
            // make sure to close the cursor
            cursor.close();
            return comments;
        }

        private Comment cursorToComment(Cursor cursor) {
            Comment comment = new Comment();
            comment.setId(cursor.getLong(0));
            comment.setComment(cursor.getString(1));
            return comment;
        }*/
}