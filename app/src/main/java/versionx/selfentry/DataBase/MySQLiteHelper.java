package versionx.selfentry.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by alkaaswal on 6/12/17.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {


    public static final String TABLE_PATH_NAME = "ImagesRef";

    public static final String TABLE_NAME = "Images";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_STORAGE_PATH = "firebase_path";
    public static final String COLUMN_IMAGE_PATH = "img_path";
    public static final String COLUMN_HIS_REF = "his_key";
    public static final String COLUMN_MOB = "mobile";
    public static final String COLUMN_CUST_KEY = "custom_key";

    public static final String COLUMN_SESSION_URI = "session_uri";
    //  public static final String COLUMN_VEHICLE_NO = "veh_no";
    //  public static final String COLUMN_IMAGE = "image";

    private static final String DATABASE_NAME = "Images.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table IF NOT EXISTS "
            + TABLE_NAME + "( " + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_HIS_REF + " text, "
            + COLUMN_IMAGE_PATH + " text, "
            + COLUMN_MOB + " text, "
            + COLUMN_CUST_KEY + " text, "
            + COLUMN_SESSION_URI + " text, "
            + COLUMN_STORAGE_PATH + " text );";


    private static final String TABLE_PATH_REF_CREATE = "create table IF NOT EXISTS "
            + TABLE_PATH_NAME + "( " + COLUMN_ID +
            " integer ,"
            + COLUMN_HIS_REF + " text, "
            + COLUMN_STORAGE_PATH + " text, " +
            " FOREIGN KEY (" + COLUMN_ID + ") REFERENCES " + TABLE_PATH_NAME + " (" + COLUMN_ID + ") );";


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

      /*  super(context, Environment.getExternalStorageDirectory()
                + File.separator + "Alka_Database"
                + File.separator + DATABASE_NAME, null, DATABASE_VERSION);*/
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DATABASE_CREATE);
        database.execSQL(TABLE_PATH_REF_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

       /* db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
        onCreate(db);*/
    }

}
