package versionx.selfentry.Networking;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import android.widget.Toast;


import versionx.why.selfentry.BaseURLs;
import versionx.selfentry.Util.CommonMethods;
import versionx.selfentry.Util.Global;
import versionx.selfentry.interfaces.AsynctaskFinishedListner;


public class NetworkingApiConnect extends AsyncTask<Void, Void, String> {

    String apiUrl;
    String requestType;
    String data;
    URL url;
    AsynctaskFinishedListner listner;
    String progressMessage;
    Context context;
    String flag;
    int timeout;
    int responseCode = 0;
    ProgressDialog progressdialog;
    Activity activity;

    public NetworkingApiConnect(Context context, String apiUrl, String requestType, String data, String progressMessage, String flag, int timeout) {

        this.apiUrl = apiUrl;
        this.requestType = requestType;
        this.data = data;
        this.context = context;
        this.progressMessage = progressMessage;
        this.flag = flag;     //
        this.timeout = timeout;
        if (!progressMessage.isEmpty())
            activity = (Activity) context;

    }

    public void onAsynctaskFinished(AsynctaskFinishedListner listner) {
        this.listner = listner;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

       /* if (!progressMessage.isEmpty())
            progressdialog = ProgressDialog.show(context, progressMessage, "In Progress...");
*/
    }

    @Override
    protected String doInBackground(Void... params) {
        // TODO Auto-generated method stub

        HttpURLConnection conn = null;
        String response = null;

        try {
            url = new URL(apiUrl);

            conn = (HttpURLConnection) url.openConnection();

            if (timeout != 0)
                conn.setConnectTimeout(timeout);
        } catch (UnknownHostException e) {
            if (!progressMessage.isEmpty()) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {

                        Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (java.net.SocketTimeoutException e) {
            if (!progressMessage.isEmpty()) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {

                        Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        switch (requestType) {

            // this is for handling get requests

            case "get":
                try {
                    conn.setRequestMethod("GET");

                    responseCode = conn.getResponseCode();
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    response = CommonMethods.getStringFromInputStream(in);

                } catch (UnknownHostException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (java.net.SocketTimeoutException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (FileNotFoundException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.SERVER_ERROR_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                break;

            // this is for handling post request
            case "post":

                try {
                    conn.setRequestMethod("POST");

                } catch (ProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setRequestProperty("Content-Type", "application/json");
                if (flag.equalsIgnoreCase("Notification"))
                    conn.addRequestProperty("Authorization", "key=" + BaseURLs.FCM_AUTH);
                // conn.setRequestProperty("Accept", "application/json");
                try {
                    conn.connect();

                } catch (UnknownHostException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (java.net.SocketTimeoutException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (FileNotFoundException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.SERVER_ERROR_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }

                OutputStream os;
                try {
                    os = conn.getOutputStream();
                    OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

                    osw.write(data);
                    osw.flush();
                    osw.close();
                    os.close();

                    responseCode = conn.getResponseCode();
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    response = CommonMethods.getStringFromInputStream(in);

                } catch (UnknownHostException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (java.net.SocketTimeoutException e) {
                    if (!progressMessage.isEmpty()) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                Toast.makeText(context, Global.CHECK_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
            default:
                break;
        }

        // read the response


        return response;
    }


    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        // super.onPostExecute(result);

        if (!progressMessage.isEmpty()) {
            progressdialog.cancel();
            progressdialog.dismiss();
        }

      /*  if (result != null)
        listner.asynctaskFinished(result, responseCode, flag);*/

    }


}
