package versionx.selfentry.Networking;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import versionx.selfentry.Printing.DrawerService;


/**
 * Created by Developer on 12/16/2016.
 */

public class PingNetworkPrinterAsync extends AsyncTask<String, Void, Boolean> {

    private Context context;
  //  private ProgressDialog progressDialog;
    private boolean separateKOT;

    public PingNetworkPrinterAsync(Context context, boolean separateKOT) {
        this.context = context;
     //   progressDialog = new ProgressDialog(this.context);
        this.separateKOT = separateKOT;
      /*  progressDialog.setMessage("Connecting Network Printer...");
        progressDialog.setCancelable(true);*/
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
       /* if (!((Activity) context).isFinishing()) {
            progressDialog.show();
        }*/
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try {
            InetAddress inetAddress = InetAddress.getByName(params[0]);

            DrawerService.workThread.disconnectNet();

            if (inetAddress.isReachable(10000)) {
                //    DrawerService.workThread.connectNet(params[0],9100);
                return true;
            } else {

                DrawerService.workThread.connectNet(params[0], 9100);

                return true;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Boolean aVoid) {
        super.onPostExecute(aVoid);
       /* if (!((Activity) context).isFinishing() && progressDialog != null) {
            progressDialog.dismiss();
            if (aVoid) {
                //Common.showToast(context,"Successfully Connected");
            } else {
                //Common.showToast(context,"Printer is not Connected");
            }
        }*/
    }
}
