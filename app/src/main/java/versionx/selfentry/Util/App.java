package versionx.selfentry.Util;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.bumptech.glide.request.target.ViewTarget;

import versionx.selfentry.DataBase.AppDatabase;
import versionx.selfentry.R;
import versionx.selfentry.R;

/**
 * Created by alkaaswal on 5/10/17.
 */

public class App extends Application {
    static AppDatabase db;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        ViewTarget.setTagId(R.id.glide_tag);
        App.context = getApplicationContext();


    }

    public static Context getAppContext() {
        return App.context;
    }

    public static AppDatabase getDbInstance() {
        if (db == null) {
            db = Room.databaseBuilder(getAppContext(),
                    AppDatabase.class, "customField").build();
        }
        return db;
    }


    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;
}