package versionx.selfentry.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created by alkaaswal on 7/28/17.
 */

public class CaseInsensitiveMap extends TreeMap<String, String> {

    @Override
    public String put(String key, String value) {
        return super.put(key.toUpperCase(), value);
    }

    // not @Override because that would require the key parameter to be of type Object
    public String get(String key) {
        return super.get(key.toUpperCase());
    }




}