package versionx.selfentry.Util;



/* this class will handle all the common methods used by other class*/

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import org.json.JSONException;
import org.json.JSONObject;

import versionx.selfentry.Activity.HomeActivity;
import versionx.selfentry.CustomControls.TextViewOpenSans;
import versionx.selfentry.Networking.NetworkingApiConnect;
import versionx.selfentry.Printing.DrawerService;
import versionx.selfentry.Printing.ImageProcessing;
import versionx.selfentry.Activity.HomeActivity;
import versionx.selfentry.R;
import versionx.why.selfentry.BaseURLs;
import versionx.selfentry.Networking.NetworkingApiConnect;
import versionx.selfentry.Printing.DrawerService;
import versionx.selfentry.Printing.ImageProcessing;
import versionx.selfentry.R;


import static android.content.Context.MODE_PRIVATE;


public class CommonMethods {


    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    static String barcode = "";
    private static int[] p0 = {0, 0x80};
    private static int[] p1 = {0, 0x40};
    private static int[] p2 = {0, 0x20};
    private static int[] p3 = {0, 0x10};
    private static int[] p4 = {0, 0x08};
    private static int[] p5 = {0, 0x04};
    private static int[] p6 = {0, 0x02};


    // this method is using zxing library to get the barcode pattern bitmap image

    public static Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

   /* public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }*/

    private static String guessAppropriateEncoding(CharSequence contents) {

        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    // hide the keyboard

    public static void setHideSoftKeyboard(Activity activity, EditText ed) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ed.getWindowToken(), 0);
    }


    public static String getStringFromInputStream(InputStream stream) throws IOException {
        int n = 0;
        char[] buffer = new char[1024 * 4];
        InputStreamReader reader = new InputStreamReader(stream, "UTF8");
        StringWriter writer = new StringWriter();
        while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
        return writer.toString();
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap, String sourcePath, String imageKey, Context context) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if (sourcePath != null) {
            try {
                bitmap = rotateBitmap(sourcePath, bitmap, imageKey, context);
            } catch (Exception e) {
                Crashlytics.logException(e);


            }
        }
        bitmap.compress(CompressFormat.JPEG, 80, outputStream);

        return outputStream.toByteArray();
    }


    public static File rotateFileBitmap(File imageFile, Bitmap bitmap) throws IOException {
        int rotate = 0;
        FileInputStream inputStream = new FileInputStream(imageFile);

        Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, null);
        inputStream.close();
        imageFile.createNewFile();
        try {

            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {

        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);

        selectedBitmap = Bitmap.createBitmap(selectedBitmap, 0, 0, selectedBitmap.getWidth(), selectedBitmap.getHeight(), matrix, true);


        FileOutputStream outputStream = new FileOutputStream(imageFile);
        selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return imageFile;

    }


    public static Bitmap rotateBitmap(String sourcepath, Bitmap bitmap, String imageKey, Context context) {
        int rotate = 0;

        try {
            File imageFile = new File(sourcepath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            //  e.printStackTrace();

          /*  SharedPreferences imgPathSharedPref=
                    context.getSharedPreferences(Global.VIZ_IMAGE_SP,Context.MODE_PRIVATE);
            if(imageKey!=null && imgPathSharedPref.contains(imageKey)) {
                try {
                    File WhoCameDirectory = new File(imgPathSharedPref.getString(imageKey, ""));

                    if (WhoCameDirectory.exists())
                        WhoCameDirectory.delete();
                }catch (Exception ex)
                {
                    CommonMethods.writeToLogFile(ex.toString());
                }
                finally {
                    imgPathSharedPref.edit().remove(imageKey).apply();
                }

            }*/
            // CommonMethods.writeToLogFile(e.toString());

        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);


        return bitmap;

    }

    public static String getCurrentTime(String format) {
        Calendar c = Calendar.getInstance();
        //  System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }


    /* public static String getStringFromBitmap(Bitmap bitmapPicture) {
     *//*
     * This functions converts Bitmap picture to a string which can be
     * JSONified.
     * *//*
        final int COMPRESSION_QUALITY = 100;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }*/

    public static boolean isInternetWorking(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static byte[] eachLinePixToCmd(byte[] src, int nWidth, int nMode) {
        int nHeight = src.length / nWidth;
        int nBytesPerLine = nWidth / 8;
        byte[] data = new byte[nHeight * (8 + nBytesPerLine)];
        int offset = 0;
        int k = 0;
        for (int i = 0; i < nHeight; i++) {
            offset = i * (8 + nBytesPerLine);
            data[offset + 0] = 0x1d;
            data[offset + 1] = 0x76;
            data[offset + 2] = 0x30;
            data[offset + 3] = (byte) (nMode & 0x01);
            data[offset + 4] = (byte) (nBytesPerLine % 0x100);
            data[offset + 5] = (byte) (nBytesPerLine / 0x100);
            data[offset + 6] = 0x01;
            data[offset + 7] = 0x00;
            for (int j = 0; j < nBytesPerLine; j++) {
                data[offset + 8 + j] = (byte) (p0[src[k]] + p1[src[k + 1]]
                        + p2[src[k + 2]] + p3[src[k + 3]] + p4[src[k + 4]]
                        + p5[src[k + 5]] + p6[src[k + 6]] + src[k + 7]);
                k = k + 8;
            }
        }

        return data;
    }

    public static byte[] bitmapToBWPix(Bitmap mBitmap) {

        int[] pixels = new int[mBitmap.getWidth() * mBitmap.getHeight()];
        byte[] data = new byte[mBitmap.getWidth() * mBitmap.getHeight()];

        mBitmap.getPixels(pixels, 0, mBitmap.getWidth(), 0, 0,
                mBitmap.getWidth(), mBitmap.getHeight());

        // for the toGrayscale, we need to select a red or green or blue color
        ImageProcessing.format_K_dither16x16(pixels, mBitmap.getWidth(),
                mBitmap.getHeight(), data);

        return data;
    }

    // generating bar code
    public static String barcodeGenerator() {
        Calendar cal = Calendar.getInstance();

        String month, date, hours, minutes, seconds;
        if (cal.get(Calendar.MONTH) + 1 < 10)
            month = "0" + (cal.get(Calendar.MONTH) + 1);
        else
            month = "" + (cal.get(Calendar.MONTH) + 1);

        if (cal.get(Calendar.DATE) < 10)
            date = "0" + cal.get(Calendar.DATE);
        else
            date = "" + cal.get(Calendar.DATE);

        if (cal.get(Calendar.HOUR_OF_DAY) < 10)
            hours = "0" + cal.get(Calendar.HOUR_OF_DAY);
        else
            hours = "" + cal.get(Calendar.HOUR_OF_DAY);

        if (cal.get(Calendar.MINUTE) < 10)
            minutes = "0" + cal.get(Calendar.MINUTE);
        else
            minutes = "" + cal.get(Calendar.MINUTE);

        if (cal.get(Calendar.SECOND) < 10)
            seconds = "0" + cal.get(Calendar.SECOND);
        else
            seconds = "" + cal.get(Calendar.SECOND);

        //  String barcode = month + date + hours + minutes + seconds;

        String barcode = date + hours + minutes + seconds;

        return barcode;
    }

    public static void updateOutTime(final String barcode, final DatabaseReference visitorRef,
                                     final Context context, final int updatedBy, String mob, final String accessTyp) {

        final SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);


        // visitorRef.keepSynced(true);

        visitorRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                boolean alarm = false;

                if (dataSnapshot.hasChild("alarm") && dataSnapshot.child("alarm").getValue(Boolean.class))
                    alarm = true;

            /*    if(!loginSp.getBoolean(Global.SETTINGS_MARK_OUT,false) && dataSnapshot.hasChild("uOut") &&
                        CommonMethods.isTimeExceeded(dataSnapshot.child("uOut").getValue(Long.class),
                                dataSnapshot.child("out").getValue(Long.class), TimeUnit.MINUTES.toMillis(loginSp.getInt(Global.MARK_OUT_MIN,15)))
                        || alarm) {*/


                if (!loginSp.getBoolean(Global.SETTINGS_MARK_OUT, false) && dataSnapshot.hasChild("uOut") &&
                        CommonMethods.isTimeExceeded(System.currentTimeMillis(), dataSnapshot.child("uOut").getValue(Long.class)
                                , TimeUnit.MINUTES.toMillis(loginSp.getInt(Global.MARK_OUT_MIN, 15)))
                        || alarm) {


                    showStopVisitorDialog(context, dataSnapshot, loginSp, visitorRef, updatedBy, alarm, "scan", accessTyp);


                } else {
                    Toast toast = Toast.makeText(context, "Successfully checked out!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    if (dataSnapshot.getValue() != null) {
                        visitorRef.child("out").setValue(Calendar.getInstance().getTimeInMillis());
                        visitorRef.child("o").setValue(updatedBy);

                        if (accessTyp.equals(Global.VISITOR) && dataSnapshot.child("whoCame").hasChild(Global.VEHICLE)) {
                            String vehValue = dataSnapshot.child("whoCame").child(Global.VEHICLE)
                                    .child("val").getValue(String.class);
                            CommonMethods.removeVehData(context, accessTyp, vehValue);
                        }

                    } else {
                        DatabaseReference checkOutQueue = PersistentDatabase.getDatabase()
                                .getReference("queue/" +
                                        loginSp.getString(Global.BIZ_ID, "")
                                        + "/" + loginSp.getString(Global.GROUP_ID, ""));


                        HashMap<String, Object> queueMap = new HashMap<>();
                        queueMap.put("out", System.currentTimeMillis());
                        queueMap.put("o", updatedBy);
                        HashMap<String, Object> mainMap = new HashMap<>();
                        mainMap.put(barcode, queueMap);
                        checkOutQueue.updateChildren(mainMap);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static boolean isTimeExceeded(long currentDt, long inDt, long timeLimit) {

        Date cDt = new Date(currentDt);
        Date iDt = new Date(inDt);


        long mills = cDt.getTime() - iDt.getTime();

        if (mills > (timeLimit)) {
            return true;
        } else {
            return false;
        }


    }

    public static Calendar getTodaysDate() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.MILLISECOND, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.HOUR_OF_DAY, 0);

        return today;
    }

    public static Calendar getEndTimeToday() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.MILLISECOND, 999);
        today.set(Calendar.SECOND, 59);
        today.set(Calendar.MINUTE, 59);
        today.set(Calendar.HOUR_OF_DAY, 23);

        return today;
    }

    public static Calendar getEndTimeYesterDay() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.MILLISECOND, 999);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.HOUR_OF_DAY, 23);

        return cal;
    }


    public static Calendar getFirstDayOfMonth() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.MILLISECOND, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.DATE, 1);

        return today;
    }


    public static Calendar getYesterDaysDate() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        return cal;

    }

    public static void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                DeleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }

    public static void enablebluetooth() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (null != adapter) {
            if (!adapter.isEnabled()) {
                if (adapter.enable()) {
                    // while(!adapter.isEnabled());

                } else {

                    return;
                }
            }
        }
    }

    public static void disableBluetooth() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();
        }
    }

    public static InputFilter getCapsTextNumFilter() {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {

                    if (Character.isLowerCase(source.charAt(i))) {
                        char[] v = new char[end - start];
                        TextUtils.getChars(source, start, end, v, 0);
                        String s = new String(v).toUpperCase();

                        if (source instanceof Spanned) {
                            SpannableString sp = new SpannableString(s);
                            TextUtils.copySpansFrom((Spanned) source,
                                    start, end, null, sp, 0);
                            return sp;
                        } else {
                            return s;
                        }
                    }

                    if (!Character.isLetterOrDigit(source.charAt(i))) {
                        return "";
                    }

                }
                return null;
            }
        };

        return filter;
    }


    //update out time of visitor

    public static String getDate(long milliSeconds, String dateFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        // options.inJustDecodeBounds = true;
        // BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 2;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }
        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static void writeToLogFile(String log) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "SelfEntry");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "log");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append("dt: " + Calendar.getInstance().getTime() + "\n" + log + "\n");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void showToast(Context context, String msg, int time) {
        Toast toast = Toast.makeText(context, msg, time);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /*public static void uploadImgTask(final Context context, final String key, StorageReference storageref, String imgPath,
                                     final DatabaseReference profileRef, final DatabaseReference visitorRef,
                                     final DatabaseReference mobBizRef, final DatabaseReference vehRef, final String fieldKey) {

        final SharedPreferences imgPathSharedPref = context.getSharedPreferences(Global.VIZ_IMAGE_SP, Context.MODE_PRIVATE);
        if (!imgPathSharedPref.contains(key))
            imgPathSharedPref.edit().putString(key, imgPath).apply();


        try {

            //  Uri file = Uri.fromFile(saveBitmapToFile(new File(imgPath)));

            UploadTask uploadTask = storageref.putStream(saveBitmapToFile(new File(imgPath)));
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    HashMap<String, Object> updatehashmap = new HashMap<String, Object>();
                    updatehashmap.put(fieldKey, taskSnapshot.getDownloadUrl().toString());
                 *//*   HashMap<String, Object> profMap = new HashMap<String, Object>();
                    profMap.put(fieldKey, taskSnapshot.getDownloadUrl().toString());*//*

                    if (profileRef != null)
                        profileRef.updateChildren(updatehashmap);
                    if (mobBizRef != null)
                        mobBizRef.updateChildren(updatehashmap);

                    if (vehRef != null)
                        vehRef.setValue(taskSnapshot.getDownloadUrl().toString());
                    visitorRef.updateChildren(updatehashmap);

                    try {
                        File WhoCameDirectory = new File(imgPathSharedPref.getString(key, ""));
                        if (WhoCameDirectory.getAbsoluteFile().exists()) {
                            WhoCameDirectory.getAbsoluteFile().delete();
                        }

                    } catch (Exception e) {
                        FirebaseCrash.report(e);
                        //CommonMethods.writeToLogFile("9 "+e.toString()); // writing to log file
                    } finally {
                        imgPathSharedPref.edit().remove(key).apply();

                    }

                }
            });

        } catch (Exception e) {

            FirebaseCrash.report(e);

            if (imgPathSharedPref.contains(key)) {
                try {
                    File WhoCameDirectory = new File(imgPathSharedPref.getString(key, ""));
                    if (WhoCameDirectory.exists())
                        WhoCameDirectory.delete();
                } catch (Exception ex) {
                } finally {
                    imgPathSharedPref.edit().remove(key).apply();

                }
            }


            // CommonMethods.writeToLogFile("11 "+e.toString()); // writing to log file
        }
    }*/


    public static void showEpassDialog(Context context) {

        try {

            final Dialog dialog = new Dialog(context);

            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
            lp.dimAmount = 1.0f;

            dialog.setContentView(R.layout.allow_block_dialog);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();


            LinearLayout llBtn = (LinearLayout) dialog.findViewById(R.id.ll_dialog_btn);
            ImageView iv_block = (ImageView) dialog.findViewById(R.id.iv_for_allow_block);
       /* TextViewOpenSans dialog_title = (TextViewOpenSans) dialog.findViewById(R.id.dialog_title);
        ImageView closeDailog = (ImageView) dialog.findViewById(R.id.iv_close_dialog);*/
            TextView appointment_text = (TextView) dialog.findViewById(R.id.appointment_text);
            llBtn.setVisibility(View.GONE);
            appointment_text.setVisibility(View.VISIBLE);
            appointment_text.setText("\n\nE-PASS sent to your MOBILE\n\n");
            appointment_text.setTextSize(30);
       /* dialog_title.setText("Epass!");
        closeDailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/
            Runnable runnable = new Runnable() {
                @Override
                public void run() {

                    dialog.dismiss();

                }
            };


            Handler handler = new Handler();

            handler.postDelayed(runnable, 5000);
        } catch (Exception e) {
            CommonMethods.showToast(context, "E-PASS sent to your mobile", Toast.LENGTH_LONG);
        }

    }

    public static void updateAccessTime(DatabaseReference vizProfileRef, String key, String inOrOut, String hisRef, int calling) {
        vizProfileRef.child(key).child("ref").child("dt").setValue(System.currentTimeMillis());
        vizProfileRef.child(key).child("ref").child("tDt").setValue(CommonMethods.getFirstDayOfMonth().getTimeInMillis());
        vizProfileRef.child(key).child("ref").child("id").setValue(hisRef);
        vizProfileRef.child(key).child("ref").child("a").setValue(inOrOut);
        vizProfileRef.child(key).child("ref").child("c").setValue(calling);
    }

    /*  public static void updateAccessTime(DatabaseReference vizProfileRef, String inOrOut, String hisRef, int calling) {
          vizProfileRef.child("ref").child("dt").setValue(System.currentTimeMillis());
          vizProfileRef.child("ref").child("tDt").setValue(CommonMethods.getFirstDayOfMonth().getTimeInMillis());
          vizProfileRef.child("ref").child("id").setValue(hisRef);
          vizProfileRef.child("ref").child("a").setValue(inOrOut);
          vizProfileRef.child("ref").child("c").setValue(calling);


      }
  */
    public static String getAccessType(String accessType) {
        switch (accessType) {
            case Global.PARENT:
                return "PARENT";
            case Global.STAFF:
            case Global.VENDOR:
                return "STAFF";
            case Global.RESIDENT:
                return "RESIDENT";
            case Global.STUDENT:
                return "STUDENT";
            case Global.ALLOWED_VISITOR:
                return "ALLOW";
            case Global.HELPER:
                return "HELPER";
            default:
                return "STAFF";

        }
    }

/*    public void deleteCache(Context context) {
        File cache = context.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));

                }
            }
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }*/

    public static String getAccessNode(String accessType) {
        switch (accessType) {
            case Global.PARENT:
                return "parent";
            case Global.STAFF:
                return "access";
            case Global.VENDOR:
                return "access";
            case Global.RESIDENT:
                return "resident";
            case Global.STUDENT:
                return "student";
            case Global.ALLOWED_VISITOR:
                return "allow";
            case Global.HELPER:
                return "helper";
            default:
                return "access";
        }
    }


    public static HashMap<String, DatabaseReference> saveReservedFieldData(String fieldName, String key, HashMap<String, Object> fieldObj, Context context) {
        SharedPreferences loginSP = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

        HashMap<String, DatabaseReference> reservedFieldRef = new HashMap<>();

        if (fieldName.equalsIgnoreCase(Global.VEHICLE)) {

            DatabaseReference vehProfRef = PersistentDatabase.getDatabase().getReference("vehicleProfile/" +
                    loginSP.getString(Global.BIZ_ID, "") + "/" + loginSP.getString(Global.GROUP_ID, ""));

            vehProfRef.child(key).updateChildren(fieldObj);

            reservedFieldRef.put(Global.VEHICLE, vehProfRef.child(key));
        }

        return reservedFieldRef;
    }

    public static void removeVehData(Context context, final String accessTyp, String key) {
        SharedPreferences logingSP = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        final DatabaseReference vehProfRef = PersistentDatabase.getDatabase().getReference("vehicleProfile")
                .child(logingSP.getString(Global.BIZ_ID, "")).child(logingSP.getString(Global.GROUP_ID, ""))
                .child(key);

        vehProfRef.keepSynced(true);

        vehProfRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (dataSnapshot.child("typ").getChildrenCount() > 1) {
                        vehProfRef.child("typ").child(accessTyp).removeValue();
                    } else {
                        vehProfRef.removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void showStopVisitorDialog(final Context context, final DataSnapshot dataSnapshot, final SharedPreferences loginSp,
                                             final DatabaseReference visitorRef, final int updatedBy, final Boolean isAlarm, final String type, final String accessTyp) {

        final MediaPlayer mMediaPlayer;

        Uri alert = Uri.parse("android.resource://" + Global.PACKAGE_NAME + "/raw/alarm");

        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }

        } catch (Exception e) {

        }


        // ImageLoader imageLoader = null;
        final Dialog dialog = new Dialog(context);
        /* dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);*/
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 2.0f;

        dialog.setContentView(R.layout.alarm_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView alarm_dialog_viz_name = (TextView) dialog.findViewById(R.id.alarm_dialog_viz_name);
        final EditText ed_reason = (EditText) dialog.findViewById(R.id.ed_reason);
        ImageView iv_pic = (ImageView) dialog.findViewById(R.id.iv_alarm_pic);
        TextView alarm_dialog_tv_stop = (TextView) dialog.findViewById(R.id.alarm_dialog_tv_stop);
        ImageView iv_alarm_stop = (ImageView) dialog.findViewById(R.id.iv_alarm_stop);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_alarm_close_dialog);
        Button btn_call = (Button) dialog.findViewById(R.id.btn_alarm_call);
        LinearLayout to_meet_ll = (LinearLayout) dialog.findViewById(R.id.to_meet_ll);
        Button btn_alarm_ok = (Button) dialog.findViewById(R.id.btn_alarm_ok);

        if (isAlarm) {
            alarm_dialog_tv_stop.setText("STOP!");
            iv_pic.setVisibility(View.VISIBLE);
            to_meet_ll.setVisibility(View.VISIBLE);
            iv_alarm_stop.setVisibility(View.GONE);
            btn_alarm_ok.setVisibility(View.GONE);
            ed_reason.setVisibility(View.GONE);


        } else {
            alarm_dialog_tv_stop.setText("TIME OUT!");
            iv_pic.setVisibility(View.GONE);
            to_meet_ll.setVisibility(View.GONE);
            iv_alarm_stop.setVisibility(View.VISIBLE);
            btn_alarm_ok.setVisibility(View.VISIBLE);
            ed_reason.setVisibility(View.VISIBLE);
        }
        alarm_dialog_viz_name.setText(dataSnapshot.child("whoCame")
                .child("nm").getValue(String.class));


        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMediaPlayer.stop();
                dialog.dismiss();


            }
        });
        btn_alarm_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {

                    mMediaPlayer.stop();
                    dialog.dismiss();
                    Activity activity = (Activity) context;

                    if (type.equals("mannual"))
                        activity.finish();
                   /* DatabaseReference vizRef = PersistentDatabase.getDatabase().
                            getReference("alarm/" + loginSp.getString(Global.BIZ_ID + "_0", "")
                                    + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + dataSnapshot.getKey());

                    vizRef.removeValue();*/

                    if (!TextUtils.isEmpty(ed_reason.getText().toString().trim()))
                        visitorRef.child("rsn").setValue(ed_reason.getText().toString());
                    visitorRef.child("tmO").setValue(true);
                    if (dataSnapshot.getValue() != null) {
                        visitorRef.child("out").setValue(Calendar.getInstance().getTimeInMillis());
                        visitorRef.child("o").setValue(updatedBy);

                        //written by Rajesh....
                        //we're doing this only for Visitor.....
                        if (accessTyp.equalsIgnoreCase(Global.VISITOR) && dataSnapshot.child("whoCame").hasChild(Global.VEHICLE)) {
                            String vehValue = dataSnapshot.child("whoCame").child(Global.VEHICLE)
                                    .child("val").getValue(String.class);
                            CommonMethods.removeVehData(context, accessTyp, vehValue);
                        }


                    } else {
                        DatabaseReference checkOutQueue = PersistentDatabase.getDatabase()
                                .getReference("queue/" +
                                        loginSp.getString(Global.BIZ_ID, "")
                                        + "/" + loginSp.getString(Global.GROUP_ID, ""));


                        HashMap<String, Object> queueMap = new HashMap<>();
                        queueMap.put("out", System.currentTimeMillis());
                        queueMap.put("o", updatedBy);
                        HashMap<String, Object> mainMap = new HashMap<>();
                        mainMap.put(barcode, queueMap);
                        checkOutQueue.updateChildren(mainMap);
                    }
                } catch (Exception e) {

                }


            }
        });


        if (dataSnapshot != null && dataSnapshot.getValue() != null && isAlarm) {

           /* if (imageLoader == null)
                imageLoader = new ImageLoader(context);*/
            // this class use to take the image from url
            if (dataSnapshot.child("whoCame").hasChild("img") && !dataSnapshot.child("whoCame").child("img").getValue().equals(""))
                // imageLoader.DisplayImage(dataSnapshot.child("whoCame").child("img").getValue().toString(), iv_pic);

                Glide.with(context).load(dataSnapshot.child("whoCame").child("img").getValue().toString())
                        .placeholder(R.drawable.default_user_picture).into(iv_pic);

            btn_call.setText(dataSnapshot.child("meet").child("nm").getValue().toString());

        }

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatabaseReference meetRef = PersistentDatabase.getDatabase().
                        getReference("access/" +
                                loginSp.getString(Global.BIZ_ID, "") + "/"
                                + loginSp.getString(Global.GROUP_ID, "") + "/" +
                                dataSnapshot.child("meet").child("id").getValue().toString());

                meetRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot meetSnapShot) {

                        new CommonMethods().call(context, meetSnapShot.child("mob").getValue().toString());


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });

    }

    public String[] getSlipMessages(String type, Context context) {
        int size = 0;
        SharedPreferences loginsp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        String[] message = null;
        if (type.equalsIgnoreCase("footer")) {
            message = getFooterMessage(loginsp);
            size = message.length;
        }
        if (type.equalsIgnoreCase("header")) {
            message = getHeaderMessage(loginsp);
            size = message.length;
        }

        if (type.equalsIgnoreCase("branding")) {
            message = getBrandingMessage(loginsp);
            size = message.length;
        }


       /* String[] printMessage = new String[size];

        for (int i = 0; i < size; i++) {


            if (message[i].length() > 48) {
                char ch = message[i].charAt(48);

                if (ch != ' ') {

                    String msg1 = message[i].substring(0, 48);
                    if (message[i].contains(" ")) {
                        String msg3 = message[i].substring(0, msg1.lastIndexOf(" ") + 1);
                        String msg2 = message[i].substring(msg3.length(), message[i].length());
                        printMessage[i] = center(msg3.trim(), 48, " ") + "\n" + center(msg2, 48, " ") + "\n";

                    } else {
                        String msg2 = message[i].substring(msg1.length() + 1, message[i].length());
                        printMessage[i] = center(msg1.trim(), 48, " ") + "\n" + center(msg2.trim(), 48, " ") + "\n";
                    }

                }
            } else {
                printMessage[i] = center(message[i].trim(), 48, " ");

            }
        }*/

        return message;
    }

    public String center(String s, int size, String pad) {

        if (pad == null)
            throw new NullPointerException("pad cannot be null");
        if (pad.length() <= 0)
            throw new IllegalArgumentException("pad cannot be empty");
        if (s == null || size <= s.length())
            return s;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < (size - s.length()) / 2; i++) {
            sb.append(pad);
        }
        sb.append(s);
        while (sb.length() < size) {
            sb.append(pad);

        }

        return sb.toString();
    }

    public String[] getFooterMessage(SharedPreferences loginsp) {


        int size = loginsp.getInt(Global.SETTINGS_SLIP_FOOTER_MSG_SIZE, 0);
        String array[] = null;

        if (size > 0) {
            array = new String[size];
            for (int i = 0; i < size; i++)
                array[i] = loginsp.getString(Global.SETTINGS_SLIP_FOOTER_LINE + i, null);
        } else {
            array = new String[2];
            array[0] = "Please return slip while leaving";
        }
        return array;

    }

    public String[] getHeaderMessage(SharedPreferences loginsp) {


        int size = loginsp.getInt(Global.SETTINGS_SLIP_HEADER_MSG_SIZE, 0);
        String array[] = null;

        if (size > 0) {
            array = new String[size];
            for (int i = 0; i < size; i++)
                array[i] = loginsp.getString(Global.SETTINGS_SLIP_HEADER_LINE + i, null);
        } else {
            array = new String[2];
            array[0] = loginsp.getString(Global.BIZ_COMPANY_NAME, "");
        }
        return array;

    }

    public String[] getBrandingMessage(SharedPreferences loginsp) {


        int size = loginsp.getInt(Global.SETTINGS_SLIP_BRANDING_MSG_SIZE, 0);
        String array[] = null;

        if (size > 0) {
            array = new String[size];
            for (int i = 0; i < size; i++)
                array[i] = loginsp.getString(Global.SETTINGS_SLIP_BRANDING_LINE + i, null);
        } else {
            array = new String[2];
            array[0] = "";
        }
        return array;

    }


   /* public byte[] getPrintingBytes(Context context, String nm, String mob,
                                   String toMeet, String inTime, final String purpose, byte[] barcodeImage,
                                   byte[] imageCombined, byte[] logoBytes, final ArrayList<HashMap> toMeetCustomFieldsHashmAp,
                                   final ArrayList<HashMap> whoCameCustomFieldsHashmAp,
                                   boolean imagePrintRequired, DataSnapshot dataSnapshot) {

        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);

        String[] heading = getSlipMessages("header", context);
        String[] footer = getSlipMessages("footer", context);
        String[] branding = getSlipMessages("branding", context);

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {

            outputStream.write(alignText(Global.CENTER_ALIGN));

            if (logoBytes != null) {
                outputStream.write(logoBytes);
                outputStream.write("\n".getBytes());
            }
            outputStream.write(alignText(Global.UNDERLINE));

            outputStream.write(alignText(loginSp.getString(Global.HEADER_SIZE, Global.SIZE_NORMAL)));

            for (int i = 0; i < heading.length; i++) {
                outputStream.write(heading[i].getBytes());
                outputStream.write("\n".getBytes());
            }


            outputStream.write(alignText(Global.LEFT_ALIGN));

            //custom Fields

            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                if (dataSnapshot1.getKey().equals("mob") || dataSnapshot1.getKey().equals("nm")
                        || dataSnapshot1.getKey().equals("p") || dataSnapshot1.getKey().equals("dt")
                        || dataSnapshot1.getKey().equals("img")
                        || dataSnapshot1.getKey().equals("qr")
                        || dataSnapshot1.getKey().equals("toMeet")) {

                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));

                    if (dataSnapshot1.child("pNm").getValue(Boolean.class)
                            && !dataSnapshot1.getKey().equals("img") && !dataSnapshot1.getKey().equals("qr")) {

                        outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                    }
                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                    if (dataSnapshot1.getKey().equals("mob")) {
                        outputStream.write(mob.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("nm")) {
                        outputStream.write(nm.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("p")) {
                        outputStream.write(purpose.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("dt")) {
                        outputStream.write(inTime.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("img") && imagePrintRequired) {
                        outputStream.write("\n".getBytes());
                        outputStream.write(imageCombined);
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("qr")) {
                        outputStream.write(barcodeImage);

                    } else if (dataSnapshot1.getKey().equals("toMeet")) {

                        outputStream.write(toMeet.getBytes());
                        outputStream.write("\n".getBytes());

                    }


                } else

                {
                    if (whoCameCustomFieldsHashmAp != null) {
                        for (final HashMap<String, HashMap> entry : whoCameCustomFieldsHashmAp) {
                            for (final String key : entry.keySet()) {

                                final HashMap whoCameHasMap = entry.get(key);
                                if (key.equals(dataSnapshot1.getKey()) && (dataSnapshot1.child("typ").getValue(String.class).equals(Global.TEXTBOX)
                                        || dataSnapshot1.child("typ").getValue(String.class).equals(Global.NUMBER))) {

                                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                    if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {
                                        outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                                    }

                                    outputStream.write(whoCameHasMap.get("val").toString().getBytes());
                                    outputStream.write("\n".getBytes());
                                    break;
                                }

                                if (key.equals(dataSnapshot1.getKey()) &&
                                        (dataSnapshot1.child("typ").getValue(String.class).equals(Global.DROPDOWN)
                                                || dataSnapshot1.child("typ").getValue(String.class).equals(Global.SEARCHABLE))) {
                                    try {
                                        HashMap<String, String> hashMap = (HashMap<String, String>) whoCameHasMap.get("val");
                                        Iterator myVeryOwnIterator = hashMap.keySet().iterator();
                                        while (myVeryOwnIterator.hasNext()) {
                                            String key1 = (String) myVeryOwnIterator.next();
                                            outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                            if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {
                                                outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                                            }

                                            outputStream.write(hashMap.get(key1).toString().getBytes());
                                            //  System.out.println("key is 2" + hashMap.get(key1).toString());
                                            outputStream.write("\n".getBytes());

                                            break;
                                        }
                                    } catch (Exception e) {

                                        // System.out.println("exception is " + e.toString());
                                    }
                                }
                            }
                        }
                    }
                    if (toMeetCustomFieldsHashmAp != null) {
                        for (final HashMap<String, HashMap> entry : toMeetCustomFieldsHashmAp) {
                            for (final String key : entry.keySet()) {
                                final HashMap meetHasMap = entry.get(key);
                                if (key.equals(dataSnapshot1.getKey())
                                        && (dataSnapshot1.child("typ").getValue(String.class).equals(Global.TEXTBOX)
                                        || dataSnapshot1.child("typ").getValue(String.class).equals(Global.NUMBER))) {
                                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));

                                    if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {

                                        outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                                    }
                                    outputStream.write(meetHasMap.get("val").toString().getBytes());
                                    outputStream.write("\n".getBytes());
                                    break;
                                }


                                if (key.equals(dataSnapshot1.getKey()) &&
                                        (dataSnapshot1.child("typ").getValue(String.class).equals(Global.DROPDOWN)
                                                || dataSnapshot1.child("typ").getValue(String.class).equals(Global.SEARCHABLE))) {
                                    try {
                                        HashMap<String, String> hashMap = (HashMap<String, String>) meetHasMap.get("val");
                                        Iterator myVeryOwnIterator = hashMap.keySet().iterator();
                                        while (myVeryOwnIterator.hasNext()) {
                                            String key1 = (String) myVeryOwnIterator.next();
                                            outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                            if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {
                                                outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                                            }
                                            outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                            outputStream.write(hashMap.get(key1).toString().getBytes());
                                            //  System.out.println("key is 2" + hashMap.get(key1).toString());
                                            outputStream.write("\n".getBytes());
                                            break;
                                        }
                                    } catch (Exception e) {

                                        // System.out.println("exception is " + e.toString());

                                    }


                                }
                            }


                        }
                    }

                }

            }


            outputStream.write("\n".getBytes());
            outputStream.write(alignText(Global.CENTER_ALIGN));
            outputStream.write(alignText(Global.SIZE_NORMAL));
            for (int i = 0; i < footer.length; i++) {
                outputStream.write(footer[i].getBytes());
                outputStream.write("\n".getBytes());
            }

            for (int i = 0; i < branding.length; i++) {
                outputStream.write(branding[i].getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.write("\n\n\n".getBytes());
            outputStream.write(new byte[]{0x1B, 'i'});

            byte[] printBytes = outputStream.toByteArray();

            outputStream.flush();
            outputStream.close();
            return printBytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }

    }*/


    public byte[] getPrintingBytes(Context context, String nm, String mob,
                                   String toMeet, String inTime, final String purpose, byte[] barcodeImage,
                                   byte[] imageCombined, byte[] logoBytes, final ArrayList<HashMap> toMeetCustomFieldsHashmAp,
                                   final ArrayList<HashMap> whoCameCustomFieldsHashmAp,
                                   boolean imagePrintRequired, DataSnapshot dataSnapshot, String tknNo) {


        String[] heading = getSlipMessages("header", context);
        String[] footer = getSlipMessages("footer", context);
        String[] branding = getSlipMessages("branding", context);

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {

            outputStream.write(alignText(Global.CENTER_ALIGN));

            if (logoBytes != null) {
                outputStream.write(logoBytes);
                outputStream.write("\n".getBytes());
            }
            outputStream.write(alignText(Global.UNDERLINE));


            for (int i = 0; i < heading.length; i++) {
                outputStream.write(heading[i].getBytes());
                outputStream.write("\n".getBytes());
            }
            if (!tknNo.equals("0")) {
                outputStream.write(alignText(Global.SIZE_LARGE));
                outputStream.write(tknNo.getBytes());
                outputStream.write("\n".getBytes());
            }


            outputStream.write(alignText(Global.LEFT_ALIGN));

            //custom Fields

            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                if (dataSnapshot1.getKey().equals("mob") || dataSnapshot1.getKey().equals("nm")
                        || dataSnapshot1.getKey().equals("p") || dataSnapshot1.getKey().equals("dt")
                        || dataSnapshot1.getKey().equals("img")
                        || dataSnapshot1.getKey().equals("qr")
                        || dataSnapshot1.getKey().equals("host")) {

                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));

                    if (dataSnapshot1.child("pNm").getValue(Boolean.class)
                            && !dataSnapshot1.getKey().equals("img") && !dataSnapshot1.getKey().equals("qr")) {

                        if (dataSnapshot1.getKey().equalsIgnoreCase("host") && toMeet.trim().isEmpty()) {

                        } else {

                            outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                        }
                    }
                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                    if (dataSnapshot1.getKey().equals("mob")) {
                        outputStream.write(mob.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("nm")) {
                        outputStream.write(nm.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("p")) {
                        outputStream.write(purpose.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("dt")) {
                        outputStream.write(inTime.getBytes());
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("img") && imagePrintRequired) {
                        outputStream.write("\n".getBytes());
                        outputStream.write(imageCombined);
                        outputStream.write("\n".getBytes());

                    } else if (dataSnapshot1.getKey().equals("qr")) {
                        outputStream.write(barcodeImage);

                    } else if (dataSnapshot1.getKey().equals("host") && !toMeet.trim().isEmpty()) {

                        outputStream.write(toMeet.getBytes());
                        outputStream.write("\n".getBytes());

                    }


                } else

                {
                    if (whoCameCustomFieldsHashmAp != null) {
                        for (final HashMap<String, HashMap> entry : whoCameCustomFieldsHashmAp) {
                            for (final String key : entry.keySet()) {

                                final HashMap whoCameHasMap = entry.get(key);
                                if (key.equals(dataSnapshot1.getKey()) && (dataSnapshot1.child("typ").getValue(String.class).equals(Global.TEXTBOX)
                                        || dataSnapshot1.child("typ").getValue(String.class).equals(Global.NUMBER))) {

                                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                    if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {
                                        outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());


                                    }
                                    outputStream.write(whoCameHasMap.get("val").toString().getBytes());
                                    outputStream.write("\n".getBytes());
                                    break;
                                }

                                if (key.equals(dataSnapshot1.getKey()) &&
                                        (dataSnapshot1.child("typ").getValue(String.class).equals(Global.DROPDOWN)
                                                || dataSnapshot1.child("typ").getValue(String.class).equals(Global.SEARCHABLE)
                                        || dataSnapshot1.child("typ").getValue(String.class).equals(Global.RADIOBUTTON))) {
                                    try {
                                        HashMap<String, String> hashMap = (HashMap<String, String>) whoCameHasMap.get("val");
                                        Iterator myVeryOwnIterator = hashMap.keySet().iterator();
                                        while (myVeryOwnIterator.hasNext()) {
                                            String key1 = (String) myVeryOwnIterator.next();
                                            outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                            if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {
                                                outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                                            }

                                            outputStream.write(hashMap.get(key1).toString().getBytes());
                                            //  System.out.println("key is 2" + hashMap.get(key1).toString());
                                            outputStream.write("\n".getBytes());

                                            break;
                                        }
                                    } catch (Exception e) {

                                        // System.out.println("exception is " + e.toString());
                                    }
                                }
                            }
                        }
                    }
                    if (toMeetCustomFieldsHashmAp != null) {
                        for (final HashMap<String, HashMap> entry : toMeetCustomFieldsHashmAp) {
                            for (final String key : entry.keySet()) {
                                final HashMap meetHasMap = entry.get(key);
                                if (key.equals(dataSnapshot1.getKey())
                                        && (dataSnapshot1.child("typ").getValue(String.class).equals(Global.TEXTBOX)
                                        || dataSnapshot1.child("typ").getValue(String.class).equals(Global.NUMBER))) {
                                    outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));

                                    if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {

                                        outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                                    }
                                    outputStream.write(meetHasMap.get("val").toString().getBytes());
                                    outputStream.write("\n".getBytes());
                                    break;
                                }


                                if (key.equals(dataSnapshot1.getKey()) &&
                                        (dataSnapshot1.child("typ").getValue(String.class).equals(Global.DROPDOWN)
                                                || dataSnapshot1.child("typ").getValue(String.class).equals(Global.SEARCHABLE)
                                        || dataSnapshot1.child("typ").getValue(String.class).equals(Global.RADIOBUTTON))) {
                                    try {
                                        HashMap<String, String> hashMap = (HashMap<String, String>) meetHasMap.get("val");
                                        Iterator myVeryOwnIterator = hashMap.keySet().iterator();
                                        while (myVeryOwnIterator.hasNext()) {
                                            String key1 = (String) myVeryOwnIterator.next();
                                            outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                            if (dataSnapshot1.child("pNm").getValue(Boolean.class)) {
                                                outputStream.write((dataSnapshot1.child("nm").getValue().toString() + ": ").getBytes());
                                            }
                                            outputStream.write(alignText(dataSnapshot1.child("s").getValue(String.class)));
                                            outputStream.write(hashMap.get(key1).toString().getBytes());
                                            //  System.out.println("key is 2" + hashMap.get(key1).toString());
                                            outputStream.write("\n".getBytes());
                                            break;
                                        }
                                    } catch (Exception e) {

                                        // System.out.println("exception is " + e.toString());

                                    }


                                }
                            }


                        }
                    }

                }

            }


            outputStream.write("\n".getBytes());
            outputStream.write(alignText(Global.CENTER_ALIGN));
            outputStream.write(alignText(Global.SIZE_NORMAL));
            for (int i = 0; i < footer.length; i++) {
                outputStream.write(footer[i].getBytes());
                outputStream.write("\n".getBytes());
            }

            for (int i = 0; i < branding.length; i++) {
                outputStream.write(branding[i].getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.write("\n\n\n".getBytes());
            outputStream.write(new byte[]{0x1B, 'i'});

            byte[] printBytes = outputStream.toByteArray();

            outputStream.flush();
            outputStream.close();
            return printBytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }

    }

    public String getTimeDiff(long mills, Date currentDt, Date frDate, Date toDate) {
        String isBeforeOrEarly = "";
        if (currentDt.before(frDate)) {
            mills = frDate.getTime() - currentDt.getTime();
            isBeforeOrEarly = "Early by";
        }
        if (currentDt.after(toDate)) {
            mills = currentDt.getTime() - frDate.getTime();
            isBeforeOrEarly = "Late by";
        }
        int Hours = (int) (mills / (1000 * 60 * 60));
        int Mins = (int) (mills / (1000 * 60)) % 60;
        String time;

        if (Hours == 0) {
            time = isBeforeOrEarly + " " + Mins + " mins";
        } else if (Mins == 0) {
            time = isBeforeOrEarly + " " + Hours + " hrs";
        } else {
            time = isBeforeOrEarly + " " + Hours + ":" + Mins + " hrs";
        }

        return time;
    }

   /* private void getCustomFields(Context context, String nm, String mob, String toMeet,
                                 String inTime, String purpose, ArrayList<HashMap>
                                         toMeetCustomFieldsHashmAp, ArrayList<HashMap> customFieldsHashmAp, String barcode_data,
                                 Bitmap imageBitmap) {
        Bitmap bit = null;
        try {
            //generate barcode
            bit = CommonMethods.encodeAsBitmap(barcode_data, BarcodeFormat.QR_CODE, 200, 200);
        } catch (WriterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int nWidth = 576;
        int nMode = 0;
        // int width = ((nWidth + 7) / 8) * 8;

       *//* Bitmap rszPicBitmap = null;
        Bitmap grayPicBitmap = null;*//*

        byte[] combinebit = POS_PrintBWPic(bit, 200, nMode);
        byte buf[] = null;

        SharedPreferences loginsp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);


        if (loginsp.getBoolean(Global.SETTINGS_PRINT_PHOTO, false)) {

            try {
                byte[] photoBit = POS_PrintPicture(imageBitmap, 200, nMode);
                buf = new CommonMethods().getPrintingBytes(context, nm, mob, toMeet, inTime, purpose, combinebit, photoBit,
                        toMeetCustomFieldsHashmAp, customFieldsHashmAp, true);


              *//*  if (rszPicBitmap != null && !rszPicBitmap.isRecycled())
                    rszPicBitmap.recycle();
                if (grayPicBitmap != null && !grayPicBitmap.isRecycled())
                    grayPicBitmap.recycle();*//*
            } catch (Exception e) {
                buf = new CommonMethods().getPrintingBytes(context, nm, mob, toMeet, inTime, purpose, combinebit,
                        null, toMeetCustomFieldsHashmAp, customFieldsHashmAp, false);

               *//* if (rszPicBitmap != null && !rszPicBitmap.isRecycled())
                    rszPicBitmap.recycle();
                if (grayPicBitmap != null && !grayPicBitmap.isRecycled())
                    grayPicBitmap.recycle();*//*
            }
        } else {


            buf = new CommonMethods().getPrintingBytes(context, nm, mob, toMeet, inTime, purpose, combinebit, null,
                    toMeetCustomFieldsHashmAp, customFieldsHashmAp, false);
*//*
            if (rszPicBitmap != null && !rszPicBitmap.isRecycled())
                rszPicBitmap.recycle();
            if (grayPicBitmap != null && !grayPicBitmap.isRecycled())
                grayPicBitmap.recycle();*//*
        }

        getPrintingType(context, buf);


    }*/


    private void getCustomFields(Context context, String nm, String mob, String toMeet,
                                 String inTime, String purpose, ArrayList<HashMap>
                                         toMeetCustomFieldsHashmAp,
                                 ArrayList<HashMap> customFieldsHashmAp, String barcode_data,
                                 Bitmap imageBitmap, DataSnapshot dataSnapshot, String accessType, String tknNo) {

        SharedPreferences loginsp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);


        Bitmap qrCodeBitMap = null;
        try {

            qrCodeBitMap = CommonMethods.encodeAsBitmap(barcode_data,
                    BarcodeFormat.QR_CODE, loginsp.getInt(Global.QR_CODE_SIZE, 200), loginsp.getInt(Global.QR_CODE_SIZE, 200));


        } catch (WriterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        int nWidth = 576;
        int nMode = 0;

        Bitmap logoBitmap = null;
        byte[] logoBytes = null;

        if (loginsp.getBoolean(Global.SETTINGS_PRINT_LOGO, false) && !loginsp.getString(Global.LOGO, "").isEmpty()) {
            logoBytes = Base64.decode(loginsp.getString(Global.LOGO, "").getBytes(), Base64.DEFAULT);
            logoBitmap = BitmapFactory.decodeByteArray(logoBytes, 0, logoBytes.length);
            logoBytes = POS_PrintBWPic(logoBitmap, 300, nMode);
        }


        byte[] qrCode = POS_PrintBWPic(qrCodeBitMap, loginsp.getInt(Global.QR_CODE_SIZE, 200), nMode);

        byte buf[] = null;

        if (loginsp.getBoolean(Global.SETTINGS_PRINT_PHOTO, false)) {

            try {

                byte[] photoBit = POS_PrintPicture(imageBitmap, 200, nMode);


                buf = getPrintingBytes(context, nm, mob, toMeet, inTime, purpose, qrCode, photoBit, logoBytes,
                        toMeetCustomFieldsHashmAp, customFieldsHashmAp, true, dataSnapshot, tknNo);


            } catch (Exception e) {


                buf = getPrintingBytes(context, nm, mob, toMeet, inTime, purpose, qrCode,
                        null, logoBytes, toMeetCustomFieldsHashmAp, customFieldsHashmAp, false,
                        dataSnapshot, tknNo);


            }


        } else {


            buf = getPrintingBytes(context, nm, mob, toMeet, inTime, purpose, qrCode, null, logoBytes,
                    toMeetCustomFieldsHashmAp, customFieldsHashmAp, false, dataSnapshot,
                    tknNo);


        }


        getPrintingType(context, buf);

       /* if (imageBitmap != null && !imageBitmap.isRecycled())
            imageBitmap.recycle();
        if (qrCodeBitMap != null && !qrCodeBitMap.isRecycled())
            qrCodeBitMap.recycle();
        if (logoBitmap != null && !logoBitmap.isRecycled())
            logoBitmap.recycle();

*/


    }

   /* private void getPrintingType(Context context, byte[] buf) {
        SharedPreferences bluetoothSp = context.getSharedPreferences(Global.BLUETOOTH_SP, MODE_PRIVATE);
        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);

        switch (loginSp.getInt(Global.PRINTING, 0)) {
            case Global.BLUETOOTH_PRINT:


                if (DrawerService.workThread != null && !DrawerService.workThread.isConnecting()) {


                    if (DrawerService.workThread != null && DrawerService.workThread.isConnected()) {
                        Bundle data = new Bundle();
                        data.putByteArray(Global.BYTESPARA1, buf);
                        data.putInt(Global.INTPARA1, 0);
                        data.putInt(Global.INTPARA2, buf.length);


                        DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);


                    } else if (!bluetoothSp.getString(Global.DEVICE_NAME, "").isEmpty()) {
                        HomeActivity.buf = buf;


                        DrawerService.workThread.connectBt(bluetoothSp.getString(Global.DEVICE_NAME, ""));
                    } else
                        CommonMethods.showToast(context, Global.CONNECT_PRINTER, Toast.LENGTH_LONG);


                } else {
                    Toast.makeText(context, "Wait, Bluetooth is being connected", Toast.LENGTH_LONG).show();
                }

                break;

            case Global.NETWORK_PRINT:
               *//* if (DrawerService.workThread != null && !DrawerService.workThread.isConnected()) {

                    HomeActivity.buf = buf;

                    DrawerService.workThread.connectNet(loginSp.getString(Global.NETWORK_IP, ""), 9100);
                } else {
                    if (buf != null) {
                        if (DrawerService.workThread != null) {
                            Bundle data = new Bundle();
                            data.putByteArray(Global.BYTESPARA1, buf);
                            data.putInt(Global.INTPARA1, 0);
                            data.putInt(Global.INTPARA2, buf.length);
                            data.putInt(Global.INTPARA3, 0);
                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);
                            Bundle bundle = new Bundle();
                            bundle.putByteArray(Global.BYTESPARA1, " ".getBytes());
                            bundle.putInt(Global.INTPARA1, 0);
                            bundle.putInt(Global.INTPARA2, " ".getBytes().length);
                            bundle.putInt(Global.INTPARA3, 1);

                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, bundle);
                        }

                    }


                }*//*

                if (DrawerService.workThread != null && !DrawerService.workThread.isConnected()) {

                    HomeActivity.buf = buf;

                    DrawerService.workThread.connectNet(loginSp.getString(Global.NETWORK_IP, ""), 9100);
                } else {
                    if (buf != null) {
                        if (DrawerService.workThread != null && DrawerService.workThread.isConnected()) {
                            Bundle data = new Bundle();
                            data.putByteArray(Global.BYTESPARA1, buf);
                            data.putInt(Global.INTPARA1, 0);
                            data.putInt(Global.INTPARA2, buf.length);
                            data.putInt(Global.INTPARA3, 0);
                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);
                            *//*Bundle bundle = new Bundle();
                            bundle.putByteArray(Global.BYTESPARA1, " ".getBytes());
                            bundle.putInt(Global.INTPARA1, 0);
                            bundle.putInt(Global.INTPARA2, " ".getBytes().length);
                            bundle.putInt(Global.INTPARA3, 1);

                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, bundle);*//*
                        }

                    }


                }


                break;
            case Global.USB_PRINT:


                final UsbManager mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);


                HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
                Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

                UsbDevice device = null;
                while (deviceIterator.hasNext()) {
                    device = deviceIterator.next();
                    if (device.getProductId() == loginSp.getInt(Global.USB_PRODUCT_ID, 0)
                            && device.getVendorId() == loginSp.getInt(Global.USB_VENDORID, 0)) {
                        break;
                    }

                }


                if (DrawerService.workThread != null) {

                    if (!DrawerService.workThread.isConnected())

                        DrawerService.workThread.connectUsb(mUsbManager, device);
                    else {
                        if (buf != null) {
                            Bundle data = new Bundle();
                            data.putByteArray(Global.BYTESPARA1, buf);
                            data.putInt(Global.INTPARA1, 0);
                            data.putInt(Global.INTPARA2, buf.length);
                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);

                        }
                    }
                }

                break;

        }


    }*/

    private void getPrintingType(Context context, byte[] buf) {
        SharedPreferences bluetoothSp = context.getSharedPreferences(Global.BLUETOOTH_SP, MODE_PRIVATE);
        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);

        switch (loginSp.getInt(Global.PRINTING, 0)) {
            case Global.BLUETOOTH_PRINT:


                if (DrawerService.workThread != null && !DrawerService.workThread.isConnecting()) {


                    if (DrawerService.workThread != null && DrawerService.workThread.isConnected()) {
                        Bundle data = new Bundle();
                        data.putByteArray(Global.BYTESPARA1, buf);
                        data.putInt(Global.INTPARA1, 0);
                        data.putInt(Global.INTPARA2, buf.length);


                        DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);


                    } else if (!bluetoothSp.getString(Global.DEVICE_NAME, "").isEmpty()) {
                        HomeActivity.buf = buf;
                        DrawerService.workThread.connectBt(bluetoothSp.getString(Global.DEVICE_NAME, ""));
                    } else
                        CommonMethods.showToast(context, Global.CONNECT_PRINTER, Toast.LENGTH_LONG);


                } else {
                    Toast.makeText(context, "Wait, Bluetooth is being connected", Toast.LENGTH_LONG).show();
                }

                break;

            case Global.NETWORK_PRINT:
                if (DrawerService.workThread != null && !DrawerService.workThread.isConnected()) {

                    HomeActivity.buf = buf;

                    DrawerService.workThread.connectNet(loginSp.getString(Global.NETWORK_IP, ""), 9100);
                } else {
                    if (buf != null) {
                        if (DrawerService.workThread != null) {
                            Bundle data = new Bundle();
                            data.putByteArray(Global.BYTESPARA1, buf);
                            data.putInt(Global.INTPARA1, 0);
                            data.putInt(Global.INTPARA2, buf.length);
                            data.putInt(Global.INTPARA3, 0);
                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);
                           /* Bundle bundle = new Bundle();
                            bundle.putByteArray(Global.BYTESPARA1, " ".getBytes());
                            bundle.putInt(Global.INTPARA1, 0);
                            bundle.putInt(Global.INTPARA2, " ".getBytes().length);
                            bundle.putInt(Global.INTPARA3, 1);

                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, bundle);*/
                        }

                    }


                }


                break;
            case Global.USB_PRINT:


                final UsbManager mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);


                HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
                Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

                UsbDevice device = null;
                while (deviceIterator.hasNext()) {
                    device = deviceIterator.next();
                    if (device.getProductId() == loginSp.getInt(Global.USB_PRODUCT_ID, 0)
                            && device.getVendorId() == loginSp.getInt(Global.USB_VENDORID, 0)) {
                        break;
                    }

                }


                if (DrawerService.workThread != null) {

                    if (!DrawerService.workThread.isConnected()) {
                        HomeActivity.buf = buf;
                        DrawerService.workThread.connectUsb(mUsbManager, device);
                    } else {
                        if (buf != null) {
                            Bundle data = new Bundle();
                            data.putByteArray(Global.BYTESPARA1, buf);
                            data.putInt(Global.INTPARA1, 0);
                            data.putInt(Global.INTPARA2, buf.length);
                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);

                        }
                    }
                }

                break;

        }


    }


    public void printSlip(final Context context, final String nm, final String mob, final String toMeet,
                          final String inTime, final String purpose, final ArrayList<HashMap> toMeetCustomFieldsHashmAp,
                          final ArrayList<HashMap> whoCameCustomFieldsHashmAp,
                          final String accessType, final String barcode_data, final Bitmap imageBitmap,
                          final String sNo, long inTimeMili, boolean isQ, String oldToken) {

        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        String tkNo = "" + 0;

        if (!loginSp.getBoolean(Global.EPASS, false)) {

            String qrcode = null;
            if (accessType != Global.STAFF) {

                if (loginSp.getBoolean(Global.TOKEN, false) && isQ) {

                    if (oldToken == null) {
                        tkNo = (loginSp.getInt(Global.TOKEN_NO, 0)) + "";

                        HashMap<String, Object> tknMap = new HashMap<>();
                        tknMap.put("no", loginSp.getInt(Global.TOKEN_NO, 0));

                        HashMap<String, Object> finaltknMap = new HashMap<>();
                        finaltknMap.put(getTodaysDate().getTimeInMillis() + "", tknMap);
                        PersistentDatabase.getDatabase().getReference("device")
                                .child(loginSp.getString(Global.BIZ_ID, ""))
                                .child(Global.APP_NAME).child(loginSp.getString(Global.UUID, "")).child("tkn")
                                .setValue(finaltknMap);


                        tkNo = loginSp.getString(Global.DEVICE_CODE, "") + (loginSp.getInt(Global.TOKEN_NO, 0));
                    } else {
                        tkNo = oldToken;
                    }

                }

                qrcode = "#" + Global.VISITOR + "|" + barcode_data + "|" + CommonMethods.getTodaysDate().getTimeInMillis();
            } else {
                if (sNo != null && !sNo.equals(null))
                    qrcode = "#" + Global.STAFF + "|" + mob;
                else {
                    try {
                        CryptLib cryptLib = new CryptLib();
                        qrcode = cryptLib.encrypt("#" + Global.STAFF + "|" + mob + "|" + sNo, "versionx", "123");
                    } catch (Exception e) {
                        qrcode = "#" + Global.STAFF + "|" + mob;
                    }
                }
            }

            final String qrCode = qrcode;


           /* getCustomFields(context, nm, mob, toMeet,
                    inTime, purpose, toMeetCustomFieldsHashmAp,
                    whoCameCustomFieldsHashmAp, qrcode, imageBitmap);*/
            final String tknNo = tkNo;

            DatabaseReference dbRef = PersistentDatabase.getDatabase().getReference("printFormat/"
                    + loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, "")).child("0");
            dbRef.keepSynced(true);

            dbRef.orderByChild("o").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    getCustomFields(context, nm, mob, toMeet,
                            inTime, purpose, toMeetCustomFieldsHashmAp,
                            whoCameCustomFieldsHashmAp, qrCode, imageBitmap, dataSnapshot, accessType, tknNo + "");

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("bk", loginSp.getString(Global.BIZ_ID, ""));
                jsonObject.put("gk", loginSp.getString(Global.GROUP_ID, ""));
                jsonObject.put("dt", CommonMethods.getFirstDayOfMonth().getTimeInMillis());
                jsonObject.put("mob", mob);
                jsonObject.put("vhkey", barcode_data);


                jsonObject.put("in", inTimeMili);

                System.out.println("json " + jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }


            NetworkingApiConnect networkingApiConnect = new NetworkingApiConnect(context, BaseURLs.EPASSURL,
                    "post", jsonObject.toString(), "", "viz", 20000);
            networkingApiConnect.execute();


            showEpassDialog(context);


        }

    }

    public String getVerisonNumber(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionName;
    }

    public static void logout(Context context) {

        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        String uuid = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        DatabaseReference codeRef = PersistentDatabase.getDatabase().getReference("device/" +
                loginSp.getString(Global.BIZ_ID, "") + "/" + Global.APP_NAME + "/" + uuid);
        codeRef.getRef().child("del").setValue(true);
        codeRef.getRef().child("dt").setValue(Calendar.getInstance().getTimeInMillis());
        SharedPreferences sp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorlogin = sp.edit();
        editorlogin.clear();
        editorlogin.commit();

        FirebaseAuth.getInstance().signOut();
        deleteCache(context);
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    //
  /*  public void showDailogForAllowReject(final Context context, final String action,
                                         final DataSnapshot visitordataSnapshot, final String updatedBy,
                                         final boolean finishActivity) {

        final SharedPreferences bluetoothSp = context.getSharedPreferences(team.why.selfentry.Util.Global.BLUETOOTH_SP, MODE_PRIVATE);

        final Dialog dialog = new Dialog(context);
        //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 2.0f;

        dialog.setContentView(R.layout.allow_block_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        final TextView dialogText = (TextView) dialog.findViewById(R.id.appointment_text);
        Button firstBtn = (Button) dialog.findViewById(R.id.firstBtn);
        Button secondBtn = (Button) dialog.findViewById(R.id.secondBtn);
        final LinearLayout dailog_ll = (LinearLayout) dialog.findViewById(R.id.dialog_ll);
        final TextView dailog_title = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView reason = (TextView) dialog.findViewById(R.id.appointment_reason);
        final TextView welcomeNote = (TextView) dialog.findViewById(R.id.welcome_note);
        Button OkButton = (Button) dialog.findViewById(R.id.OkBtn);
        final ImageView closeDailog = (ImageView) dialog.findViewById(R.id.iv_close_dialog);
        ImageView iv_dialog_call = (ImageView) dialog.findViewById(R.id.iv_dialog_call);
        final ImageView allowBlock = (ImageView) dialog.findViewById(R.id.iv_for_allow_block);
        dialogText.setVisibility(View.GONE);

        firstBtn.setVisibility(View.GONE);

        iv_dialog_call.setVisibility(View.GONE);
        secondBtn.setVisibility(View.GONE);

        final SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

        final DatabaseReference waitRef = PersistentDatabase.getDatabase().getReference("wait/" +
                loginSp.getString(Global.BIZ_ID, "")
                + "/" + loginSp.getString(Global.GROUP_ID, ""));

        final DatabaseReference vizRef = PersistentDatabase.getDatabase().
                getReference("visitor/" + loginSp.getString(Global.BIZ_ID , "'")
                        + "/" + loginSp.getString(Global.GROUP_ID, ""));

        closeDailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                // if user allowed, update the check in time
                if (updatedBy.equals("u")) {
                    waitRef.child(visitordataSnapshot.getKey()).removeValue();
                    HashMap<String, Object> vizHashMap = new HashMap<String, Object>();
                    vizHashMap.put("in", Calendar.getInstance().getTimeInMillis());
                    vizRef.child(visitordataSnapshot.getKey()).updateChildren(vizHashMap);
                }

            }
        });


// checking if allowed or blocked
        OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, Object> vizHashMap = new HashMap<String, Object>();
                vizHashMap.put("in", Calendar.getInstance().getTimeInMillis());
                vizRef.child(visitordataSnapshot.getKey()).updateChildren(vizHashMap);
                if (finishActivity) {
                    Activity activity = (Activity) context;
                    activity.finish();
                }

                dialog.dismiss();


                final DatabaseReference meetRef = PersistentDatabase.getDatabase().
                        getReference("access/" + loginSp.getString(Global.BIZ_ID, "'")
                                + "/" + loginSp.getString(Global.GROUP_ID, "") + "/"
                                + visitordataSnapshot.child("meet").child("id").getValue().toString());

// if blocked
                if (action.equalsIgnoreCase("block")) {

                    waitRef.child(visitordataSnapshot.getKey()).removeValue();


                    meetRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot meetSnap) {


                            if (meetSnap.getValue() != null) {
                                DatabaseReference blockRef = PersistentDatabase.getDatabase().
                                        getReference("cast/" + meetSnap.child("mob").getValue().toString()
                                                + "/" + loginSp.getString(Global.BIZ_ID , "'")
                                                + "/" + loginSp.getString(Global.GROUP_ID, "") + "/r");

                                DatabaseReference waitcatRef = PersistentDatabase.getDatabase().
                                        getReference("cast/" + meetSnap.child("mob").getValue().toString()
                                                + "/" + loginSp.getString(Global.BIZ_ID , "'")
                                                + "/" + loginSp.getString(Global.GROUP_ID, "") + "/wait/who");

                                // remove VID from wait bucket and move it to reject bucket
                                waitcatRef.child(visitordataSnapshot.getKey()).removeValue();
                                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                                hashMap.put(visitordataSnapshot.getKey(), true);
                                blockRef.updateChildren(hashMap);

                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    // guard blocked the user
                    if (updatedBy.equals("g")) {
                        HashMap<String, Object> vizMap = new HashMap<String, Object>();
                        vizMap.put("a", "r");
                        vizMap.put("by", "g");
                        vizRef.child(visitordataSnapshot.getKey()).updateChildren(vizMap);

                    }
                }
                // allowed
                else {
                    // print required
                    if (loginSp.getBoolean(Global.SETTINGS_PRINT_REQUIRED, false)) {
                        final String nm = " " + visitordataSnapshot.child("whoCame").child("nm").getValue().toString();

                        final String mob = visitordataSnapshot.child("whoCame").child("mob").getValue().toString();

                        final String toMeet = visitordataSnapshot.
                                child("meet").child("nm").getValue().toString();

                        final String inTime = getDate(Long.parseLong(visitordataSnapshot.child("in").getValue().toString())
                                , "dd MMM hh:mm a");

                        final String purpose = visitordataSnapshot.
                                child("meet").child("p").getValue().toString();


                        final ArrayList<HashMap> toMeetCustomFieldsHashmap = new ArrayList<HashMap>();

                        final ArrayList<HashMap> whoCameCustomFieldsHashmap = new ArrayList<HashMap>();
                        for (DataSnapshot whoCameSnap : visitordataSnapshot.child("whoCame").getChildren()) {
                            if (!whoCameSnap.getKey().equals("nm") && !whoCameSnap.getKey().equals("img") && !whoCameSnap.getKey().equals("mob")
                                    && !whoCameSnap.getKey().equals("id")) {
                                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                                hashMap.put(whoCameSnap.getKey(), whoCameSnap.getValue());
                                whoCameCustomFieldsHashmap.add(hashMap);

                            }
                        }


                        for (DataSnapshot meetSnap : visitordataSnapshot.child("meet").getChildren()) {
                            if (!meetSnap.getKey().equals("nm") && !meetSnap.getKey().equals("img") && !meetSnap.getKey().equals("mob")
                                    && !meetSnap.getKey().equals("p") && !meetSnap.getKey().equals("pId")
                                    && !meetSnap.getKey().equals("id")) {
                                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                                hashMap.put(meetSnap.getKey(), meetSnap.getValue());
                                toMeetCustomFieldsHashmap.add(hashMap);
                            }

                        }


                        //   ImageLoader imageLoaderOriginal = new ImageLoader(context);


                        if (visitordataSnapshot.child("whoCame").hasChild("img")) {
                          *//*  printSlip(context, nm, mob, toMeet, inTime, purpose, toMeetCustomFieldsHashmap, whoCameCustomFieldsHashmap, "#" + Global.VISITOR + "|" +
                                            visitordataSnapshot.getKey(),
                                    imageLoaderOriginal.getBitmap(visitordataSnapshot.child("whoCame")
                                            .child("img").getValue().toString()));*//*

                            Glide.with(context).load(visitordataSnapshot.child("whoCame")
                                    .child("img").getValue().toString()).asBitmap().override(200, 200)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {


                                            printSlip(context, nm, mob, toMeet, inTime, purpose, toMeetCustomFieldsHashmap, whoCameCustomFieldsHashmap, "#" + Global.VISITOR + "|" +
                                                            visitordataSnapshot.getKey(),
                                                    resource);

                                        }
                                    });


                        } else {
                            printSlip(context, nm, mob, toMeet, inTime, purpose,
                                    toMeetCustomFieldsHashmap, whoCameCustomFieldsHashmap, "#" + Global.VISITOR + "|" +
                                            visitordataSnapshot.getKey(),
                                    null);
                        }

*//*
                        if (DrawerService.workThread != null &&
                                DrawerService.workThread.isConnected()) {
                            Bundle data = new Bundle();
                            data.putByteArray(Global.BYTESPARA1, buf);
                            data.putInt(Global.INTPARA1, 0);
                            data.putInt(Global.INTPARA2, buf.length);
                            DrawerService.workThread.handleCmd(Global.CMD_POS_WRITE, data);

                        } else {

                            if (!bluetoothSp.getString(Global.DEVICE_NAME, "").isEmpty())
                                DrawerService.workThread.connectBt(bluetoothSp.getString(Global.DEVICE_NAME, ""));
                            else
                                CommonMethods.showToast(context, Global.CONNECT_PRINTER, Toast.LENGTH_LONG);

                           *//**//* BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                            if (null == adapter) {

                                // break;
                            }

                            if (!adapter.isEnabled()) {
                                if (adapter.enable()) {
                                    while (!adapter.isEnabled()) ;
                                } else {

                                    //break;
                                }
                            }

                            adapter.cancelDiscovery();

                            adapter.startDiscovery();
*//**//*
                        }*//*


                    }


                    meetRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot meetSnap) {

                            if (meetSnap.getValue() != null) {
                                DatabaseReference allowRef = PersistentDatabase.getDatabase().
                                        getReference("cast/" + meetSnap.child("mob").getValue().toString()
                                                + "/" + loginSp.getString(Global.BIZ_ID , "'")
                                                + "/" + loginSp.getString(Global.GROUP_ID, "") + "/a");

                                DatabaseReference waitcatRef = PersistentDatabase.getDatabase().
                                        getReference("cast/" + meetSnap.child("mob").getValue().toString()
                                                + "/" + loginSp.getString(Global.BIZ_ID , "'")
                                                + "/" + loginSp.getString(Global.GROUP_ID, "") + "/wait/who");

                                // remove VID from wait bucket in cast node and move it to allwed bucket
                                waitcatRef.child(visitordataSnapshot.getKey()).removeValue();
                                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                                hashMap.put(visitordataSnapshot.getKey(), true);
                                allowRef.updateChildren(hashMap);

                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                    if (updatedBy.equals("g")) {
                        HashMap<String, Object> vizMap = new HashMap<String, Object>();
                        vizMap.put("a", "a");
                        vizMap.put("by", "g");
                        vizRef.child(visitordataSnapshot.getKey()).updateChildren(vizMap);
                    }

                    waitRef.child(visitordataSnapshot.getKey()).removeValue();
                }

            }
        });


        //Dailog UI changes according to action block or reject
        if (action.equalsIgnoreCase("block")) {


            allowBlock.setVisibility(View.VISIBLE);
            allowBlock.setBackgroundResource(R.drawable.no);
            dailog_ll.setBackgroundColor(Color.parseColor("#ffffff"));
            welcomeNote.setText(visitordataSnapshot.child("whoCame").child("nm").getValue().toString());
            welcomeNote.setVisibility(View.VISIBLE);
            dailog_title.setText("Block");
            dialogText.setTextColor(Color.parseColor("#000000"));

        } else if (action.equalsIgnoreCase("allow")) {
            closeDailog.setVisibility(View.VISIBLE);
            dailog_title.setText("Allow");
            welcomeNote.setText("Welcome " + visitordataSnapshot.child("whoCame").child("nm").getValue().toString());
            allowBlock.setVisibility(View.VISIBLE);
            welcomeNote.setVisibility(View.VISIBLE);
            allowBlock.setBackgroundResource(R.drawable.yes);
            dailog_ll.setBackgroundColor(Color.parseColor("#ffffff"));
            dialogText.setTextColor(Color.parseColor("#000000"));


        }

    }*/

    public void call(Context context, String mobile_no) {

        PackageManager pm = context.getPackageManager();

        if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {

            if (!mobile_no.equals("")) {

                if (checkForSimCard(context)) {


                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + mobile_no));
                    context.startActivity(callIntent);
                } else {

                    Toast toast = Toast.makeText(context, "No sim card!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }
            } else {
                Toast.makeText(context, "Please enter Mobile number!", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(context, "Calling feature not available!", Toast.LENGTH_SHORT).show();

        }
    }

    // check for sim card
    private boolean checkForSimCard(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int SIM_STATE = telephonyManager.getSimState();
        if (SIM_STATE == TelephonyManager.SIM_STATE_READY)
            return true;
        else {
            switch (SIM_STATE) {
                case TelephonyManager.SIM_STATE_ABSENT: //SimState = "No Sim Found!";
                    break;
                case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = "Network Locked!";
                    break;
                case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = "PIN Required to access SIM!";
                    break;
                case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = "PUK Required to access SIM!"; // Personal Unblocking Code
                    break;
                case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = "Unknown SIM State!";
                    break;
            }
            return false;
        }
    }

    public byte[] POS_PrintPicture(Bitmap mBitmap, int nWidth, int nMode) {
        int width = (nWidth + 7) / 8 * 8;
        //int height = mBitmap.getHeight() * width / mBitmap.getWidth();
        int height = 200;
        height = (height + 7) / 8 * 8;
        Bitmap rszBitmap = ImageProcessing.resizeImage(mBitmap, width, height);
        Bitmap grayBitmap = ImageProcessing.toGrayscale(rszBitmap);
        byte[] dithered = bitmapToBWPix(grayBitmap);
        byte[] data = eachLinePixToCmd(dithered, width, nMode);
        return data;
    }

    public byte[] POS_PrintBWPic(Bitmap mBitmap, int nWidth, int nMode) {
        int width = (nWidth + 7) / 8 * 8;
        int height = mBitmap.getHeight() * width / mBitmap.getWidth();
        height = (height + 7) / 8 * 8;
        Bitmap rszBitmap = mBitmap;
        if (mBitmap.getWidth() != width) {
            rszBitmap = ImageProcessing.resizeImage(mBitmap, width, height);
        }

        Bitmap grayBitmap = ImageProcessing.toGrayscale(rszBitmap);
        byte[] dithered = thresholdToBWPix(grayBitmap);
        byte[] data = this.eachLinePixToCmd(dithered, width, nMode);
        return data;
    }

    private byte[] thresholdToBWPix(Bitmap mBitmap) {
        int[] pixels = new int[mBitmap.getWidth() * mBitmap.getHeight()];
        byte[] data = new byte[mBitmap.getWidth() * mBitmap.getHeight()];
        mBitmap.getPixels(pixels, 0, mBitmap.getWidth(), 0, 0, mBitmap.getWidth(), mBitmap.getHeight());
        ImageProcessing.format_K_threshold(pixels, mBitmap.getWidth(), mBitmap.getHeight(), data);
        return data;
    }


    private byte[] alignText(String type) {
        byte[] format = {27, 33, 0};
        byte[] arrayOfByte1 = {27, 33, 0};


        switch (type) {
            case Global.LEFT_ALIGN:
                return new byte[]{0x1B, 0x61, 0};

            case Global.RIGHT_ALIGN:
                return new byte[]{0x1B, 0x61, 2};

            case Global.CENTER_ALIGN:

                return new byte[]{0x1b, 0x61, 0x01};

            case Global.SIZE_NORMAL:
                format[2] = ((byte) (0x0 | arrayOfByte1[2]));
                return format;

            case Global.SIZE_MEDIUM:
                format[2] = ((byte) (0x20 | arrayOfByte1[2]));
                return format;

            case Global.SIZE_LARGE:
                format[2] = ((byte) (0x30 | arrayOfByte1[2]));
                return format;

            case Global.UNDERLINE:
                format[2] = ((byte) (0x80 | arrayOfByte1[2]));
                return format;


        }
        format[2] = ((byte) (0x0 | arrayOfByte1[2]));
        return format;

    }


    public static FileInputStream saveBitmapToFile(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 65;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();


            int rotate = 0;

            //file.createNewFile();
            try {

                ExifInterface exif = new ExifInterface(
                        file.getAbsolutePath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
            } catch (Exception e) {

                return inputStream;

            }
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);

            selectedBitmap = Bitmap.createBitmap(selectedBitmap, 0, 0, selectedBitmap.getWidth(), selectedBitmap.getHeight(), matrix, true);


            FileOutputStream outputStream = new FileOutputStream(file);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);

            FileInputStream inputStream1 = new FileInputStream(file);
            return inputStream1;
        } catch (Exception e) {
            return null;
        }
    }


    public static void updateData(final Context context, String refId, ArrayList<String> urlList, HashMap<String, Object> hashMap) {
        HashMap<String, Object> data = new HashMap<>();
        for (String urls : urlList) {
            data.put(urls + "/" + refId, hashMap);


        }
        PersistentDatabase.getDatabase().getReference().updateChildren(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {


                if (task.getException() != null) {
                    showError(context, "Please do the entry again!",
                            new Exception("Error while doing visitor Entry " + task.getException()));
                }


            }
        });
    }

    public static void showError(Context context, String msg, Exception e) {


        final Dialog dialog = new Dialog(context);

        //  dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 2.0f;

        dialog.setContentView(R.layout.allow_block_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        LinearLayout llBtn = (LinearLayout) dialog.findViewById(R.id.ll_dialog_btn);
        ImageView iv_block = (ImageView) dialog.findViewById(R.id.iv_for_allow_block);
        TextViewOpenSans dialog_title = (TextViewOpenSans) dialog.findViewById(R.id.dialog_title);
        ImageView closeDailog = (ImageView) dialog.findViewById(R.id.iv_close_dialog);
        TextView appointment_text = (TextView) dialog.findViewById(R.id.appointment_text);
        llBtn.setVisibility(View.GONE);
        appointment_text.setVisibility(View.VISIBLE);
        iv_block.setVisibility(View.GONE);
        iv_block.setImageResource(R.drawable.no);
        dialog_title.setText("ERROR");
        appointment_text.setText(msg);
        appointment_text.setTextColor(Color.parseColor("#000000"));
        closeDailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                dialog.dismiss();

            }
        };


        Handler handler = new Handler();

        handler.postDelayed(runnable, 5000);
        System.out.println("error is here " + e.toString());
        Crashlytics.logException(new Exception("Error while doing visitor Entry " + e));


    }

    public static ArrayList<String> getHistoryNode(String accessType, long dtTm, SharedPreferences loginSp) {


        ArrayList<String> urlList = new ArrayList<>();
        switch (accessType) {
            case Global.PARENT:
            case Global.VISITOR:
            case Global.ALLOWED_VISITOR:

                urlList.add("sync/visitorHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyDate(dtTm));
                urlList.add("visitorHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyMonth(dtTm));
                return urlList;
            case Global.STAFF:
            case Global.VENDOR:

                urlList.add("sync/accessHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyDate(dtTm));
                urlList.add("accessHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyMonth(dtTm));
                return urlList;
            case Global.STUDENT:

                urlList.add("sync/studentHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyDate(dtTm));
                urlList.add("studentHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyMonth(dtTm));
                return urlList;
            case Global.RESIDENT:

                urlList.add("sync/studentHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyDate(dtTm));
                urlList.add("studentHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyMonth(dtTm));
                return urlList;
            case Global.HELPER:

                urlList.add("sync/helperHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyDate(dtTm));
                urlList.add("helperHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyMonth(dtTm));
                return urlList;

            case Global.HEADCOUNT:

                urlList.add("sync/headCountHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyDate(dtTm));
                urlList.add("headCountHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyMonth(dtTm));
                return urlList;
            default:

                urlList.add("sync/visitorHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyDate(dtTm));
                urlList.add("visitorHistory/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + loginSp.getString(Global.GROUP_ID, "") + "/" + getOnlyMonth(dtTm));


                return urlList;

        }
    }

    public static long getOnlyDate(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);


        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);

        return calendar.getTimeInMillis();
    }

    public static long getOnlyMonth(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.DATE, 1);

        return calendar.getTimeInMillis();
    }

    public static String getAccessHistoryNode(String accessType) {
        switch (accessType) {
            case Global.PARENT:
            case Global.VISITOR:
            case Global.ALLOWED_VISITOR:
                return "visitorHistory";
            case Global.STAFF:
            case Global.VENDOR:
                return "accessHistory";
            case Global.STUDENT:
                return "studentHistory";
            case Global.RESIDENT:
                return "residentHistory";
            case Global.HELPER:
                return "helperHistory";
            default:
                return "visitorHistory";

        }
    }

    public static String getSyncHistoryNode(String accessType) {
        switch (accessType) {
            case Global.PARENT:
            case Global.VISITOR:
            case Global.ALLOWED_VISITOR:
                return "sync/visitorHistory";
            case Global.STAFF:
            case Global.VENDOR:
                return "sync/accessHistory";
            case Global.STUDENT:
                return "sync/studentHistory";
            case Global.RESIDENT:
                return "sync/residentHistory";
            case Global.HELPER:
                return "sync/helperHistory";
            case Global.HEADCOUNT:
                return "sync/headCountHistory";
            default:
                return "sync/visitorHistory";

        }
    }


    private void updateQNode(String hiskey, String id, String nm, SharedPreferences loginsp) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(id, nm);
        PersistentDatabase.getDatabase().getReference("q").child(loginsp.getString(Global.BIZ_ID, ""))
                .child(loginsp.getString(Global.GROUP_ID, "")).child(CommonMethods.getTodaysDate().getTimeInMillis() + "")
                .child("his").child(hiskey).updateChildren(hashMap);

    }

    public static void setData(String refId, ArrayList<String> urlList, String propertyNm, Object val) {
        HashMap<String, Object> data = new HashMap<>();
        for (String urls : urlList) {
            data.put(urls + "/" + refId + "/" + propertyNm, val);

        }
        PersistentDatabase.getDatabase().getReference().updateChildren(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {


                System.out.println("task is " + task.getException());
            }
        });
    }


}