/*package versionx.app.Util;

import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DoorOpenAsyncTask extends AsyncTask<Void, Void, String> {
    private Connection connection;

    @Override
    protected String doInBackground(Void... voids) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            System.err.println("Cannot create connection");
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://192.168.1.105:3306/NxMD4W", "root", "pegasus");
            Statement statement = connection.createStatement();

            String query = "INSERT INTO Emergency (`Door Number`) VALUES (1)";
            //  query = query +"'" +variable+"'";
            boolean result = statement.execute(query);

            System.out.println("result is " + result);
        } catch (Exception e) {
            Crashlytics.log("door open "+e.toString());
        }

        return "Executed";
    }
}*/


package versionx.selfentry.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static android.content.Context.MODE_PRIVATE;

public class DeleteOldDataFromDbAsync extends AsyncTask<Void, Void, String> {
    private Connection connection;
    SharedPreferences loginSp;
    String counter_nm, counter_Id, token_No, Name, key;
    long dt;
    Context context;


    public DeleteOldDataFromDbAsync(SharedPreferences loginSp, Context context) {
        this.loginSp = loginSp;
        this.context = context;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Void... voids) {
        Statement statement = null;
        ResultSet rs = null;
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            System.err.println("Cannot create connection");
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + loginSp.getString(Global.HARDWARE_IP, "") + "/"
                            + loginSp.getString(Global.HARDWARED_DB, "")
                            + "?allowMultiQueries=true",
                    loginSp.getString(Global.HARDWARE_DB_USER, ""),
                    loginSp.getString(Global.HARDWARE_DB_PASSWORD, ""));

            statement = connection.createStatement();

            String disQ = "SET SQL_SAFE_UPDATES = 0; DELETE FROM  queue where `dt`< " + CommonMethods.getEndTimeYesterDay().getTimeInMillis() + ";"
                    + "DELETE FROM  display where `dt` <" + CommonMethods.getEndTimeYesterDay().getTimeInMillis();

            rs = statement.executeQuery(disQ);


        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("db exception" + e.toString());

            return e.toString();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                statement.close();
            } catch (Exception e) {
            }
            try {
                connection.close();
            } catch (Exception e) {
            }
        }

        return "Executed";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);


    }
}

