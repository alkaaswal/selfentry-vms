package versionx.selfentry.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.Gravity;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import versionx.selfentry.Activity.HomeActivity;
import versionx.selfentry.Networking.NetworkingApiConnect;
import versionx.selfentry.gettersetter.ToMeet;
import versionx.selfentry.gettersetter.Visitor;

/**
 * Created by alkaaswal on 4/20/18.
 */

public class FCMTokens {
    DataSnapshot dataSnapshot;
    Context context;

    public FCMTokens(DataSnapshot dataSnapshot, Context context) {
        this.dataSnapshot = dataSnapshot;
        this.context = context;
    }


    public String getTokenId() {
        if (dataSnapshot.hasChild("tkns") &&
                dataSnapshot.child("tkns").hasChild("vx")) {
            return "true";
        } else {
            return null;
        }

    }


    public ArrayList<String> getAllTokens() {
        ArrayList<String> tokens = new ArrayList<>();
        if (dataSnapshot.hasChild("tkns")) {

            if (dataSnapshot.child("tkns").hasChild("vi")) {
                for (DataSnapshot tknSnap : dataSnapshot.child("tkns").child("vi").getChildren()) {
                    tokens.add(tknSnap.getValue(String.class));
                }
            }

            if (dataSnapshot.child("tkns").hasChild("vx")) {
                for (DataSnapshot tknSnap : dataSnapshot.child("tkns").child("vx").getChildren()) {
                    tokens.add(tknSnap.getValue(String.class));
                }
            }
        }

        return tokens;
    }



}
