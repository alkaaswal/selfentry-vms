package versionx.selfentry.Util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Base64;
import android.view.View;
import android.widget.Button;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.HashMap;

import versionx.selfentry.Activity.VerificationActivity;
import versionx.selfentry.Printing.DrawerService;
import versionx.selfentry.Activity.VerificationActivity;
import versionx.selfentry.Printing.DrawerService;

/**
 * Created by alkaaswal on 1/4/18.
 */

public class FirebaseDatabaseSettings {


    Context context;
    DatabaseReference deviceDbREf, deviceDialRef;
    SharedPreferences loginSp;

    ValueEventListener printListner, masterRefListner, deviceSettingsRefListner, deviceVerRefListner,
            printNodeListner, deviceEpassRefListner;
    DatabaseReference printDbRef, masterRef, deviceSettingsRef, deviceVerRef, deviceEpassRef;
    Button btn_epass;

    public FirebaseDatabaseSettings(Context context, Button btn_epass) {
        this.context = context;
        loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        this.btn_epass = btn_epass;

    }

    public void getAllSettings() {


        //// updating time everytime app opens
        PersistentDatabase.getDatabase().
                getReference("device/" + loginSp.getString(Global.BIZ_ID, "")
                        + "/" + Global.APP_NAME + "/" +
                        loginSp.getString(Global.UUID, "")).child("dt").setValue(System.currentTimeMillis());


        printDbRef = PersistentDatabase.getDatabase()
                .getReference("device").child(loginSp.getString(Global.BIZ_ID, ""))
                .child(Global.APP_NAME)
                .child(loginSp.getString(Global.UUID, ""));


        printListner = printDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                try {

                    if (dataSnapshot.exists()) {

                        if (dataSnapshot.hasChild("print"))

                        {
                            loginSp.edit().putInt(Global.PRINTING, dataSnapshot.child("print")
                                    .getValue(Integer.class)).apply();
                            if (dataSnapshot.child("print").getValue(Integer.class) == Global.NETWORK_PRINT &&
                                    dataSnapshot.hasChild("ip")) {
                                loginSp.edit().putString(Global.NETWORK_IP, dataSnapshot.child("ip").getValue(String.class)).apply();
                            }
                        } else {
                            loginSp.edit().putInt(Global.PRINTING, Global.BLUETOOTH_PRINT).apply();
                        }


                    } else {
                        loginSp.edit().putInt(Global.PRINTING, Global.BLUETOOTH_PRINT).apply();

                    }

                    if (dataSnapshot.hasChild("cnfm")) {
                        loginSp.edit().putBoolean(Global.CONFIRM, dataSnapshot.child("cnfm").getValue(boolean.class)).apply();
                    } else {
                        loginSp.edit().putBoolean(Global.CONFIRM, false).apply();

                    }
                    if (dataSnapshot.hasChild("acnfm")) {
                        loginSp.edit().putBoolean(Global.ALWAYS_CONFIRM, dataSnapshot.child("acnfm").getValue(boolean.class)).apply();

                    } else {
                        loginSp.edit().putBoolean(Global.ALWAYS_CONFIRM, false).apply();

                    }


                } catch (Exception e) {
                    // FirebaseCrash.report(new Exception("print " + e.toString()));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        printNodeListner = printDbRef.child("print").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.exists()) {

                        if (DrawerService.workThread != null && DrawerService.workThread.isConnected()) {
                            DrawerService.workThread.disconnectBt();
                            DrawerService.workThread.disconnectNet();
                            DrawerService.workThread.disconnectUsb();
                        }
                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        //////////////////// getting masterData setting for markout and notify

        masterRef = PersistentDatabase.getDatabase().
                getReference("masterData/" + loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, ""));
        masterRef.keepSynced(true);
        masterRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    loginSp.edit().putBoolean(Global.SETTINGS_MARK_OUT, dataSnapshot.child(Global.CAST_NODE).child("markOut").getValue(Boolean.class)).apply();
                    loginSp.edit().putInt(Global.MARK_OUT_MIN, dataSnapshot.child(Global.CAST_NODE).child("min").getValue(Integer.class)).apply();
                    loginSp.edit().putBoolean(Global.VIZ_IN_NOTIFY, dataSnapshot.child("notify").child("viEnt")
                            .child("in").getValue(Boolean.class)).apply();
                    loginSp.edit().putBoolean(Global.VIZ_OUT_NOTIFY, dataSnapshot.child("notify").child("viEnt")
                            .child("out").getValue(Boolean.class)).apply();

                    loginSp.edit().putBoolean(Global.ST_OUT_NOTIFY, dataSnapshot.child("notify").child("stEnt")
                            .child("out").getValue(Boolean.class)).apply();

                    loginSp.edit().putBoolean(Global.ST_IN_NOTIFY, dataSnapshot.child("notify").child("stEnt")
                            .child("in").getValue(Boolean.class)).apply();

                    if (dataSnapshot.child(Global.APP_NAME).hasChild("face"))
                        loginSp.edit().putBoolean(Global.FACE_DETECTION, dataSnapshot.child(Global.APP_NAME).child("face").getValue(boolean.class)).apply();
                    /* loginSp.edit().putBoolean(Global.AUTO_PRINT, dataSnapshot.child(Global.APP_NAME).child("autoPrint").getValue(Boolean.class)).apply();*/

                    loginSp.edit().putBoolean(Global.AUTO_PRINT, true).apply();


                    if (dataSnapshot.hasChild("visitor")) {

                        if (dataSnapshot.child("visitor").child("sync").getValue(Boolean.class)) {
                            DatabaseReference visitorProfileSynced = PersistentDatabase.getDatabase().
                                    getReference("visitor/" + loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, ""));

                            visitorProfileSynced.keepSynced(true);
                        } else {
                            DatabaseReference visitorProfileSynced = PersistentDatabase.getDatabase().
                                    getReference("visitor/" + loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, ""));

                            visitorProfileSynced.keepSynced(false);
                        }
                        if (dataSnapshot.child("visitor").hasChild("clTmOut")) {
                            loginSp.edit().putLong(Global.CALL_WAIT, dataSnapshot.child("visitor").child("clTmOut").getValue(Long.class)).apply();
                        }

                        if (dataSnapshot.child("visitor").hasChild("qrS")) {
                            loginSp.edit().putInt(Global.QR_CODE_SIZE,
                                    dataSnapshot.child("visitor").child("qrS").getValue(Integer.class)).apply();
                        }

                        if (dataSnapshot.child("visitor").hasChild("reImg")) {
                            loginSp.edit().putBoolean(Global.IMG_REQUIRED,
                                    dataSnapshot.child("visitor").child("reImg").getValue(Boolean.class)).apply();

                        } else {
                            loginSp.edit().putBoolean(Global.IMG_REQUIRED, false).apply();
                        }
                    }


                    if (dataSnapshot.child(Global.APP_NAME).hasChild("pin")) {
                        loginSp.edit().putInt(Global.LOCK_PIN, dataSnapshot.child(Global.APP_NAME).child("pin").getValue(Integer.class)).apply();
                    } else {
                        loginSp.edit().putInt(Global.LOCK_PIN, -1).apply();
                    }


                } catch (Exception e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        /////////////////////////////////////////// device settings ////////////////////////////////////
        final SharedPreferences.Editor editor = loginSp.edit();

        deviceSettingsRef = PersistentDatabase.getDatabase().getReference("deviceSetting/" + loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, ""));

        if (deviceSettingsRefListner != null)
            deviceSettingsRef.removeEventListener(deviceSettingsRefListner);

        deviceSettingsRefListner = deviceSettingsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {

                        /*if (dataSnapshot.hasChild("verifyMob"))
                            editor.putBoolean(Global.SETTINGS_MOBILE_VERIFICATION, Boolean.parseBoolean(dataSnapshot.child("verifyMob").child("state").getValue().toString()));*/
                    if (dataSnapshot.hasChild("printSlip")) {
                        editor.putBoolean(Global.SETTINGS_PRINT_REQUIRED,
                                Boolean.parseBoolean(dataSnapshot.child("printSlip").child("state").getValue().toString()));

                           /* if (dataSnapshot.child("printSlip").child("state").getValue(Boolean.class))
                                CommonMethods.enablebluetooth();
                            else
                                CommonMethods.disableBluetooth();*/


                    }
                    if (dataSnapshot.child("printSlip").hasChild("footer"))
                        editor.putString(Global.SETTINGS_SLIP_FOOTER_MSG, dataSnapshot.child("printSlip").child("footer").getValue().toString());
                    if (dataSnapshot.child("printSlip").hasChild("header"))
                        editor.putString(Global.SETTINGS_SLIP_HEADER_MSG, dataSnapshot.child("printSlip").child("header").getValue().toString());
                    /*if (dataSnapshot.hasChild("photo"))
                        editor.putBoolean(Global.SETTINGS_PHOTO_REQUIRED, Boolean.parseBoolean(dataSnapshot.child("photo").child("state").getValue().toString()));
                   */ if (dataSnapshot.hasChild("disconnect"))
                        editor.putBoolean(Global.SETTINGS_AUTO_CUT_MOBILE, Boolean.parseBoolean(dataSnapshot.child("disconnect").child("state").getValue().toString()));
                    if (dataSnapshot.hasChild("printImg"))
                        editor.putBoolean(Global.SETTINGS_PRINT_PHOTO, Boolean.parseBoolean(dataSnapshot.child("printImg").child("state").getValue().toString()));
                    if (dataSnapshot.hasChild("confirm"))
                        editor.putBoolean(Global.SETTINGS_CONFIRM_REQUIRED, Boolean.parseBoolean(dataSnapshot.child("confirm").child("state").getValue().toString()));
                    if (dataSnapshot.hasChild("pass"))
                        editor.putInt(Global.SETTINGS_CHECKOUT_TIME, Integer.parseInt(dataSnapshot.child("pass").child("hr").getValue().toString()));
                    if (dataSnapshot.hasChild("visitorMob"))
                        editor.putBoolean(Global.SETTINGS_VIZ_MOB, Boolean.parseBoolean(dataSnapshot.child("visitorMob").child("state").getValue().toString()));


                    if (dataSnapshot.hasChild("pLogo")) {
                        editor.putBoolean(Global.SETTINGS_PRINT_LOGO, dataSnapshot.child("pLogo").child("state").getValue(Boolean.class));


                        if (dataSnapshot.child("pLogo").hasChild("img")) {
                            Glide.with(context).load(dataSnapshot.child("pLogo").child("img").getValue().toString()).asBitmap().override(400, 500).fitCenter().into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    resource.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                                    byte[] b = baos.toByteArray();
                                    String logo = Base64.encodeToString(b, Base64.DEFAULT);
                                    editor.putString(Global.LOGO, logo).apply();

                                }
                            });


                        } else {
                            editor.putString(Global.LOGO, "").apply();
                        }
                    }


                    editor.apply();

                    setFooterMessage();

                    setHeaderMessage();

                } catch (Exception e) {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        /////////////////////////////////////////// getting branding from admin node ////////////////////////////////////


        DatabaseReference adminRef = PersistentDatabase.getDatabase().getReference("admin/printSlip");

        adminRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    editor.putString(Global.SETTINGS_SLIP_BRANDING_MSG, dataSnapshot.child("branding").getValue().toString());
                    editor.apply();
                    setBrandingMessage();
                } catch (Exception e) {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        ////////// device epass////////////////////

        deviceEpassRef = PersistentDatabase.getDatabase().
                getReference("device/" + loginSp.getString(Global.BIZ_ID, "") + "/"
                        + Global.APP_NAME
                        + "/" + loginSp.getString(Global.UUID, "") + "/epass");


        deviceEpassRefListner = deviceEpassRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.exists() && dataSnapshot.getValue(Boolean.class)) {

                    btn_epass.setVisibility(View.VISIBLE);

                    // loginSp.edit().putBoolean(Global.EPASS, dataSnapshot.getValue(Boolean.class)).apply();

                } else {
                    // loginSp.edit().putBoolean(Global.EPASS, false).apply();
                    btn_epass.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        ////////////////////////////////// sending verison number ////////////////////////////////////////


        deviceVerRef = PersistentDatabase.getDatabase().
                getReference("device/" + loginSp.getString(Global.BIZ_ID, "") + "/"
                        + Global.APP_NAME
                        + "/" + loginSp.getString(Global.UUID, "") + "/l");


        deviceVerRefListner = deviceVerRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                try {

                    if (loginSp.getBoolean(Global.IS_LOGGED_IN, false)
                            && dataSnapshot != null && dataSnapshot.getValue() != null
                            && !dataSnapshot.getValue().toString().equalsIgnoreCase("")) {

                        String version = new CommonMethods().getVerisonNumber(context);
                        DatabaseReference verRef = PersistentDatabase.getDatabase()
                                .getReference("device/" + loginSp.getString(Global.BIZ_ID, "")
                                        + "/" + Global.APP_NAME
                                        + "/" + loginSp.getString(Global.UUID, ""));

                        HashMap<String, Object> hashMap = new HashMap<String, Object>();
                        if (dataSnapshot.getValue().toString().equalsIgnoreCase("ver"))
                            hashMap.put(dataSnapshot.getValue().toString(), version);
                        if (dataSnapshot.getValue().toString().equalsIgnoreCase("now"))
                            hashMap.put("dt", Calendar.getInstance().getTimeInMillis());
                        verRef.updateChildren(hashMap);

                        if (dataSnapshot.getValue().toString().equalsIgnoreCase("status")) {
                            CommonMethods.logout(context);
                            Intent in = new Intent(context, VerificationActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            ((Activity) (context)).finish();
                            context.startActivity(in);
                        }


                        deviceVerRef.setValue("");

                    }


                } catch (Exception e) {

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void setBrandingMessage() {

        String message[] = loginSp.getString(Global.SETTINGS_SLIP_BRANDING_MSG, "").split("\\r?\\n");
        SharedPreferences.Editor editor = loginSp.edit();

        for (int i = 0; i < message.length; i++) {
            editor.putString("BrandingMessageLine_" + i, message[i]);
        }


        editor.putInt(Global.SETTINGS_SLIP_BRANDING_MSG_SIZE, message.length);


        editor.commit();


    }

    public void setHeaderMessage() {

        String message[] = loginSp.getString(Global.SETTINGS_SLIP_HEADER_MSG, "").split("\\r?\\n");
        SharedPreferences.Editor editor = loginSp.edit();

        for (int i = 0; i < message.length; i++) {
            editor.putString("HeaderMessageLine_" + i, message[i]);
        }


        editor.putInt(Global.SETTINGS_SLIP_HEADER_MSG_SIZE, message.length);


        editor.commit();


    }


    public void setFooterMessage() {

        String message[] = loginSp.getString(Global.SETTINGS_SLIP_FOOTER_MSG, "").split("\\r?\\n");
        SharedPreferences.Editor editor = loginSp.edit();

        for (int i = 0; i < message.length; i++) {
            editor.putString("Authentication_Array_" + i, message[i]);
        }


        editor.putInt(Global.SETTINGS_SLIP_FOOTER_MSG_SIZE, message.length);


        editor.commit();


    }


    public void removeListners() {
        try {

            if (printDbRef != null && printListner != null)
                printDbRef.removeEventListener(printListner);
            if (masterRef != null && masterRefListner != null)
                masterRef.removeEventListener(masterRefListner);

            if (deviceSettingsRef != null && deviceSettingsRefListner != null)
                deviceSettingsRef.removeEventListener(deviceSettingsRefListner);

            if (deviceVerRef != null && deviceVerRefListner != null) {
                deviceVerRef.removeEventListener(deviceVerRefListner);
            }

            if (printDbRef != null && printNodeListner != null) {
                printDbRef.child("print").removeEventListener(printNodeListner);
            }

            if (deviceEpassRef != null && deviceEpassRefListner != null) {
                deviceEpassRef.removeEventListener(deviceEpassRefListner);
            }
        } catch (Exception e) {

        }
    }


}
