package versionx.selfentry.Util;


public class Global {


    public static String PROJECT_ID = "293044879387";  // "558700389898"
    public static String SERVER_ERROR_MSG = "Server error! please try after some time";
    public static String NO_INTERNET_MSG = "No Internet Connection! Try again";
    public static String CHECK_INTERNET_MSG = "Check your internet connection and try again!";
    public static String CONNECT_PRINTER = "Please connect to the printer first from setting menu";


    /* public static final String BYTESPARA1 = "bytespara1";

     public static final String STRPARA1 = "strpara1";*/
    public static final int CMD_POS_STEXTOUT = 100110;
    public static final String BYTESPARA1 = "bytespara1";
    public static final String BYTESPARA2 = "bytespara2";
    public static final String BYTESPARA3 = "bytespara3";
    public static final String BYTESPARA4 = "bytespara4";
    public static final String INTPARA1 = "intpara1";
    public static final String INTPARA2 = "intpara2";
    public static final String INTPARA3 = "intpara3";
    public static final String INTPARA4 = "intpara4";
    public static final String INTPARA5 = "intpara5";
    public static final String INTPARA6 = "intpara6";
    public static final String STRPARA1 = "strpara1";
    public static final String STRPARA2 = "strpara2";
    public static final String STRPARA3 = "strpara3";
    public static final String STRPARA4 = "strpara4";
    public static final int FONTSTYLE_BOLD = 0x08;
    public static final int CMD_POS_WRITE = 100100;
    public static String APP_NAME = "selfEntry";
    public static String BIZ_ID = "biz_id";
    public static String BIZ_PERSON_NAME = "biz_person_name"; //ChairmanName
    public static String BIZ_COMPANY_NAME = "biz_company_name";
    public static String BIZ_COMPANY_IMG = "ProfileImg";
    public static String BIZ_EXPIRY_DATE = "expiryDate";
    public static String LOGIN_SP = "Login";
    public static String VIZ_IMAGE_SP = "VIZ_IMAGE";
    public static final String VISITOR_ID = "vID";
    public static String VIZ_IN_NOTIFY = "vizInNotify";
    public static String VIZ_OUT_NOTIFY = "vizOutNotify";
    public static String AUTO_PRINT = "autoPrint";
    public static String LOCK_PIN = "lock_pin";
    public static String SELF_MODE = "selfMode";
    public static final String COUNT_FIELD_ID = "count";
    public static final String TOKEN = "tkn";
    public static final String TOKEN_NO = "tknNo";
    public static final String DEVICE_CODE = "deviceCode";
    public static final String IMG_REQUIRED="reenterImg";

    public static String FACE_DETECTION = "faceDetection";
    public static final String MOB_LENGTH = "mob_length";

    public static final String PACKAGE_NAME = "versionx.selfentry";
    public static final String ACTION_USB_PERMISSION = PACKAGE_NAME + ".USB_PERMISSION";

    public static final String CAST_NODE = "cast";
    public static final String VISITOR_HISTORY_NODE = "visitorHistory";
    public static final String HARDWARE = "hardware";
    public static final String HARDWARE_IP = "hardware_ip";
    public static final String HARDWARE_DB_USER = "db_usernm";
    public static final String HARDWARE_DB_PASSWORD = "db_userPassword";
    public static final String HARDWARED_DB = "hwDb";
    public static final String RE_ENTER_TO_MEET = "reEnterToMeet";


    //added by Rajesh...
    //we're creating a variable to hold the business type....
    public static String BIZ_TYPE = "biz_type";


    public static String SETTINGS_VIZ_MOB = "visitorMob";
    public static String SETTINGS_PHOTO_REQUIRED = "photoRequired";
    public static String SETTINGS_PRINT_REQUIRED = "printRequired";
    public static String SETTINGS_PRINT_PHOTO = "printImageRequired";
    public static String SETTINGS_CONFIRM_REQUIRED = "confirmRequired";
    public static String SETTINGS_CHECKOUT_TIME = "checkOutTime";
    public static String SETTINGS_SLIP_FOOTER_MSG = "Authentication_Array";
    public static String SETTINGS_SLIP_FOOTER_MSG_SIZE = "Authentication_Array_size";
    public static String SETTINGS_SLIP_BRANDING_MSG = "BrandingMessageArray";
    public static String SETTINGS_SLIP_BRANDING_MSG_SIZE = "BrandingMessageArray_size";
    public static String SETTINGS_SLIP_FOOTER_LINE = "Authentication_Array_";
    public static String SETTINGS_SLIP_HEADER_LINE = "HeaderMessageLine_";
    public static String IS_LOGGED_IN = "IsLoggedIn";
    public static String SETTINGS_SLIP_BRANDING_LINE = "BrandingMessageLine_";
    public static String SETTINGS_MARK_OUT = "markOut";
    public static String MARK_OUT_MIN = "markOutMin";
    public static final String DEV_USER_LOGIN = "ULOGIN";
    public static final String COMPLETED = "completed";
    public static String CAST_HOME_ACTIVITY = "versionx.app.Home_Activity";


    public static String SETTINGS_SLIP_HEADER_MSG = "HeaderMessageArray";
    public static String SETTINGS_SLIP_HEADER_MSG_SIZE = "HeaderMessageArray_size";
    public static String SETTINGS_VEHICLE = "vehicle";
    public static String BLUETOOTH_SP = "Bluetooth";
    public static String DEVICE_NAME = "device_name";
    public static String SETTINGS_AUTO_CUT_MOBILE = "autoCutMobile";
    public static String VERSION_NUMBER = "verisonNumber";

    public static String VERSION_EXPIRED = "verisonExp";
    public static String VERSION_UPDATE_DATE = "verisonUpdateDate";
    public static String GROUP_ID = "groupId";
    public static final String UUID = "uuid";
    public static final String GROUP_NAME = "groupName";
    public static final String FIREBASE_TOKEN = "firebaseToken";
    public static final String DATE_FORMAT = "d MMM h:mm a";

    public static final int CALL_DIALED = 1;
    public static final int CALL_MANNUAL = 2;
    public static final int CALL_VENDOR_PASS_SCAN = 3;
    public static final int IN_VENDOR_PASS_NFC = 4;
    public static final int CALL_EPASS = 5;

    public static final int OUT_SCAN_QR = 1;
    public static final int OUT_MANNUAL = 2;
    public static final int OUT_VENDOR_PASS_SCAN = 3;
    public static final int OUT_VENDOR_PASS_NFC = 4;


    public static final String LAST_HIS_KEY = "lastHisKey";
    public static final String LAST_TOKEN_NO = "lastTokenNo";
    public static final String LAST_PHONE_NO = "lastPhoneNo";
    public static final String LAST_CALLING = "lastCalling";
    public static final String IS_DEV_USER_LOGGEDIN = "isDevUserLoggedIn";
    public static final String LOGGNED_IN_USER_NM = "username";
    public static final String LOGGNED_IN_USER_ID = "userId";
    public static final String LOGGNED_IN_USER_MOB = "userMob";
    public static final String EPASS = "epass";
    public static final String SCREEN_TYPE_MEET = "meet";
    public static final String SCREEN_TYPE_WHOCAME = "whoCame";
    public static final String START_SCREEN_MSG = "startMsg";
    public static final String START_SCREEN = "startScreen";
    public static final String CALL_BLOCK = "callblock";
    public static final String WAITING_TM = "waitingTm";
    public static final String WAIT_MSG = "waitMsg";
    public static final String NO_RESPONSE_MSG = "noResMsg";
    public static final String REJECT_MSG = "rejectMsg";
    public static final String CONFIRM = "confirm";
    public static final String ALWAYS_CONFIRM = "alwaysConfirm";
    public static final String BLOCK_MSG = "blockMsg";
    public static final String THRESHOLD = "thr";
    public static final String TIME_TRACK = "timeTrack";
    public static String PARENT_RELATION = "Parent_Rel";
    public static String CALL_TIME_OUT = "clTmOut";
    public static String QR_CODE_SIZE = "qrCodeSize";
    public static String HEAD_COUNT_TIME = "headCountTime";
    public static String HC_IMG_REQUIRED = "headCountImg";
    public static String HC_PRINT_REQUIRED = "headCountPrint";
    public static String HEADCOUNT_ENABLE = "headCountEnable";
    public static String HC_HEADER = "headCountHeader";
    public static String ALLOWED_PRINT = "allowVisitorPrint";
    public static String PARENT_PRINT = "parentPrint";
    public static String HEADER_SIZE = "headerSize";
    public static String LOGO = "logo";
    public static String SETTINGS_PRINT_LOGO = "printLogo";
    public static String FORM_RESTRICT_ID="formRestrictId";
    public static String PURPOSE_LBL="pLbl";
    public static String TO_MEET_LBL="toMeetLbl";

    public static String FORM_RESTRICT_NM="formRestrictNm";
    public static String IS_DATA_LOADED ="isDataLoaded";


    //Access codes

    public static final String STAFF = "ST";
    public static final String RESIDENT = "RE";
    public static final String PARENT = "PA";
    public static final String VISITOR = "VI";
    public static final String ALLOWED_VISITOR = "VIA";
    public static final String VENDOR = "VE";
    public static final String STUDENT = "STU";
    public static final String HELPER = "HLP";
    public static final String HEADCOUNT = "VHC";


    //Custom Fields

    public static final String TEXTAREA = "ta";
    public static final String TEXTBOX = "tb";
    public static final String DROPDOWN = "dd";
    public static final String IMG = "img";
    public static final String SEARCHABLE = "srch";
    public static final String NUMBER = "num";
    public static final String SEPERATION = "sep";
    public static final String RADIOBUTTON = "rb";

    public static final String SIZE_MEDIUM = "M";
    public static final String SIZE_NORMAL = "N";
    public static final String SIZE_LARGE = "L";


    public static final String LEFT_ALIGN = "left";
    public static final String RIGHT_ALIGN = "right";
    public static final String CENTER_ALIGN = "center";
    public static final String UNDERLINE = "underLine";

    //reserved Fields...
    public static final String VEHICLE = "veh";


    public static final String DIAL_SCREEN = "dialScreen";

    public static final String CAM = "camera";
    public static final String CALL_WAIT = "callWait";


    public static final int KIOSK_SELF = 1;
    public static final int KIOSK_DESKTOP = 2;


    ///printing type
    public static final String PRINTING = "print";
    public static final int BLUETOOTH_PRINT = 0;
    public static final int NETWORK_PRINT = 1;
    public static final int USB_PRINT = 2;


    ///button show and hide
    public static final int ONLY_PRINT = 0;
    public static final int BOTH_EPASS_PRINT = 2;
    public static final int ONLY_EPASS = 1;
    public static final String BTN_REQUIRE = "btnReq";


    public static final String NETWORK_IP = "ipAdd";
    public static String USB_PRODUCT_ID = "usbDeviceId";
    public static String USB_VENDORID = "usbDeviceId";

    public static String ST_IN_NOTIFY = "stInNotify";
    public static String ST_OUT_NOTIFY = "stOutNotify";



    //firebase DBs
    public static int PRIMARY_DB = 0;
    public static int SECONDARY_DB = 1;

    //node names

/*    public static final String DEVICE_REF="device/";
    public static final String ACCESS_REF="access/";
    public static final String ACCESS_HISTORY_REF="accessHistory/";
    public static final String ADMIN_REF="admin/";
    public static final String APPOINTMENT_REF="appt/";
    public static final String BIZ_REF="biz/";
    public static final String DEVICE_SETTING_REF="deviceSetting/";
    public static final String FIELD_REF="field/";
    public static final String MOBBIZ_REF="mobBiz/";
    public static final String PURPOSE_REF="purpose/";
    public static final String VISIOR_REF="visitor/";
    public static final String VISITOR_PROFILE="visitorProfile/";
    public static final String WAIT_REF="wait/";*/


}
