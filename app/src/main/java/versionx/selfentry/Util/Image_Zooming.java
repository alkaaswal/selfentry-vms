package versionx.selfentry.Util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import versionx.selfentry.R;
import versionx.selfentry.R;

import java.io.File;

public class Image_Zooming extends Activity {
    SubsamplingScaleImageView imageDetail;

    static final int NONE = 0;
    Bitmap bitmap;
    int width, height;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image__zooming);
        getScreenSize();
        imageDetail = (SubsamplingScaleImageView) findViewById(R.id.imageView1);
        //  System.out.println("inside zooming");


        if (getIntent().getStringExtra("BitmapPath") != null && isFileExists(getIntent().getStringExtra("BitmapPath"))) {


            Glide.with(Image_Zooming.this)
                    .load(getIntent().getStringExtra("BitmapPath"))
                    .asBitmap()
                    .override(500, 500)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                            imageDetail.setImage(ImageSource.bitmap(resource));


                        }
                    });
/*
            bitmap = CommonMethods.rotateBitmap(getIntent().
                    getStringExtra("BitmapPath"), new CommonMethods().decodeSampledBitmapFromFile(getIntent().getStringExtra("BitmapPath"), 700, 700), null, Image_Zooming.this);*/
            //  imageDetail.setImageBitmap(bitmap);


            // imageDetail.setImage(ImageSource.bitmap(bitmap));
        } else if (getIntent().getByteArrayExtra("BitmapImage") != null) {

            bitmap = BitmapFactory.decodeByteArray(getIntent().getByteArrayExtra("BitmapImage"), 0, getIntent().getByteArrayExtra("BitmapImage").length);
            // imageDetail.setImageBitmap(bitmap);
            bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
            imageDetail.setImage(ImageSource.bitmap(bitmap));
        } else if (getIntent().getStringExtra("BitmapUrl") != null) {

        /*    if (orgimageLoader == null)
                orgimageLoader = new ImageLoaderOriginal(getApplicationContext());

            orgimageLoader.DisplayImage(getIntent().getStringExtra("BitmapUrl"), imageDetail);*/
        }

        imageDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


    }


    public void getScreenSize() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    public boolean isFileExists(String filePath) {
        File file = new File(filePath);
        if (file.exists())
            return true;
        else
            return false;
    }

    public Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bitmap != null && bitmap.isRecycled())
            bitmap.recycle();

    }
}