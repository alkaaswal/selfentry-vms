/*package versionx.app.Util;

import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DoorOpenAsyncTask extends AsyncTask<Void, Void, String> {
    private Connection connection;

    @Override
    protected String doInBackground(Void... voids) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            System.err.println("Cannot create connection");
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://192.168.1.105:3306/NxMD4W", "root", "pegasus");
            Statement statement = connection.createStatement();

            String query = "INSERT INTO Emergency (`Door Number`) VALUES (1)";
            //  query = query +"'" +variable+"'";
            boolean result = statement.execute(query);

            System.out.println("result is " + result);
        } catch (Exception e) {
            Crashlytics.log("door open "+e.toString());
        }

        return "Executed";
    }
}*/


package versionx.selfentry.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static android.content.Context.MODE_PRIVATE;

public class TokenUpdate extends AsyncTask<Void, Void, String> {
    private Connection connection;
    SharedPreferences loginSp;
    String counter_nm, counter_Id, token_No, Name, key;
    long dt;
    Context context;


    public TokenUpdate(SharedPreferences loginSp, String counter_nm,
                       String counter_Id, long dt, String token_No, String Name, String key, Context context) {
        this.loginSp = loginSp;
        this.counter_nm = counter_nm;
        this.counter_Id = counter_Id;
        this.dt = dt;
        this.Name = Name;
        this.token_No = token_No;
        this.key = key;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Void... voids) {
        // String result="Exceuted";
        ResultSet rs = null, rsTkn = null;
        Statement statement = null;
        ResultSet qrs = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            System.err.println("Cannot create connection");
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + loginSp.getString(Global.HARDWARE_IP, "")
                            + "/" + loginSp.getString(Global.HARDWARED_DB, "") + "?allowMultiQueries=true",
                    loginSp.getString(Global.HARDWARE_DB_USER, ""),
                    loginSp.getString(Global.HARDWARE_DB_PASSWORD, ""));


            statement = connection.createStatement();

            String disQ = "SELECT COUNT(*) c FROM display WHERE cId ='" + counter_Id + "'";

            rs = statement.executeQuery(disQ);

            if (rs.next()) {

                int rowCount = rs.getInt("c");

                if (rowCount == 0) {
                    String disQuery = "INSERT INTO display (`cNm`, `cId` , `dt`, `tkn`, `viNm`, `viKey`) VALUES " +
                            "('" + counter_nm + "', '" + counter_Id + "', " + dt + ", '" + token_No + "', '" + Name + "','" + key + "')";
                    statement.execute(disQuery);


                    PersistentDatabase.getDatabase().getReference("masterData").child(
                            loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, "")).child("entryToken")
                            .child("hw").child("dbU").setValue(System.currentTimeMillis());

                } else {
                    String disTkn = "SELECT COUNT(*) c FROM display WHERE tkn ='" + token_No + "'";

                    rsTkn = statement.executeQuery(disTkn);
                    if (rsTkn.next()) {

                        int rCount = rsTkn.getInt("c");
                        if (rCount == 0)
                        {

                            String qCheck = "SELECT COUNT(*) c FROM queue WHERE tkn ='" + token_No + "'";
                            qrs = statement.executeQuery(qCheck);

                            if (qrs.next()) {
                                int qrowCount = qrs.getInt("c");
                                if (qrowCount == 0) {
                                    String query = "INSERT INTO queue (`cNm`, `cId` , `dt`, `tkn`, `viNm`, `viKey`) VALUES ('" + counter_nm + "', '"
                                            + counter_Id + "', " + dt + ", '" + token_No + "', '" + Name + "','" + key + "')";
                                    statement.execute(query);

                                } else {
                                    String audioQry = "SET SQL_SAFE_UPDATES = 0; UPDATE queue SET `cNm`='" + counter_nm + "' ,  `cId`='" + counter_Id + "' , `viNm`='" + Name
                                            + "' WHERE `tkn` ='" + token_No + "'";
                                    statement.execute(audioQry);

                                }
                            }
                        } else {
                            String audioQry = "SET SQL_SAFE_UPDATES = 0; UPDATE display SET `cNm`='" + counter_nm + "' ,  `cId`='" + counter_Id + "' , `viNm`='" + Name
                                    + "' WHERE `tkn` ='" + token_No + "'";
                            statement.execute(audioQry);
                        }

                    }

                }

            }


        } catch (Exception e) {
            Crashlytics.logException(e);
            Crashlytics.log("db exception" + e.toString());

            return e.toString();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }

            try {
                qrs.close();
            } catch (Exception e) {
            }

            try {
                rsTkn.close();
            } catch (Exception e) {

            }
            try {

                statement.close();
            } catch (Exception e) {
            }
            try {
                connection.close();
            } catch (Exception e) {
            }
        }

        return "Executed";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        for (int i = 0; i < 2; i++) {
            final Toast toast = Toast.makeText(context, s, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}

