package versionx.selfentry.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.IntegerRes;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import versionx.selfentry.gettersetter.ToMeet;
import versionx.selfentry.gettersetter.Visitor;
import versionx.selfentry.interfaces.AsynctaskFinishedListner;
import versionx.selfentry.gettersetter.ToMeet;
import versionx.selfentry.gettersetter.Visitor;
import versionx.selfentry.interfaces.AsynctaskFinishedListner;


/**
 * Created by alkaaswal on 9/6/17.
 */

public class TransactionData implements AsynctaskFinishedListner {

    Context context;
    SharedPreferences loginSp;
    boolean verified;
    String vizKey;
    String visitorMob, visitorNm;
    long dt;
    AsynctaskFinishedListner listner;
    String vizUrl;
    long inDt;
    long currentMonth;
    Visitor visitor;
    ToMeet selectedToMeet;
    DatabaseReference vizHis, vizPendAgg;
    DatabaseReference visitorReportDataRef;
    ArrayList<String> urlList;


    public TransactionData(final Context context, final SharedPreferences loginSp,
                           final boolean verified, final String vizKey, final long dt, String vizUrl,
                           long inDt, Visitor visitor, ToMeet selectedToMeet) {

        this.context = context;
        this.loginSp = loginSp;
        this.verified = verified;
        this.vizKey = vizKey;
        this.visitorMob = visitor.getMobile();
        this.visitorNm = visitor.getName();
        this.dt = dt;
        this.vizUrl = vizUrl;
        this.visitor = new Visitor(visitor);
        this.inDt = inDt;
        this.selectedToMeet = selectedToMeet;
        currentMonth = CommonMethods.getFirstDayOfMonth().getTimeInMillis();

        vizHis = PersistentDatabase.getDatabase().getReference(Global.VISITOR_HISTORY_NODE).
                child(loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, ""))
                .child(visitor.getPrevtDt() + "").child(vizKey).child("agg");

        urlList = CommonMethods.getHistoryNode(Global.VISITOR, CommonMethods.getOnlyDate(inDt), loginSp);

        vizPendAgg = PersistentDatabase.getDatabase().getReference(Global.VISITOR_HISTORY_NODE).
                child(loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, ""))
                .child(visitor.getPrevtDt() + "").child(vizKey).child("pendAgg");

        visitorReportDataRef = PersistentDatabase.getDatabase().getReference("visitorReportData")
                .child(loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, "")).
                        child("" + Calendar.getInstance().get(Calendar.YEAR)).child("" + (Calendar.getInstance().get(Calendar.MONTH) + 1));

    }


    public void visitorUpdate() {
        if (CommonMethods.isInternetWorking(context)) {
            CheckInternetLimitedAccess checkInternetLimitedAccess = new CheckInternetLimitedAccess();
            checkInternetLimitedAccess.onAsynctaskFinished(this);
            checkInternetLimitedAccess.execute();
        } else {
            updatePendingTransaction();
        }

    }


    private void addCount(final SharedPreferences loginSp, final boolean verified, final String vizKey,
                          final String visitorMob, final String visitorNm,
                          final long dt, final Visitor visitor, final ToMeet selectedToMeet) {
        final DatabaseReference visitorReportRef = PersistentDatabase.getDatabase().getReference("visitorReport")
                .child(loginSp.getString(Global.BIZ_ID, "")).child(loginSp.getString(Global.GROUP_ID, "")).
                        child("" + Calendar.getInstance().get(Calendar.YEAR)).child("" + (Calendar.getInstance().get(Calendar.MONTH) + 1));


        visitorReportRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {


                if (mutableData.getValue() == null) {


                    //cnt node //month
                    HashMap<String, Object> hashMapMonth = new HashMap<String, Object>();
                    hashMapMonth.put("nv", verified ? 0 : 1);
                    hashMapMonth.put("v", verified ? 1 : 0);
                    hashMapMonth.put("ep", loginSp.getBoolean(Global.EPASS, false) ? 1 : 0);
                    hashMapMonth.put("tot", 1);
                    hashMapMonth.put("viCnt", visitor.getCount());
                    hashMapMonth.put("fst", visitor.isFirstTime() ? 1 : 0);
                    //    hashMapMonth.put("rVI", String.format("%.2f", (Float) (((Float.parseFloat(hashMapMonth.get("tot").toString()) - (Float.parseFloat(hashMapMonth.get("fst").toString()))) / (Float.parseFloat(hashMapMonth.get("tot").toString()))) * 100)) + "%");
                    hashMapMonth.put("apt", visitor.isAppointment() ? 1 : 0);


                    mutableData.child("cnt").child("nv").setValue(hashMapMonth.get("nv"));
                    mutableData.child("cnt").child("v").setValue(hashMapMonth.get("v"));
                    mutableData.child("cnt").child("ep").setValue(hashMapMonth.get("ep"));
                    mutableData.child("cnt").child("tot").setValue(hashMapMonth.get("tot"));
                    mutableData.child("cnt").child("viCnt").setValue(hashMapMonth.get("viCnt"));
                    mutableData.child("cnt").child("fst").setValue(hashMapMonth.get("fst"));
                    //        mutableData.child("cnt").child("rVI").setValue(hashMapMonth.get("rVI"));
                    mutableData.child("cnt").child("apt").setValue(hashMapMonth.get("apt"));


                    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("nv").setValue(hashMapMonth.get("nv"));
                    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("v").setValue(hashMapMonth.get("v"));
                    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("ep").setValue(hashMapMonth.get("ep"));
                    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("tot").setValue(hashMapMonth.get("tot"));
                    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("viCnt").setValue(hashMapMonth.get("viCnt"));
                    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("fst").setValue(hashMapMonth.get("fst"));
                    //        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("rVI").setValue(hashMapMonth.get("rVI"));
                    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("apt").setValue(hashMapMonth.get("apt"));
                    /* mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("" + Calendar.getInstance().get(Calendar.DATE)).setValue(hashMapMonth);
                     */


                 /*   HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("viCnt/cnt", visitor.getCount());
                    hashMap.put("viCnt/id", visitor.getVisitorId());
                    hashMap.put("tot/cnt", 1);
                    hashMap.put("via/cnt", visitor.isAllow() ? 1 : 0);
                    hashMap.put("via/id", visitor.isAllow() ? visitor.getVisitorId() : null);
                    hashMap.put("pa/cnt", visitor.isParent() ? 1 : 0);
                    hashMap.put("pa/id", visitor.isParent() ? visitor.getVisitorId() : null);
                    hashMap.put("vi/cnt", visitor.isVisitor() ? 1 : 0);
                    hashMap.put("vi/id", visitor.isVisitor() ? vizKey : null);*/
                  /*  HashMap<String,Object> mainMap = new HashMap<>();
                    mainMap.put("ent",hashMap);*/
                    mutableData.child("_ent").child("viCnt").child("cnt").setValue(visitor.getCount());
                    mutableData.child("_ent").child("viCnt").child("id").setValue(vizKey);
                    mutableData.child("_ent").child("tot").child("cnt").setValue(1);
                    mutableData.child("_ent").child("tot").child("id").setValue(vizKey);
                    mutableData.child("_ent").child("via").child("cnt").setValue(visitor.isAllow() ? 1 : 0);
                    mutableData.child("_ent").child("via").child("id").setValue(visitor.isAllow() ? vizKey : "");
                    mutableData.child("_ent").child("pa").child("cnt").setValue(visitor.isParent() ? 1 : 0);
                    mutableData.child("_ent").child("pa").child("id").setValue(visitor.isParent() ? vizKey : "");
                    mutableData.child("_ent").child("vi").child("cnt").setValue(visitor.isVisitor() ? 1 : 0);
                    mutableData.child("_ent").child("vi").child("id").setValue(visitor.isVisitor() ? vizKey : "");

                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("viCnt").child("cnt").setValue(visitor.getCount());
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("viCnt").child("id").setValue(vizKey);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("cnt").setValue(1);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("id").setValue(vizKey);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("via").child("cnt").setValue(visitor.isAllow() ? 1 : 0);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("via").child("id").setValue(visitor.isAllow() ? vizKey : "");
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("cnt").setValue(visitor.isParent() ? 1 : 0);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("id").setValue(visitor.isParent() ? vizKey : "");
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("cnt").setValue(visitor.isVisitor() ? 1 : 0);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("id").setValue(visitor.isVisitor() ? vizKey : "");


                    //    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).setValue(mainMap);

                   /* mutableData.child("ent").child("viCnt").child("cnt").setValue(visitor.getCount());
                    mutableData.child("ent").child("viCnt").child("id").setValue(visitor.getVisitorId());
                    mutableData.child("ent").child("tot").child("cnt").setValue(1);
                    mutableData.child("ent").child("tot").child("id").setValue(visitor.getVisitorId());
                    mutableData.child("ent").child("pa").child("cnt").setValue(visitor.isParent() ? 1 : 0);
                    mutableData.child("ent").child("pa").child("id").setValue(visitor.isParent() ? visitor.getVisitorId() : null);
                    mutableData.child("ent").child("vi").child("cnt").setValue(visitor.isVisitor() ? 1 : 0);
                    mutableData.child("ent").child("vi").child("id").setValue(visitor.isVisitor() ? visitor.getVisitorId() : null);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("ent").child("tot").child("cnt").setValue(1);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("ent").child("viCnt").setValue(visitor.getCount());
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("ent").child("pa").setValue(visitor.isParent() ? 1 : 0);
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("ent").child("vi").setValue(visitor.isVisitor() ? 1 : 0);*/


                    ///////vi node /// Alka Aswal
                 /*   HashMap<String, Object> hashMapViRef = new HashMap<String, Object>();
                    hashMapViRef.put(vizKey, currentMonth);
                    mutableData.child("vi").child(visitorMob).child("ref").setValue(hashMapViRef);

                    mutableData.child("vi").child(visitorMob).child("nm").setValue(visitorNm);
                    mutableData.child("vi").child(visitorMob).child("cnt").setValue(1);
                    if (vizUrl != null && !vizUrl.equals(null))
                        mutableData.child("vi").child(visitorMob).child("img").setValue(vizUrl);
*/

                    //     mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("vi").child(visitorMob).setValue(1);


                    ////// end vi node


                    ///////st node
                    HashMap<String, Object> hashMapstMonth = new HashMap<String, Object>();
                    if (selectedToMeet != null && selectedToMeet.getId() != null) {
                        HashMap<String, Object> hashMapStViMonth = new HashMap<String, Object>();


                        hashMapStViMonth.put(vizKey, currentMonth);

                        visitorReportDataRef.child("st").child(selectedToMeet.getId()).updateChildren(hashMapStViMonth);

                        visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).updateChildren(hashMapStViMonth);


                        //  hashMapstMonth.put("vi", hashMapStViMonth); ///Alka Aswal
                        hashMapstMonth.put("cnt", 1);
                        hashMapstMonth.put("nm", selectedToMeet.getNm());

                        mutableData.child("st").child(selectedToMeet.getId()).setValue(hashMapstMonth);


                        HashMap<String, Object> hashMapstDate = new HashMap<String, Object>();
                        // HashMap<String, Object> hashMapStViDate = new HashMap<String, Object>();
                        // hashMapStViDate.put(vizKey, currentMonth);
                        //   hashMapstDate.put("vi", hashMapStViDate); ///Alka Aswal
                        hashMapstDate.put("cnt", 1);
                        hashMapstDate.put("nm", selectedToMeet.getNm());


                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).setValue(hashMapstDate);
                    }


                    HashMap<String, Object> hashMapPMonth = new HashMap<String, Object>();
                    if (visitor.getPurpose() != null && visitor.getPurpose().getpId() != null) {
                        HashMap<String, Object> hashMapPViMonth = new HashMap<String, Object>();


                        hashMapPViMonth.put(vizKey, currentMonth);

                        visitorReportDataRef.child("p").child(visitor.getPurpose().getpId()).updateChildren(hashMapPViMonth);
                        visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).updateChildren(hashMapPViMonth);
                        //   hashMapPMonth.put("vi", hashMapPViMonth);  ///Alka Aswal
                        hashMapPMonth.put("cnt", 1);
                        hashMapPMonth.put("nm", visitor.getPurpose().getP());

                        mutableData.child("p").child(visitor.getPurpose().getpId()).setValue(hashMapPMonth);


                        HashMap<String, Object> hashMapPDate = new HashMap<String, Object>();
                        HashMap<String, Object> hashMapPViDate = new HashMap<String, Object>();
                        hashMapPViDate.put(vizKey, currentMonth);
                        //   hashMapPDate.put("vi", hashMapPViDate);  ///Alka Aswal
                        hashMapPDate.put("cnt", 1);
                        hashMapPDate.put("nm", visitor.getPurpose().getP());


                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).setValue(hashMapPDate);
                    }


                    /////end st node


                    ////////dev node


                    HashMap<String, Object> hashMapDev = new HashMap<String, Object>();

                    if (mutableData.hasChild("dev") && mutableData.child("dev").hasChild(loginSp.getString(Global.UUID, "")))
                        hashMapDev.put("ent", mutableData.child("dev").child(loginSp.getString(Global.UUID, "")).child("ent").getValue(Integer.class) + 1);
                    else
                        hashMapDev.put("ent", 1);


                    hashMapDev.put("nm", loginSp.getString(Global.DEVICE_NAME, ""));

                    mutableData.child("dev").child(loginSp.getString(Global.UUID, "")).setValue(hashMapDev);


                    ///dev node datewise


                    HashMap<String, Object> hashMapDevDate = new HashMap<String, Object>();

                    if (mutableData.hasChild("dev") && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("dev").hasChild(loginSp.getString(Global.UUID, "")))
                        hashMapDevDate.put("ent", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("dev").child(loginSp.getString(Global.UUID, "")).child("ent").getValue(Integer.class) + 1);
                    else
                        hashMapDevDate.put("ent", 1);
                    hashMapDevDate.put("nm", loginSp.getString(Global.DEVICE_NAME, ""));
                    mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("dev").child(loginSp.getString(Global.UUID, "")).setValue(hashMapDevDate);


////////////// usernode


                    if (!loginSp.getString(Global.LOGGNED_IN_USER_ID, "").isEmpty()) {
                        HashMap<String, Object> hashMapUser = new HashMap<String, Object>();

                        hashMapUser.put("ent", 1);


                        hashMapUser.put("nm", loginSp.getString(Global.LOGGNED_IN_USER_NM, ""));

                        mutableData.child("u").child(loginSp.getString(Global.UUID, "")).setValue(hashMapUser);


                        HashMap<String, Object> hashMapUserDate = new HashMap<String, Object>();

                        hashMapUserDate.put("ent", 1);


                        hashMapUserDate.put("nm", loginSp.getString(Global.LOGGNED_IN_USER_NM, ""));


                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("u").child(loginSp.getString(Global.UUID, "")).setValue(hashMapUserDate);

                    }

                    return Transaction.success(mutableData);
                } else {

                    if (visitor.getPrevCalling() == 0) {
                        ////////////cnt node
                        HashMap<String, Object> hashMapMonth = new HashMap<String, Object>();
                        hashMapMonth.put("nv", verified ? mutableData.child("cnt").child("nv").getValue(Integer.class) : mutableData.child("cnt").child("nv").getValue(Integer.class) + 1);
                        hashMapMonth.put("v", verified ? mutableData.child("cnt").child("v").getValue(Integer.class) + 1 : mutableData.child("cnt").child("v").getValue(Integer.class));
                        hashMapMonth.put("ep", loginSp.getBoolean(Global.EPASS, false) ? mutableData.child("cnt").child("ep").getValue(Integer.class) + 1 : mutableData.child("cnt").child("ep").getValue(Integer.class));
                        hashMapMonth.put("apt", visitor.isAppointment() ? mutableData.child("cnt").child("apt").getValue(Integer.class) + 1 : mutableData.child("cnt").child("apt").getValue(Integer.class));
                        hashMapMonth.put("tot", mutableData.child("cnt").child("tot").getValue(Integer.class) + 1);
                        hashMapMonth.put("viCnt", mutableData.child("cnt").hasChild("viCnt") ? mutableData.child("cnt").child("viCnt").getValue(Integer.class) + visitor.getCount() : visitor.getCount());


                        if (mutableData.child("cnt").hasChild("fst"))
                            hashMapMonth.put("fst", visitor.isFirstTime() ? mutableData.child("cnt").child("fst").getValue(Integer.class) + 1 : mutableData.child("cnt").child("fst").getValue(Integer.class));
                        else {
                            hashMapMonth.put("fst", visitor.isFirstTime() ? 1 : 0);
                        }

                        //           hashMapMonth.put("rVI", String.format("%.2f", (Float) ((((Float.parseFloat(hashMapMonth.get("tot").toString())) - (Float.parseFloat(hashMapMonth.get("fst").toString()))) / (Float.parseFloat(hashMapMonth.get("tot").toString())) * 100))) + "%");


                     /*   hashMapMonth.put("rVI", String.format("%.2f", (Float) ((((Float.parseFloat(mutableData.child("cnt").child("tot").getValue().toString()) -
                                Float.parseFloat(mutableData.child("cnt").child("fst").getValue().toString())) / Float.parseFloat(mutableData.child("cnt").child("tot").getValue().toString())) * 100))) + "%");
*/
                        HashMap<String, Object> hashMapDate = new HashMap<String, Object>();

                        if (!mutableData.child("cnt").hasChild("" + Calendar.getInstance().get(Calendar.DATE))) {


                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("cnt").setValue(visitor.isVisitor() ? 1 : 0);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("id").setValue(visitor.isVisitor() ? vizKey : "");
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("cnt").setValue(1);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("id").setValue(visitor.isAllow() ? 1 : 0);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("viCnt").child("cnt").setValue(visitor.getCount());
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("viCnt").child("id").setValue(vizKey);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("cnt").setValue(visitor.isParent() ? 1 : 0);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("id").setValue(visitor.isVisitor() ? vizKey : "");
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("cnt").setValue(visitor.isVisitor() ? 1 : 0);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("id").setValue(visitor.isVisitor() ? vizKey : "");
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("via").child("cnt").setValue(visitor.isAllow() ? 1 : 0);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("via").child("id").setValue(visitor.isAllow() ? vizKey : "");



                        } else {


                            hashMapDate.put("nv", verified ? mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("nv").getValue(Integer.class) : mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("nv").getValue(Integer.class) + 1);


                            hashMapDate.put("v", verified ? mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("v").getValue(Integer.class) + 1 : mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("v").getValue(Integer.class));
                            hashMapDate.put("tot", mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("tot").getValue(Integer.class) + 1);

                            hashMapDate.put("viCnt", mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).hasChild("viCnt") ? mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("viCnt").getValue(Integer.class) + visitor.getCount() : visitor.getCount());

                            hashMapDate.put("ep", loginSp.getBoolean(Global.EPASS, false) ? mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("ep").getValue(Integer.class) + 1 : mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("ep").getValue(Integer.class));
                            hashMapDate.put("apt", visitor.isAppointment() ?
                                    (mutableData.child("cnt").child("" + Calendar.getInstance()
                                            .get(Calendar.DATE)).child("apt").getValue(Integer.class) + 1) : mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).child("apt").getValue(Integer.class));


                            if (mutableData.child("cnt").child("" + Calendar.getInstance()
                                    .get(Calendar.DATE)).hasChild("fst")) {
                                hashMapDate.put("fst", visitor.isFirstTime() ? mutableData.child("cnt").child("" + Calendar.getInstance()
                                        .get(Calendar.DATE)).child("fst").getValue(Integer.class) + 1 : mutableData.child("cnt").child("" + Calendar.getInstance()
                                        .get(Calendar.DATE)).child("fst").getValue(Integer.class));
                            } else {
                                hashMapDate.put("fst", visitor.isFirstTime() ? 1 : 0);
                            }



                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("cnt").
                                    setValue(mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("cnt").getValue(Integer.class) + 1);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("id").
                                    setValue(vizKey);
                            if (visitor.isAllow()) {
                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("via").child("cnt").
                                        setValue(mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("via").child("cnt").getValue(Integer.class) + 1);
                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("tot").child("id").
                                        setValue(vizKey);
                            }
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("viCnt").child("cnt").
                                    setValue(mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").hasChild("viCnt") ?
                                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("viCnt").child("cnt").getValue(Integer.class) + visitor.getCount() : visitor.getCount());
                            if (visitor.isParent()) {
                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("cnt").
                                        setValue(visitor.isParent() ? (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("cnt").getValue(Integer.class) + 1)
                                                : (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("cnt").getValue(Integer.class)));

                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("pa").child("id").
                                        setValue(vizKey);
                            }

                            if (visitor.isVisitor()) {

                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("cnt").
                                        setValue(visitor.isVisitor() ? (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("cnt").getValue(Integer.class) + 1)
                                                : (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("cnt").getValue(Integer.class)));

                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("_ent").child("vi").child("id").
                                        setValue(vizKey);
                            }


                        }


                        mutableData.child("cnt").child("nv").setValue(hashMapMonth.get("nv"));
                        mutableData.child("cnt").child("v").setValue(hashMapMonth.get("v"));
                        mutableData.child("cnt").child("ep").setValue(hashMapMonth.get("ep"));
                        mutableData.child("cnt").child("tot").setValue(hashMapMonth.get("tot"));
                        mutableData.child("cnt").child("viCnt").setValue(hashMapMonth.get("viCnt"));
                        mutableData.child("cnt").child("fst").setValue(hashMapMonth.get("fst"));
                        //  mutableData.child("cnt").child("rVI").setValue(hashMapMonth.get("rVI"));
                        mutableData.child("cnt").child("apt").setValue(hashMapMonth.get("apt"));

                        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("nv").setValue(hashMapDate.get("nv"));
                        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("v").setValue(hashMapDate.get("v"));
                        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("ep").setValue(hashMapDate.get("ep"));
                        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("tot").setValue(hashMapDate.get("tot"));
                        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("viCnt").setValue(hashMapDate.get("viCnt"));
                        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("fst").setValue(hashMapDate.get("fst"));
                        //    mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("rVI").setValue(hashMapDate.get("rVI"));
                        mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("apt").setValue(hashMapDate.get("apt"));



                        mutableData.child("_ent").child("tot").child("cnt").setValue(mutableData.child("_ent").child("tot").child("cnt").getValue(Integer.class) + 1);
                        mutableData.child("_ent").child("tot").child("id").setValue(vizKey);

                        mutableData.child("_ent").child("viCnt").child("cnt").setValue(mutableData.child("_ent").hasChild("viCnt") ? mutableData.child("_ent").child("viCnt").child("cnt").getValue(Integer.class) + visitor.getCount() : visitor.getCount());
                        mutableData.child("_ent").child("viCnt").child("id").setValue(vizKey);

                        if (visitor.isVisitor()) {
                            mutableData.child("_ent").child("vi").child("cnt").setValue(visitor.isVisitor() ? (mutableData.child("_ent").child("vi").child("cnt").getValue(Integer.class) + 1) :
                                    (mutableData.child("_ent").child("vi").child("cnt").getValue(Integer.class)));
                            mutableData.child("_ent").child("vi").child("id").setValue(vizKey);
                        }

                        if (visitor.isAllow()) {
                            mutableData.child("_ent").child("via").child("cnt").setValue(visitor.isVisitor() ? (mutableData.child("_ent").child("via").getValue(Integer.class) + 1) : (mutableData.child("_ent").child("via").getValue(Integer.class)));

                            mutableData.child("_ent").child("via").child("id").setValue(vizKey);
                        }

                        if (visitor.isParent()) {
                            mutableData.child("_ent").child("pa").child("cnt").setValue(visitor.isParent() ? (mutableData.child("_ent").child("pa").getValue(Integer.class) + 1) : (mutableData.child("_ent").child("pa").getValue(Integer.class)));
                            mutableData.child("_ent").child("pa").child("id").setValue(vizKey);

                        }


                        //////st node month wise

                        HashMap<String, Object> hashMapstMonth = new HashMap<String, Object>();

                        if (selectedToMeet != null && selectedToMeet.getId() != null) {
                            if (mutableData.hasChild("st") && mutableData.child("st").hasChild(selectedToMeet.getId())
                                    && mutableData.child("st").child(selectedToMeet.getId()).hasChild("cnt"))
                                hashMapstMonth.put("cnt", mutableData.child("st").child(selectedToMeet.getId()).child("cnt").getValue(Integer.class) + 1);
                            else
                                hashMapstMonth.put("cnt", 1);
                            hashMapstMonth.put("nm", selectedToMeet.getNm());

                            mutableData.child("st").child(selectedToMeet.getId()).child("nm").setValue(hashMapstMonth.get("nm"));
                            mutableData.child("st").child(selectedToMeet.getId()).child("cnt").setValue(hashMapstMonth.get("cnt"));

                            visitorReportDataRef.child("st").child(selectedToMeet.getId()).child(vizKey).setValue(currentMonth);
                            //     mutableData.child("st").child(selectedToMeet.getId()).child("vi").child(vizKey).setValue(currentMonth);  Alka Aswal


///////// st    datewise
                            HashMap<String, Object> hashMapstDate = new HashMap<String, Object>();

                            if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).hasChild("st") && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").hasChild(selectedToMeet.getId())
                                    && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).hasChild("cnt"))
                                hashMapstDate.put("cnt", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).child("cnt").getValue(Integer.class) + 1);
                            else
                                hashMapstDate.put("cnt", 1);
                            hashMapstDate.put("nm", selectedToMeet.getNm());


                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st")
                                    .child(selectedToMeet.getId()).child("nm").setValue(hashMapstDate.get("nm"));

                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st")
                                    .child(selectedToMeet.getId()).child("cnt").setValue(hashMapstDate.get("cnt"));
                           /* mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st")
                                    .child(selectedToMeet.getId()).child("vi").child(vizKey).setValue(currentMonth);*/

                            visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).child(vizKey).setValue(currentMonth);

                        }


                        /////////end of st node////


////p node


                        HashMap<String, Object> hashMappMonth = new HashMap<String, Object>();

                        if (visitor.getPurpose() != null && visitor.getPurpose().getpId() != null) {
                            if (mutableData.hasChild("p") && mutableData.child("p").hasChild(visitor.getPurpose().getpId())
                                    && mutableData.child("p").child(visitor.getPurpose().getpId()).hasChild("cnt"))
                                hashMappMonth.put("cnt", mutableData.child("p").child(visitor.getPurpose().getpId()).child("cnt").getValue(Integer.class) + 1);
                            else
                                hashMappMonth.put("cnt", 1);
                            hashMappMonth.put("nm", visitor.getPurpose().getP());

                            mutableData.child("p").child(visitor.getPurpose().getpId()).child("nm").setValue(hashMappMonth.get("nm"));
                            mutableData.child("p").child(visitor.getPurpose().getpId()).child("cnt").setValue(hashMappMonth.get("cnt"));
                            //  mutableData.child("p").child(visitor.getPurpose().getpId()).child("vi").child(vizKey).setValue(currentMonth); //Alka Aswal

                            visitorReportDataRef.child("p").child(visitor.getPurpose().getpId()).child(vizKey).setValue(currentMonth);
                            visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).child(vizKey).setValue(currentMonth);
///////// p    datewise
                            HashMap<String, Object> hashMapPDate = new HashMap<String, Object>();

                            if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).hasChild("p") && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").hasChild(visitor.getPurpose().getpId())
                                    && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).hasChild("cnt"))
                                hashMapPDate.put("cnt", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).child("cnt").getValue(Integer.class) + 1);
                            else
                                hashMapPDate.put("cnt", 1);
                            hashMapPDate.put("nm", visitor.getPurpose().getP());


                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p")
                                    .child(visitor.getPurpose().getpId()).child("nm").setValue(hashMapPDate.get("nm"));

                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p")
                                    .child(visitor.getPurpose().getpId()).child("cnt").setValue(hashMapPDate.get("cnt"));

                        }


                        ////////dev node

                        HashMap<String, Object> hashMapDev = new HashMap<String, Object>();

                        if (mutableData.hasChild("dev") && mutableData.child("dev").hasChild(loginSp.getString(Global.UUID, "")))
                            hashMapDev.put("ent", mutableData.child("dev").child(loginSp.getString(Global.UUID, "")).child("ent").getValue(Integer.class) + 1);
                        else
                            hashMapDev.put("ent", 1);


                        hashMapDev.put("nm", loginSp.getString(Global.DEVICE_NAME, ""));

                        mutableData.child("dev").child(loginSp.getString(Global.UUID, "")).setValue(hashMapDev);


                        ////// dev node datewise


                        HashMap<String, Object> hashMapDevDate = new HashMap<String, Object>();

                        if (mutableData.hasChild("" + Calendar.getInstance().get(Calendar.DATE)) && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("dev").hasChild(loginSp.getString(Global.UUID, "")))
                            hashMapDevDate.put("ent", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("dev").child(loginSp.getString(Global.UUID, "")).child("ent").getValue(Integer.class) + 1);
                        else
                            hashMapDevDate.put("ent", 1);


                        hashMapDevDate.put("nm", loginSp.getString(Global.DEVICE_NAME, ""));


                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("dev").child(loginSp.getString(Global.UUID, "")).setValue(hashMapDevDate);

                        ///user node


                        if (!loginSp.getString(Global.LOGGNED_IN_USER_ID, "").isEmpty()) {
                            HashMap<String, Object> hashMapUser = new HashMap<String, Object>();
                            if (mutableData.hasChild("u") && mutableData.child("u").hasChild(loginSp.getString(Global.LOGGNED_IN_USER_ID, "")))
                                hashMapUser.put("ent", mutableData.child("u").child(loginSp.getString(Global.LOGGNED_IN_USER_ID, "")).child("ent").getValue(Integer.class) + 1);
                            else
                                hashMapUser.put("ent", 1);


                            hashMapUser.put("nm", loginSp.getString(Global.LOGGNED_IN_USER_NM, ""));

                            mutableData.child("u").child(loginSp.getString(Global.UUID, "")).setValue(hashMapUser);


                            HashMap<String, Object> hashMapUserDate = new HashMap<String, Object>();
                            if (mutableData.hasChild("" + Calendar.getInstance().get(Calendar.DATE))
                                    && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).hasChild("u") && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("u").hasChild(loginSp.getString(Global.LOGGNED_IN_USER_ID, "")))
                                hashMapUserDate.put("ent", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("u").child(loginSp.getString(Global.LOGGNED_IN_USER_ID, "")).child("ent").getValue(Integer.class) + 1);
                            else
                                hashMapUserDate.put("ent", 1);


                            hashMapUserDate.put("nm", loginSp.getString(Global.LOGGNED_IN_USER_NM, ""));

                            mutableData.child("u").child(loginSp.getString(Global.UUID, "")).setValue(hashMapUser);


                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("u").child(loginSp.getString(Global.UUID, "")).setValue(hashMapUserDate);

                        }

                    } else {

                        ////////////cnt node

                        boolean preVerified = visitor.getPrevCalling() != 2 ? true : false;
                        if (verified != preVerified) {
                            HashMap<String, Object> hashMapMonth = new HashMap<String, Object>();

                            if (preVerified) {

                                hashMapMonth.put("nv", mutableData.child("cnt").child("nv").getValue(Integer.class) + 1);
                                hashMapMonth.put("v", mutableData.child("cnt").child("v").getValue(Integer.class) - 1);
                            } else {
                                hashMapMonth.put("nv", mutableData.child("cnt").child("nv").getValue(Integer.class) - 1);
                                hashMapMonth.put("v", mutableData.child("cnt").child("v").getValue(Integer.class) + 1);
                            }
                            hashMapMonth.put("tot", mutableData.child("cnt").child("tot").getValue(Integer.class));
                            hashMapMonth.put("ep", mutableData.child("cnt").child("ep").getValue(Integer.class));
                            hashMapMonth.put("apt", mutableData.child("cnt").child("apt").getValue(Integer.class));

                            mutableData.child("cnt").child("nv").setValue(hashMapMonth.get("nv"));
                            mutableData.child("cnt").child("v").setValue(hashMapMonth.get("v"));
                            mutableData.child("cnt").child("tot").setValue(hashMapMonth.get("tot"));

                            HashMap<String, Object> hashMapDate = new HashMap<String, Object>();


                            if (!mutableData.child("cnt").hasChild("" + Calendar.getInstance().get(Calendar.DATE)))

                            {


                                hashMapDate.put("nv", verified ? 0 : 1);
                                hashMapDate.put("v", verified ? 1 : 0);
                                hashMapDate.put("tot", 1);

                            } else {


                                if (preVerified) {

                                    hashMapDate.put("nv", mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("nv").getValue(Integer.class) + 1);
                                    hashMapDate.put("v", mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("v").getValue(Integer.class) - 1);
                                } else {

                                    hashMapDate.put("nv", mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("nv").getValue(Integer.class) - 1);
                                    hashMapDate.put("v", mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("v").getValue(Integer.class) + 1);

                                }
                                hashMapDate.put("tot", mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("tot").getValue(Integer.class));


                            }


                            mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("v").setValue(hashMapDate.get("v"));
                            mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("nv").setValue(hashMapDate.get("nv"));
                            mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("tot").setValue(hashMapDate.get("tot"));

                        }

                        String currentTomeet = null;

                        if (selectedToMeet != null && selectedToMeet.getId() != null)
                            currentTomeet = selectedToMeet.getId();


                        //////st node
                        if (((currentTomeet != null &&
                                !currentTomeet.equals(visitor.getPrevTomeet())) || visitor.getPrevTomeet() != null && !visitor.getPrevTomeet().equals(currentTomeet))) {


                            if (visitor.getPrevTomeet() != null) {

                                HashMap<String, Object> hashMapstMonth = new HashMap<String, Object>();
                                if (mutableData.hasChild("st") && mutableData.child("st").hasChild(visitor.getPrevTomeet())
                                        && mutableData.child("st").child(visitor.getPrevTomeet()).hasChild("cnt")) {

                                    if (mutableData.child("st").child(visitor.getPrevTomeet()).child("cnt").getValue(Integer.class) > 1) {
                                        hashMapstMonth.put("cnt", mutableData.child("st").child(visitor.getPrevTomeet()).child("cnt").getValue(Integer.class) - 1);

                                        mutableData.child("st").child(visitor.getPrevTomeet()).child("cnt").setValue(hashMapstMonth.get("cnt"));

                                        visitorReportDataRef.child("st").child(visitor.getPrevTomeet()).child(vizKey).setValue(null);

                                        //  mutableData.child("st").child(visitor.getPrevTomeet()).child("vi").child(vizKey).setValue(null); /// Alka Aswal
                                    } else {

                                        mutableData.child("st").child(visitor.getPrevTomeet()).setValue(null);
                                        visitorReportDataRef.child("st").child(visitor.getPrevTomeet()).setValue(null);
                                        // mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(prevTomeet).setValue(null);
                                    }

                                }


                                HashMap<String, Object> hashMapstDate = new HashMap<String, Object>();


                                if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).hasChild("st") && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").hasChild(visitor.getPrevTomeet())
                                        && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(visitor.getPrevTomeet()).hasChild("cnt")) {

                                    if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(visitor.getPrevTomeet()).child("cnt").getValue(Integer.class) > 1) {
                                        hashMapstDate.put("cnt", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st")
                                                .child(visitor.getPrevTomeet()).child("cnt").getValue(Integer.class) - 1);
                                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(visitor.getPrevTomeet()).child("cnt").setValue(hashMapstDate.get("cnt"));
                                        // mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(visitor.getPrevTomeet()).child("vi").child(vizKey).setValue(null);
                                        //Alka Aswal

                                        visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(visitor.getPrevTomeet()).child(vizKey).setValue(null);

                                    } else {
                                        // mutableData.child("st").child(prevTomeet).setValue(null);
                                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(visitor.getPrevTomeet()).setValue(null);

                                        visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(visitor.getPrevTomeet()).setValue(null);
                                    }

                                }
                            }


                            if (currentTomeet != null) {


                                HashMap<String, Object> hashMapst1 = new HashMap<String, Object>();


                                if (mutableData.hasChild("st") && mutableData.child("st").hasChild(selectedToMeet.getId())
                                        && mutableData.child("st").child(selectedToMeet.getId()).hasChild("cnt")) {
                                    hashMapst1.put("cnt", mutableData.child("st").child(selectedToMeet.getId()).child("cnt").getValue(Integer.class) + 1);

                                } else
                                    hashMapst1.put("cnt", 1);
                                hashMapst1.put("nm", selectedToMeet.getNm());

                                mutableData.child("st").child(selectedToMeet.getId()).child("cnt").setValue(hashMapst1.get("cnt"));
                                mutableData.child("st").child(selectedToMeet.getId()).child("nm").setValue(hashMapst1.get("nm"));

                                visitorReportDataRef.child("st").child(selectedToMeet.getId()).child(vizKey).setValue(currentMonth);

                                // mutableData.child("st").child(selectedToMeet.getId()).child("vi").child(vizKey).setValue(currentMonth);
                                //Alka Aswal

                                HashMap<String, Object> hashMapst1Date = new HashMap<String, Object>();
                                if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).hasChild("st")
                                        && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").hasChild(selectedToMeet.getId())
                                        && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).hasChild("cnt")) {
                                    hashMapst1Date.put("cnt", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).child("cnt").getValue(Integer.class) + 1);
                                } else
                                    hashMapst1Date.put("cnt", 1);
                                hashMapst1Date.put("nm", selectedToMeet.getNm());


                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).child("cnt").setValue(hashMapst1Date.get("cnt"));
                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).child("nm").setValue(hashMapst1Date.get("nm"));
                                visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).child(vizKey).setValue(currentMonth);
                              /*  mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(selectedToMeet.getId()).child("vi")
                                        .child(vizKey).setValue(currentMonth);
*/ //Alka Aswal

                            }


                        }


                        //purpose


                        if (((visitor.getPurpose().getpId() != null &&
                                !visitor.getPurpose().getpId().equals(visitor.getPrevPurpose().getpId()))
                                || visitor.getPrevPurpose() != null && !visitor.getPrevPurpose().getpId().equals(visitor.getPurpose().getpId()))) {


                            if (visitor.getPrevPurpose().getpId() != null) {

                                HashMap<String, Object> hashMapstMonth = new HashMap<String, Object>();
                                if (mutableData.hasChild("p") && mutableData.child("p").hasChild(visitor.getPrevPurpose().getpId())
                                        && mutableData.child("p").child(visitor.getPrevPurpose().getpId()).hasChild("cnt")) {

                                    if (mutableData.child("p").child(visitor.getPrevPurpose().getpId()).child("cnt").getValue(Integer.class) > 1) {
                                        hashMapstMonth.put("cnt", mutableData.child("p").child(visitor.getPrevPurpose().getpId()).child("cnt").getValue(Integer.class) - 1);

                                        mutableData.child("p").child(visitor.getPrevPurpose().getpId()).child("cnt").setValue(hashMapstMonth.get("cnt"));

                                        visitorReportDataRef.child("p").child(visitor.getPrevPurpose().getpId()).child(vizKey).setValue(null);

                                        //  mutableData.child("p").child(visitor.getPrevPurpose().getpId()).child("vi").child(vizKey).setValue(null); //Alka Aswal
                                    } else {
                                        mutableData.child("p").child(visitor.getPrevPurpose().getpId()).setValue(null);
                                        visitorReportDataRef.child("p").child(visitor.getPrevPurpose().getpId()).setValue(null);
                                        // mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("st").child(prevTomeet).setValue(null);
                                    }

                                }


                                HashMap<String, Object> hashMapstDate = new HashMap<String, Object>();


                                if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).hasChild("p") && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").hasChild(visitor.getPrevPurpose().getpId())
                                        && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPrevPurpose().getpId()).hasChild("cnt")) {

                                    if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPrevPurpose().getpId()).child("cnt").getValue(Integer.class) > 1) {
                                        hashMapstDate.put("cnt", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p")
                                                .child(visitor.getPrevPurpose().getpId()).child("cnt").getValue(Integer.class) - 1);
                                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPrevPurpose().getpId()).child("cnt").setValue(hashMapstDate.get("cnt"));
                                        // mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPrevPurpose().getpId()).child("vi").child(vizKey).setValue(null);
//Alka aswal


                                        visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPrevPurpose().getpId()).child(vizKey).setValue(null);

                                    } else {
                                        // mutableData.child("st").child(prevTomeet).setValue(null);
                                        mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPrevPurpose().getpId()).setValue(null);
                                        visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPrevPurpose().getpId()).setValue(null);
                                    }

                                }
                            }


                            if (visitor.getPurpose().getpId() != null) {

                                HashMap<String, Object> hashMapst1 = new HashMap<String, Object>();


                                if (mutableData.hasChild("p") && mutableData.child("p").hasChild(visitor.getPurpose().getpId())
                                        && mutableData.child("p").child(visitor.getPurpose().getpId()).hasChild("cnt")) {
                                    hashMapst1.put("cnt", mutableData.child("p").child(visitor.getPurpose().getpId()).child("cnt").getValue(Integer.class) + 1);

                                } else
                                    hashMapst1.put("cnt", 1);
                                hashMapst1.put("nm", visitor.getPurpose().getP());

                                mutableData.child("p").child(visitor.getPurpose().getpId()).child("cnt").setValue(hashMapst1.get("cnt"));
                                mutableData.child("p").child(visitor.getPurpose().getpId()).child("nm").setValue(hashMapst1.get("nm"));

                                visitorReportDataRef.child("p").child(visitor.getPurpose().getpId()).child(vizKey).setValue(currentMonth);
                                //  mutableData.child("p").child(visitor.getPurpose().getpId()).child("vi").child(vizKey).setValue(currentMonth);
//Alka Aswal

                                HashMap<String, Object> hashMapst1Date = new HashMap<String, Object>();
                                if (mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).hasChild("p") && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").hasChild(visitor.getPurpose().getpId())
                                        && mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).hasChild("cnt")) {
                                    hashMapst1Date.put("cnt", mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).child("cnt").getValue(Integer.class) + 1);
                                } else
                                    hashMapst1Date.put("cnt", 1);
                                hashMapst1Date.put("nm", visitor.getPurpose().getP());


                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).child("cnt").setValue(hashMapst1Date.get("cnt"));
                                mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).child("nm").setValue(hashMapst1Date.get("nm"));
                                visitorReportDataRef.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).child(vizKey).setValue(currentMonth);
                               /* mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("p").child(visitor.getPurpose().getpId()).child("vi")
                                        .child(vizKey).setValue(currentMonth);*/
                                //Alka Aswal
                            }
                        }
                        if (visitor.getCount() != visitor.getPrevCount()) {
                            int vizCount = visitor.getCount() - visitor.getPrevCount();
                            mutableData.child("cnt").child("viCnt").setValue(mutableData.child("cnt").child("viCnt").getValue(Integer.class) + vizCount);
                            mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("viCnt")
                                    .setValue(mutableData.child("cnt").child("" + Calendar.getInstance().get(Calendar.DATE)).child("viCnt").getValue(Integer.class) + vizCount);

                            mutableData.child("ent").child("viCnt").setValue(mutableData.child("ent").child("viCnt").getValue(Integer.class) + vizCount);
                            mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("ent").child("viCnt").setValue(mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("ent").child("viCnt").getValue(Integer.class) + vizCount);

                        }

                    }


                    return Transaction.success(mutableData);

                }


                //return null;
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot
                    dataSnapshot) {


                if (databaseError != null) {
                    updatePendingTransaction();

                } else {


                  //  CommonMethods.setData(context, vizKey, urlList, "agg", true);
                     vizHis.setValue(true);

                    visitorReportDataRef.child("vi").child(visitorMob).runTransaction(new Transaction.Handler() {
                        @Override
                        public Transaction.Result doTransaction(MutableData mutableData) {


                            if (mutableData.getValue() == null) {


                                ///////vi node /// Alka Aswal
                                HashMap<String, Object> hashMapViRef = new HashMap<String, Object>();
                                hashMapViRef.put(vizKey, currentMonth);
                                mutableData.child("ref").setValue(hashMapViRef);

                                mutableData.child("nm").setValue(visitorNm);
                                mutableData.child("cnt").setValue(1);
                                if (vizUrl != null && !vizUrl.equals(null))
                                    mutableData.child("img").setValue(vizUrl);

                                //     mutableData.child("" + Calendar.getInstance().get(Calendar.DATE)).child("vi").child(visitorMob).setValue(1);


                                ////// end vi node


                                return Transaction.success(mutableData);
                            } else {

                                if (visitor.getPrevCalling() == 0) {
                                    ////////////cnt node


                                    ////////vi node  ////Alka Aswal
                                    HashMap<String, Object> hashMapvi = new HashMap<String, Object>();
                                    if (mutableData.child("vi").hasChild(visitorMob)) {
                                        if (mutableData.hasChild("cnt"))
                                            hashMapvi.put("cnt", mutableData.child("cnt").getValue(Integer.class) + 1);
                                        else {
                                            hashMapvi.put("cnt", 1);

                                        }
                                    } else {
                                        hashMapvi.put("cnt", 1);

                                    }

                                    hashMapvi.put("nm", visitorNm);
                                    /////////vi node

                                    if (vizUrl != null && !vizUrl.equals(null))
                                        hashMapvi.put("img", vizUrl);

                                    mutableData.child("cnt").setValue(hashMapvi.get("cnt"));
                                    mutableData.child("nm").setValue(hashMapvi.get("nm"));
                                    if (vizUrl != null && !vizUrl.equals(null))
                                        mutableData.child("img").setValue(hashMapvi.get("img"));


                                    mutableData
                                            .child("ref").child(vizKey).setValue(currentMonth);
                                    /// vi node end


                                }

                                return Transaction.success(mutableData);

                            }


                            //return null;
                        }

                        @Override
                        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                        }


                    });


                }


            }


        });


    }


    @Override
    public void asynctaskFinished(int responseCode, final boolean flag) {


        if (flag) {

            vizHis.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (!dataSnapshot.exists() || (dataSnapshot.exists() && dataSnapshot.getValue(boolean.class))) {


                        addCount(loginSp, verified, vizKey, visitorMob, visitorNm, dt, visitor, selectedToMeet);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {


            updatePendingTransaction();
        }


    }


    private void updatePendingTransaction() {


        vizHis.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.exists() || (dataSnapshot.exists() && dataSnapshot.getValue(boolean.class))) {


                    boolean preVerified = visitor.getPrevCalling() == 2 ? false : true;
                    HashMap<String, Object> hashMap = new HashMap<String, Object>();

                    HashMap<String, Object> hashMap1 = new HashMap<>();
                    if (visitor.getPrevCalling() == 0) {

                        hashMap1.put("dt", dt);
                        // hashMap1.put("agg", false);
                    }

                    if (visitor.isRepeated() && ((visitor.getPrevCalling() != 0 && verified != preVerified) || (selectedToMeet != null && selectedToMeet.getId() != null
                            && visitor.getPrevTomeet() != null && !visitor.getPrevTomeet().equals(selectedToMeet.getId()))
                            || (visitor.getPurpose() != null && visitor.getPurpose().getpId() != null
                            && visitor.getPrevPurpose().getpId() != null && !visitor.getPrevPurpose().getpId().equals(visitor.getPurpose().getpId()))
                            || ((visitor.getPurpose().getpId() == null && visitor.getPrevPurpose().getpId() != null)
                            || (visitor.getPrevPurpose().getpId() == null && visitor.getPurpose() != null && visitor.getPurpose().getpId() != null))
                            || ((selectedToMeet != null && selectedToMeet.getId() == null && visitor.getPrevTomeet() != null)
                            || (visitor.getPrevTomeet() == null && selectedToMeet != null && selectedToMeet.getId() != null))
                            || (visitor.getPrevCount() != visitor.getCount()))) {

                        hashMap1.put("dt", dt);
                        //  hashMap1.put("agg", false);
                        hashMap1.put("in", inDt);
                        if ((verified != preVerified)) {
                            hashMap1.put("oc", verified);
                            hashMap1.put("c", visitor.getPrevCalling() == 2 ? false : true);

                        }

                        if ((selectedToMeet != null && selectedToMeet.getId() != null
                                && ((visitor.getPrevTomeet() != null && !visitor.getPrevTomeet().equals(selectedToMeet.getId()))
                                || visitor.getPrevTomeet() == null))
                                || (selectedToMeet != null && selectedToMeet.getId() == null && visitor.getPrevTomeet() != null)) {
                            if (visitor.getPrevTomeet() != null)
                                hashMap1.put("oId", visitor.getPrevTomeet());
                            if (selectedToMeet != null && selectedToMeet.getId() != null) {
                                hashMap1.put("id", selectedToMeet.getId());
                                hashMap1.put("nm", selectedToMeet.getNm());
                            }
                        }


                        if (visitor.getPrevCount() != visitor.getCount()) {
                            hashMap1.put("viCnt", visitor.getCount() - visitor.getPrevCount());
                        }


                        if ((visitor.getPurpose() != null && visitor.getPurpose().getpId() != null
                                && ((visitor.getPrevPurpose().getpId() != null && !visitor.getPrevPurpose().getpId().equals(visitor.getPurpose().getpId()))
                                || visitor.getPrevPurpose().getpId() == null))
                                || (visitor.getPurpose().getpId() == null && visitor.getPrevPurpose().getpId() != null)) {
                            if (visitor.getPrevPurpose().getpId() != null)
                                hashMap1.put("poId", visitor.getPrevPurpose().getpId());
                            if (visitor.getPurpose() != null && visitor.getPurpose().getpId() != null) {
                                hashMap1.put("pId", visitor.getPurpose().getpId());
                                hashMap1.put("pNm", visitor.getPurpose().getP());
                            }
                        }


                    }
                    if (!dataSnapshot.exists() || (dataSnapshot.exists() && !dataSnapshot.getValue(boolean.class)))

                    vizHis.setValue(false);
                    vizPendAgg.updateChildren(hashMap1);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public boolean isOnline() {
        try {
            int timeoutMs = 1500;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (IOException e) {
            return false;
        }
    }


    public class CheckInternetLimitedAccess extends AsyncTask<Void, Void, Boolean> {


        AsynctaskFinishedListner listner;
        int responseCode = 0;


        public void onAsynctaskFinished(AsynctaskFinishedListner listner) {
            this.listner = listner;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return isOnline();
        }


        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            // super.onPostExecute(result);


            listner.asynctaskFinished(responseCode, result);

        }


    }


}



