package versionx.selfentry.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import versionx.selfentry.gettersetter.ToMeet;
import versionx.selfentry.gettersetter.ToMeet;

/**
 * Created by developer on 6/9/16.
 */
public class AutoCompeleteArrayAdapter extends ArrayAdapter<ToMeet> {
    private Context context;
    private int resource;
    private List<ToMeet> toMeetItems;
    private List<ToMeet> tempItems;
    private List<ToMeet> suggestions;

    public AutoCompeleteArrayAdapter(Context context, int resource, List<ToMeet> items) {
        super(context, resource, 0, items);

        this.context = context;
        this.resource = resource;
        this.toMeetItems = items;
        tempItems = new ArrayList<ToMeet>(this.toMeetItems);
        suggestions = new ArrayList<ToMeet>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, parent, false);
        }

        ToMeet toMeet = toMeetItems.get(position);

        if (toMeet != null && view instanceof TextView) {
            if (toMeet.getAptNo() == null)
                ((TextView) view).setText(toMeet.getNm());
            else
                ((TextView) view).setText(toMeet.getAptNo() + " - " + toMeet.getNm());

        }

        return view;
    }

    public void setTempItems(List<ToMeet> items) {
        tempItems.clear();
        toMeetItems.clear();
        toMeetItems.addAll(items);
        tempItems.addAll(items);
        notifyDataSetChanged();
    }


    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
       /* @Override
        public CharSequence convertResultToString(Object resultValue)
        {
            ToMeet toMeet = (ToMeet) resultValue;

            return toMeet.getNm();
        }*/


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();

                for (ToMeet toMeet : tempItems) {
                    if (toMeet.getNm().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            (toMeet.getAptNo() != null && toMeet.getAptNo().toLowerCase().toLowerCase().
                                    contains(constraint.toString().toLowerCase()))) {

                        suggestions.add(toMeet);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<ToMeet> filterList = (ArrayList<ToMeet>) results.values;
            if (results != null && results.count > 0) {
                ArrayList<ToMeet> filteredList = (ArrayList<ToMeet>) ((ArrayList<ToMeet>)results.values).clone();
                clear();
                for (ToMeet toMeet : filteredList) {
                    add(toMeet);
                    notifyDataSetChanged();
                }
            }
        }
    };
}