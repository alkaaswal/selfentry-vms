package versionx.selfentry.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import versionx.selfentry.Printing.DrawerService;
import versionx.selfentry.Util.Global;
import versionx.selfentry.Printing.DrawerService;
import versionx.selfentry.R;
import versionx.selfentry.Util.Global;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by developer on 18/1/17.
 */

public class CustomAdapter extends SimpleAdapter {

    SharedPreferences bluetoothSp;
    /**
     * Constructor
     *
     * @param context  The context where the View associated with this SimpleAdapter is running
     * @param data     A List of Maps. Each entry in the List corresponds to one row in the list. The
     *                 Maps contain the data for each row, and should include all the entries specified in
     *                 "from"
     * @param resource Resource identifier of a view layout that defines the views for this list
     *                 item. The layout file should include at least those named views defined in "to"
     * @param from     A list of column names that will be added to the Map associated with each
     *                 item.
     * @param to       The views that should display column in the "from" parameter. These should all be
     *                 TextViews. The first N views in this list are given the values of the first N columns
     */
    public CustomAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
         bluetoothSp = context.getSharedPreferences(Global.BLUETOOTH_SP, MODE_PRIVATE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        Object item = getItem(position);
        TextView text = (TextView)v.findViewById(R.id.tvListItemPrinterMac);
        TextView conText= (TextView)v.findViewById(R.id.tvListItemConnected);

        if(DrawerService.workThread!=null && DrawerService.workThread.isConnected() &&
                !text.getText().toString().trim().isEmpty() &&
                text.getText().toString().equals(bluetoothSp.getString(Global.DEVICE_NAME,"")))

        {
            conText.setVisibility(View.VISIBLE);
            conText.setText("Connected");
        }
        else
        {
            conText.setVisibility(View.GONE);
        }
        return v;
    }
}