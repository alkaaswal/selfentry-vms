package versionx.selfentry.gettersetter;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.vision.text.Line;

import java.util.ArrayList;


/**
 * Created by developer on 23/6/16.
 */
@Entity
public class CustomFields {

    public static String GRP_WHOCAME = "whoCame";
    public static String GRP_TOMEET = "meet";
    @NonNull
    @PrimaryKey
    public String id;
    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "type")
    public String type;

    @ColumnInfo(name = "fieldGrp")
    public String fieldGrp;

    @ColumnInfo(name = "lbl")
    public String lbl;

    @ColumnInfo(name = "mod")
    public String mod;


    @ColumnInfo(name = "order")
    public int order;

    @ColumnInfo(name = "val")
    public String val;

    @ColumnInfo(name = "hide")
    public boolean hide;


    @ColumnInfo(name = "reenter")
    public boolean reenter;

    @ColumnInfo(name = "required")
    public boolean required;

    @ColumnInfo(name = "depFids")
    public String depFids;

    @ColumnInfo(name = "dep")
    public boolean dep;

    @ColumnInfo(name = "edt")
    public boolean edt;

    @ColumnInfo(name = "srch")
    public boolean srch;

    @ColumnInfo(name = "dsbl")
    public boolean dsbl;

    @Ignore
    ArrayList<FieldsOptions> options;


    @Ignore
    String selectedOptId;

    @Ignore
    ImageView clrImg;

    @Ignore
    boolean depReq;



    @Ignore
    View view;


    public void setDep(boolean dep) {
        this.dep = dep;
    }

    public boolean isDep() {
        return dep;
    }

    public void setDepReq(boolean depReq) {
        this.depReq = depReq;
    }

    public boolean isDepReq() {
        return depReq;
    }

    public void setClrImg(ImageView clrImg) {
        this.clrImg = clrImg;
    }

    public ImageView getClrImg() {
        return clrImg;
    }

    public void setView(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }



    public void setDsbl(boolean dsbl) {
        this.dsbl = dsbl;
    }

    public boolean isDsbl() {
        return dsbl;
    }

    public void setSrch(boolean srch) {
        this.srch = srch;
    }

    public boolean isSrch() {
        return srch;
    }

    public void setEdt(boolean edt) {
        this.edt = edt;
    }

    public boolean isEdt() {
        return edt;
    }

    public void setLbl(String lbl) {
        this.lbl = lbl;
    }

    public String getLbl() {
        return lbl;
    }

    public void setMod(String mod) {
        this.mod = mod;
    }

    public String getMod() {
        return mod;
    }

    public void setSelectedOptId(String selectedOptId) {
        this.selectedOptId = selectedOptId;
    }

    public String getSelectedOptId() {
        return selectedOptId;
    }

    public void setOptions(ArrayList<FieldsOptions> options) {
        this.options = options;
    }

    public ArrayList<FieldsOptions> getOptions() {
        return options;
    }

    public void setFieldGrp(String fieldGrp) {
        this.fieldGrp = fieldGrp;
    }

    public String getFieldGrp() {
        return fieldGrp;
    }

    public void setDepFids(String depFids) {
        this.depFids = depFids;
    }

    public String getDepFids() {
        return depFids;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public boolean isHide() {
        return hide;
    }

    public void setReenter(boolean reenter) {
        this.reenter = reenter;
    }

    public boolean isReenter() {
        return reenter;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isRequired() {
        return required;
    }

}
