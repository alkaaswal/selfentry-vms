package versionx.selfentry.gettersetter;

import java.util.ArrayList;

/**
 * Created by developer on 16/8/16.
 */
public class Field {

    String nm,key,group;
    ArrayList<FieldInfo> fieldData;

    public void setFieldData(ArrayList<FieldInfo> fieldData) {
        this.fieldData = fieldData;
    }

    public ArrayList<FieldInfo> getFieldData() {
        return fieldData;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getNm() {
        return nm;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }
}





