package versionx.selfentry.gettersetter;

import java.util.HashMap;

import versionx.selfentry.Util.CaseInsensitiveMap;
import versionx.selfentry.Util.CaseInsensitiveMap;

/**
 * Created by developer on 16/8/16.
 */
public class FieldInfo {
    String fieldNm, fieldKey, fieldTyp, fieldValue, optionKey, fieldGroupNm, depKey, size;

    boolean fieldMandatory, fieldHide, printable,reEnter;
    CaseInsensitiveMap fieldOptions;

    public void setReEnter(boolean reEnter) {
        this.reEnter = reEnter;
    }

    public boolean isReEnter() {
        return reEnter;
    }

    public HashMap<String, Object> getDepOptions() {
        return depOptions;
    }

    public void setDepOptions(HashMap<String, Object> depOptions) {
        this.depOptions = depOptions;
    }

    HashMap<String, Object> depOptions;

    public void setDepKey(String depKey) {
        this.depKey = depKey;
    }

    public String getDepKey() {
        return depKey;
    }

    public boolean isFieldMandatory() {
        return fieldMandatory;
    }

    public boolean isPrintable() {
        return printable;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setPrintable(boolean printable) {
        this.printable = printable;
    }

    public void setFieldGroupNm(String fieldGroupNm) {
        this.fieldGroupNm = fieldGroupNm;
    }

    public String getFieldGroupNm() {
        return fieldGroupNm;
    }

    public String getOptionKey() {
        return optionKey;
    }

    public void setOptionKey(String optionKey) {
        this.optionKey = optionKey;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public void setFieldKey(String fieldKey) {
        this.fieldKey = fieldKey;
    }

    public String getFieldKey() {
        return fieldKey;
    }

  /*  public CaseInsensitiveMap<String, String> getFieldOptions() {
        return fieldOptions;
    }

    public void setFieldOptions(HashMap<String, String> fieldOptions) {
        this.fieldOptions = fieldOptions;
    }*/

    public CaseInsensitiveMap getFieldOptions() {
        return fieldOptions;
    }

    public void setFieldOptions(CaseInsensitiveMap fieldOptions) {
        this.fieldOptions = fieldOptions;
    }

    public void setFieldMandatory(boolean fieldMandatory) {
        this.fieldMandatory = fieldMandatory;
    }

    public void setFieldNm(String fieldNm) {
        this.fieldNm = fieldNm;
    }

    public void setFieldTyp(String fieldTyp) {
        this.fieldTyp = fieldTyp;
    }


    public boolean getFieldMandatory() {
        return fieldMandatory;
    }

    public String getFieldNm() {
        return fieldNm;
    }

    public String getFieldTyp() {
        return fieldTyp;
    }


    public void setFieldHide(boolean fieldHide) {
        this.fieldHide = fieldHide;
    }

    public boolean getFieldHide() {
        return fieldHide;
    }

}