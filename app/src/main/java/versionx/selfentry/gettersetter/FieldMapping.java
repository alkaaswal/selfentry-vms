package versionx.selfentry.gettersetter;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(primaryKeys = {"optionId","depFid"})
public class FieldMapping {



    @NonNull
    @ColumnInfo(name = "optionId")
    public String optionId;


    @ColumnInfo(name = "fId")
    public String fId;
    @NonNull
    @ColumnInfo(name = "depOpt")
    public String depOptions;


    @NonNull
    @ColumnInfo(name = "depFid")
    public String depFid;


    @ColumnInfo(name = "check")
    public boolean check;


    @ColumnInfo(name = "hide")
    public boolean hide= false;

    @ColumnInfo(name = "lbl")
    public String lbl;

    @ColumnInfo(name = "mod")
    public String mod;


    @ColumnInfo(name = "req")
    public boolean req;

    @ColumnInfo(name = "allDepIds")
    public String allDepIds;

    public void setAllDepIds(String allDepIds) {
        this.allDepIds = allDepIds;
    }

    public String getAllDepIds() {
        return allDepIds;
    }

    public boolean isReq() {
        return req;
    }

    public void setReq(boolean req) {
        this.req = req;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public boolean isHide() {
        return hide;
    }

    public void setLbl(String lbl) {
        this.lbl = lbl;
    }

    public String getLbl() {
        return lbl;
    }

    public void setMod(String mod) {
        this.mod = mod;
    }

    public String getMod() {
        return mod;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public boolean isCheck() {
        return check;
    }

    public void setDepFid(String depFid) {
        this.depFid = depFid;
    }

    public String getDepFid() {
        return depFid;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public String getfId() {
        return fId;
    }

    public void setDepOptions(String depOptions) {
        this.depOptions = depOptions;
    }

    public String getDepOptions() {
        return depOptions;
    }

}
