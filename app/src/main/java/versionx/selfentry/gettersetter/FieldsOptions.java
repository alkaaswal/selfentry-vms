package versionx.selfentry.gettersetter;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class FieldsOptions {

    @ColumnInfo(name = "fId")
    public String fId;


    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "optionId")
    public String optionId;

    @ColumnInfo(name = "name")
    public String name;


    @ColumnInfo(name = "typ")
    public String typ;

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getTyp() {
        return typ;
    }

    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionId() {
        return optionId;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof String) {
            String name = obj.toString();
            return name.equals(this.name);
        }

        return super.equals(obj);

    }
}
