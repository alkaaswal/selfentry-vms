package versionx.selfentry.gettersetter;

import java.util.ArrayList;

/**
 * Created by alkaaswal on 6/12/17.
 */

public class Images {

    String storagePath, imgPath, mob, customKey, vehNo, sessionUri;
    byte[] img;
    String ref;
    ArrayList<String> pathRef;
    int id;

    public String getSessionUri() {
        return sessionUri;
    }

    public void setSessionUri(String sessionUri) {
        this.sessionUri = sessionUri;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    public String getVehNo() {
        return vehNo;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public void setCustomKey(String customKey) {
        this.customKey = customKey;
    }

    public String getCustomKey() {
        return customKey;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgPath() {
        return imgPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public byte[] getImg() {
        return img;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }


    public ArrayList<String> getPathRef() {
        return pathRef;
    }

    public void setPathRef(ArrayList<String> pathRef) {
        this.pathRef = pathRef;
    }
}
