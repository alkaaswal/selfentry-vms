package versionx.selfentry.gettersetter;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class MultiDepMapping {

    @PrimaryKey(autoGenerate = true)
    public   int id;

    @ColumnInfo(name = "optId")
    public   String optId;

    @ColumnInfo(name = "optNm")
    public  String optNm;

    @ColumnInfo(name = "ref")
    public String ref;

    @ColumnInfo(name = "fId")
    public String fId;

    public void setfId(String fId) {
        this.fId = fId;
    }

    public String getfId() {
        return fId;
    }

    public void setOptNm(String optNm) {
        this.optNm = optNm;
    }

    public String getOptNm() {
        return optNm;
    }

    public void setOptId(String optId) {
        this.optId = optId;
    }

    public String getOptId() {
        return optId;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }


    public String getRef() {
        return ref;
    }
}
