package versionx.selfentry.gettersetter;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class RestrictFields {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "fId")
    public String fId;

    @ColumnInfo(name = "restrictId")
    public String restrictId;

    @ColumnInfo(name = "opt")
    public String opt;

    @ColumnInfo(name = "hide")
    public boolean hide;

    @ColumnInfo(name = "req")
    public boolean req;

    @ColumnInfo(name = "del")
    public boolean del;

    @Ignore
    public String restrictNm;

    public void setRestrictNm(String restrictNm) {
        this.restrictNm = restrictNm;
    }

    public String getRestrictNm() {
        return restrictNm;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public String getOpt() {
        return opt;
    }

    public void setRestrictId(String restrictId) {
        this.restrictId = restrictId;
    }

    public String getRestrictId() {
        return restrictId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public String getfId() {
        return fId;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

    public boolean isHide() {
        return hide;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public boolean isDel() {
        return del;
    }

    public void setReq(boolean req) {
        this.req = req;
    }

    public boolean isReq() {
        return req;
    }
}
