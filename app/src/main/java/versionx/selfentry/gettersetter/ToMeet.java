package versionx.selfentry.gettersetter;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;

/**
 * Created by developer on 16/9/16.
 */
public class ToMeet implements Parcelable {

    String aka, mob, nm, token, typ, id, aptNo, fmlyMob, dndRsn;
    boolean isFmlyMember, dndMode, q, isManualAllowed, isConfirmReq, isNotiReq;
    long vTd;
    ArrayList allTokens;

    public void setAllTokens(ArrayList allTokens) {
        this.allTokens = allTokens;
    }

    public void setManualAllowed(boolean manualAllowed) {
        isManualAllowed = manualAllowed;
    }

    public boolean isManualAllowed() {
        return isManualAllowed;
    }

    public void setConfirmReq(boolean confirmReq) {
        isConfirmReq = confirmReq;
    }

    public boolean isConfirmReq() {
        return isConfirmReq;
    }

    public void setNotiReq(boolean notiReq) {
        isNotiReq = notiReq;
    }

    public boolean isNotiReq() {
        return isNotiReq;
    }

    public ArrayList getAllTokens() {
        return allTokens;
    }

    public void setQ(boolean q) {
        this.q = q;
    }

    public boolean isQ() {
        return q;
    }

    public void setDndMode(boolean dndMode) {
        this.dndMode = dndMode;
    }

    public boolean isDndMode() {
        return dndMode;
    }

    public void setvTd(long vTd) {
        this.vTd = vTd;
    }

    public long getvTd() {
        return vTd;
    }


    public void setDndRsn(String dndRsn) {
        this.dndRsn = dndRsn;
    }

    public String getDndRsn() {
        return dndRsn;
    }

    public void setFmlyMob(String fmlyMob) {
        this.fmlyMob = fmlyMob;
    }

    public String getFmlyMob() {
        return fmlyMob;
    }

    public void setAptNo(String aptNo) {
        this.aptNo = aptNo;
    }

    public String getAptNo() {
        return aptNo;
    }

    public void setFmlyMember(boolean fmly) {
        isFmlyMember = fmly;
    }

    public boolean isFmlyMember() {
        return isFmlyMember;
    }
    /* HashMap<String,Family> fmly;

    public void setFmly(HashMap<String, Family> fmly) {
        this.fmly = fmly;
    }

    public HashMap<String, Family> getFmly() {
        return fmly;
    }*/
    /*  Boolean notify;

    public void setNotify(Boolean notify) {
        this.notify = notify;
    }

    public Boolean getNotify() {
        return notify;
    }*/

    @Exclude
    public String getAka() {
        return aka;
    }

    @Exclude
    public void setAka(String aka) {
        this.aka = aka;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object other) {
        if (!(other instanceof ToMeet)) {
            return false;
        }
        ToMeet toMeet = (ToMeet) other;
        return toMeet.getNm().equals(this.getNm());
    }

    public void reset() {
        this.id = null;
        this.nm = null;
        this.token = null;
        this.mob = null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    class Family {
        String nm;

        public void setNm(String nm) {
            this.nm = nm;
        }

        public String getNm() {
            return nm;
        }
    }


}
