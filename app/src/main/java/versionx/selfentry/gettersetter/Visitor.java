package versionx.selfentry.gettersetter;

import java.util.ArrayList;
import java.util.HashMap;

import versionx.selfentry.Util.CommonMethods;
import versionx.selfentry.Util.Global;


public class Visitor implements Comparable<Visitor> {

    String act, name, toMeet, mobile, purposeNm, barCode, inTime, outTime, photoUrl, purposeId,
            photoPath, visitorId, type, vehNo, entryDt, prevTomeet, sNo, tknNo;
    int prevCalling, count, prevCount;
    boolean isFirstTime, isRepeated, isAppointment, isParent, isVisitor, isAllowedBefore;
    String status_check, Status_UpdateCheck;
    boolean alarm, notify, allow = false, rePrint;
    ArrayList<HashMap> meetHashMap, whoCameHashMap;
    Purpose purpose, prevPurpose;
    long prevtDt;


    public void setPurposeNm(String purposeNm) {
        this.purposeNm = purposeNm;
    }


    public Visitor()

    {
        this.type = Global.VISITOR;
        this.notify = false;
        this.isFirstTime = true;
        this.prevTomeet = null;
        this.prevCalling = 0;
        this.isRepeated = false;
        this.isParent = false;
        this.isAppointment = false;
        this.isVisitor = true;
        this.count = 1;
        this.prevCount = 0;
        purpose = new Purpose();
        prevPurpose = new Purpose();
        this.prevtDt = CommonMethods.getFirstDayOfMonth().getTimeInMillis();
        rePrint = false;
    }


    public void setTknNo(String tknNo) {
        this.tknNo = tknNo;
    }

    public String getTknNo() {
        return tknNo;
    }

    public void setRePrint(boolean rePrint) {
        this.rePrint = rePrint;
    }

    public boolean isRePrint() {
        return rePrint;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setPrevCount(int prevCount) {
        this.prevCount = prevCount;
    }

    public int getPrevCount() {
        return prevCount;
    }

    public boolean isAllowedBefore() {
        return isAllowedBefore;
    }

    public void setAllowedBefore(boolean allowedBefore) {
        isAllowedBefore = allowedBefore;
    }

    public void setVisitor(boolean visitor) {
        isVisitor = visitor;
    }

    public boolean isVisitor() {
        return isVisitor;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setAppointment(boolean appointment) {
        isAppointment = appointment;
    }

    public boolean isAppointment() {
        return isAppointment;
    }

    public void setsNo(String sNo) {
        this.sNo = sNo;
    }

    public String getsNo() {
        return sNo;
    }

    public boolean isRepeated() {
        return isRepeated;
    }

    public void setRepeated(boolean repeated) {
        isRepeated = repeated;
    }

    public void setPrevtDt(long prevtDt) {
        this.prevtDt = prevtDt;
    }

    public long getPrevtDt() {
        return prevtDt;
    }

    public void setPrevPurpose(Purpose prevPurpose) {
        this.prevPurpose = prevPurpose;
    }

    public Purpose getPrevPurpose() {
        return prevPurpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPrevCalling(int prevCalling) {
        this.prevCalling = prevCalling;
    }

    public int getPrevCalling() {
        return prevCalling;
    }

    public void setPrevTomeet(String prevTomeet) {
        this.prevTomeet = prevTomeet;
    }

    public String getPrevTomeet() {
        return prevTomeet;
    }


    public void setFirstTime(boolean firstTime) {
        isFirstTime = firstTime;
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }

    public void setEntryDt(String entryDt) {
        this.entryDt = entryDt;
    }

    public String getEntryDt() {
        return entryDt;
    }

    public boolean isAllow() {
        return allow;
    }

    public void setAllow(boolean allow) {
        this.allow = allow;
    }

    public void setMeetHashMap(ArrayList<HashMap> meetHashMap) {
        this.meetHashMap = meetHashMap;
    }

    public ArrayList<HashMap> getMeetHashMap() {
        return meetHashMap;
    }

    public void setWhoCameHashMap(ArrayList<HashMap> whoCameHashMap) {
        this.whoCameHashMap = whoCameHashMap;
    }

    public ArrayList<HashMap> getWhoCameHashMap() {
        return whoCameHashMap;
    }

    HashMap<String, Object> meetCustomFields, whoCameCustomFields;


    public HashMap<String, Object> getMeetCustomFields() {
        return meetCustomFields;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    public String getVehNo() {
        return vehNo;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public String getStatus_UpdateCheck() {
        return Status_UpdateCheck;
    }

    public void setStatus_UpdateCheck(String status_UpdateCheck) {
        Status_UpdateCheck = status_UpdateCheck;
    }

    public String getStatus_check() {
        return status_check;
    }

    public void setStatus_check(String status_check) {
        this.status_check = status_check;
    }

    byte[] photo;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    private String rowId;

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }


    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }


    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }


    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getInTime() {
        return inTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToMeet() {
        return toMeet;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setPurposeId(String purposeId) {
        this.purposeId = purposeId;
    }

    public String getPurposeId() {
        return purposeId;
    }

    public String getPurposeNm() {
        return purposeNm;
    }

    public void setPurpose(String purpose) {
        this.purposeNm = purpose;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public void setToMeet(String toMeet) {
        this.toMeet = toMeet;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    public boolean isNotify() {
        return notify;
    }

    public Visitor(Visitor visitor)

    {
        this.isAppointment = visitor.isAppointment;
        this.isFirstTime = visitor.isFirstTime;
        this.prevCalling = visitor.prevCalling;
        this.prevTomeet = visitor.prevTomeet;
        this.isRepeated = visitor.isRepeated;
        this.isParent = visitor.isParent;
        this.isVisitor = visitor.isVisitor;
        // this.purpose = visitor.getPurpose();
        //this.prevPurpose = visitor.getPrevPurpose();
        this.prevtDt = visitor.prevtDt;
        this.purpose = new Purpose();
        this.prevPurpose = new Purpose();
        this.prevPurpose.copy(visitor.getPrevPurpose().getP(), visitor.getPrevPurpose().getpId());
        this.purpose.copy(visitor.getPurpose().getP(), visitor.getPurpose().getpId());
    }

    @Override
    public int compareTo(Visitor visitor) {
        return getInTime().compareTo(visitor.getInTime());
    }

    public void clear() {
        this.type = Global.VISITOR;
        this.notify = false;
        this.isFirstTime = true;
        this.isAllowedBefore = false;
        this.prevTomeet = null;
        this.prevCalling = 0;
        this.name = null;
        this.mobile = null;
        this.isRepeated = false;
        this.isAppointment = false;
        this.isVisitor = true;
        this.isParent = false;
        this.purpose.clear();
        this.purposeNm = null;
        this.prevPurpose.clear();
        this.count = 1;
        this.prevCount = 0;
        this.prevtDt = CommonMethods.getFirstDayOfMonth().getTimeInMillis();
        this.rePrint = false;
        this.tknNo=null;

    }


    public class Purpose {

        String pId, p;

        Purpose() {
            this.p = null;
            this.pId = null;
        }

        public void setpId(String pId) {
            this.pId = pId;
        }

        public String getpId() {
            return pId;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getP() {
            return p;
        }

        public void clear() {
            this.pId = null;
            this.p = null;

        }

        public void copy(String p, String pId) {
            this.p = p;
            this.pId = pId;
        }


        @Override
        public boolean equals(Object other) {
            if (!(other instanceof Purpose)) {
                return false;
            }

            Purpose that = (Purpose) other;

            // Custom equality check here.
            return this.p.equals(that.p)
                    && this.pId.equals(that.pId);
        }
    }


    public void setVisitorData(String nm, String mob, Purpose purpose, Purpose prevPurpose) {
        this.name = nm;
        this.mobile = mob;
        this.purpose.copy(purpose.getP(), purpose.getpId());
        this.prevPurpose.copy(prevPurpose.getP(), prevPurpose.getpId());
    }

}
