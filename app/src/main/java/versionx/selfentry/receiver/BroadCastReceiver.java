package versionx.selfentry.receiver;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.view.WindowManager;
import android.widget.Toast;


import com.crashlytics.android.Crashlytics;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import versionx.selfentry.Activity.HomeActivity;
import versionx.selfentry.MyApplication;
import versionx.selfentry.Activity.HomeActivity;
import versionx.selfentry.MyApplication;
import versionx.selfentry.Printing.DrawerService;
import versionx.selfentry.Util.Global;


/**
 * Created by developer on 1/8/16.
 */
public class BroadCastReceiver extends BroadcastReceiver {

    SharedPreferences loginSp;


    @Override
    public void onReceive(Context context, Intent intent) {
        loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

        if (loginSp.getBoolean(Global.IS_LOGGED_IN, false)) {

            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            /*savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");*/
            } else if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
                String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);

                int state = 0;
                if (stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    state = TelephonyManager.CALL_STATE_IDLE;
                } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    state = TelephonyManager.CALL_STATE_OFFHOOK;
                } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    state = TelephonyManager.CALL_STATE_RINGING;

                    String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);



/*
                new InVisitorRegistration().updateText(number);*/
                    disconnectCall();
                    if (!MyApplication.isActivityVisible()) {
                        Intent intent1 = new Intent(context, HomeActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent1.putExtra("phoneNo", number);
                        context.startActivity(intent1);
                    } else {
                        Intent intent1 = new Intent("versionx.call");
                        intent1.putExtra("phoneNo", number);
                        context.sendBroadcast(intent1);
                    }
                }
            }
            if (intent.getAction().equals("android.hardware.usb.action.USB_DEVICE_ATTACHED")) {


                if (loginSp.getInt(Global.PRINTING, 0) == Global.USB_PRINT) {
                    UsbManager mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
                    setUSBPrinter(context, mUsbManager);
                }
            }

         /*   if (intent.getAction().equals("android.hardware.usb.action.USB_DEVICE_ATTACHED")) {

                if (loginSp.getBoolean(Global.USB_PRINT, false)) {
                  if(DrawerService.workThread!=null && DrawerService.workThread.isConnected())
                }
            }*/

            if (intent.getAction().equalsIgnoreCase(Global.ACTION_USB_PERMISSION)) {


                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)
                        && loginSp.getInt(Global.PRINTING, 0) == Global.USB_PRINT) {

                    UsbManager mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    loginSp.edit().putInt(Global.USB_PRODUCT_ID, device.getProductId()).apply();
                    loginSp.edit().putInt(Global.USB_VENDORID, device.getVendorId()).apply();
                    if (DrawerService.workThread != null)

                        DrawerService.workThread.connectUsb(mUsbManager, device);

                }
            }


        }

    }


    public void disconnectCall() {
        try {

            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";
            Class<?> telephonyClass;
            Class<?> telephonyStubClass;
            Class<?> serviceManagerClass;
            Class<?> serviceManagerNativeClass;
            Method telephonyEndCall;
            Object telephonyObject;
            Object serviceManagerObject;
            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);
            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
            telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyEndCall = telephonyClass.getMethod("endCall");
            telephonyEndCall.invoke(telephonyObject);

        } catch (Exception e) {
            Crashlytics.logException(e);

        }
    }

    private void setUSBPrinter(Context context, UsbManager mUsbManager) {


        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        UsbDevice device = null;
        ArrayList<UsbDevice> usbDeviceArrayList = new ArrayList<>();
        while (deviceIterator.hasNext()) {
            device = deviceIterator.next();
            usbDeviceArrayList.add(device);


        }
        openDialogForGroupSelection(context, usbDeviceArrayList, mUsbManager);
    }


    public void openDialogForGroupSelection(Context context, final ArrayList<UsbDevice> usbDevices, final UsbManager mUsbManager) {

        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);


            b.setTitle("Select USB Printer");
            final Context context1 = context;
            final String[] groupListName = new String[usbDevices.size()];
            for (int i = 0; i < usbDevices.size(); i++)
                if(Build.VERSION.SDK_INT>=21)
                groupListName[i] = usbDevices.get(i).getManufacturerName() + "-" + usbDevices.get(i).getProductName();
            else
                    groupListName[i] = usbDevices.get(i).getProductId()+"";

            b.setSingleChoiceItems(groupListName, 0, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {

                        PendingIntent mPermissionIntent = PendingIntent
                                .getBroadcast(
                                        context1,
                                        0,
                                        new Intent(
                                                Global.ACTION_USB_PERMISSION),
                                        0);
                        if (usbDevices.get(i) != null) {
                            if (!mUsbManager.hasPermission(usbDevices.get(i))) {
                                mUsbManager.requestPermission(usbDevices.get(i),
                                        mPermissionIntent);

                            } else {
                                // DrawerService.workThread.connectUsb(mUsbManager, device);
                            }
                        }


                    } catch (Exception e) {
                    } finally {
                        dialogInterface.dismiss();
                    }


                }
            });
            AlertDialog dialog = b.create();
            dialog.getWindow().setType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);



            dialog.show();
        } catch (Exception e) {

        }
    }

}
